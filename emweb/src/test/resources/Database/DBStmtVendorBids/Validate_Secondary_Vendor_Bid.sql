set newpage NONE
set wrap off
set linesize 10000
set pagesize 10000
set heading off
set termout off
set verify off
set feedback off
set trims on
set serveroutput on

/* Directory to export the file */
-----------------------------------------------------------------------------------
spool '&1'



/* SQL Statement */
-----------------------------------------------------------------------------------
SELECT &2
  FROM (SELECT vbidps.vendor_product_number                               sec_ven_product_number,
               vbidps.vendor_product_name                                 sec_ven_product_name,
               pktype.package_type                                        sec_ven_product_unit,
               vbidps.product_brand                                       sec_ven_product_brand,
               TRIM(to_char(vbidps.conversion_factor, '9,999,990.0000'))  sec_ven_product_conv,
               vbidps.inverted_flag                                       sec_ven_inverted_flag,
               TRIM(to_char(vbidps.bid_price, '9,999,990.0000'))          sec_ven_bid_price,
               vbidps.split_flag                                          sec_ven_split_flag,
               vbidps.substitute_flag                                     sec_ven_substitute_flag,
               vbidps.not_approved                                        sec_ven_not_approved_flag,
               vbidps.restricted                                          sec_ven_restricted_flag,
               taxhdr.tax_code                                            sec_ven_tax_code,
               to_char(vbidps.begin_date, 'mm/dd/yyyy')                   sec_ven_begin_Date,
               to_char(vbidps.end_date, 'mm/dd/yyyy')                     sec_ven_end_Date
          FROM t_product_company        pc,
               t_vendor_bid_product_sec vbidps,
               t_supply                 sup,
               t_market                 mkt,
               t_package_type           pktype,
               t_tax_table_hdr          taxhdr
         WHERE vbidps.supply_id = sup.supply_id
           AND vbidps.market_pk = mkt.market_pk
           AND vbidps.product_id = pc.product_id
           AND vbidps.begin_date <> vbidps.end_date
           AND vbidps.package_pk = pktype.package_pk
           AND vbidps.tax_table_hdr_pk = taxhdr.tax_table_hdr_pk(+)
           AND trunc(vbidps.last_touch_date) = trunc(SYSDATE)
           AND pc.product_name_number = '&3'
           AND vbidps.vendor_product_number = '&4'
           AND sup.supply_name = '&5'
           AND mkt.market_name = '&6'
           AND vbidps.begin_date = to_date('&7', 'mm/dd/yyyy')
         ORDER BY vbidps.begin_date ASC, vbidps.end_date) validate_vendor_bid;


spool off

EXIT
