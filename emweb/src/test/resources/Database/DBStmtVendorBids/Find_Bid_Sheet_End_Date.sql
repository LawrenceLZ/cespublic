set newpage NONE
set wrap off
set linesize 10000
set pagesize 10000
set heading off
set termout off
set verify off
set feedback off
set trims on
set serveroutput on

/* Directory to export the file */
-----------------------------------------------------------------------------------
spool '&1'



/* SQL Statement */
-----------------------------------------------------------------------------------
SELECT to_char(bid.contract_end_date, 'mm/dd/yyyy') bid_sheet_end_date
  FROM t_bid bid
 WHERE upper(bid.bid_name) = upper('&2');


spool off

EXIT
