set newpage NONE
set wrap off
set linesize 10000
set pagesize 10000
set heading off
set termout off
set verify off
set feedback off
set trims on
set serveroutput on

/* Directory to export the file */
-----------------------------------------------------------------------------------
spool '&1'



/* SQL Statement */
-----------------------------------------------------------------------------------
SELECT &2
  FROM (SELECT pc.product_name                                          						ven_company_product_name,
               pc.product_name_number                                   						ven_company_product_number,
               pktype.package_type                                      						ven_company_product_inv_unit,
               vbidp.vendor_product_number                              						ven_product_number,
               venpktype.package_type                                   						ven_unit,
               vbidp.product_brand                                      						ven_product_brand,
               TRIM(to_char(vbidp.conversion_factor, '9,999,990.0000')) 						ven_conversion,
               TRIM(to_char(vbidp.bid_price/(vbidp.conversion_factor
                      * decode(sup.supply_currency_pk, mkt.currency_pk, 1,
							  (SELECT currency_ratio FROM t_currency_ratio
							   WHERE sup.supply_currency_pk = from_currency_pk
							   AND mkt.currency_pk = to_currency_pk))), '9,999,990.0000')) 	ven_bid_price,
			   TRIM(to_char(vbidp.bid_price_taxed/(vbidp.conversion_factor
                       			* decode(sup.supply_currency_pk, mkt.currency_pk, 1,
							  (SELECT currency_ratio FROM t_currency_ratio
							   WHERE sup.supply_currency_pk = from_currency_pk
							   AND mkt.currency_pk = to_currency_pk))), '9,999,990.0000'))  ven_bid_price_taxed,
               taxhdr.tax_code                                          						ven_tax_code,
               vbidp.allow_split                                        						ven_allow_split_flag,
               vbidp.reference_number                                   						ven_reference_number,
               TRIM(vbidp.reference_line_number)                        						ven_line_number,
               to_char(vbidp.expiration_date, 'mm/dd/yyyy')             						ven_expiration_date,
               to_char(vbidp.begin_date, 'mm/dd/yyyy')                  						ven_begin_Date,
               to_char(vbidp.end_date, 'mm/dd/yyyy')                   							ven_end_Date
          FROM t_vendor_bid_product vbidp,
               t_supply             sup,
               t_market             mkt,
               t_product_company    pc,
               t_package_type       pktype,
               t_package_type       venpktype,
               t_tax_table_hdr      taxhdr
         WHERE vbidp.supply_id = sup.supply_id
           AND vbidp.market_pk = mkt.market_pk
           AND vbidp.product_id = pc.product_id
           AND vbidp.begin_date <> vbidp.end_date
           AND vbidp.package_pk = venpktype.package_pk
           AND pc.inventory_package_pk = pktype.package_pk
           AND vbidp.tax_table_hdr_pk = taxhdr.tax_table_hdr_pk(+)
           AND pc.product_name_number = '&3'
           AND sup.supply_name = '&4'
           AND mkt.market_name = '&5'
           AND vbidp.begin_date <= to_date('&6', 'mm/dd/yyyy')
           AND vbidp.end_date > to_date('&6', 'mm/dd/yyyy')
         ORDER BY vbidp.begin_date ASC, vbidp.end_date) validate_vendor_bid;



spool off

EXIT
