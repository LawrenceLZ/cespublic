set newpage NONE
set wrap off
set linesize 10000
set pagesize 10000
set heading off
set termout off
set verify off
set feedback off
set trims on
set serveroutput on

/* Directory to export the file */
-----------------------------------------------------------------------------------
spool '&1'



/* SQL Statement */
-----------------------------------------------------------------------------------
SELECT &2
  FROM (SELECT         pc.product_name                                                              con_company_product_name,
                       pc.product_name_number                                                       con_company_product_number,
                       pktype.package_type                                                          con_package_type,
                       cpr.product_brand                                                            con_product_brand,
                       TRIM(to_char(cpr.contract_price/(vbp.conversion_factor
                       			* decode(sup.supply_currency_pk, mkt.currency_pk, 1,
							  (SELECT currency_ratio FROM t_currency_ratio
							   WHERE sup.supply_currency_pk = from_currency_pk
							   AND mkt.currency_pk = to_currency_pk))), '9,999,990.0000'))   		con_price,
                       to_char(cpr.contract_begin_date, 'mm/dd/yyyy')                               con_begin_Date,
                       to_char(cpr.contract_end_date, 'mm/dd/yyyy')                                 con_end_Date,
                       cpr.contract_price_pk                                                        con_price_pk,
                       sup.supply_name                                                              con_vendor
                  FROM t_contract_price     cpr,
                       t_supply             sup,
                       t_market             mkt,
                       t_product_company    pc,
                       t_package_type       pktype,
                       t_vendor_bid_product vbp
                 WHERE cpr.supply_id = sup.supply_id
                   AND cpr.market_pk = mkt.market_pk
                   AND cpr.product_id = pc.product_id
                   AND cpr.contract_begin_date < cpr.contract_end_date
                   AND cpr.package_pk = pktype.package_pk
                   AND trunc(cpr.last_touch_date) = trunc(SYSDATE)
                   AND pc.product_name_number = '&3'
                   AND mkt.market_name = '&4'
                   AND cpr.contract_begin_date <= to_date('&5', 'mm/dd/yyyy')
                   AND vbp.vendor_bid_product_pk = cpr.vendor_bid_product_pk
                 ORDER BY cpr.contract_begin_date ASC, cpr.contract_end_date) validate_single_contract;



spool off

EXIT
