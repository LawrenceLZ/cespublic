set newpage NONE
set wrap off
set linesize 10000
set pagesize 10000
set heading off
set termout off
set verify off
set feedback off
set trims on
set serveroutput on

/* Directory to export the file */
-----------------------------------------------------------------------------------
spool '&1'

/* SQL Statement */
-----------------------------------------------------------------------------------

SELECT PC.PRODUCT_NAME_NUMBER
FROM T_CATEGORY           CAT,
     T_PRODUCT_COMPANY    PC,
     T_BID_SHEET          BDSH,
     T_BID                BID
WHERE CAT.CATEGORY_NAME = '&2'
  AND PC.CATEGORY_PK = CAT.CATEGORY_PK
  AND BID.BID_NAME = '&3'
  AND BID.BID_PK = BDSH.BID_PK
  AND BDSH.PRODUCT_ID = PC.PRODUCT_ID
  AND BDSH.BEGIN_DATE <= TO_DATE('&4', 'mm/dd/yyyy')
  AND BDSH.END_DATE > TO_DATE('&4', 'mm/dd/yyyy')
ORDER BY 1;

spool off

EXIT
