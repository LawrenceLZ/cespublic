set newpage NONE
set wrap off
set linesize 10000
set pagesize 10000
set heading off
set termout off
set verify off
set feedback off
set trims on
set colsep ':del:'
set serveroutput on

/* Directory to export the file */
-----------------------------------------------------------------------------------
spool '&1'



/* SQL Statement */
-----------------------------------------------------------------------------------
SELECT pc.product_name_number,
	   sup.supply_name
	FROM t_vendor_bid_product vbp,
		 t_bid bid,
		 t_product_company pc,
		 t_supply sup,
		-- This query (y) finds the vendor_bid_product_pk that corresponds with the best price in the market (found in the select below)
          	(SELECT vbp2.product_id 			product_id,
          			vbp2.vendor_bid_product_pk 	vendor_bid_product_pk
          		FROM t_vendor_bid_product 	vbp2,
          			 t_bid 					bid2,
          			 t_market 				mrkt2,
          			 t_supply 				sup2
			   WHERE vbp2.begin_date <= to_date('&2', 'mm/dd/yyyy')
				 AND vbp2.end_date > to_date('&2', 'mm/dd/yyyy')
				 AND vbp2.bid_pk = bid2.bid_pk
				 AND bid2.bid_name = '&3'
				 AND vbp2.market_pk = mrkt2.market_pk
				 AND mrkt2.market_name = '&4'
				 AND vbp2.supply_id = sup2.supply_id
				 AND (vbp2.bid_price/ (vbp2.conversion_factor * decode(sup2.supply_currency_pk, mrkt2.currency_pk, 1,
					  (SELECT currency_ratio
						FROM t_currency_ratio
					   WHERE sup2.supply_currency_pk = from_currency_pk
						 AND mrkt2.currency_pk = to_currency_pk)))) IN
				--The query below gives the minimum bid price in the given market for the product
				  (SELECT min(vbp3.bid_price/ (vbp3.conversion_factor * decode(sup3.supply_currency_pk, mrkt3.currency_pk, 1,
																			  (SELECT currency_ratio FROM t_currency_ratio
																			   WHERE sup3.supply_currency_pk = from_currency_pk
																			   AND mrkt3.currency_pk = to_currency_pk))))
					FROM t_vendor_bid_product 	vbp3,
						 t_market 				mrkt3,
						 t_supply 				sup3,
						 t_bid bid3
					WHERE vbp3.product_id = vbp2.product_id
					  AND vbp3.begin_date <= to_date('&2', 'mm/dd/yyyy')
					  AND vbp3.end_date > to_date('&2', 'mm/dd/yyyy')
					  AND vbp3.supply_id = sup3.supply_id
					  AND mrkt3.market_name = '&4'
					  AND vbp3.bid_pk = bid3.bid_pk
					  AND bid3.bid_name = '&3'
					  AND vbp3.market_pk = mrkt3.market_pk)) y
WHERE bid.bid_pk = vbp.bid_pk
  AND bid.bid_name = '&3'
  AND vbp.product_id = pc.product_id
  AND vbp.product_id = y.product_id
  AND vbp.vendor_bid_product_pk = y.vendor_bid_product_pk
  AND vbp.supply_id = sup.supply_id
ORDER BY  pc.product_name_number;

spool off

EXIT
