
/* This script is to cleanup the contracts created by the test and to add contracts for those products whose
contracts were ended by these tests
*/

/* End all contracts created by the test.
*/

update t_contract_price cpr
set cpr.contract_end_date = cpr.contract_begin_date
where cpr.bid_pk =
   (select bid.bid_pk
   from t_bid bid
	where bid.bid_name = 'AUTOMATION TEST DO NOT USE - LAL')
	and cpr.contract_begin_date < cpr.contract_end_date;

	commit;

/* Create another contract for AUTOLAL002 if it has been deleted.
*/

 insert into t_contract_price
   (contract_price_pk,
    market_pk,
    bid_pk,
    supply_id,
    product_id,
    package_pk,
    contract_price,
    contract_begin_date,
    contract_end_date,
    vendor_bid_product_pk,
    replicated_flag)
SELECT seq_t_contract_price.nextval,
       vbp.market_pk,
       vbp.bid_pk,
       vbp.supply_id,
       vbp.product_id,
       vbp.package_pk,
       vbp.bid_price,
       to_date('&1' || ' 00:00:00','mm/dd/yyyy hh24:mi:ss'),
       vbp.end_date,
       vbp.vendor_bid_product_pk,
       'N'
FROM t_vendor_bid_product vbp, t_supply sup, t_market mrkt, t_product_company pc
WHERE vbp.supply_id = sup.supply_id
  AND vbp.market_pk = mrkt.market_pk
  AND vbp.product_id = pc.product_id
  AND sup.supply_name = 'Bon Appetit'
  AND mrkt.market_name = 'Breckenridge'
  and pc.product_name_number = 'AUTOLAL002';

 commit;
