set newpage NONE
set wrap off
set linesize 10000
set pagesize 10000
set heading off
set termout off
set verify off
set feedback off
set trims on
set serveroutput on

/* Directory to export the file */
-----------------------------------------------------------------------------------
spool '&1'

/* SQL Statement */
-----------------------------------------------------------------------------------

SELECT substr(mrkt.market_name,1,20) 					market,
       substr(pc.product_name,1,25) 					product,
       cpr.contract_price_pk							pk,
       cpr.contract_price								price,
       to_char(cpr.contract_begin_date,'mm/dd/yyyy')	begindate,
       to_char(cpr.contract_end_date,'mm/dd/yyyy')		end_date
FROM t_contract_price 					cpr,
     t_product_company					pc,
     t_market 							mrkt,
     t_bid 								bid
WHERE (cpr.market_pk,
       cpr.product_id) IN
         (SELECT DISTINCT cpr1.market_pk,
                          cpr1.product_id
          FROM t_contract_price cpr1,
               t_contract_price cpr2
          WHERE cpr1.contract_price_pk != cpr2.contract_price_pk
            AND cpr1.product_id = cpr2.product_id
            AND cpr1.market_pk = cpr2.market_pk
            AND cpr1.contract_end_date > trunc(sysdate)
            AND cpr2.contract_end_date > trunc(sysdate)
            AND cpr1.bid_pk = bid.bid_pk
            AND cpr2.bid_pk = bid.bid_pk
            AND cpr1.contract_begin_date < cpr1.contract_end_date
            AND cpr2.contract_begin_date < cpr2.contract_end_date
            AND((cpr1.contract_begin_date <= cpr2.contract_begin_date
                 AND cpr1.contract_end_date > cpr2.contract_begin_date)
                 OR (cpr1.contract_begin_date <= cpr2.contract_begin_date
                  AND cpr1.contract_end_date > cpr2.contract_end_date)))
      AND pc.product_id = cpr.product_id
      AND mrkt.market_pk = cpr.market_pk
      AND cpr.contract_begin_date < cpr.contract_end_date
      AND cpr.bid_pk = bid.bid_pk
      AND bid.bid_name = '&2'
ORDER BY mrkt.market_name,
         PC.PRODUCT_NAME,
         cpr.contract_begin_date;