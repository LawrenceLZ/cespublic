DECLARE
  n_recipe_name VARCHAR2(80 CHAR) := '%' || '&1' || '%';
BEGIN
  FOR curs_rcp_pk IN (SELECT rcp.recipe_pk
                      FROM t_recipe rcp
                      WHERE upper(rcp.recipe_name) LIKE
                            upper(n_recipe_name)) LOOP

/***
delete substitutes in t_recipe_component_subst table
***/
    DELETE t_recipe_component_subst rcpcsub
    WHERE rcpcsub.recipe_component_pk IN
          (SELECT rcpcomp.recipe_component_pk
           FROM t_recipe_component rcpcomp
           WHERE rcpcomp.recipe_pk = curs_rcp_pk.recipe_pk);

/***
delete components in t_recipe_component table
***/
    DELETE t_recipe_component rcpcomp
    WHERE rcpcomp.recipe_pk = curs_rcp_pk.recipe_pk;

/***
delete rcp location records in T_RECIPE_LOCATION table
***/
    DELETE T_RECIPE_LOCATION rcploc
    WHERE rcploc.recipe_pk = curs_rcp_pk.recipe_pk;

/***
delete rcp header records in t_recipe table
***/
    DELETE t_recipe rcporigin
    WHERE rcporigin.recipe_pk = curs_rcp_pk.recipe_pk;
  END LOOP;

  FOR curs_prod_id IN (SELECT pc.product_id
                       FROM t_product_company pc
                       WHERE pc.product_name LIKE n_recipe_name) LOOP

/***
delete audit records in t_audit_pc table
***/
  DELETE t_audit_pc adtpc
  WHERE adtpc.product_id = curs_prod_id.product_id;

/***
delete location audit records in T_PRODUCT_LOCATION_AUDIT table
***/
  DELETE T_PRODUCT_LOCATION_AUDIT adtpl
  WHERE adtpl.product_id = curs_prod_id.product_id;

/***
delete adjustment records in T_INV_DTL_ADJUSTMENT table
***/
  DELETE T_INV_DTL_ADJUSTMENT idaj
  WHERE idaj.product_id = curs_prod_id.product_id;

/***
delete company product department records in t_product_company_co_dept table
***/
  DELETE t_product_company_co_dept pccodpt
  WHERE pccodpt.product_id = curs_prod_id.product_id;

/***
delete concept records in T_PRODUCT_CONCEPT table
***/
  DELETE T_PRODUCT_CONCEPT pconcpt
  WHERE pconcpt.product_id = curs_prod_id.product_id;

/***
delete product comment records in t_product_company_comments table
***/
  DELETE t_product_company_comments prodcc
  WHERE prodcc.product_id = curs_prod_id.product_id;

/***
delete product image records in t_product_company_comments table
***/
  DELETE t_product_company_images prodcimg
  WHERE prodcimg.product_id = curs_prod_id.product_id;

/***
delete product file records in t_product_company_files table
***/
  DELETE t_product_company_files prodcf
  WHERE prodcf.product_id = curs_prod_id.product_id;

/***
delete product fact records in t_product_company_fact table
***/
  DELETE t_product_company_fact pcft
  WHERE pcft.product_id = curs_prod_id.product_id;

/***
delete product location records in T_PRODUCT_LOCATION table
***/
  DELETE T_PRODUCT_LOCATION pl
  WHERE pl.product_id = curs_prod_id.product_id;

/***
delete product pricing records in T_PRODUCT_PRICING table
***/
    DELETE T_PRODUCT_PRICING pprice
    WHERE pprice.product_id = curs_prod_id.product_id;

/***
delete product inventory schedule records in T_DTL_INVENTORY_SCHEDULE table
***/
    DELETE T_DTL_INVENTORY_SCHEDULE dis
    WHERE dis.product_id = curs_prod_id.product_id;

/***
delete product inventory post records in T_INV_DTL_POST table
***/
    DELETE T_INV_DTL_POST idp
    WHERE idp.product_id = curs_prod_id.product_id;

/***
delete product inventory records in T_INVENTORY table
***/
    DELETE T_INVENTORY inv
    WHERE inv.product_id = curs_prod_id.product_id;

/***
delete product inventory action records in T_INV_DTL_ACTION table
***/
    DELETE T_INV_DTL_ACTION ida
    WHERE ida.product_id = curs_prod_id.product_id;

/***
delete product sequence records in T_SEQUENCE table
***/
    DELETE T_SEQUENCE seq
    WHERE seq.product_id = curs_prod_id.product_id;

  END LOOP;

  FOR curs_ori_prod_id IN (SELECT pc.product_id
                           FROM t_product_company pc
                           WHERE pc.product_name LIKE n_recipe_name) LOOP

/***
delete product header records in t_product_company table
***/
    DELETE t_product_company pcorigin
    WHERE pcorigin.product_id = curs_ori_prod_id.product_id;
  END LOOP;

END;
/

COMMIT;
EXIT;