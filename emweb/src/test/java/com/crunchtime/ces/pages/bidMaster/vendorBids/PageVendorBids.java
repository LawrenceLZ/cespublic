package com.crunchtime.ces.pages.bidMaster.vendorBids;

import com.crunchtime.ces.helper.ActionsEMW;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class PageVendorBids {
	private final WebDriver driver;

	public PageVendorBids(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(id = "emweb-bidMaster-vendorBid-view-vendorBidsGrid-body")
	public WebElement vendorBidsBody;
	@FindBy(css = "div[id*='loadmask'][class='x-mask-msg-text']")
	public WebElement loadSpinner;
	/**
	 * Vendor Bids detail screen
	 */
	@FindBy(id = "vendorBid-view-vendorBidContainer-panel-productParams-button-launchBidSheetsPopup-btnIconEl")
	public WebElement bidSheetsBtn;
	@FindBy(css = "table[id*='vendorBid-view-vendorBidContainer-panel-productParams-combobox-vendors-triggerWrap'] input")
	public WebElement vendorInputCombobox;
	@FindBy(css = "table[id*='vendorBid-view-vendorBidContainer-panel-productParams-combobox-vendors-triggerWrap'] div[class*='x-form-arrow-trigger']")
	public WebElement vendorInputComboboxTrigger;
	@FindBy(css = "table[id*='vendorBid-view-vendorBidContainer-panel-productParams-combobox-markets-triggerWrap']>tbody>tr>td>input")
	public WebElement marketInputCombobox;
	@FindBy(css = "table[id*='vendorBid-view-vendorBidContainer-panel-productParams-combobox-markets-triggerWrap']>tbody>tr>td>div[class*='x-form-arrow-trigger']")
	public WebElement marketInputComboboxTrigger;
	@FindBy(css = "#vendorBid-view-vendorBidContainer-panel-productParams-button-launchSaveToMarketsPopup[class$='small-noicon']")
	public WebElement additionalMarketsBtn;
	@FindBy(css = "table[id*='vendorBid-view-vendorBidContainer-panel-productParams-combobox-effectiveDate-triggerWrap']>tbody>tr>td>input")
	public WebElement effectiveDatePicker;
	@FindBy(id = "vendorBid-view-vendorBidContainer-panel-productParams-button-retrieve")
	public WebElement retrieveBtn;
	@FindBy(css = "div[id='emweb-bidMaster-vendorBid-view-vendorBidsProductsGrid_header-body']>div>div>div>img[class*='x-tool-img x-tool-filter-']")
	public WebElement vendorBidsFilterIcon;
	@FindBy(id = "vendorBid-view-vendorBidsProductsGrid-form-filterPanel-productName-inputEl")
	public WebElement filterProductNameTextbox;
	@FindBy(id = "vendorBid-view-vendorBidsProductsGrid-form-filterPanel-productNumber-inputEl")
	public WebElement filterProductNumberTextbox;
	@FindBy(id = "vendorBid-view-vendorBidsProductsGrid-form-filterPanel-vendorProductNumber-inputEl")
	public WebElement filterVendorProductNumberTextbox;
	@FindBy(xpath = "//div[@id='vendorBid-view-vendorBidsProductsGrid']//span[.='Apply'][@class='x-btn-button']")
	public WebElement vendorBidsFilterApplyBtn;
	@FindBy(css = "div[id='emweb-bidMaster-vendorBid-view-vendorBidsProductsGrid-body']>div>table>tbody")
	public WebElement vendorBidsGrid;
	/**
	 * Additional Markets popup window screen
	 */
	@FindBy(id = "vendorBid-view-vendorMarketSaveSettings-grid-markets-body")
	public WebElement additionalMarketsPopupGrid;
	@FindBy(id = "vendorBid-view-vendorMarketSaveSettings-split-sub-checkbox")
	public WebElement additionalMarketsPopupIncludeSplitSubCheckbox;
	@FindBy(id = "vendorBid-view-vendorMarketSaveSettings-button-apply")
	public WebElement additionalMarketsApplyBtn;
	@FindBy(id = "vendorBid-view-vendorBidEditForm-form-edit-field-vendorProductNumber-inputEl")
	public WebElement bidEditFormVendorProdNumberTextbox;
	@FindBy(id = "vendorBid-view-vendorBidEditForm-form-edit-field-productBrand-inputEl")
	public WebElement bidEditFormVendorProdBrandTextbox;
	@FindBy(id = "vendorBid-view-vendorBidEditForm-form-edit-combobox-vendorUnitPackagePk-inputEl")
	public WebElement bidEditFormVendorProdUnitComboboxInput;
	@FindBy(css = "table[id='vendorBid-view-vendorBidEditForm-form-edit-combobox-vendorUnitPackagePk-triggerWrap'] div[class*='x-form-arrow-trigger']")
	public WebElement bidEditFormVendorProdUnitComboboxTriger;
	@FindBy(id = "vendorBid-view-vendorBidEditForm-form-edit-combobox-conversionFactor-inputEl")
	public WebElement bidEditFormConversionTextbox;
	@FindBy(id = "vendorBid-view-vendorBidEditForm-form-edit-field-bidPrice-inputEl")
	public WebElement bidEditFormPriceTextbox;
	@FindBy(id = "vendorBid-view-vendorBidEditForm-form-edit-combobox-taxCodeId-inputEl")
	public WebElement bidEditFormVendorProdTaxComboboxInput;
	@FindBy(css = "table[id='vendorBid-view-vendorBidEditForm-form-edit-combobox-taxCodeId-triggerWrap'] div[class*='x-form-arrow-trigger']")
	public WebElement bidEditFormVendorProdTaxComboboxTrigger;
	@FindBy(id = "vendorBid-view-vendorBidEditForm-form-edit-datefield-endDate-inputEl")
	public WebElement bidEditFormEndDateTextbox;
	@FindBy(id = "vendorBid-view-vendorBidEditForm-form-edit-checkboxfield-allowSplit")
	public WebElement bidEditFormAllowSplitCheckbox;
	@FindBy(id = "vendorBid-view-vendorBidEditForm-form-edit-field-referenceNumber-inputEl")
	public WebElement bidEditFormReferenceNumberTextbox;
	@FindBy(id = "vendorBid-view-vendorBidEditForm-form-edit-field-referenceLineNumber-inputEl")
	public WebElement bidEditFormLineNumberTextbox;
	@FindBy(id = "vendorBid-view-vendorBidEditForm-form-edit-datefield-expirationDate-inputEl")
	public WebElement bidEditFormExpirationDateTextbox;
	@FindBy(css = "img[class='x-tool-img x-tool-collapse-right']")
	public WebElement bidEditFormCollapseIcon;
	@FindBy(css = "img[class = 'x-tool-img x-tool-expand-left']")
	public WebElement bidEditFormExpandIcon;
	@FindBy(css = "[ces-selenium-id='ces-emweb-vendorBid-secondaryVendorProducts-grid_secondaryVendorProductsGrid'] [ces-selenium-id='gridview']")
	public WebElement secondaryBidsPopupGrid;
	@FindBy(id = "vendorBid-view-secondaryVendorProducts-button-add")
	public WebElement secondaryBidsPopupAddBtn;
	@FindBy(css = "div[id='emweb-bidMaster-vendorBid-view-secondaryVendorProducts']:not([class*='x-hide-offsets']) #vendorBid-view-secondaryVendorProducts-button-save-btnEl")
	public WebElement secondaryBidsPopupSaveAndCloseBtn;
	@FindBy(css = "div[id='emweb-bidMaster-vendorBid-view-secondaryVendorProducts']:not([class*='x-hide-offsets']) #vendorBid-view-secondaryVendorProducts-button-close")
	public WebElement secondaryBidsPopupCloseBtn;
	@FindBy(css = "div[class*='x-window select-bid-sheet x-layer x-window-default x-closable']")
	public WebElement selectBidSheetsPopup;
	@FindBy(css = "div[class*='x-window select-bid-sheet x-layer x-window-default x-closable'] div[class*='x-column-header x-column-header-checkbox'] span")
	public WebElement selectBidSheetsPopupSelectAllCheckbox;
	@FindBy(css = "div[class*='x-window select-bid-sheet x-layer x-window-default x-closable']>div>div>div>div>div>div>div>img[class*='x-tool-img x-tool-filter-']")
	public WebElement selectBidSheetsPopupFilterIcon;
	@FindBy(xpath = "//div[contains(@class,'x-window select-bid-sheet x-layer x-window-default x-closable')]//span[.='Apply'][@class='x-btn-button']")
	public WebElement selectBidSheetsPopupApplyBth;
	@FindBy(css = "div[class*='x-window grid-column-filters-window']>div>div>div>span>div>div>div>div>table:nth-child(1)>tbody>tr>td:nth-child(2)>input")
	public WebElement filterBidSheetTextbox;
	@FindBy(xpath = "//div[contains(@class,'x-window grid-column-filters-window')]//span/span/span[.='Apply']/following-sibling::span")
	public WebElement filterBidSheetApplyBtn;

	public PageVendorBids waitForVendorInputCombobxEnable() {
		String cssString = vendorInputCombobox.getAttribute("id");
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.elementToBeClickable(By.id(cssString))
		);
		return this;
	}

	public PageVendorBids waitFoVendorBidsGridReturn() throws InterruptedException {
		Thread.sleep(2000);
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div[id='emweb-bidMaster-vendorBid-view-vendorBidsProductsGrid-body'] tr"))
		);
		return this;
	}

	public PageVendorBids waitForRetrieveBtnEnable() throws Exception {
		Thread.sleep(1000);
		String idString = retrieveBtn.getAttribute("id");
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.elementToBeClickable(By.id(idString))
		);
		return this;
	}

	public WebElement vendorBidsGridCellItem(int rowNum, String columnHdrLabel) throws Exception {
		String colcssPath = null;
		switch (columnHdrLabel.toUpperCase()) {
			case "PRODUCT NAME":
				colcssPath = "div[id='emweb-bidMaster-vendorBid-view-vendorBidsProductsGrid-body']>div>table>colgroup>col[class*='x-grid-cell-headerId-gridcolumn_productName']";
				break;
			case "PRODUCT #":
				colcssPath = "div[id='emweb-bidMaster-vendorBid-view-vendorBidsProductsGrid-body']>div>table>colgroup>col[class*='x-grid-cell-headerId-vendorBid-view-vendorBidsProductsGrid-grid-products-column-productNumber']";
				break;
			case "INV.UNIT":
				colcssPath = "div[id='emweb-bidMaster-vendorBid-view-vendorBidsProductsGrid-body']>div>table>colgroup>col[class*='x-grid-cell-headerId-vendorBid-view-vendorBidsProductsGrid-grid-products-column-inventoryPackageType']";
				break;
			case "VENDOR #":
				colcssPath = "div[id='emweb-bidMaster-vendorBid-view-vendorBidsProductsGrid-body']>div>table>colgroup>col[class*='x-grid-cell-headerId-vendorBid-view-vendorBidsProductsGrid-grid-products-column-vendorProductNumber']";
				break;
			case "VENDOR UNIT":
				colcssPath = "div[id='emweb-bidMaster-vendorBid-view-vendorBidsProductsGrid-body']>div>table>colgroup>col[class*='x-grid-cell-headerId-vendorBid-view-vendorBidsProductsGrid-grid-products-column-vendorUnit']";
				break;
			case "BRAND":
				colcssPath = "col[class*='x-grid-cell-headerId-vendorBid-view-vendorBidsProductsGrid-grid-products-column-productBrand']";
				break;
			case "CONV.":
				colcssPath = "div[id='emweb-bidMaster-vendorBid-view-vendorBidsProductsGrid-body']>div>table>colgroup>col[class*='x-grid-cell-headerId-vendorBid-view-vendorBidsProductsGrid-grid-products-column-conversionFactor']";
				break;
			case "PRICE":
				colcssPath = "div[id='emweb-bidMaster-vendorBid-view-vendorBidsProductsGrid-body']>div>table>colgroup>col[class*='x-grid-cell-headerId-vendorBid-view-vendorBidsProductsGrid-grid-products-column-bidPrice']";
				break;
			case "ALLOW SPLIT":
				colcssPath = "div[id='emweb-bidMaster-vendorBid-view-vendorBidsProductsGrid-body']>div>table>colgroup>col[class*='x-grid-cell-headerId-vendorBid-view-vendorBidsProductsGrid-grid-products-column-allowSplit']";
				break;
			case "ALT. UNITS":
				colcssPath = "div[id='emweb-bidMaster-vendorBid-view-vendorBidsProductsGrid-body']>div>table>colgroup>col[class*='x-grid-cell-headerId-gridcolumn_hasSecondary']";
				break;
			case "TAX CODE":
				colcssPath = "div[id='emweb-bidMaster-vendorBid-view-vendorBidsProductsGrid-body']>div>table>colgroup>col[class*='x-grid-cell-headerId-vendorBid-view-vendorBidsProductsGrid-grid-products-column-taxCodeId']";
				break;
			case "CHECKBOX":
				colcssPath = "div[id='emweb-bidMaster-vendorBid-view-vendorBidsProductsGrid-body']>div>table>colgroup>col[class*='x-grid-cell-headerId-gridcolumn_checked']";
				break;
			//This is a hidden column by default
			case "BID END DATE":
				colcssPath = "div[id='emweb-bidMaster-vendorBid-view-vendorBidsProductsGrid-body']>div>table>colgroup>col[class*='x-grid-cell-headerId-vendorBid-view-vendorBidsProductsGrid-grid-products-column-endDate']";
				break;
			case "REFERENCE #":
				colcssPath = "col[class*='x-grid-cell-headerId-vendorBid-view-vendorBidsProductsGrid-grid-products-column-referenceNumber']";
				break;
			case "REFERENCE LINE #":
				colcssPath = "col[class*='x-grid-cell-headerId-vendorBid-view-vendorBidsProductsGrid-grid-products-column-referenceLineNumber']";
				break;
			case "EXPIRATION DATE":
				colcssPath = "col[class*='x-grid-cell-headerId-vendorBid-view-vendorBidsProductsGrid-grid-products-column-expirationDate']";
				break;
			/**
			 *
			 * TO DO:
			 * Need to add hidden columns to the switch/case statement
			 *
			 */
		}
		List<WebElement> colgroup = driver.findElements(By.cssSelector("div[id='emweb-bidMaster-vendorBid-view-vendorBidsProductsGrid-body'] col"));
		int colgroupIndex = colgroup.indexOf(driver.findElement(By.cssSelector(colcssPath))) + 1;
		//System.out.println(colgroupIndex);
		String cellcssSelectorPath = "div[id='emweb-bidMaster-vendorBid-view-vendorBidsProductsGrid-body']>div>table>tbody>tr:nth-child(" + rowNum + ")>td:nth-child(" + colgroupIndex + ")";
		//System.out.println(cellcssSelectorPath);
		return driver.findElement(By.cssSelector(cellcssSelectorPath));
	}

	public PageVendorBids waitForAdditionalMarketsBtnEnable() {
		String idString = additionalMarketsBtn.getAttribute("id");
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.elementToBeClickable(By.id(idString))
		);
		return this;
	}

	public PageVendorBids waitForAdditionalMarketsPopupReturn() throws Exception {
		Waiter.waitForOneSecond();
		String idString = additionalMarketsPopupGrid.getAttribute("id");
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#" + idString + " tr"))
		);
		return this;
	}

	public void selectAdditinalMarketByName(String marketName, String setToYorN) throws Exception {
		Waiter.waitForOneSecond();
		int rowCnt = additionalMarketsPopupGrid.findElements(By.cssSelector("tr")).size();
		int rowNum = 0;
		for (int i = 1; i <= rowCnt; i++) {
			String evlString = additionalMarketsPopupGrid.findElement(By.cssSelector("tr:nth-child(" + i + ")")).getText();
			if (evlString.contains(marketName)) {
				rowNum = i;
				break;
			}
		}
		boolean selectFlag = additionalMarketsPopupGrid.findElement(By.cssSelector("tr:nth-child(" + rowNum + ")")).getAttribute("class").contains("selected");
		if (selectFlag) if (setToYorN.toUpperCase().equals("N"))
			additionalMarketsPopupGrid.findElement(By.cssSelector("tr:nth-child(" + rowNum + ")>td:nth-child(2)")).click();
		if (!selectFlag) if (setToYorN.toUpperCase().equals("Y"))
			additionalMarketsPopupGrid.findElement(By.cssSelector("tr:nth-child(" + rowNum + ")>td:nth-child(2)")).click();
	}

	public void editAdditionalMarketsPopupIncludeSplitSubCheckbox(String setValueToYorN) throws Exception {
		boolean checkedFlag = additionalMarketsPopupIncludeSplitSubCheckbox.getAttribute("class").contains("checked");
		if (checkedFlag) if (setValueToYorN.toUpperCase().equals("N"))
			additionalMarketsPopupIncludeSplitSubCheckbox.findElement(By.tagName("input")).click();
		if (!checkedFlag) if (setValueToYorN.toUpperCase().equals("Y"))
			additionalMarketsPopupIncludeSplitSubCheckbox.findElement(By.tagName("input")).click();
	}

	/**
	 * Vendor Bids detail screen > Bid Edit Form
	 */
	public PageVendorBids waitFoVendorBidsEditFormReturn() {
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("emweb-bidMaster-vendorBid-view-vendorBidEditForm"))
				//presenceOfElementLocated(By.id("emweb-bidMaster-vendorBid-view-vendorBidEditForm"))
		);
		return this;
	}

	public void editbidEditFormAllowSplitCheckbox(String setValueToYorN) throws Exception {
		boolean checkedFlag = bidEditFormAllowSplitCheckbox.getAttribute("class").contains("checked");
		if (checkedFlag) if (setValueToYorN.toUpperCase().equals("N"))
			bidEditFormAllowSplitCheckbox.findElement(By.tagName("input")).click();
		if (!checkedFlag) if (setValueToYorN.toUpperCase().equals("Y"))
			bidEditFormAllowSplitCheckbox.findElement(By.tagName("input")).click();
	}

	/**
	 * Vendor Bid Split and Secondary Vendor Products popup
	 */

	public PageVendorBids waitForSecondaryBidsPopupReturn() throws Exception {
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.presenceOfElementLocated(By.cssSelector("div[ces-selenium-id='ces-emweb-vendorBid-secondaryVendorProducts-grid_secondaryVendorProductsGrid'] tbody"))
		);
		Thread.sleep(1000);
		int gridRowCnt = driver.findElements(By.cssSelector("div[ces-selenium-id='ces-emweb-vendorBid-secondaryVendorProducts-grid_secondaryVendorProductsGrid'] tr")).size();
		if (gridRowCnt != 0) {
			new WebDriverWait(driver, 60, 1000).until(
					ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div[ces-selenium-id='ces-emweb-vendorBid-secondaryVendorProducts-grid_secondaryVendorProductsGrid'] tr"))
			);
		}
		Waiter.waitForOneSecond();
		return this;
	}

	public WebElement secondaryBidsGridCellItem(int rowNum, String columnHdrLabel) throws Exception {
		String colcssPath = null;
		switch (columnHdrLabel.toUpperCase()) {
			case "VENDOR #":
				colcssPath = "td[class*='column-productNumber'] div";
				break;
			case "VENDOR PRODUCT NAME":
				colcssPath = "td[class*='column-productName'] div";
				break;
			case "VENDOR UNIT":
				colcssPath = "td[class*='column-packageId'] div";
				break;
			case "BRAND":
				colcssPath = "td[class*='column-productBrand'] div";
				break;
			case "CONV.":
				colcssPath = "td[class*='column-conversionFactor'] div";
				break;
			case "INVERTED":
				colcssPath = "td[class*='column-invertFlag'] div img";
				break;
			case "PRICE":
				colcssPath = "td[class*='column-price'] div";
				break;
			case "SPLIT":
				colcssPath = "td[class*='column-splitFlag'] div img";
				break;
			case "SUB.":
				colcssPath = "td[class*='column-subFlag'] div img";
				break;
			case "NOT APPROVED":
				colcssPath = "td[class*='column-approved'] div img";
				break;
			case "RESTRICTED":
				colcssPath = "td[class*='column-restricted'] div img";
				break;
			case "TAX CODE":
				colcssPath = "td[class*='column-taxCodeId'] div";
				break;
			case "END DATE":
				colcssPath = "td[class*='column-endDate'] div";
				break;
			case "DELETE":
				colcssPath = "td[class*='column-deleteAction'] div img";
				break;
		}
		Thread.sleep(1000);
		String cellcssSelectorPath = "div[ces-selenium-id='ces-emweb-vendorBid-secondaryVendorProducts-grid_secondaryVendorProductsGrid'] [ces-selenium-id='gridview'] tbody tr:nth-child(" + rowNum + ") " + colcssPath;
		return driver.findElement(By.cssSelector(cellcssSelectorPath));
	}

	public void editSecondaryBidsGridCellItem(WebElement element, String editValue) throws Exception {
		Waiter.waitForOneSecond();
		String elementTagName = element.getTagName();
		if (elementTagName.equals("img")) {
			boolean checkedFlag;
			checkedFlag = element.getAttribute("class").contains("checked");
			if (editValue.equals("N")) if (checkedFlag) element.click();
			if (editValue.equals("Y")) if (!checkedFlag) element.click();
		} else {
			Waiter.waitForOneSecond();
			org.openqa.selenium.interactions.Actions cellAction = new org.openqa.selenium.interactions.Actions(driver);
			cellAction.doubleClick(element).build().perform();
			Waiter.waitUntilElementAttributeContain(element, "style", "visibility: hidden");
			WebElement inputElementLocaor = driver.switchTo().activeElement();
			Waiter.waitForElementToBeVisible(driver, inputElementLocaor);
			if (inputElementLocaor.getAttribute("name").contains("combobox")) {
				ActionsEMW.setComboBoxDropdownByValue(driver, inputElementLocaor, editValue);
			} else {
				inputElementLocaor.click();
				inputElementLocaor.clear();
				inputElementLocaor.sendKeys(editValue + Keys.ENTER);
			}
			Waiter.waitForOneSecond();
		}
	}

	/**
	 * Select Bid Sheets popup window
	 */
	public PageVendorBids waitForSelectBidSheetsPopupReturn() throws Exception {
		Thread.sleep(1000);
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div[class*='x-window select-bid-sheet x-layer x-window-default x-closable'] tbody tr"))
		);
		Thread.sleep(1000);
		return this;
	}
}