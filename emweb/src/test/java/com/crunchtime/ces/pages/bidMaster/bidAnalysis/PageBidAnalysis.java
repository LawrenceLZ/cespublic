package com.crunchtime.ces.pages.bidMaster.bidAnalysis;

import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class PageBidAnalysis {
	private final WebDriver driver;

	public PageBidAnalysis(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "div[id*='ces-emweb-bid-analysis-detail'][class*='panel-body'] div[id*='tabpanel'][class*='body']")
	public WebElement bidAnalysisPageBody;
	@FindBy(css = ".x-mask-msg-text")
	public WebElement loadSpinner;
	@FindBy(css = "a[data-qtip='Contract Selection']")
	public WebElement contractSelectionTabLink;
	@FindBy(xpath = "//span[.='Bid Sheets']")
	public WebElement bidSheetsBtn;
	@FindBy(css = "div[id*='-locked'][class*='locked'] table tbody")
	public WebElement contractSelectionTabLockedTable;
	@FindBy(css = "div[id*='normal'] table tbody")
	public WebElement contractSelectionTabVendorBidGrid;
	@FindBy(css = "[id*='emweb-bid-analysis-detail'] input[role='combobox']")
	public WebElement marketInputCombobox;
	//date field
	@FindBy(css = "[id*='emweb-bid-analysis-detail'] table[id*='datefield'] input")
	public WebElement effectiveDatePicker;
	//retrieve button
	@FindBy(css = "div[ces-selenium-id='toolbar_analyzeGridToolbar'] div[ces-selenium-id='container'] a[data-qtip*='Retrieve']")
	public WebElement retrieveBtn;
	//contract page best button
	@FindBy(css = "div[ces-selenium-id='toolbar_analyzeGridToolbar'] div[ces-selenium-id='container'] span a[data-qtip*='best prices in selected Market for products on this page']")
	public WebElement contractPageBest;
	//end page best button
	@FindBy(css = "div[ces-selenium-id='toolbar_analyzeGridToolbar'] div[ces-selenium-id='container'] span a[data-qtip*='End contracts for products on this page']")
	public WebElement endPageContracts;
	//contract all best button
	@FindBy(css = "div[ces-selenium-id='toolbar_analyzeGridToolbar'] div[ces-selenium-id='container'] span a[data-qtip*='best prices in selected Market for all products']")
	public WebElement contractAllBest;
	//end page best button
	@FindBy(css = "div[ces-selenium-id='toolbar_analyzeGridToolbar'] div[ces-selenium-id='container'] span a[data-qtip='End contracts for all products']")
	public WebElement endAllContracts;
	//bidAwards button
	@FindBy(css = "[id*='emweb-bid-analysis-detail'] a[data-qtip*='awarded']")
	public WebElement bidAwards;
	@FindBy(css = "[id*='emweb-bid-analysis-detail'] [class*='filter']")
	public WebElement filterBidAnalysisGrid;
	@FindBy(css = "div[id*='bid-analysis-bid-awards'] span[id*='textEl']")
	public WebElement bidAwardsSelectPopup;
	@FindBy(css = "div[id*='bid-analysis-bid-awards'] div[class*='checkbox'] span")
	public WebElement bidAwardsSelectPopupSelectAll;
	@FindBy(css = "div[id*='window'] img[class='x-tool-img x-tool-close']")
	public WebElement bidAwardsSelectPopupCloseButton;
	@FindBy(css = "div[id*='product-details']")
	public WebElement productDetailsPopup;
	@FindBy(css = "div[id*='bid-analysis-product-details'] img[class*='x-tool-close']")
	public WebElement productDetailsPopupCloseButton;
	//include tax checkbox
	@FindBy(css = "[id*='emweb-bid-analysis-detail'] input[id*='checkbox']")
	public WebElement includeTaxCheckbox;
	@FindBy(css = "div[class*='x-window select-bid-sheet']")
	public WebElement selectBidSheetsPopup;
	@FindBy(css = "div[class*='x-window select-bid-sheet'] div[class*='x-column-header-checkbox'] span")
	public WebElement selectBidSheetsPopupSelectAllCheckbox;
	@FindBy(css = "div[class*='x-window select-bid-sheet'] img[class*='x-tool-img x-tool-filter-']")
	public WebElement selectBidSheetsPopupFilterIcon;
	@FindBy(xpath = "//div[contains(@class,'x-window select-bid-sheet')]//span[.='Apply'][@class='x-btn-button']")
	public WebElement selectBidSheetsPopupApplyBth;
	@FindBy(css = "div[class*='x-window grid-column-filters-window'] input[name='name']")
	public WebElement filterBidSheetTextbox;
	//Find Bid Analysis grid unlocked panel cell item
	@FindBy(xpath = "//div[contains(@class,'x-window grid-column-filters-window')]//span[.='Apply'][@class='x-btn-button']")
	public WebElement filterBidSheetApplyBtn;
	@FindBy(xpath=".//div[@ces-selenium-id = 'window']//label[contains(.,'Subcategory')]")
	public WebElement filterSubCategoryRadioButton;
	@FindBy(css = "div[class*='x-window grid-column-filters-window'] input[name='category']")
	public WebElement filterCategoryDropdown;
	@FindBy(css = "div[class*='x-window grid-column-filters-window'] input[name='subCategory']")
	public WebElement filterSubcategoryDropdown;
	@FindBy(css = "div[class$='filters-window x-layer x-window-default x-border-box x-resizable x-window-resizable x-window-default-resizable'] a[data-qtip='Filters']")
	public WebElement filterApplyBtn;
	@FindBy(css = "div[id*='bid-analysis-bid-awards'] table[id*='gridview'] tbody tr td[class*='cell-first']")
	private List<WebElement> listBidAwardsSetupVendors;
	@FindBy(css = "div[id*='product-details'] tbody div:first-child")
	private List<WebElement> listProductDetails;
	@FindBy(css = "div[id*='analyzeGrid'] tbody tr td div a[href*='#']")
	private List<WebElement> listProductLinks;
	@FindBy(css = "div[id*='normal'] div[class*='x-grid'] div[id*='-textEl'] div:first-child")
	private List<WebElement> listCurrentVendors;
	@FindBy(css = "div[id*='locked'] tbody tr")
	private List<WebElement> listProductNumbers;

	/**
	 * Contract Selection tab
	 */
	public PageBidAnalysis waitForBidAnalysisGridReturn() throws InterruptedException {
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div[id*='normal'] tbody tr:nth-child(1) td:nth-child(1)")));
		return this;
	}

	public WebElement bidAnalysisGridCellItem(int rowNum, int colNum) throws Exception {
		String cellcssSelectorPath = "div[id*='normal'] tbody tr:nth-child(" + rowNum + ") td:nth-child(" + colNum + ")";
		return driver.findElement(By.cssSelector(cellcssSelectorPath));
	}

	public WebElement bidAnalysisLockedGridCellItem(int rowNum, int colNum) throws Exception {
		String cellcssSelectorPath = "div[id*='locked'] tbody tr:nth-child(" + rowNum + ") td:nth-child(" + colNum + ")";
		return driver.findElement(By.cssSelector(cellcssSelectorPath));
	}

	public Boolean bidAnalysisCheckIfGridCellItemSelected(int rowNum, int colNum) {
		String cellcssSelectorPath = "div[id*='normal'] tbody tr:nth-child(" + rowNum + ") td:nth-child(" + colNum + ")";
		WebElement gridCell = driver.findElement(By.cssSelector(cellcssSelectorPath));
		String classValue = gridCell.getAttribute("class");
		if (classValue.contains("selected")) {
			return true;
		} else {
			return false;
		}
	}

	public Boolean bidAnalysisCheckIfGridCellItemBestPrice(int rowNum, int colNum) {
		String cellcssSelectorPath = "div[id*='normal'] tbody tr:nth-child(" + rowNum + ") td:nth-child(" + colNum + ")";
		WebElement gridCell = driver.findElement(By.cssSelector(cellcssSelectorPath));
		String classValue = gridCell.getAttribute("class");
		if (classValue.contains("price-best")) {
			return true;
		} else {
			return false;
		}
	}

	public int bidAnalysisProductIndex(String passedProductNumber) throws Exception {
		String[] productNumbers = getProductNumbers();
		int rowIndex = 0;

		for (rowIndex = 0; rowIndex < productNumbers.length; rowIndex++) {
			if (productNumbers[rowIndex].equals(passedProductNumber)) {
				break;
			}
		}
		return rowIndex;
	}

	public int bidAnalysisVendorIndex(String passedVendorName) throws Exception {

		String[] vendorNames = getVendorNames();
		int columnIndex = 0;

		for (columnIndex = 0; columnIndex < vendorNames.length; columnIndex++) {
			if (vendorNames[columnIndex].equals(passedVendorName)) {
				break;
			}
		}
		return columnIndex;
	}

	/**
	 * Contract Selection tab --> Select Bid Sheets popup window
	 */
	public PageBidAnalysis waitForSelectBidSheetsPopupReturn() throws Exception {
		Waiter.waitForOneSecond();
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div[class*='x-window select-bid-sheet x-layer x-window-default x-closable'] tbody tr"))
		);
		Waiter.waitForOneSecond();
		return this;
	}

	public WebElement getVendorCheckbox(String vendorName) throws Exception {
		List<WebElement> vendorCheckbox = driver.findElements(By.cssSelector("div[id*='normal'] div[class*='x-grid'] div[id*='-textEl'] div span"));
		int index = bidAnalysisVendorIndex(vendorName);
		return vendorCheckbox.get(index);
	}

	public String[] getVendorNames() {
		List<String> vendorNameList = new ArrayList<>();
		for (WebElement vendorName : listCurrentVendors) {
			vendorNameList.add(vendorName.getText());
		}
		return vendorNameList.toArray(new String[vendorNameList.size()]);
	}

	public String[] getProductNumbers() throws Exception {
		List<String> productNumberList = new ArrayList<>();
		int i = 1;
		for (WebElement productNumber : listProductNumbers) {
			productNumberList.add(bidAnalysisLockedGridCellItem(i, 2).getText());
			i++;
		}
		return productNumberList.toArray(new String[productNumberList.size()]);
	}

	public String[] getBidAwardsVendorNames() {
		List<String> vendorNameList = new ArrayList<>();
		for (WebElement vendorName : listBidAwardsSetupVendors) {
			vendorNameList.add(vendorName.getText());
		}
		return vendorNameList.toArray(new String[vendorNameList.size()]);
	}

	public String[] getProductDetails() {
		List<String> productDetailList = new ArrayList<>();
		for (WebElement productDetails : listProductDetails) {
			productDetailList.add(productDetails.getText());
		}
		return productDetailList.toArray(new String[productDetailList.size()]);
	}

	public void clickFirstHyperlink() {
		listProductLinks.get(0).click();
	}
}