package com.crunchtime.ces.pages.products.companyProducts;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageCompanyProductsSummary {
	private final WebDriver driver;

	public PageCompanyProductsSummary(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "form#netchefForm img[src*='spinner.gif']") public WebElement compProdSummScrnSpinnerIcon;
	@FindBy(css = "form#netchefForm table tbody") public WebElement companyProductsSummScrnGrid;
}