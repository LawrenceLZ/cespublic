package com.crunchtime.ces.pages.otherTasks;

import com.crunchtime.ces.helper.Waiter;
import com.crunchtime.ces.pages.bidMaster.bidAnalysis.PageBidAnalysis;
import com.crunchtime.ces.pages.bidMaster.vendorBids.PageVendorBids;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageOtherTasks {
	private final WebDriver driver;

	public PageOtherTasks(WebDriver driver) {
		this.driver = driver;
	}

	private static final String vendorBidsScrnPageUrl = "/vendorBidIndex.ct";
	private static final String bidAnalysisScrnPageUrl = "/bidAnalysisIndex.ct";

	@FindBy(id = "menu.bidmaster.vendorBid.new") private WebElement vendorBidsLink;
	@FindBy(id = "menu.bidmaster.bidAnalysis") private WebElement bidAnalysisLink;
	@FindBy(id = "emweb-bidMaster-vendorBid-view-vendorBidsGrid-body") private WebElement pageVendorBidsBody;
	@FindBy(css = "div[id*='ces-emweb-bid-analysis-detail'][class*='panel-body'] div[id*='tabpanel'][class*='body']") private WebElement pageBidAnalysisPageBody;

	public PageVendorBids navigateToVendorBidsScreen() throws Exception {
		vendorBidsLink.click();
		//wait to make sure recipe summary screen is returned
		Waiter.waitForElementToBeVisible(driver, pageVendorBidsBody);
		if (!driver.getCurrentUrl().endsWith(vendorBidsScrnPageUrl)) {
			throw new IllegalStateException("Unable to open EMW > Other Tasks > Vendor Bids page.");
		}
		return new PageVendorBids(driver);
	}

	public PageBidAnalysis navigateToBidAnalysisScreen() throws Exception {
		bidAnalysisLink.click();
		//wait to make sure recipe summary screen is returned
		Waiter.waitForElementToBeVisible(driver, pageBidAnalysisPageBody);
		if (!driver.getCurrentUrl().contains(bidAnalysisScrnPageUrl)) {
			throw new IllegalStateException("Unable to open EMW > Other Tasks > Bid Analysis page.");
		}
		return new PageBidAnalysis(driver);
	}
}