package com.crunchtime.ces.pages.products.packageTypes;

import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class PagePackageTypes {
	private final WebDriver driver;

	public PagePackageTypes(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "div[id*='loadmask'][class='x-mask-msg-text']") private List<WebElement> loadSpinnerMasks;
	public void waitForSpinnerToDisappear() throws Exception{
		Waiter.waitForOneSecond();
		//wait to make sure summary screen is returned
		for (WebElement loadSpinnerMask : loadSpinnerMasks){
			try {
				Waiter.waitForElementToDisappear(driver, loadSpinnerMask);
			} catch (Exception e){
				break;
			}
		}
		Waiter.waitForOneSecond();
	}
	@FindBy(css = ".x-mask-msg-text")
	public WebElement loadSpinner;
	@FindBy(id = "ces-emweb-products-packageTypes-view-PackageTypeGrid-button-add")
	public WebElement addBtn;
	@FindBy(css = "div[id='emweb-products-packageTypes-view-PackageTypeGrid_header-body'] img[class*='x-tool-export']")
	public WebElement packageTypePrintBtn;
	@FindBy(css = "div[id='emweb-products-packageTypes-view-PackageTypeGrid_header-body'] img[class*='x-tool-filter']")
	public WebElement packageTypeFilterBtn;
	@FindBy(css = "div[id='emweb-products-packageTypes-view-PackageTypeGrid_header-body'] img[class*='filter-applied-button']")
	public WebElement packageTypeFilterAppliedBtn;
	@FindBy(css = "div[id='packageType-view-PackageTypeGrid-grid-column-packageType']")
	public WebElement packageTypeColumnHdrText;
	@FindBy(css = "div[id='packageType-view-PackageTypeGrid-grid-column-packageType-titleEl']")
	public WebElement packageTypeColumnHdr;
	@FindBy(css = "div[id='packageType-view-PackageTypeGrid-grid-column-inventoryType-textEl'] span")
	public WebElement inventoryTypeColumnHdrCheckbox;
	@FindBy(css = "div[id='packageType-view-PackageTypeGrid-grid-column-recipeType-textEl'] span")
	public WebElement recipeTypeColumnHdrCheckbox;
	@FindBy(css = "div[id='packageType-view-PackageTypeGrid-grid-column-purchasingType-textEl'] span")
	public WebElement purchasingTypeColumnHdrCheckbox;
	@FindBy(css = "div[id*='messagebox'] tbody div:first-child")
	public WebElement messageBox;
	@FindBy(css = "div[id*='messagebox'] a[ces-selenium-id='button_ok']")
	public WebElement messageBoxOK;
	@FindBy(css = "div[id*='messagebox'] a[ces-selenium-id='button_cancel']")
	public WebElement messageBoxCancel;
	//Create Package Type Popup Objects
	@FindBy(css = "div[id='emweb-products-packageType-view-createPackageType-body'] input[id*='packageType-inputEl']")
	public WebElement createPackageTypeTextbox;
	@FindBy(css = "div[id='emweb-products-packageType-view-createPackageType-body'] input[id*='field-description-inputEl']")
	public WebElement createPackageDescriptionTextbox;
	@FindBy(css = "div[id='emweb-products-packageType-view-createPackageType-body'] table[class*='x-form-cb-checked'] input[id*='inventoryType-inputEl']")
	public WebElement createInventoryTypeCheckboxChecked;
	@FindBy(css = "div[id='emweb-products-packageType-view-createPackageType-body'] table[class*='x-form-dirty'] input[id*='inventoryType-inputEl']")
	public WebElement createInventoryTypeCheckboxUnchecked;
	@FindBy(css = "div[id='emweb-products-packageType-view-createPackageType-body'] table[class*='x-form-cb-checked'] input[id*='recipeType-inputEl']")
	public WebElement createRecipeTypeCheckboxChecked;
	@FindBy(css = "div[id='emweb-products-packageType-view-createPackageType-body'] table[class*='x-form-dirty'] input[id*='recipeType-inputEl']")
	public WebElement createRecipeTypeCheckboxUnchecked;
	@FindBy(css = "div[id='emweb-products-packageType-view-createPackageType-body'] table[class*='x-form-cb-checked'] input[id*='purchasingType-inputEl']")
	public WebElement createPurchasingTypeCheckboxChecked;
	@FindBy(css = "div[id='emweb-products-packageType-view-createPackageType-body'] table[class*='x-form-dirty'] input[id*='purchasingType-inputEl']")
	public WebElement createPurchasingTypeCheckboxUnchecked;
	@FindBy(css = "div[id='emweb-products-packageType-view-createPackageType'] img[class='x-tool-img x-tool-close']")
	public WebElement createXBtn;
	@FindBy(css = "div[id='emweb-products-packageType-view-createPackageType'] a[id*='updateAndSave']")
	public WebElement createUpdateAndSaveBtn;
	@FindBy(css = "div[id='emweb-products-packageType-view-createPackageType'] a[id*='button-close']")
	public WebElement getCreateCloseButtonBtn;
	@FindBy(css="div[id='packageType-view-createPackageType-form-create-field-packageType-errorEl'] li")
	public WebElement packageTypeErrorMessage;
	@FindBy(css="div[id='packageType-view-createPackageType-form-create-field-description-errorEl'] li")
	public WebElement packageDescErrorMessage;



	//Filter Package Type Popup
	@FindBy(css = "div[id='packageType-view-PackageTypeGrid-filter-body'] input[id*='packageName-inputEl']")
	public WebElement filterPackageNameTextbox;
	@FindBy(css = "div[id='packageType-view-PackageTypeGrid-filter-body'] input[id*='packageDesc-inputEl']")
	public WebElement filterPackageDescTextbox;
	@FindBy(css = "div[id='packageType-view-PackageTypeGrid-form-filterPanel-body'] input[id*='filterPanel-inventoryType-inputEl']")
	public WebElement filterInventoryTypeCheckbox;
	@FindBy(css = "div[id='packageType-view-PackageTypeGrid-form-filterPanel-body'] input[id*='filterPanel-recipeType-inputEl']")
	public WebElement filterRecipeTypeCheckbox;
	@FindBy(xpath = "//div[contains(@id,'packageType-view-PackageTypeGrid-filter')]//span[.='Apply'][@class='x-btn-button']")
	public WebElement filterAcceptBtn;
	@FindBy(xpath = "//div[contains(@id,'packageType-view-PackageTypeGrid-filter')]//span[.='Cancel'][@class='x-btn-button']")
	public WebElement filterCancelBtn;
	@FindBy(xpath = "//div[contains(@id,'packageType-view-PackageTypeGrid-filter')]//span[.='Clear'][@class='x-btn-button']")
	public WebElement filterClearBtn;
	//Pagination Tool Bar
	@FindBy(css = "div[id*='pagingtoolbar'] input[id*='numberfield']")
	public WebElement pageNumberTextbox;
	@FindBy(css = "div[id*='pagingtoolbar'] span[class*='page-next']")
	public WebElement pageNextArrow;
	@FindBy(css = "div[id*='pagingtoolbar'] span[class*='page-last']")
	public WebElement pageLastArrow;
	@FindBy(css = "div[id*='pagingtoolbar'] span[class*='page-prev']")
	public WebElement pagePrevArrow;
	@FindBy(css = "div[id*='pagingtoolbar'] span[class*='page-first']")
	public WebElement pageFirstArrow;
	@FindBy(css = "div[ces-selenium-id*='afterTextItem']")
	public WebElement pageLimits;
	@FindBy(css = "div[ces-selenium-id='tbtext_displayItem']")
	public WebElement paginationTlBarText;
	@FindBy(css = "a[ces-selenium-id='button_yes']")
	public WebElement deleteYesBtn;
	@FindBy(css = "div[id='emweb-products-packageTypes-view-PackageTypeGrid-body'] tbody tr")
	private List<WebElement> listPackageTypes;


	public WebElement getPackageTypeGridCellItem(int rowNum, String columnHdrLabel) throws Exception {
		String colcssPath = null;
		switch (columnHdrLabel.toUpperCase()) {
			case "PACKAGE TYPE":
				colcssPath = "div[id='emweb-products-packageTypes-view-PackageTypeGrid-body'] col[class*='x-grid-cell-headerId-gridcolumn_packageName']";
				break;
			case "PACKAGE DESCRIPTION":
				colcssPath = "div[id='emweb-products-packageTypes-view-PackageTypeGrid-body'] col[class*='grid-column-packageDescription']";
				break;
			case "INVENTORY":
				colcssPath = "div[id='emweb-products-packageTypes-view-PackageTypeGrid-body'] col[class*='grid-column-inventoryType']";
				break;
			case "RECIPE":
				colcssPath = "div[id='emweb-products-packageTypes-view-PackageTypeGrid-body'] col[class*='grid-column-recipeType']";
				break;
			case "PURCHASING":
				colcssPath = "div[id='emweb-products-packageTypes-view-PackageTypeGrid-body'] col[class*='grid-column-purchasingType']";
				break;
		}

		List<WebElement> colgroup = driver.findElements(By.cssSelector("div[id='emweb-products-packageTypes-view-PackageTypeGrid-body'] col"));
		int colgroupIndex = colgroup.indexOf(driver.findElement(By.cssSelector(colcssPath))) + 1;
		//System.out.println(colgroupIndex);
		String cellcssSelectorPath;
		if (columnHdrLabel.toUpperCase().contentEquals("PACKAGE TYPE") || columnHdrLabel.toUpperCase().contentEquals("PACKAGE DESC")){
			cellcssSelectorPath = "div[id='emweb-products-packageTypes-view-PackageTypeGrid-body'] tr:nth-child(" + rowNum + ")>td:nth-child(" + colgroupIndex + ")";
		}
		else {
			cellcssSelectorPath = "div[id='emweb-products-packageTypes-view-PackageTypeGrid-body'] tr:nth-child(" + rowNum + ")>td:nth-child(" + colgroupIndex + ") img";
		}
		//System.out.println(cellcssSelectorPath);
		return driver.findElement(By.cssSelector(cellcssSelectorPath));
	}

	public WebElement getDeleteButton(int packageTypeIndex) throws Exception {
		return driver.findElement(By.cssSelector("div[id='emweb-products-packageTypes-view-PackageTypeGrid-body'] tbody tr:nth-child(" + packageTypeIndex + ") img[src*='Delete']"));
	}

	public String[] getPackageNames() throws Exception {
		List<String> productNumberList = new ArrayList<>();
		int i = 1;
		for (WebElement productNumber : listPackageTypes) {
			productNumberList.add(getPackageTypeGridCellItem(i, "Package Type").getText());
			i++;
		}
		return productNumberList.toArray(new String[productNumberList.size()]);
	}

	public int getPackageTypeIndex(String passedPackageName) throws Exception {
		String[] packageNames = getPackageNames();
		int rowIndex = 0;

		for (rowIndex = 0; rowIndex < packageNames.length; rowIndex++) {
			if (packageNames[rowIndex].equals(passedPackageName)) {
				return rowIndex + 1;
			}
		}
		return -1;
	}

	public String getPackageTypeCount() {
		String pagTlBarTextStr = paginationTlBarText.getText();
		String[] pagTlBarElements = pagTlBarTextStr.split(" ");
		return pagTlBarElements[pagTlBarElements.length - 1];
	}
}