package com.crunchtime.ces.pages.logout;

import com.crunchtime.ces.helper.ActionsEMW;
import com.crunchtime.ces.helper.Waiter;
import com.crunchtime.ces.pages.login.PageEMWLogin;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageEMWLogout {
	private final WebDriver driver;

	public PageEMWLogout(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "img[src*='emweb_new_logo.jpg']") private WebElement staticCTLogo;
	@FindBy(css = "a[title='Log Out'] img") private WebElement logoutIcon;



	public PageEMWLogin logoutEMW() throws Exception {
		driver.switchTo().defaultContent();
		Waiter.waitForOneSecond();
		boolean logoutBtnExists = logoutIcon.isDisplayed();
		if (logoutBtnExists) {
			logoutIcon.click();
		}
		boolean secAlertExists = ActionsEMW.isAlertPresent(driver);
		if (secAlertExists) {
			driver.switchTo().alert().accept();
			driver.switchTo().defaultContent();
		}
		Waiter.waitForElementToBeVisible(driver, staticCTLogo);
		if (!driver.getCurrentUrl().contains("login.do")) {
			throw new IllegalStateException("Unable to logout EMW and return to login page.");
		}
		return new PageEMWLogin(driver);
	}
}