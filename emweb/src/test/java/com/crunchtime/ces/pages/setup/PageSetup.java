package com.crunchtime.ces.pages.setup;

import com.crunchtime.ces.helper.Waiter;
import com.crunchtime.ces.pages.production.recipe.PageRecipeSummary;
import com.crunchtime.ces.pages.products.companyProducts.PageCompanyProductsSummary;
import com.crunchtime.ces.pages.products.packageTypes.PagePackageTypes;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageSetup {
	private final WebDriver driver;

	public PageSetup(WebDriver driver) {
		this.driver = driver;
	}

	private static final String rcpSummScrnPageUrl = "recipe/index.ct";
	private static final String compProdSummScrnPageUrl = "companyProduct/index.ct";
	private static final String packageTypesPageUrl = "packageType.ct";
	@FindBy(css = "div#menu") public WebElement pageSetupMainMenu;
	@FindBy(css = "a[id='menu.setup.recipe']") public WebElement setupRecipeLink;
	@FindBy(css = "a[id='menu.setup.companyProduct']") public WebElement setupCompanyProductsMenuLink;
	@FindBy(css = "a[id='menu.setup.packageType']") public WebElement setupPackageTypesMenuLink;
	@FindBy(css = "div.recipeIndexContainer div img[src*='spinner.gif']") private WebElement pageRecipeSummScrnSpinnerIcon;
	@FindBy(css = "div[ces-selenium-id='loadmask'] [class='x-mask-msg-text']") private WebElement pagePackageTypeScrnInitialLoadSpinner;
	@FindBy(css = "form#netchefForm img[src*='spinner.gif']") private WebElement pageCompProdSummScrnSpinnerIcon;


	public PageRecipeSummary navigateToRecipeSummaryScreen() throws Exception {
		setupRecipeLink.click();
		//wait to make sure recipe summary screen is returned
		Waiter.waitForElementToDisappear(driver, pageRecipeSummScrnSpinnerIcon);
		if (!driver.getCurrentUrl().contains(rcpSummScrnPageUrl)) {
			throw new IllegalStateException("Unable to open EMW > Setup > Recipe page.");
		}
		return new PageRecipeSummary(driver);
	}

	public PageCompanyProductsSummary navigateToCompanyProductsSummaryScreen() throws Exception {
		setupCompanyProductsMenuLink.click();
		//wait to make sure recipe summary screen is returned
		Waiter.waitForElementToDisappear(driver, pageCompProdSummScrnSpinnerIcon);
		if (!driver.getCurrentUrl().endsWith(compProdSummScrnPageUrl)) {
			throw new IllegalStateException("Unable to open EMW > Setup > Company Products page.");
		}
		return new PageCompanyProductsSummary(driver);
	}

	public PagePackageTypes navigateToPackageTypesScreen() throws Exception {
		setupPackageTypesMenuLink.click();
		//wait to make sure Package Types screen is returned
		Waiter.waitForElementToDisappear(driver, pagePackageTypeScrnInitialLoadSpinner);
		if (!driver.getCurrentUrl().endsWith(packageTypesPageUrl)) {
			throw new IllegalStateException("Unable to open EMW > Setup > Package Types page");
		}
		return new PagePackageTypes(driver);
	}
}