package com.crunchtime.ces.pages.production.recipe;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageRecipeSummary {
	private final WebDriver driver;

	public PageRecipeSummary(WebDriver driver) {
		this.driver = driver;
	}

	/**
	 * Recipe summary screen
	 */
	@FindBy(css = "div.recipeIndexContainer")
	public WebElement recipeSummScrnContainer;

	/**
	 *
	 *
	 *
	 * To-Do: clean up:
	 */
	@FindBy(css = "div.recipeIndexContainer tbody") public WebElement recipeSummScrnGrid;
	@FindBy(css = "div.recipeIndexContainer div img[src*='spinner.gif']") public WebElement recipeSummScrnSpinnerIcon;
	@FindBy(id = "action.add") public WebElement recipeSummScrnAddBtn;
	@FindBy(id = "numberFilter") public WebElement recipeSummScrnProdNumFilterInput;
	@FindBy(id = "recipeNameFilter") public WebElement recipeSummScrnProdNameFilterInput;

}