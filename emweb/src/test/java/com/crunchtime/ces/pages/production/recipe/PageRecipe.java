package com.crunchtime.ces.pages.production.recipe;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageRecipe {
	private final WebDriver driver;

	public PageRecipe(WebDriver driver) {
		this.driver = driver;
	}

	/**
	 * Recipe summary screen
	 */
	@FindBy(css = "div.recipeIndexContainer")
	public WebElement recipeSummScrnContainer;

	/**
	 *
	 *
	 *
	 * To-Do: clean up:
	 */
	@FindBy(css = "div.recipeIndexContainer tbody")
	public WebElement recipeSummScrnGrid;
	@FindBy(css = "div.recipeIndexContainer div img[src*='spinner.gif']")
	public WebElement recipeSummScrnSpinnerIcon;
	@FindBy(id = "action.add")
	public WebElement recipeSummScrnAddBtn;
	@FindBy(id = "numberFilter")
	public WebElement recipeSummScrnProdNumFilterInput;
	@FindBy(id = "recipeNameFilter")
	public WebElement recipeSummScrnProdNameFilterInput;
	/**
	 * Recipe detail screen
	 */
	@FindBy(css = "form button#saveAndCloseBtn")
	public WebElement recipeSaveAndCloseBtn;
	@FindBy(css = "a[href='#recipeHeaderFormContainer']")
	public WebElement recipeTabLink;
	@FindBy(id = "recipeName")
	public WebElement recipeDtlScrnRcpNameInputTextbox;
	@FindBy(css = "div[id*='categoryId'] a")
	public WebElement recipeDtlScrnCategoryDropdown;
	@FindBy(id = "categoryId")
	public WebElement recipeDtlScrnCategoryDropdownIE;
	@FindBy(id = "productNumber")
	public WebElement recipeDtlScrnProdNumInputTextbox;
	@FindBy(id = "plu")
	public WebElement recipeDtlScrnPluInputTextbox;
	/**
	 * TO-DO: fill in all the missing WebElement
	 */

	@FindBy(id = "batchQty")
	public WebElement recipeDtlScrnBatchSizeInputTextbox;
	@FindBy(css = "div[id*='batchUnit'] a")
	public WebElement recipeDtlScrnBatchUnitDropdown;
	@FindBy(id = "batchUnit")
	public WebElement recipeDtlScrnBatchUnitDropdownIE;
	@FindBy(id = "portionQty")
	public WebElement recipeDtlScrnPortionYieldInputTextbox;
	@FindBy(id = "portionAmount")
	public WebElement recipeDtlScrnPortionAmtInputTextbox;
	/**
	 * TO-DO: fill in all the missing WebElement
	 */

	@FindBy(css = "div[id*='recipeUnitOne'] a")
	public WebElement recipeDtlScrnRcpUnitOneDropdown;
	/**
	 * Component detail screen
	 */
	@FindBy(xpath = "//a[@href='#componentTableContainer']/..")
	public WebElement recipeComponentLink;
	@FindBy(id = "componentTableContainer")
	public WebElement recipeComponentScrnContainer;
	@FindBy(css = "table[id='componentTable'] tbody")
	public WebElement recipeComponentScrnGrid;
	@FindBy(css = "div[id='componentTableContainer'] button[id='AddButton']")
	public WebElement recipeComponentScrnAddBtn;
	/**
	 * Component detail screen > Add Recipe Components popup window
	 */
	@FindBy(css = "div[id='addComponentsContainer']")
	public WebElement recipeAddRcpComponentsPopupContainer;
	@FindBy(css = "table[id='addComponentsTable'] tbody")
	public WebElement recipeAddRcpComponentsPopupGrid;
	@FindBy(css = "div[id='addComponentsContainer'] button[id='Recipe.Recipe.Add'] span[id='Recipe.Recipe.AddText']")
	public WebElement recipeAddRcpComponentsPopupAddBtn;
	@FindBy(css = "div[id='addComponentsContainer'] button[id='Recipe.Recipe.AddAndClose'] span[id='Recipe.Recipe.AddAndCloseText']")
	public WebElement recipeAddRcpComponentsPopupAddAndCloseBtn;
	@FindBy(id = "addComponentsTable_paginate")
	public WebElement recipeAddRcpComponentsPopupPaginationLinks;
	@FindBy(id = "addComponentsTable_first")
	public WebElement recipeAddRcpComponentsPopupFirstPageLink;
	@FindBy(id = "addComponentsTable_last")
	public WebElement recipeAddRcpComponentsPopupLastPageLink;


	public void waitForRecipeDetailScreenReturn() throws Exception {
		Thread.sleep(1000);
		String idString = recipeDtlScrnRcpNameInputTextbox.getAttribute("id");
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.presenceOfElementLocated(By.id(idString))
		);
	}

	public void waitForRecipeTabAllFieldsValidationToPass() throws Exception {
		Thread.sleep(1000);
		boolean uiErrorMsg = recipeTabLink.getAttribute("class").contains("ui-state-error");
		for (int i = 0; i < 60; i++) {
			if (uiErrorMsg) Thread.sleep(1000);
			else break;
		}
	}

	public void waitForRecipeComponentScreenReturn() throws Exception {
		String idString = recipeComponentScrnContainer.getAttribute("id");
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div[id='" + idString + "'] div tr"))
		);
		Thread.sleep(1000);
	}

	public void waitForAddRcpComponentsPopupReturn() throws Exception {
		Thread.sleep(1000);
		String idString = recipeAddRcpComponentsPopupContainer.getAttribute("id");
		new WebDriverWait(driver, 120, 1000).until(
				ExpectedConditions.not(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[id='" + idString + "'] img[src*='spinner.gif']")))
		);
		Thread.sleep(1000);
	}

	public int getTotalPageCountForAddRcpComponentsPopup() throws Exception {
		recipeAddRcpComponentsPopupLastPageLink.click();
		waitForAddRcpComponentsPopupReturn();
		int totalPageCount = Integer.parseInt(recipeAddRcpComponentsPopupPaginationLinks.findElement(By.cssSelector("span a:last-child")).getText());
		recipeAddRcpComponentsPopupFirstPageLink.click();
		waitForAddRcpComponentsPopupReturn();
		Thread.sleep(1000);
		return totalPageCount;
	}

	public void recipeAddRcpComponentsPopupPaginateByPage(String pageNumberOrPageLink) throws Exception {
		Thread.sleep(1000);
		recipeAddRcpComponentsPopupPaginationLinks.findElement(By.xpath("//a[.='" + pageNumberOrPageLink + "']")).click();
		waitForAddRcpComponentsPopupReturn();
	}
}