package com.crunchtime.ces.pages.login;

import com.crunchtime.ces.helper.Waiter;
import com.crunchtime.ces.pages.setup.PageSetup;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

public class PageEMWLogin {
	private final WebDriver driver;

	public PageEMWLogin(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = ".cellItem.cellSelectLocationItem.cell-lable") private WebElement chooseLocationTitle;
	@FindBy(id = "username") private WebElement userName;
	@FindBy(id = "password") private WebElement userPwd;
	@FindBy(id = "locationId") private WebElement selectLocationNameDropdown;
	@FindBy(id = "action.submitButton") private WebElement singInBtn;
	@FindBy(css = "div#menu") private WebElement pageSetupMainMenu;

	public PageSetup loginEMW(String userId, String userPassword, String locationCanBeEmpty) throws Exception {
		userName.clear();
		userName.sendKeys(userId);
		userPwd.clear();
		userPwd.sendKeys(userPassword);
		singInBtn.submit();
		Waiter.waitForElementToBeVisible(driver, chooseLocationTitle);
		Select locName = new Select(selectLocationNameDropdown);
		if (!locationCanBeEmpty.isEmpty()) {
			locName.selectByVisibleText(locationCanBeEmpty);
			singInBtn.submit();
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		} else {
			singInBtn.submit();
		}
		Waiter.waitForElementToBeVisible(driver, pageSetupMainMenu);
		if (!driver.getCurrentUrl().contains("chooseLoginLocation.do")) {
			throw new IllegalStateException("Unable to open EMW > Setup page.");
		}
		return new PageSetup(driver);
	}
}