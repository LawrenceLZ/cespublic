package com.crunchtime.ces.base;

import com.crunchtime.ces.driver.DriverOfActiveThread;
import com.crunchtime.ces.pages.bidMaster.bidAnalysis.PageBidAnalysis;
import com.crunchtime.ces.pages.bidMaster.bidSheet.PageBidSheet;
import com.crunchtime.ces.pages.bidMaster.vendorBids.PageVendorBids;
import com.crunchtime.ces.pages.login.PageEMWLogin;
import com.crunchtime.ces.pages.logout.PageEMWLogout;
import com.crunchtime.ces.pages.otherTasks.PageOtherTasks;
import com.crunchtime.ces.pages.production.recipe.PageRecipe;
import com.crunchtime.ces.pages.production.recipe.PageRecipeSummary;
import com.crunchtime.ces.pages.products.companyProducts.PageCompanyProductsSummary;
import com.crunchtime.ces.pages.products.packageTypes.PagePackageTypes;
import com.crunchtime.ces.pages.setup.PageSetup;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Reporter;
import org.testng.annotations.*;

import java.net.MalformedURLException;
import java.net.URL;

@Listeners(ScreenshotOnFailureListenerEMW.class)
public class BaseTestEMW {
	private static final String ESCAPE_PROPERTY = "org.uncommons.reportng.escape-output";
	protected WebDriver driver;

	/**
	 * Generic page actionsStatic
	 */
	public BasePageEMW basePageEMW;
	public PageEMWLogin pageEMWLogin;
	public PageEMWLogout pageEMWLogout;
	public PageSetup pageSetup;
	public PageOtherTasks pageOtherTasks;
	/**
	 * ** Products module
	 */
	//Company Products page module
	public PageCompanyProductsSummary pageCompanyProductsSummary;
	//Package Types page module
	public PagePackageTypes pagePackageTypes;
	/**
	 * ** Production module
	 */
	//Recipe page module
	public PageRecipeSummary pageRecipeSummary;
	public PageRecipe pageRecipe;
	/**
	 * ** Bid Master module
	 */
	//Bid Sheet page module
	public PageBidSheet pageBidSheet;
	//Vendor Bids page module
	public PageVendorBids pageVendorBids;
	//Bid Analysis page module
	public PageBidAnalysis pageBidAnalysis;

	@Parameters({"IP", "BROWSER"})
	@BeforeClass(alwaysRun = true)
	//for now, the default machine is on localhost, and the default browser is firefox
	//create testNG xml suite file to test in other browsers
	public void beforeClass(@Optional("localhost") String ip, @Optional("CHROME") String browser) throws MalformedURLException {
		System.setProperty(ESCAPE_PROPERTY, "false");
		DesiredCapabilities capabilities = null;
		switch (browser.toUpperCase()) {
			case "FIREFOX":
				capabilities = DesiredCapabilities.firefox();
//                //run through a proxy
//                String PROXY = "5.101.130.95:8080";
//                org.openqa.selenium.Proxy proxy = new org.openqa.selenium.Proxy();
//                proxy
//                        //.setHttpProxy(PROXY)
//                        //.setFtpProxy(PROXY)
//                        .setSslProxy(PROXY);
//                //set proxy capability
//                capabilities.setCapability(CapabilityType.PROXY, proxy);
				capabilities.setJavascriptEnabled(true);
				capabilities.setPlatform(Platform.ANY);
				break;
			case "INTERNETEXPLORER":
				capabilities = DesiredCapabilities.internetExplorer();
				capabilities.setJavascriptEnabled(true);
				capabilities.setPlatform(Platform.ANY);
				break;
			case "CHROME":
				capabilities = DesiredCapabilities.chrome();
				capabilities.setJavascriptEnabled(true);
				capabilities.setPlatform(Platform.ANY);
				break;
			case "SAFARI":
				capabilities = DesiredCapabilities.safari();
				capabilities.setJavascriptEnabled(true);
				capabilities.setPlatform(Platform.ANY);
				break;
			case "PHANTOMJS":
				capabilities = DesiredCapabilities.phantomjs();
				capabilities.setJavascriptEnabled(true);
				capabilities.setPlatform(Platform.ANY);
				capabilities.setCapability("takesScreenshot", true);
				break;
		}

		DriverOfActiveThread.setDriver(new Augmenter().augment(new RemoteWebDriver(new URL("http://" + ip + ":5555/wd/hub"), capabilities)));
		this.driver = DriverOfActiveThread.getDriver();

		/**
		 * Generic page actionsStatic
		 */
		this.basePageEMW = PageFactory.initElements(this.driver, BasePageEMW.class);
		this.pageEMWLogin = PageFactory.initElements(this.driver, PageEMWLogin.class);
		this.pageEMWLogout = PageFactory.initElements(this.driver, PageEMWLogout.class);
		this.pageSetup = PageFactory.initElements(this.driver, PageSetup.class);
		this.pageOtherTasks = PageFactory.initElements(this.driver, PageOtherTasks.class);

		/**
		 *
		 *** Products module
		 *
		 */
		//Company Products page module
		this.pageCompanyProductsSummary = PageFactory.initElements(this.driver, PageCompanyProductsSummary.class);

		//Package Types page module
		this.pagePackageTypes = PageFactory.initElements(this.driver, PagePackageTypes.class);

		/**
		 *
		 *** Production module
		 *
		 */
		//Recipe page module
		this.pageRecipeSummary = PageFactory.initElements(this.driver, PageRecipeSummary.class);
		this.pageRecipe = PageFactory.initElements(this.driver, PageRecipe.class);

		/**
		 *
		 *** Bid Master module
		 *
		 */
		//Bid Sheet page module
		this.pageBidSheet = PageFactory.initElements(this.driver, PageBidSheet.class);

		//Vendor Bids page module
		this.pageVendorBids = PageFactory.initElements(this.driver, PageVendorBids.class);

		//Bid Analysis page module
		this.pageBidAnalysis = PageFactory.initElements(this.driver, PageBidAnalysis.class);

		/**
		 ***  Launch testing Browser before each test
		 */
		basePageEMW.launchBrowser();
		Reporter.log("<p><style type=\"text/css\">");
		Reporter.log(".tg {border-collapse:collapse;border-spacing:0;border-color:#999;table-layout:fixed;width:830px;}");
		Reporter.log(".tg td{font-family:Verdana;font-size:9px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;word-wrap:break-word;border-color:#999;color:#444;background-color:#F7FDFA;text-align:left;}");
		Reporter.log(".tg th{font-family:Verdana;font-size:10px;font-weight:bold;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}");
		Reporter.log("</style><table class=\"tg\"><tbody><col width=\"70px\"> <col width=\"260px\"> <col width=\"200px\"><col width=\"300px\"><tr><th>JIRA ID</th><th>Test Description</th><th>Test Name</th><th>Test Class</th></tr>");
	}

//	@BeforeClass(alwaysRun = true)
//	public void beforeClass() throws Exception {
//		this.driver = DriverOfActiveThread.getDriver();
//		System.out.println("---------------before class-----------------");
//		System.out.println("driver = " + driver);
//		System.out.println("this.driver = " + this.driver);
//	}

	@AfterClass(alwaysRun = true)
	public void aftetClass() throws Exception {
		Reporter.log("</tbody></table></p><br>");
		Thread.sleep(1000);
		this.driver = DriverOfActiveThread.getDriver();
		this.driver.quit();
	}
}
