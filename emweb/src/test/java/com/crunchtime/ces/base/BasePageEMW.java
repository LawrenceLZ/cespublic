package com.crunchtime.ces.base;

import com.crunchtime.ces.helper.Waiter;
import com.crunchtime.ces.pages.login.PageEMWLogin;
import com.crunchtime.ces.pages.otherTasks.PageOtherTasks;
import com.crunchtime.ces.pages.setup.PageSetup;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BasePageEMW {
	private final WebDriver driver;

	public BasePageEMW(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "a[title='Setup']") private WebElement setupNaviIcon;
	@FindBy(css = "a[title='Other Tasks']") private WebElement otherTasksNaviIcon;
	@FindBy(css = "table[class='menuTable']") private WebElement detailContentTable;

	public void launchBrowser() {
		driver.manage().window().maximize();
	}

	public PageEMWLogin openTestSite(String siteName) {
		driver.get("http://" + siteName);
		if (!"Enterprise Manager Web".equals(driver.getTitle())) {
			throw new IllegalStateException("Unable to open EMW login page.");
		}
		return new PageEMWLogin(driver);
	}

	public PageSetup navigateToPageSetup() throws Exception {
		setupNaviIcon.click();
		Waiter.waitForElementToBeVisible(driver, detailContentTable);
		if (!driver.getCurrentUrl().contains("method=setup")) {
			throw new IllegalStateException("Unable to open EMW > Setup page.");
		}
		return new PageSetup(driver);
	}

	public PageOtherTasks navigateToPageOtherTasks() throws Exception {
		otherTasksNaviIcon.click();
		Waiter.waitForElementToBeVisible(driver, detailContentTable);
		if (!driver.getCurrentUrl().contains("method=otherTasks")) {
			throw new IllegalStateException("Unable to open EMW > Other Tasks page.");
		}
		return new PageOtherTasks(driver);
	}
}