package com.crunchtime.ces.base;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BaseDBConnectionEMW {
	private static final String DB_DRIVER = "oracle.jdbc.driver.OracleDriver";
	private static final String TNS_DIR = System.getProperty("user.dir") + "\\src\\test\\resources\\";

	protected static Connection getConnection(String dbSite) {
		System.setProperty("oracle.net.tns_admin", TNS_DIR);
		Connection dbConnection = null;
		String[] dbSiteSplit = dbSite.toUpperCase().split("/");
		String DB_USER = dbSiteSplit[0];
		String dbSiteSecondPart = dbSiteSplit[1];
		String[] dbSiteSplitTwo = dbSiteSecondPart.split("@");
		String DB_PASSWORD = dbSiteSplitTwo[0];
		String DB_SOURCE = dbSiteSplitTwo[1];
		String DB_CONNECTION = "jdbc:oracle:thin:@" + DB_SOURCE;
		try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			return dbConnection;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return dbConnection;
	}
}