package com.crunchtime.ces.test.production.recipe;

import com.crunchtime.ces.base.BaseTestEMW;
import com.crunchtime.ces.database.production.recipe.DBStmtRecipe;
import com.crunchtime.ces.helper.ActionsEMW;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class TC_StressTestToCreateRecipesSimple extends BaseTestEMW {
	private static final String dbConnectionString = "ram/ram@CTQA24010";
	private static final String testSiteName = "ram-em.net-chef.local";
	private static final String userId = "ram";
	private static final String userPwd = "ram";
	private static final String testLocationName = "RAM INTERNATIONAL HOLDING COMPANY-123";
	private static final String testRecipeNamePrefix = "AUTOLoadTest L-0";
	private static final String testCategoryDropdownValue = "BEVERAGE - JUICE - JUICE";
	private static final String testBatchSize = "2";
	private static final String testBatchSizeDropdownValue = "1 / 500";
	private static final int totalNumOfAddCompPagesToLoopForFirstTime = 1;
	private static final int totalNumOfAddCompPagesToLoopForSecondTime = 1;
	private static final int testNumOfComponentsToAddOnEachPage = 1;
	private static final int testLoadByNumOfRecipesToCreate = 1; //change this to increase the loop/load

	@BeforeMethod(alwaysRun = true)
	public void setup() throws Exception {
		basePageEMW.openTestSite(testSiteName);
		pageEMWLogin.loginEMW(userId, userPwd, testLocationName);
		pageSetup.navigateToRecipeSummaryScreen();
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown() throws Exception {
		DBStmtRecipe.deleteRecipes(dbConnectionString, testRecipeNamePrefix);
		pageEMWLogout.logoutEMW();
	}

	@Test
	public void stressTestToCreateRecipes() throws Exception {
		for (int x = 0; x < testLoadByNumOfRecipesToCreate; x++) {
			//Mark down the start time of each loop
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss:SS");
			Calendar loopStartCalendar = Calendar.getInstance();
			String loopStartTime = simpleDateFormat.format(loopStartCalendar.getTime());
			Date loopStartTimeStamp = simpleDateFormat.parse(loopStartTime);

			pageRecipe.recipeSummScrnAddBtn.click();
			pageRecipe.waitForRecipeDetailScreenReturn();

			/**
			 * Recipe tab: fill in ONLY required fields
			 */
			//stage values for required fields on Recipe tab/screen
			String timeStamp = ActionsEMW.setDateBasedOnToday("MM-dd-yy-HH-mm-ss", 0);
			String recipeName = testRecipeNamePrefix + x + " " + timeStamp;
			//assign staged values to required fields
			pageRecipe.recipeDtlScrnRcpNameInputTextbox.clear();
			pageRecipe.recipeDtlScrnRcpNameInputTextbox.sendKeys(recipeName);
			/**
			 *
			 * recipe dropdown element differs between IE and other browsers,
			 * so different actions to be taken
			 *
			 */
			String testBrowserName = ActionsEMW.getBrowserName(driver);
			if (testBrowserName.contains("InternetExplorer")) {
				pageRecipe.recipeDtlScrnCategoryDropdownIE.click();
				ActionsEMW.setSelectDropdownByValue(pageRecipe.recipeDtlScrnCategoryDropdownIE, testCategoryDropdownValue);
			} else {
				ActionsEMW.setComboBoxDropdownByValue(driver, pageRecipe.recipeDtlScrnCategoryDropdown, testCategoryDropdownValue);
			}
			pageRecipe.recipeDtlScrnBatchSizeInputTextbox.clear();
			pageRecipe.recipeDtlScrnBatchSizeInputTextbox.sendKeys(testBatchSize + Keys.TAB);

			/**
			 *
			 * Problematic area to push the batch unit dropdown,
			 * so go to PLU input field first, and then go to
			 * batch unit dropdown
			 *
			 */
			Thread.sleep(1000);
			pageRecipe.recipeDtlScrnPluInputTextbox.click();
			Thread.sleep(5000);
			/**
			 *
			 * recipe dropdown element differs between IE and other browsers,
			 * so different actions to be taken
			 *
			 */
			if (testBrowserName.contains("InternetExplorer")) {
				pageRecipe.recipeDtlScrnBatchUnitDropdownIE.click();
				ActionsEMW.setSelectDropdownByValue(pageRecipe.recipeDtlScrnBatchUnitDropdownIE, testBatchSizeDropdownValue);
			} else {
				ActionsEMW.setComboBoxDropdownByValue(driver, pageRecipe.recipeDtlScrnBatchUnitDropdown, testBatchSizeDropdownValue);
			}
			pageRecipe.waitForRecipeTabAllFieldsValidationToPass();

			/**
			 * Component tab: add 1-2 components from each page on component popup window (to create load)
			 */
			pageRecipe.recipeComponentLink.click();
			pageRecipe.waitForRecipeComponentScreenReturn();
			pageRecipe.recipeComponentScrnAddBtn.click();
			pageRecipe.waitForAddRcpComponentsPopupReturn();

			//get total page count for Add Recipe Components popup window
//        int totalPageCnt = pageRecipe.getTotalPageCountForAddRcpComponentsPopup();

			if (totalNumOfAddCompPagesToLoopForFirstTime > 10) {
				for (int i = 1; i < 10; i++) {
					Random rand = new Random();
					String randomRow = String.valueOf(rand.nextInt(7) + 1);
					pageRecipe.waitForAddRcpComponentsPopupReturn();
					Thread.sleep(1000);

					pageRecipe.recipeAddRcpComponentsPopupGrid.findElement(By.cssSelector("tr:nth-child(" + randomRow + ") input[type='checkbox']")).click();
					Thread.sleep(1000);
					String addedRow = pageRecipe.recipeAddRcpComponentsPopupGrid.findElement(By.cssSelector("tr:nth-child(" + randomRow + ") td:nth-child(3)")).getText();
					pageRecipe.recipeAddRcpComponentsPopupAddBtn.click();

					String idString = pageRecipe.recipeComponentScrnContainer.getAttribute("id");
					new WebDriverWait(driver, 120, 1000).until(
							ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("div[id='" + idString + "'] div tbody"), addedRow)
					);
					Thread.sleep(1000);

					pageRecipe.recipeAddRcpComponentsPopupPaginateByPage(String.valueOf(i + 1));
					new WebDriverWait(driver, 120, 1000).until(
							ExpectedConditions.not(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("table[id='addComponentsTable'] tbody tr:nth-child(" + randomRow + ")"), addedRow))
					);
					pageRecipe.waitForAddRcpComponentsPopupReturn();
				}
			} else if (totalNumOfAddCompPagesToLoopForFirstTime <= 10) {
				for (int i = 0; i < testNumOfComponentsToAddOnEachPage; i++) {
					Random rand = new Random();
					String randomRow = String.valueOf(rand.nextInt(7) + 1);
					pageRecipe.waitForAddRcpComponentsPopupReturn();
					Thread.sleep(1000);
					pageRecipe.recipeAddRcpComponentsPopupGrid.findElement(By.cssSelector("tr:nth-child(" + randomRow + ") input[type='checkbox']")).click();
					Thread.sleep(1000);
					String addedRow = pageRecipe.recipeAddRcpComponentsPopupGrid.findElement(By.cssSelector("tr:nth-child(" + randomRow + ") td:nth-child(3)")).getText();
					pageRecipe.recipeAddRcpComponentsPopupAddBtn.click();

					String idString = pageRecipe.recipeComponentScrnContainer.getAttribute("id");
					new WebDriverWait(driver, 120, 1000).until(
							ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("div[id='" + idString + "'] div tbody"), addedRow)
					);
					Thread.sleep(1000);
					pageRecipe.recipeAddRcpComponentsPopupGrid.findElement(By.cssSelector("tr:nth-child(" + randomRow + ") input[type='checkbox']")).click();
					Thread.sleep(1000);
				}
			}

			//Add and Close Add Recipe Components popup window
			pageRecipe.recipeAddRcpComponentsPopupAddAndCloseBtn.click();
			Thread.sleep(1000);
			pageRecipe.waitForRecipeComponentScreenReturn();

			//get row count on Components grid and assign a value to Quantity field
			int compGridRowCount = ActionsEMW.getEMWGridRowCnt(pageRecipe.recipeComponentScrnGrid);
			//System.out.println(compGridRowCount);
			for (int i = 1; i <= compGridRowCount; i++) {
				pageRecipe.waitForRecipeComponentScreenReturn();
				pageRecipe.recipeComponentScrnGrid.findElement(By.cssSelector("tr:nth-child(" + i + ") input[id='quantity']")).click();
				pageRecipe.recipeComponentScrnGrid.findElement(By.cssSelector("tr:nth-child(" + i + ") input[id='quantity']")).sendKeys(Keys.BACK_SPACE + "1" + Keys.RETURN);
			}

			pageRecipe.recipeSaveAndCloseBtn.click();

			boolean alertExists = ActionsEMW.isAlertPresent(driver);
			if (alertExists) {
				driver.switchTo().alert().dismiss();
				driver.switchTo().defaultContent();
			}
			Waiter.waitForElementToDisappear(driver, pageRecipeSummary.recipeSummScrnSpinnerIcon);

			//re-open recipe detail screen to add more components
			pageRecipe.recipeSummScrnProdNameFilterInput.clear();
			pageRecipe.recipeSummScrnProdNameFilterInput.sendKeys(recipeName + Keys.RETURN);
			Waiter.waitForElementToDisappear(driver, pageRecipeSummary.recipeSummScrnSpinnerIcon);
			new WebDriverWait(driver, 120, 1000).until(
					ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("div[class='recipeIndexContainer'] tbody tr:first-child a[class*='editLink']"), recipeName)
			);

			pageRecipe.recipeSummScrnGrid.findElement(By.cssSelector("tr:first-child a[class*='editLink']")).click();
			pageRecipe.waitForRecipeDetailScreenReturn();
			Thread.sleep(2000);
			pageRecipe.recipeComponentLink.click();
			Thread.sleep(2000);
			pageRecipe.waitForRecipeComponentScreenReturn();
			pageRecipe.recipeComponentScrnAddBtn.click();
			pageRecipe.waitForAddRcpComponentsPopupReturn();

			if (totalNumOfAddCompPagesToLoopForSecondTime > 10) {
				for (int i = 6; i < totalNumOfAddCompPagesToLoopForSecondTime; i++) {
					//Go to page 7 first to add components
					String firstRowOnPageOne = pageRecipe.recipeAddRcpComponentsPopupGrid.findElement(By.cssSelector("tr:nth-child(1) td:nth-child(3)")).getText();
					Thread.sleep(1000);
					int pageNumToNavigate = i + 1;
					System.out.println("Second time edit start page on Add Components popup is " + pageNumToNavigate);
					Reporter.log("Second time edit start page on Add Components popup is " + pageNumToNavigate);
					pageRecipe.recipeAddRcpComponentsPopupPaginateByPage(String.valueOf(pageNumToNavigate));
					new WebDriverWait(driver, 120, 1000).until(
							ExpectedConditions.not(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("table[id='addComponentsTable'] tbody"), firstRowOnPageOne))
					);
					pageRecipe.waitForAddRcpComponentsPopupReturn();

					Random rand = new Random();
					String randomRow = String.valueOf(rand.nextInt(7) + 1);
					pageRecipe.waitForAddRcpComponentsPopupReturn();
					Thread.sleep(1000);
					pageRecipe.waitForAddRcpComponentsPopupReturn();

					pageRecipe.recipeAddRcpComponentsPopupGrid.findElement(By.cssSelector("tr:nth-child(" + randomRow + ") input[type='checkbox']")).click();
					Thread.sleep(1000);
					String addedRow = pageRecipe.recipeAddRcpComponentsPopupGrid.findElement(By.cssSelector("tr:nth-child(" + randomRow + ") td:nth-child(3)")).getText();
					pageRecipe.recipeAddRcpComponentsPopupAddBtn.click();

					String idString = pageRecipe.recipeComponentScrnContainer.getAttribute("id");
					new WebDriverWait(driver, 120, 1000).until(
							ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("div[id='" + idString + "'] div tbody"), addedRow)
					);
					Thread.sleep(1000);
					pageRecipe.recipeAddRcpComponentsPopupGrid.findElement(By.cssSelector("tr:nth-child(" + randomRow + ") input[type='checkbox']")).click();
					Thread.sleep(1000);
				}
			} else if (totalNumOfAddCompPagesToLoopForSecondTime <= 10) {
				//Go to page 7 first to add components
				String firstRowOnPageOne = pageRecipe.recipeAddRcpComponentsPopupGrid.findElement(By.cssSelector("tr:nth-child(1) td:nth-child(3)")).getText();
				Thread.sleep(1000);
				int startPageOnPageSeven = 7;
				System.out.println("Second time edit start page on Add Components popup is " + startPageOnPageSeven);
				Reporter.log("Second time edit start page on Add Components popup is " + startPageOnPageSeven);
				pageRecipe.recipeAddRcpComponentsPopupPaginateByPage(String.valueOf(startPageOnPageSeven));
				new WebDriverWait(driver, 120, 1000).until(
						ExpectedConditions.not(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("table[id='addComponentsTable'] tbody"), firstRowOnPageOne))
				);
				pageRecipe.waitForAddRcpComponentsPopupReturn();

				for (int i = 0; i < testNumOfComponentsToAddOnEachPage; i++) {
					Random rand = new Random();
					String randomRow = String.valueOf(rand.nextInt(7) + 1);
					pageRecipe.waitForAddRcpComponentsPopupReturn();
					Thread.sleep(1000);
					pageRecipe.recipeAddRcpComponentsPopupGrid.findElement(By.cssSelector("tr:nth-child(" + randomRow + ") input[type='checkbox']")).click();
					Thread.sleep(1000);
					String addedRow = pageRecipe.recipeAddRcpComponentsPopupGrid.findElement(By.cssSelector("tr:nth-child(" + randomRow + ") td:nth-child(3)")).getText();
					pageRecipe.recipeAddRcpComponentsPopupAddBtn.click();

					String idString = pageRecipe.recipeComponentScrnContainer.getAttribute("id");
					new WebDriverWait(driver, 120, 1000).until(
							ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("div[id='" + idString + "'] div tbody"), addedRow)
					);
					Thread.sleep(1000);
					pageRecipe.recipeAddRcpComponentsPopupGrid.findElement(By.cssSelector("tr:nth-child(" + randomRow + ") input[type='checkbox']")).click();
					Thread.sleep(1000);
				}
			}

			//Add and Close Add Recipe Components popup window
			pageRecipe.recipeAddRcpComponentsPopupAddAndCloseBtn.click();
			pageRecipe.waitForRecipeComponentScreenReturn();

			//get row count on Components grid and assign a value to Quantity field
			int secCompGridRowCount = ActionsEMW.getEMWGridRowCnt(pageRecipe.recipeComponentScrnGrid);
			for (int i = 1; i <= secCompGridRowCount; i++) {
				pageRecipe.waitForRecipeComponentScreenReturn();
				pageRecipe.recipeComponentScrnGrid.findElement(By.cssSelector("tr:nth-child(" + i + ") input[id='quantity']")).click();
				pageRecipe.recipeComponentScrnGrid.findElement(By.cssSelector("tr:nth-child(" + i + ") input[id='quantity']")).sendKeys(Keys.BACK_SPACE + "1" + Keys.RETURN);
			}

			pageRecipe.recipeSaveAndCloseBtn.click();
			boolean secAlertExists = ActionsEMW.isAlertPresent(driver);
			if (secAlertExists) {
				driver.switchTo().alert().dismiss();
				driver.switchTo().defaultContent();
			}

			Waiter.waitForElementToDisappear(driver, pageRecipeSummary.recipeSummScrnSpinnerIcon);

			//go to main page and then back to recipe summary screen
			basePageEMW.navigateToPageSetup();
			pageSetup.navigateToRecipeSummaryScreen();

			//Mark down the end time of each loop
			Calendar loopEndCalendar = Calendar.getInstance();
			String loopEndTime = simpleDateFormat.format(loopEndCalendar.getTime());
			Date loopEndTimeStamp = simpleDateFormat.parse(loopEndTime);
			long loopUsedTime = loopEndTimeStamp.getTime() - loopStartTimeStamp.getTime();

			System.out.println("Recipe is created for Name = " + recipeName +
					", Start Time = " + loopStartTime + ", End Time = " + loopEndTime + ", Duration (in ms) = " + loopUsedTime);
			Reporter.log("Recipe is created for Name = " + recipeName +
					", Start Time = " + loopStartTime + ", End Time = " + loopEndTime + ", Duration (in ms) = " + loopUsedTime);
		}
	}
}