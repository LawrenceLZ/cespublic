package com.crunchtime.ces.test.bidMaster.vendorBids;

import com.crunchtime.ces.base.BaseTestEMW;
import com.crunchtime.ces.database.bidMaster.vendorBids.DBStmtVendorBids;
import com.crunchtime.ces.helper.ActionsEMW;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class TC_VendorBidsAdditionalMarketsScenarios extends BaseTestEMW {
	private static final String DB_CONNECTION_STRING = "AUTO/AUTO@CTAUTO24006";
	private static final String TEST_SITE_NAME = "auto-em.net-chef.local";
	private static final String USEER_ID = "VBIDS";
	private static final String USER_PWD = "VBIDS";
	private static final String TEST_LOCATION_NAME = "Bubba Gump Shrimp Co";
	private static final String TEST_BID_SHEET_NAME = "TestVendorBids";
	private static final String TEST_VENDOR_NAME = "TestVendorBids Ven 1";
	private static final String TEST_MARKET_NAME = "TestVendorBids";
	private static final String TEST_ADDITIONAL_MKT_NAME = "TestVendorBidsAddiMkt";
	private static final String TEST_COMPANYPRODUCT_NUMBER1 = "VBID011";
	private static final String TEST_COMPANYPRODUCT_NUMBER2 = "VBID012";
	private static final String TEST_VBID_UNIT = "CA";
	private static final String TEST_SEC_VBID_UNIT = "Oz";
	private static final String TEST_VBID_TAX = "EXC";

	@Parameters({"SMOKE"})
	@BeforeMethod(alwaysRun = true)
	public void login(@Optional(TEST_SITE_NAME) String siteName) throws Exception {
		basePageEMW.openTestSite(siteName);
		pageEMWLogin.loginEMW(USEER_ID, USER_PWD, TEST_LOCATION_NAME);
		//Navigate to Vendor Bids detail screen with testing Bid Sheet/Vendor/Market combination
		basePageEMW.navigateToPageOtherTasks();
		pageOtherTasks.navigateToVendorBidsScreen();

		pageVendorBids.bidSheetsBtn.click();
		pageVendorBids.waitForSelectBidSheetsPopupReturn();
		//Select the testing Bid Sheet
		pageVendorBids.selectBidSheetsPopupSelectAllCheckbox.click(); //first to deselect all the bid sheets
		pageVendorBids.selectBidSheetsPopupFilterIcon.click();
		pageVendorBids.filterBidSheetTextbox.clear();
		pageVendorBids.filterBidSheetTextbox.sendKeys(TEST_BID_SHEET_NAME);
		pageVendorBids.filterBidSheetApplyBtn.click();
		pageVendorBids.waitForSelectBidSheetsPopupReturn(); //wait for filtered results to return
		pageVendorBids.selectBidSheetsPopupSelectAllCheckbox.click(); //now to select the filtered bid sheet(s)
		pageVendorBids.selectBidSheetsPopupApplyBth.click();
		pageVendorBids.waitForVendorInputCombobxEnable();
		//define vendor and market and then Retrieve
		pageVendorBids.vendorInputCombobox.click();
		pageVendorBids.vendorInputComboboxTrigger.click();
		Thread.sleep(1000);
		ActionsEMW.setComboBoxDropdownByValue(driver, pageVendorBids.vendorInputCombobox, TEST_VENDOR_NAME);
		Thread.sleep(1000);
		pageVendorBids.marketInputCombobox.click();
		pageVendorBids.marketInputCombobox.clear();
		pageVendorBids.marketInputCombobox.sendKeys(TEST_MARKET_NAME);
		pageVendorBids.marketInputCombobox.sendKeys("\t");
		boolean retrieveEnabled;
		do {
			Waiter.waitForOneSecond();
			retrieveEnabled = pageVendorBids.retrieveBtn.getAttribute("class").contains("disabled");
			int timer = 0;
			timer++;
			if (timer == 120) break;
		} while (retrieveEnabled);
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		boolean closeBtnExist = ActionsEMW.isElementPresent(pageVendorBids.secondaryBidsPopupCloseBtn);
		if (closeBtnExist) pageVendorBids.secondaryBidsPopupCloseBtn.click();
		pageEMWLogout.logoutEMW();
	}

	/**
	 * Test Case Scenario,
	 * Select additional markets, Create a current Primary Vendor Bid with end date = Bid Sheet End Date
	 */
	@Test
	public void selectAdditionalMarketToEditCurrentPrimaryVendorBidWithEndDateEqualsBidSheetEndDate() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-1427", "Select additional markets, Create a current Primary Vendor Bid with end date = Bid Sheet End Date");
		ITestResult result = Reporter.getCurrentTestResult();

		//set effective date = current date
		String effDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 0);
		pageVendorBids.effectiveDatePicker.clear();
		pageVendorBids.effectiveDatePicker.sendKeys(effDate);
		pageVendorBids.effectiveDatePicker.sendKeys("\t");

		//open Additional Markets popup to add the additional testAdditionalMarketName
		pageVendorBids.waitForAdditionalMarketsBtnEnable();
		pageVendorBids.additionalMarketsBtn.click();
		pageVendorBids.waitForAdditionalMarketsPopupReturn();
		pageVendorBids.selectAdditinalMarketByName(TEST_ADDITIONAL_MKT_NAME, "Y");
		//deselect Include Split and SUb checkbox to ONLY include primary vendor bid
		pageVendorBids.editAdditionalMarketsPopupIncludeSplitSubCheckbox("N");
		pageVendorBids.additionalMarketsApplyBtn.click();
		pageVendorBids.waitForRetrieveBtnEnable();
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();

		//Filter testCompanyProductNumber1, create a new bid with End Date = End Bid Sheet
		pageVendorBids.vendorBidsFilterIcon.click();
		pageVendorBids.filterProductNumberTextbox.click();
		pageVendorBids.filterProductNumberTextbox.sendKeys(TEST_COMPANYPRODUCT_NUMBER1);
		pageVendorBids.vendorBidsFilterApplyBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		pageVendorBids.vendorBidsGridCellItem(1, "Product #").click();
		pageVendorBids.waitFoVendorBidsEditFormReturn();

		//stage the date to be used for Edit
		String timeStamp = ActionsEMW.setDateBasedOnToday("MM-dd-yy-HH-mm-ss", 0);
		String editVBidNumber = "Auto-" + TEST_COMPANYPRODUCT_NUMBER1 + "_" + timeStamp;
		String editVBidUnit = TEST_VBID_UNIT;
		String editVBidBrand = "Brand_" + timeStamp;
		String editVBidConv = "5.0000";
		Random rand = new Random();
		double priVBidPrice = rand.nextInt(15) + rand.nextDouble();
		String editVBidPrice = String.format("%.4f", priVBidPrice);
		//String editVBidSplit = "Y";
		String editVBidTax = TEST_VBID_TAX;
		DBStmtVendorBids.getBidSheetEndDate(DB_CONNECTION_STRING, TEST_BID_SHEET_NAME);
		String[] bidSheetEndDate = ActionsEMW.readSqlResults("DBStmtVendorBids", "Find_Bid_Sheet_End_Date");
		String editVBidEndDate = bidSheetEndDate[0];
		String editVBidRefNum = "Ref" + timeStamp;
		int priVBidLineNum = rand.nextInt(10);
		String editVBidLineNum = String.valueOf(priVBidLineNum);
		String editVBidExpDate = bidSheetEndDate[0];

		//assign staged date to each primary Vendor Bid field
		pageVendorBids.bidEditFormVendorProdNumberTextbox.clear();
		pageVendorBids.bidEditFormVendorProdNumberTextbox.sendKeys(editVBidNumber + Keys.TAB);
		pageVendorBids.bidEditFormVendorProdBrandTextbox.clear();
		pageVendorBids.bidEditFormVendorProdBrandTextbox.sendKeys(editVBidBrand + Keys.TAB);
		pageVendorBids.bidEditFormVendorProdUnitComboboxInput.clear();
		pageVendorBids.bidEditFormVendorProdUnitComboboxTriger.click();
		ActionsEMW.setComboBoxDropdownByValue(driver, pageVendorBids.bidEditFormVendorProdUnitComboboxInput, editVBidUnit);
		pageVendorBids.bidEditFormConversionTextbox.clear();
		pageVendorBids.bidEditFormConversionTextbox.sendKeys(editVBidConv + Keys.TAB);
		pageVendorBids.bidEditFormPriceTextbox.clear();
		pageVendorBids.bidEditFormPriceTextbox.sendKeys(editVBidPrice + Keys.TAB);
		pageVendorBids.bidEditFormVendorProdTaxComboboxInput.clear();
		pageVendorBids.bidEditFormVendorProdTaxComboboxTrigger.click();
		ActionsEMW.setComboBoxDropdownByValue(driver, pageVendorBids.bidEditFormVendorProdTaxComboboxInput, editVBidTax);
		pageVendorBids.bidEditFormEndDateTextbox.clear();
		pageVendorBids.bidEditFormEndDateTextbox.sendKeys(editVBidEndDate + Keys.TAB);
		pageVendorBids.bidEditFormReferenceNumberTextbox.clear();
		pageVendorBids.bidEditFormReferenceNumberTextbox.sendKeys(editVBidRefNum + Keys.TAB);
		pageVendorBids.bidEditFormLineNumberTextbox.clear();
		pageVendorBids.bidEditFormLineNumberTextbox.sendKeys(editVBidLineNum + Keys.TAB);
		pageVendorBids.bidEditFormExpirationDateTextbox.clear();
		pageVendorBids.bidEditFormExpirationDateTextbox.sendKeys(editVBidExpDate);
		pageVendorBids.bidEditFormCollapseIcon.click();
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.visibilityOf(pageVendorBids.bidEditFormExpandIcon)
		);
		Thread.sleep(1000);

		//run validation checkpoints
		for (int i = 0; i < 2; i++) {
			String validationMarket = null;
			if (i == 0) {
				validationMarket = TEST_MARKET_NAME;
			}
			if (i == 1) {
				validationMarket = TEST_ADDITIONAL_MKT_NAME;
			}
			//Validate vendor product number
			String checkVBidNum = pageVendorBids.vendorBidsGridCellItem(1, "vendor #").getText().trim();
			Assert.assertEquals(
					checkVBidNum, editVBidNumber,
					result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER1 +
							" for Market = " + validationMarket + ": FAILED, expected vendor product number is " + editVBidNumber
			);

			//Validate vendor bid product brand
			String checkVBidBrand = pageVendorBids.vendorBidsGridCellItem(1, "Brand").getText().trim();
			Assert.assertEquals(
					checkVBidBrand, editVBidBrand,
					result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER1 +
							" for Market = " + validationMarket + ": FAILED, expected vendor product brand is " + editVBidBrand
			);

			//Validate vendor bid product unit
			String checkVBidUnit = pageVendorBids.vendorBidsGridCellItem(1, "Vendor Unit").getText().trim();
			DBStmtVendorBids.getVendorBid(DB_CONNECTION_STRING, "ven_unit", TEST_COMPANYPRODUCT_NUMBER1, TEST_VENDOR_NAME, validationMarket, effDate);
			String[] dbVBidUnit = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Vendor_Bid");
			Assert.assertEquals(
					checkVBidUnit, dbVBidUnit[0],
					result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER1 +
							" for Market = " + validationMarket + ": FAILED, expected vendor product unit is " + dbVBidUnit[0]
			);

			//Validate vendor bid conversion
			String checkVBidConv = pageVendorBids.vendorBidsGridCellItem(1, "Conv.").getText().trim();
			DBStmtVendorBids.getVendorBid(DB_CONNECTION_STRING, "ven_conversion", TEST_COMPANYPRODUCT_NUMBER1, TEST_VENDOR_NAME, validationMarket, effDate);
			String[] dbVBidConv = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Vendor_Bid");
			Assert.assertEquals(
					checkVBidConv, dbVBidConv[0],
					result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER1 +
							" for Market = " + validationMarket + ": FAILED, expected vendor product conversion is " + dbVBidConv[0]
			);

			//Validate vendor bid price
			String checkVBidPrice = pageVendorBids.vendorBidsGridCellItem(1, "Price").getText().trim();
			DBStmtVendorBids.getVendorBid(DB_CONNECTION_STRING, "ven_bid_price", TEST_COMPANYPRODUCT_NUMBER1, TEST_VENDOR_NAME, validationMarket, effDate);
			String[] dbVBidPrice = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Vendor_Bid");
			Assert.assertEquals(
					checkVBidPrice, dbVBidPrice[0],
					result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER1 +
							" for Market = " + validationMarket + ": FAILED, expected vendor product price is " + dbVBidPrice[0]
			);

			//Validate vendor bid tax code
			String checkVBidTax = pageVendorBids.vendorBidsGridCellItem(1, "Tax Code").getText().trim();
			DBStmtVendorBids.getVendorBid(DB_CONNECTION_STRING, "ven_tax_code", TEST_COMPANYPRODUCT_NUMBER1, TEST_VENDOR_NAME, validationMarket, effDate);
			String[] dbVBidTax = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Vendor_Bid");
			Assert.assertEquals(
					checkVBidTax, dbVBidTax[0],
					result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER1 +
							" for Market = " + validationMarket + ": FAILED, expected vendor product tax code is " + dbVBidTax[0]
			);

			//Validate vendor bid end date
			String updatedVBipEndDate = pageVendorBids.vendorBidsGridCellItem(1, "Bid End Date").getText().trim();
			//When Bid End Date = Bid Sheet End Date, set the Bid End Date on screen + 1 to match database value
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Date date = simpleDateFormat.parse(updatedVBipEndDate);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.add(Calendar.DATE, 1);
			String checkVBidEndDate = simpleDateFormat.format(calendar.getTime());
			DBStmtVendorBids.getVendorBid(DB_CONNECTION_STRING, "ven_end_Date", TEST_COMPANYPRODUCT_NUMBER1, TEST_VENDOR_NAME, validationMarket, effDate);
			String[] dbVBidEndDate = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Vendor_Bid");
			Assert.assertEquals(
					checkVBidEndDate, dbVBidEndDate[0],
					result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER1 +
							" for Market = " + validationMarket + ": FAILED, expected vendor product bid end date is " + dbVBidEndDate[0]
			);

			//Validate vendor bid reference number
			String checkVBidRefNum = pageVendorBids.vendorBidsGridCellItem(1, "Reference #").getText().trim();
			DBStmtVendorBids.getVendorBid(DB_CONNECTION_STRING, "ven_reference_number", TEST_COMPANYPRODUCT_NUMBER1, TEST_VENDOR_NAME, validationMarket, effDate);
			String[] dbVBidRefNum = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Vendor_Bid");
			Assert.assertEquals(
					checkVBidRefNum, dbVBidRefNum[0],
					result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER1 +
							" for Market = " + validationMarket + ": FAILED, expected vendor product reference number is " + dbVBidRefNum[0]
			);

			//Validate vendor bid reference line number
			String checkVBidRefLineNum = pageVendorBids.vendorBidsGridCellItem(1, "Reference Line #").getText().trim();
			DBStmtVendorBids.getVendorBid(DB_CONNECTION_STRING, "ven_line_number", TEST_COMPANYPRODUCT_NUMBER1, TEST_VENDOR_NAME, validationMarket, effDate);
			String[] dbVBidRefLineNum = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Vendor_Bid");
			Assert.assertEquals(
					checkVBidRefLineNum, dbVBidRefLineNum[0],
					result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER1 +
							" for Market = " + validationMarket + ": FAILED, expected vendor product reference line number is " + dbVBidRefLineNum[0]
			);

			//Validate vendor bid expiration date
			String checkVBidExpDate = pageVendorBids.vendorBidsGridCellItem(1, "Expiration Date").getText().trim();
			DBStmtVendorBids.getVendorBid(DB_CONNECTION_STRING, "ven_expiration_date", TEST_COMPANYPRODUCT_NUMBER1, TEST_VENDOR_NAME, validationMarket, effDate);
			String[] dbVBidExpDate = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Vendor_Bid");
			Assert.assertEquals(
					checkVBidExpDate, dbVBidExpDate[0],
					result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER1 +
							" for Market = " + validationMarket + " FAILED, expected vendor product reference line number is " + dbVBidExpDate[0]
			);
		}
	}

	/**
	 * Test Case Scenario,
	 * Select additional markets, Create a future Secondary Vendor Bid with end date = Bid Sheet End Date
	 */
	@Test
	public void selectAdditionalMarketToEditFutureSecondaryVendorBidWithEndDateEqualsBidSheetEndDate() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-1427", "Select additional markets, Create a future Secondary Vendor Bid with end date = Bid Sheet End Date");
		ITestResult result = Reporter.getCurrentTestResult();

		//set effective date to a future date = current date + 60
		String effDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 60);
		pageVendorBids.effectiveDatePicker.clear();
		pageVendorBids.effectiveDatePicker.sendKeys(effDate);
		pageVendorBids.effectiveDatePicker.sendKeys("\t");
		Waiter.waitForOneSecond();

		//open Additional Markets popup to add the additional testAdditionalMarketName
		pageVendorBids.waitForAdditionalMarketsBtnEnable();
		pageVendorBids.additionalMarketsBtn.click();
		pageVendorBids.waitForAdditionalMarketsPopupReturn();
		pageVendorBids.selectAdditinalMarketByName(TEST_ADDITIONAL_MKT_NAME, "Y");
		//deselect Include Split and SUb checkbox to ONLY include primary vendor bid
		pageVendorBids.editAdditionalMarketsPopupIncludeSplitSubCheckbox("Y");
		pageVendorBids.additionalMarketsApplyBtn.click();
		pageVendorBids.waitForRetrieveBtnEnable();
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();

		//Filter testCompanyProductNumber2, create a new bid with End Date = End Bid Sheet
		pageVendorBids.vendorBidsFilterIcon.click();
		pageVendorBids.filterProductNumberTextbox.click();
		pageVendorBids.filterProductNumberTextbox.sendKeys(TEST_COMPANYPRODUCT_NUMBER2);
		pageVendorBids.vendorBidsFilterApplyBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		pageVendorBids.vendorBidsGridCellItem(1, "Product #").click();
		pageVendorBids.waitFoVendorBidsEditFormReturn();

		//stage the date to be used for Edit
		String timeStamp = ActionsEMW.setDateBasedOnToday("MM-dd-yy-HH-mm-ss", 0);
		String editVBidNumber = "Auto-" + TEST_COMPANYPRODUCT_NUMBER2 + "_" + timeStamp;
		String editVBidUnit = TEST_VBID_UNIT;
		String editVBidBrand = "Brand_" + timeStamp;
		String editVBidConv = "8.0000";
		Random rand = new Random();
		double priVBidPrice = rand.nextInt(15) + rand.nextDouble();
		String editVBidPrice = String.format("%.4f", priVBidPrice);
		String editVBidSplit = "Y";
		String editVBidTax = TEST_VBID_TAX;
		DBStmtVendorBids.getBidSheetEndDate(DB_CONNECTION_STRING, TEST_BID_SHEET_NAME);
		String[] bidSheetEndDate = ActionsEMW.readSqlResults("DBStmtVendorBids", "Find_Bid_Sheet_End_Date");
		String editVBidEndDate = bidSheetEndDate[0];
		String editVBidRefNum = "Ref" + timeStamp;
		int priVBidLineNum = rand.nextInt(10);
		String editVBidLineNum = String.valueOf(priVBidLineNum);
		String editVBidExpDate = bidSheetEndDate[0];

		//assign staged date to each primary Vendor Bid field
		pageVendorBids.bidEditFormVendorProdNumberTextbox.clear();
		pageVendorBids.bidEditFormVendorProdNumberTextbox.sendKeys(editVBidNumber + Keys.TAB);
		pageVendorBids.bidEditFormVendorProdBrandTextbox.clear();
		pageVendorBids.bidEditFormVendorProdBrandTextbox.sendKeys(editVBidBrand + Keys.TAB);
		pageVendorBids.bidEditFormVendorProdUnitComboboxInput.clear();
		pageVendorBids.bidEditFormVendorProdUnitComboboxTriger.click();
		ActionsEMW.setComboBoxDropdownByValue(driver, pageVendorBids.bidEditFormVendorProdUnitComboboxInput, editVBidUnit);
		pageVendorBids.bidEditFormConversionTextbox.clear();
		pageVendorBids.bidEditFormConversionTextbox.sendKeys(editVBidConv + Keys.TAB);
		pageVendorBids.bidEditFormPriceTextbox.clear();
		pageVendorBids.bidEditFormPriceTextbox.sendKeys(editVBidPrice + Keys.TAB);
		pageVendorBids.bidEditFormVendorProdTaxComboboxInput.clear();
		pageVendorBids.bidEditFormVendorProdTaxComboboxTrigger.click();
		ActionsEMW.setComboBoxDropdownByValue(driver, pageVendorBids.bidEditFormVendorProdTaxComboboxInput, editVBidTax);
		pageVendorBids.bidEditFormEndDateTextbox.clear();
		pageVendorBids.bidEditFormEndDateTextbox.sendKeys(editVBidEndDate + Keys.TAB);
		pageVendorBids.editbidEditFormAllowSplitCheckbox(editVBidSplit);
		pageVendorBids.bidEditFormReferenceNumberTextbox.clear();
		pageVendorBids.bidEditFormReferenceNumberTextbox.sendKeys(editVBidRefNum + Keys.TAB);
		pageVendorBids.bidEditFormLineNumberTextbox.clear();
		pageVendorBids.bidEditFormLineNumberTextbox.sendKeys(editVBidLineNum + Keys.TAB);
		pageVendorBids.bidEditFormExpirationDateTextbox.clear();
		pageVendorBids.bidEditFormExpirationDateTextbox.sendKeys(editVBidExpDate);
		pageVendorBids.bidEditFormCollapseIcon.click();
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.visibilityOf(pageVendorBids.bidEditFormExpandIcon)
		);
		Thread.sleep(1000);
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.visibilityOf(pageVendorBids.bidEditFormExpandIcon)
		);

		//open Secondary Vendor Bid popup window
		pageVendorBids.vendorBidsGridCellItem(1, "Alt. Units").click();
		pageVendorBids.waitForSecondaryBidsPopupReturn();

		//create a new secondary vendor bid via Add button and fill in all the fields for later validation
		pageVendorBids.secondaryBidsPopupAddBtn.click();
		//stage the date to be used for Edit
		String editSecVBidNumber = "Sec " + timeStamp;
		String editSecVBidName = "SecName " + timeStamp;
		String editSecVBidUnit = TEST_SEC_VBID_UNIT;
		String editSecVBidBrand = "Brand " + timeStamp;
		String editSecVBidConv = "5.0000";
		Random randSec = new Random();
		double secVbidPrice = randSec.nextInt(15) + randSec.nextDouble();
		String editSecVBidPrice = String.format("%.4f", secVbidPrice);
		String editSecVBidSplit = "Y";
		String editSecVBidTax = TEST_VBID_TAX;
		String editSecVBidEndDate = bidSheetEndDate[0];

		//assign staged date to each secondary Vendor Bid field
		int addSecVBidsRowNum = ActionsEMW.getEMWGridRowCnt(pageVendorBids.secondaryBidsPopupGrid);
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(addSecVBidsRowNum, "Vendor #"), editSecVBidNumber);
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(addSecVBidsRowNum, "Vendor Product Name"), editSecVBidName);
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(addSecVBidsRowNum, "Vendor Unit"), editSecVBidUnit);
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(addSecVBidsRowNum, "Brand"), editSecVBidBrand);
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(addSecVBidsRowNum, "Conv."), editSecVBidConv);
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(addSecVBidsRowNum, "Price"), editSecVBidPrice);
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(addSecVBidsRowNum, "Split"), editSecVBidSplit);
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(addSecVBidsRowNum, "Tax Code"), editSecVBidTax);
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(addSecVBidsRowNum, "End Date"), editSecVBidEndDate);
		//save and close the secondary vendor bid popup
		pageVendorBids.secondaryBidsPopupSaveAndCloseBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		boolean editFormPresent = ActionsEMW.isElementPresent(pageVendorBids.bidEditFormCollapseIcon);
		if (editFormPresent) {
			pageVendorBids.bidEditFormCollapseIcon.click();
			new WebDriverWait(driver, 60, 1000).until(
					ExpectedConditions.visibilityOf(pageVendorBids.bidEditFormExpandIcon)
			);
		}

		//reopen Secondary Vendor Bid popup window to collect the prior edited values for each field
		pageVendorBids.vendorBidsGridCellItem(1, "Alt. Units").click();
		pageVendorBids.waitForSecondaryBidsPopupReturn();
		//find the edited row to validate
		Integer rowNum = ActionsEMW.getEMWGridRowNumByCellValue(pageVendorBids.secondaryBidsPopupGrid, editSecVBidNumber);

		//run validation checkpoints
		for (int i = 0; i < 2; i++) {
			String validationMarket = null;
			if (i == 0) {
				validationMarket = TEST_MARKET_NAME;
			}
			if (i == 1) {
				validationMarket = TEST_ADDITIONAL_MKT_NAME;
			}
			//Validate secondary vendor product number
			String checkSecVBidNum = pageVendorBids.secondaryBidsGridCellItem(rowNum, "Vendor #").getText().trim();
			Assert.assertEquals(
					checkSecVBidNum, editSecVBidNumber,
					result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER2 +
							" for Market = " + validationMarket + ": FAILED, expected secondary vendor product number is " + editSecVBidNumber
			);

			//Validate secondary vendor product name
			String checkSecVBidName = pageVendorBids.secondaryBidsGridCellItem(rowNum, "Vendor Product Name").getText().trim();
			Assert.assertEquals(
					checkSecVBidName, editSecVBidName,
					result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER2 +
							" for Market = " + validationMarket + ": FAILED, expected secondary vendor product name is " + editSecVBidName
			);

			//Validate secondary vendor bid product unit
			String checkSecVBidUnit = pageVendorBids.secondaryBidsGridCellItem(rowNum, "Vendor Unit").getText().trim();
			DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_product_unit", TEST_COMPANYPRODUCT_NUMBER2, editSecVBidNumber, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
			String[] dbSecVBidUnit = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
			Assert.assertEquals(
					checkSecVBidUnit, dbSecVBidUnit[0],
					result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER2 +
							" for Market = " + validationMarket + ": FAILED, expected secondary vendor product unit is " + dbSecVBidUnit[0]
			);

			//Validate secondary vendor bid product brand
			String checkSecVBidBrand = pageVendorBids.secondaryBidsGridCellItem(rowNum, "Brand").getText().trim();
			DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_product_brand", TEST_COMPANYPRODUCT_NUMBER2, editSecVBidNumber, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
			String[] dbSecVBidBrand = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
			Assert.assertEquals(
					checkSecVBidBrand, dbSecVBidBrand[0],
					result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER2 +
							" for Market = " + validationMarket + ": FAILED, expected secondary vendor product brand is " + dbSecVBidBrand[0]
			);

			//Validate secondary vendor bid product conversion
			String checkSecVBidConv = pageVendorBids.secondaryBidsGridCellItem(rowNum, "Conv.").getText().trim();
			DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_product_conv", TEST_COMPANYPRODUCT_NUMBER2, editSecVBidNumber, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
			String[] dbSecVBidConv = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
			Assert.assertEquals(
					checkSecVBidConv, dbSecVBidConv[0],
					result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER2 +
							" for Market = " + validationMarket + ": FAILED, expected secondary vendor product conversion is " + dbSecVBidConv[0]
			);

			//Validate secondary vendor bid product price
			String checkSecVBidPrice = pageVendorBids.secondaryBidsGridCellItem(rowNum, "Price").getText().trim();
			DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_bid_price", TEST_COMPANYPRODUCT_NUMBER2, editSecVBidNumber, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
			String[] dbSecVBidPrice = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
			Assert.assertEquals(
					checkSecVBidPrice, dbSecVBidPrice[0],
					result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER2 +
							" for Market = " + validationMarket + ": FAILED, expected secondary vendor product price is " + dbSecVBidPrice[0]
			);

			//validate sec vendor bid product split flag
			DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_split_flag", TEST_COMPANYPRODUCT_NUMBER2, editSecVBidNumber, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
			String[] dbSecVBidSplitFlag = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
			Assert.assertEquals(
					dbSecVBidSplitFlag[0], editSecVBidSplit,
					result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER2 +
							" for Market = " + validationMarket + ": FAILED, expected secondary vendor product price is " + editSecVBidSplit
			);

			//Validate secondary vendor bid product tax code
			String checkSecVBidTax = pageVendorBids.secondaryBidsGridCellItem(rowNum, "Tax Code").getText().trim();
			DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_tax_code", TEST_COMPANYPRODUCT_NUMBER2, editSecVBidNumber, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
			String[] dbSecVBidTax = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
			Assert.assertEquals(
					checkSecVBidTax, dbSecVBidTax[0],
					result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER2 +
							" for Market = " + validationMarket + ": FAILED, expected secondary vendor product tax code is " + dbSecVBidTax[0]
			);

			//Validate vendor bid end date
			String updatedSecVBipEndDate = pageVendorBids.secondaryBidsGridCellItem(rowNum, "End Date").getText().trim();
			//When Bid End Date = Bid Sheet End Date, set the Bid End Date on screen + 1 to match database value
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Date date = simpleDateFormat.parse(updatedSecVBipEndDate);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.add(Calendar.DATE, 1);
			String checkSecVBidEndDate = simpleDateFormat.format(calendar.getTime());
			DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_end_Date", TEST_COMPANYPRODUCT_NUMBER2, editSecVBidNumber, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
			String[] dbSecVBidEndDate = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
			Assert.assertEquals(
					checkSecVBidEndDate, dbSecVBidEndDate[0],
					result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER2 +
							" for Market = " + validationMarket + ": FAILED, expected secondary vendor product bid end date is " + dbSecVBidEndDate[0]
			);
		}

		//////////////////////////////////////////////////////////////
		//Logical Delete Secondary Vendor Bid (i.e. Begin Date = End Date)
		Integer secVBidsRowCnt = ActionsEMW.getEMWGridRowCnt(pageVendorBids.secondaryBidsPopupGrid);
		for (Integer i = 1; i <= secVBidsRowCnt; i++) {
			pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(i, "End Date"), effDate);
		}
		//save and close the secondary vendor bid popup
		pageVendorBids.secondaryBidsPopupSaveAndCloseBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		//Click Retrieve button to re-retrieve/refresh the results to make sure changes are updated
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		//validate FUTURE sec vendor bid product is logically deleted
		String classString = pageVendorBids.vendorBidsGridCellItem(1, "Alt. Units").getAttribute("class");
		Assert.assertTrue(
				classString.contains("has-plus"),
				result.getMethod().getMethodName() + ": Failed, future secondary vendor bid is NOT logically deleted for market " + TEST_MARKET_NAME);

		//switch to additional market
		//Click Retrieve button to re-retrieve/refresh the results to make sure changes are updated
		ActionsEMW.setComboBoxDropdownByValue(driver, pageVendorBids.marketInputCombobox, TEST_ADDITIONAL_MKT_NAME);
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.visibilityOf(pageVendorBids.effectiveDatePicker)
		);
		pageVendorBids.waitForRetrieveBtnEnable();
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		//Filter testCompanyProductNumber2, create a new bid with End Date = End Bid Sheet
		pageVendorBids.vendorBidsFilterIcon.click();
		pageVendorBids.filterProductNumberTextbox.click();
		pageVendorBids.filterProductNumberTextbox.sendKeys(TEST_COMPANYPRODUCT_NUMBER2);
		pageVendorBids.vendorBidsFilterApplyBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		pageVendorBids.vendorBidsGridCellItem(1, "Product #").click();
		pageVendorBids.waitFoVendorBidsEditFormReturn();
		//validate FUTURE sec vendor bid product is logically deleted
		String classStringAddiMkt = pageVendorBids.vendorBidsGridCellItem(1, "Alt. Units").getAttribute("class");
		Assert.assertTrue(
				classStringAddiMkt.contains("has-plus"),
				result.getMethod().getMethodName() + ": Failed, future secondary vendor bid is NOT logically deleted for market " + TEST_ADDITIONAL_MKT_NAME);
	}
}