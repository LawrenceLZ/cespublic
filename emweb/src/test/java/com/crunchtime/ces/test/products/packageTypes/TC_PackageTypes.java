package com.crunchtime.ces.test.products.packageTypes;

import com.crunchtime.ces.base.BaseTestEMW;
import com.crunchtime.ces.database.packageType.DBStmtPackageTypes;
import com.crunchtime.ces.helper.ActionsEMW;
import com.crunchtime.ces.helper.DBMethodEMW;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

public class TC_PackageTypes extends BaseTestEMW {

/* for testing delete validations we needed to stage the data in a few different environments. Normally this should only be run in auto
The connection details for AIDACORP15 and BDADDY are only for running the deleteValdations which are not going to be run automatically
 */

/* To do; add loop for finding error messages*/

// For running in Auto
    private static final String TEST_SITE_NAME = "auto-em.net-chef.local";
    private static final String USER_ID = "AUTO";
    private static final String USER_PWD = "AUTO";
	private static final String TEST_LOCATION_NAME = "Bubba Gump Shrimp Co";
	private static final String DB_CONNECTION_STRING = "AUTO/AUTO@CTAUTO24006.WORLD";

// For running in AIDA
//	private static final String TEST_SITE_NAME = "aidacorp15-em.net-chef.local";
//	private static final String TEST_LOCATION_NAME = "AIDA Cruises";
//	private static final String DB_CONNECTION_STRING = "AIDACORP15/AIDACORP15@ctcs24005.WORLD";
//	private static final String USER_ID = "AIDACORP15";
//	private static final String USER_PWD = "AIDACORP15";

// For running in BDADDY
//  private static final String TEST_SITE_NAME = "bonedaddy-em.net-chef.local;
//	private static final String TEST_LOCATION_NAME = "Bone Daddy's House of Smoke";
//	private static final String DB_CONNECTION_STRING = "BDADDY/BDADDY@CTQA24010.WORLD";
//	private static final String USER_ID = "BDADDY";
//  private static final String USER_PWD = "BDADDY";


	private static final String TEST_PACKAGE_NAME1 = "AUTOPT001";
	private static final String TEST_PACKAGE_NAME2 = "AUTOPT002";
	private static final String TEST_PACKAGE_NAME3 = "AUTOPT003";
	private static final String TEST_PACKAGE_NAME4_UC = "AUTOPT004";
	private static final String TEST_PACKAGE_NAME4_LC = "autopt004";
	private static final String TEST_PACKAGE_NAME5 = "AUTOPT005";
	private static final String TEST_PACKAGE_NAME6 = "AUTOPT006";
	private static final String TEST_PACKAGE_NAME7_UC = "AUTOPT007DUPE";
	private static final String TEST_PACKAGE_NAME7_LC = "autopt007dupe";
	private static final String TEST_PACKAGE_NAME8 = "AUTOPT008";
	private static final String TEST_PACKAGE_NAME32 = "AUTOPT032";



	@BeforeMethod(alwaysRun = true)
	public void setup() throws Exception {
		DBMethodEMW.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtSetupUpdatePT003());
		DBMethodEMW.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtSetupDelete());
		DBMethodEMW.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtSetupUpdatePT002());
		DBMethodEMW.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtSetupInsert());
		DBMethodEMW.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtSetupUpdateCheckboxes());
		DBMethodEMW.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtSetupUpdatePT008());


		basePageEMW.openTestSite(TEST_SITE_NAME);
		pageEMWLogin.loginEMW(USER_ID, USER_PWD, TEST_LOCATION_NAME);
		pageSetup.navigateToPackageTypesScreen();
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown() throws Exception {
		pageEMWLogout.logoutEMW();
	}

	/* This is being used to drive deletePackageValidations and we are right now not automating this because it involves using different environments for
	different tables
	 */
	@DataProvider(name = "deletePackageData")
	public Object[][] createPackageData() {
		return new Object[][]{
/*
				{"t_product_company", "inventory_package_pk", "product_id"},
				{"t_product_company", "issue_package_pk", "product_id"},
				{"t_product_company", "recipe_primary_package_pk", "product_id"},
				{"t_product_company", "recipe_secondary_package_pk", "product_id"},
				{"t_product_company", "recipe_3_package_pk", "product_id"},
				{"t_product_company", "recipe_4_package_pk", "product_id"},
				{"t_product_company", "recipe_5_package_pk", "product_id"},
				{"t_product_company", "pref_vendor_package_pk", "product_id"},
				{"t_product_company", "weight_package_pk", "product_id"},
				{"t_product_company", "volume_package_pk", "product_id"},
				{"t_product_company", "alt1_package_pk", "product_id"},
				{"t_product_company", "alt2_package_pk", "product_id"},
				{"t_product_company", "alt3_package_pk", "product_id"},
				{"t_product_company", "net_weight_package_pk", "product_id"},
				{"t_product_company", "universal_package_pk", "product_id"},
				{"t_product_location", "alt1_package_pk", "product_location_pk"},
				{"t_product_location", "alt2_package_pk", "product_location_pk"},
				{"t_product_location", "alt3_package_pk", "product_location_pk"},
				{"t_recipe", "recipe_yield_package_pk", "recipe_pk"},
				{"t_vendor_bid_product", "package_pk", "vendor_bid_product_pk"},
				{"t_vendor_bid_product_sec", "package_pk", "vendor_bid_product_sec_pk"},
				{"t_contract_price", "package_pk", "contract_price_pk"},
				{"T_DTL_INVENTORY_SCHEDULE", "PRIMARY_COUNT_PACKAGE_PK", "DTL_INVENTORY_SCHEDULE_PK"},
				{"T_DTL_INVENTORY_SCHEDULE", "ALT1_COUNT_PACKAGE_PK", "DTL_INVENTORY_SCHEDULE_PK"},
				{"T_DTL_INVENTORY_SCHEDULE", "ALT2_COUNT_PACKAGE_PK", "DTL_INVENTORY_SCHEDULE_PK"},
				{"T_DTL_INVENTORY_SCHEDULE", "ALT3_COUNT_PACKAGE_PK", "DTL_INVENTORY_SCHEDULE_PK"},
				{"T_DTL_INVENTORY_SCHEDULE_AUD", "PRIMARY_COUNT_PACKAGE_PK", "dtl_inventory_schedule_aud_pk"},
				{"T_DTL_INVENTORY_SCHEDULE_AUD", "ALT1_COUNT_PACKAGE_PK", "dtl_inventory_schedule_aud_pk"},
				{"T_DTL_INVENTORY_SCHEDULE_AUD", "ALT2_COUNT_PACKAGE_PK", "dtl_inventory_schedule_aud_pk"},
				{"T_DTL_INVENTORY_SCHEDULE_AUD", "ALT3_COUNT_PACKAGE_PK", "dtl_inventory_schedule_aud_pk"},

				{"T_INV_DTL_ACTION", "RECIPE_PACKAGE_PK", "inv_dtl_action_pk"}, // needs data staging

				{"T_INV_DTL_ACTION", "PACKAGE_TYPE_FOR_ADJUST_PK", "inv_dtl_action_pk"},
				{"T_INV_DTL_ADJUSTMENT", "PRIMARY_PACKAGE_PK", "INV_DTL_ADJUSTMENT_PK"},
				{"T_INV_DTL_ADJUSTMENT", "ALT1_PACKAGE_PK", "inv_dtl_adjustment_pk"},
				{"T_INV_DTL_ADJUSTMENT", "ALT2_PACKAGE_PK", "inv_dtl_adjustment_pk"},
				{"T_INV_DTL_ADJUSTMENT", "ALT3_PACKAGE_PK", "inv_dtl_adjustment_pk"},
				{"T_CONVERSION", "FROM_PACKAGE_PK", "conversion_pk"},
				{"T_CONVERSION", "TO_PACKAGE_PK", "conversion_pk"}, */
/*
				{"T_INV_DTL_BLIND_COUNT", "PRIMARY_PACKAGE_PK", "INV_DTL_BLIND_COUNT_PK"}, // needs to be run against cruise_env
				{"T_INV_DTL_BLIND_COUNT", "ALT1_PACKAGE_PK", "INV_DTL_BLIND_COUNT_PK"}, // needs to be run against cruise_env
				{"T_INV_DTL_BLIND_COUNT", "ALT2_PACKAGE_PK", "INV_DTL_BLIND_COUNT_PK"}, // needs to be run against cruise_env
				{"T_INV_DTL_BLIND_COUNT", "ALT3_PACKAGE_PK", "INV_DTL_BLIND_COUNT_PK"} // needs to be run against cruise_env
*/
				/*
				{"T_INV_DTL_VENDOR_ORDER", "VENDOR_PACKAGE_PK", "INV_DTL_VENDOR_ORDER_PK"},
				{"T_INV_DTL_VENDOR_ORDER", "SUB_PACKAGE_PK", "INV_DTL_VENDOR_ORDER_PK"},
				{"T_INV_DTL_VENDOR_ORDER", "VENDOR_PACKAGE_ORDER_PK", "INV_DTL_VENDOR_ORDER_PK"},
				{"T_INV_DTL_VO_UNRESOLVED", "VENDOR_PACKAGE_PK", "INV_DTL_VO_UNRESOLVED_PK"},
				{"T_INV_DTL_VO_UNRESOLVED", "SUB_PROD_PACKAGE_TYPE_PK", "INV_DTL_VO_UNRESOLVED_PK"},
				{"T_INV_DTL_VO_UNRESOLVED_DFLT", "VENDOR_PACKAGE_PK", "LAST_IHVO_PK"},
				{"T_VENDOR_ORDER_HISTORY", "VENDOR_PACKAGE_PK", "PRODUCT_ID"},
				{"T_PRODUCT_VENDOR", "SHIP_PACKAGE_PK", "PRODUCT_VENDOR_ID"} */
		};
	}

	@Test
	public void createPackageType() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-6780", "This creates a package type with name " + TEST_PACKAGE_NAME1 + " and description " + TEST_PACKAGE_NAME1 + "DESC. " +
				"The recipe flag is unchecked during the creation process.");
		pagePackageTypes.addBtn.click();
		pagePackageTypes.createPackageTypeTextbox.clear();
		pagePackageTypes.createPackageTypeTextbox.sendKeys(TEST_PACKAGE_NAME1);
		pagePackageTypes.createPackageDescriptionTextbox.clear();
		pagePackageTypes.createPackageDescriptionTextbox.sendKeys(TEST_PACKAGE_NAME1 + "DESC");
		pagePackageTypes.createRecipeTypeCheckboxChecked.click();
		pagePackageTypes.createUpdateAndSaveBtn.click();

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);

		//validate Package Type in the DB

		Map<String, List<Object>> sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME1));

		Assert.assertNotEquals(sqlPackageDetails.get("PACKAGE_TYPE").size(), 0, "The package was not saved " + TEST_PACKAGE_NAME1);

		Assert.assertEquals(sqlPackageDetails.get("PACKAGE_TYPE").size(), 1, "More than one package has the same name " + TEST_PACKAGE_NAME1);

		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_TYPE").get(0).toString().contentEquals(TEST_PACKAGE_NAME1), "The package type was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_TYPE").get(0).toString());
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_DESC").get(0).toString().contentEquals(TEST_PACKAGE_NAME1 + "DESC"), "The package description was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_DESC").get(0).toString());
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_INV").get(0).toString().contentEquals("Y"), "The package inventory checkbox was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_INV").get(0).toString());
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_RCP").get(0).toString().contentEquals("N"), "The package recipe checkbox was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_RCP").get(0).toString());
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_PURCH").get(0).toString().contentEquals("Y"), "The package purchasing checkbox was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_PURCH").get(0).toString());
	}

	/*This test covers the validations for creating a new record from CES-6682

	 */


	@Test
	public void createPackageTypeValidations() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-6682", "This test is currently disabled due to defect ces-8110. It will be re-enabled when that defect is fixed in the" +
				"05.00.04.00 release.");
	//	ActionsEMW.setTestMethodDescription("ces-6682", "This creates a package type with name " + TEST_PACKAGE_NAME1 + " and description " + TEST_PACKAGE_NAME1 + "DESC. " +
	//			"Each validation that should be done during the creation process is tested.");
/*
		//Try to create a package type without entering anything into the Package Type field
		//Will fail if the warning does not appear or if the screen actually closes upon clicking Save
		//Requirement 1)a)

		pagePackageTypes.addBtn.click();
		pagePackageTypes.createPackageDescriptionTextbox.clear();
		pagePackageTypes.createPackageDescriptionTextbox.sendKeys(TEST_PACKAGE_NAME1 + "DESC");
		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.packageTypeErrorMessage);
		Assert.assertEquals(pagePackageTypes.packageTypeErrorMessage.getText(),"This field is required", "1a");
		pagePackageTypes.createPackageTypeTextbox.sendKeys("ABC");
		pagePackageTypes.createPackageTypeTextbox.clear();
		pagePackageTypes.createUpdateAndSaveBtn.click();
		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.packageTypeErrorMessage);
		Assert.assertEquals(pagePackageTypes.packageTypeErrorMessage.getText(),"This field is required", "1a");
		pagePackageTypes.getCreateCloseButtonBtn.click();

		//Try to create a package type by entering an existing package type into the Package Type field
		//Will fail if the warning does not appear or if the screen actually closes upon clicking Save
		//Requirement 2)a)

		pagePackageTypes.addBtn.click();

		pagePackageTypes.createPackageTypeTextbox.click();
		WebElement activeInput = driver.switchTo().activeElement();
		activeInput.clear();
		activeInput.sendKeys(TEST_PACKAGE_NAME2 + Keys.TAB);
		pagePackageTypes.createPackageTypeTextbox.click();
		pagePackageTypes.createPackageDescriptionTextbox.click();
		pagePackageTypes.createPackageDescriptionTextbox.sendKeys(TEST_PACKAGE_NAME1 + "DESC");
		pagePackageTypes.createPackageDescriptionTextbox.click();
		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.packageTypeErrorMessage);
		Assert.assertEquals(pagePackageTypes.packageTypeErrorMessage.getText(), "Package Type is already in use. Please enter another value.", "2a");

		pagePackageTypes.createPackageTypeTextbox.clear();
		pagePackageTypes.createPackageTypeTextbox.sendKeys("ABC");
		pagePackageTypes.createPackageTypeTextbox.clear();
		pagePackageTypes.createPackageTypeTextbox.sendKeys(TEST_PACKAGE_NAME2);
		pagePackageTypes.createPackageTypeTextbox.click();
		pagePackageTypes.createUpdateAndSaveBtn.click();
		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.packageTypeErrorMessage);
		Assert.assertEquals(pagePackageTypes.packageTypeErrorMessage.getText(), "Package Type is already in use. Please enter another value.", "2a");

		//Try to create a package type without entering anything into the Package Desc field
		//Will fail if the warning does not appear or if the screen actually closes upon clicking Save
		//Requirement 3)a)

		pagePackageTypes.addBtn.click();
		pagePackageTypes.createPackageTypeTextbox.clear();
		pagePackageTypes.createPackageTypeTextbox.sendKeys(TEST_PACKAGE_NAME1);
		pagePackageTypes.createPackageDescriptionTextbox.click();
		pagePackageTypes.createPackageDescriptionTextbox.clear();
		pagePackageTypes.createPackageTypeTextbox.click();
		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.packageDescErrorMessage);
		Assert.assertEquals(pagePackageTypes.packageDescErrorMessage.getText(), "This field is required", "3a");
		pagePackageTypes.createPackageDescriptionTextbox.sendKeys("ABC");
		pagePackageTypes.createPackageDescriptionTextbox.clear();
		pagePackageTypes.createPackageDescriptionTextbox.click();
		pagePackageTypes.createUpdateAndSaveBtn.click();
		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.packageDescErrorMessage);
		Assert.assertEquals(pagePackageTypes.packageDescErrorMessage.getText(),"This field is required", "3a");
		pagePackageTypes.getCreateCloseButtonBtn.click();

		//Try to create a package type by entering an existing package description into the Package Desc field
		//Will fail if the warning does not appear or if the screen actually closes upon clicking Save
		//Requirement 4)a)

		pagePackageTypes.addBtn.click();
		pagePackageTypes.createPackageTypeTextbox.clear();
		pagePackageTypes.createPackageTypeTextbox.sendKeys(TEST_PACKAGE_NAME1);
		pagePackageTypes.createPackageDescriptionTextbox.clear();
		pagePackageTypes.createPackageDescriptionTextbox.sendKeys("Created in Setup 002");
		pagePackageTypes.createPackageDescriptionTextbox.click();
		pagePackageTypes.createUpdateAndSaveBtn.click();
		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.packageDescErrorMessage);
		Assert.assertEquals(pagePackageTypes.packageDescErrorMessage.getText(),"Package Description is already in use. Please enter another value.", "4a");
		pagePackageTypes.getCreateCloseButtonBtn.click();

	 //Try to create a package type without having any of the checkboxes checked
	//Will fail if the warning does not appear
	// Requirement 6)a)

		//defect CES-8110 was found, this should be reenabled when that defect is fixed

		pagePackageTypes.addBtn.click();
		pagePackageTypes.createPackageTypeTextbox.clear();
		pagePackageTypes.createPackageTypeTextbox.sendKeys(TEST_PACKAGE_NAME1);
		pagePackageTypes.createPackageDescriptionTextbox.clear();
		pagePackageTypes.createPackageDescriptionTextbox.sendKeys(TEST_PACKAGE_NAME1 + "DESC");
		pagePackageTypes.createRecipeTypeCheckboxChecked.click();
		Assert.assertTrue(pagePackageTypes.createRecipeTypeCheckboxUnchecked.isDisplayed());
		pagePackageTypes.createPurchasingTypeCheckboxChecked.click();
		Assert.assertTrue(pagePackageTypes.createPurchasingTypeCheckboxUnchecked.isDisplayed());
		pagePackageTypes.createInventoryTypeCheckboxChecked.click();
		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.messageBox);
		Assert.assertEquals(pagePackageTypes.messageBox.getText(), "Unchecking all classification options will make Package Type unavailable in the application.\n" +
				"\n" +
				"Do you wish to continue?", "6a");


		pagePackageTypes.messageBoxCancel.click();
		Waiter.waitForOneSecond();

		//Verify the checkbox is now checked.

		Assert.assertTrue(pagePackageTypes.createInventoryTypeCheckboxChecked.isDisplayed());

		pagePackageTypes.createInventoryTypeCheckboxChecked.click();
		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.messageBox);
		Assert.assertEquals(pagePackageTypes.messageBox.getText(), "Unchecking all classification options will make Package Type unavailable in the application.\n" +
				"\n" +
				"Do you wish to continue?", "6a");
		pagePackageTypes.messageBoxOK.click();
		Waiter.waitForOneSecond();
		//Verify the checkbox is now unchecked.
		Assert.assertTrue(pagePackageTypes.createInventoryTypeCheckboxUnchecked.isDisplayed());

		pagePackageTypes.createPackageDescriptionTextbox.click();

		pagePackageTypes.createUpdateAndSaveBtn.click();

		//validate Package Type in the DB
		pagePackageTypes.waitForSpinnerToDisappear();
		Map<String, List<Object>> sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME1));

		Assert.assertNotEquals(sqlPackageDetails.get("PACKAGE_TYPE").size(), 0, "The package was not saved " + TEST_PACKAGE_NAME1);

		Assert.assertEquals(sqlPackageDetails.get("PACKAGE_TYPE").size(), 1, "More than one package has the same name " + TEST_PACKAGE_NAME1);

		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_TYPE").get(0).toString().contentEquals(TEST_PACKAGE_NAME1), "The package type was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_TYPE").get(0).toString());
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_DESC").get(0).toString().contentEquals(TEST_PACKAGE_NAME1 + "DESC"), "The package description was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_DESC").get(0).toString());
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_INV").get(0).toString().contentEquals("N"), "The package inventory checkbox was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_INV").get(0).toString());
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_RCP").get(0).toString().contentEquals("N"), "The package recipe checkbox was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_RCP").get(0).toString());
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_PURCH").get(0).toString().contentEquals("N"), "The package purchasing checkbox was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_PURCH").get(0).toString());
*/
	}


	@Test
	public void applyFilter() throws Exception {

		ActionsEMW.setTestMethodDescription("ces-6785", "This tests the use of a filter in the package type screen.");

		String filterPattern = "AUTOPT00";

		//find out how many package types there were initially
		int countPackageTypes = Integer.parseInt(pagePackageTypes.getPackageTypeCount());

		//open the filter popup
		pagePackageTypes.packageTypeFilterBtn.click();
		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.filterAcceptBtn);

		//Enter and apply Filter
		pagePackageTypes.filterPackageNameTextbox.sendKeys(filterPattern);
		pagePackageTypes.filterAcceptBtn.click();

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);

		Assert.assertTrue(pagePackageTypes.packageTypeFilterAppliedBtn.isDisplayed(), "Filter icon is not green");

		//find out how many package types there are filtered
		int countFilteredPackageTypes = Integer.parseInt(pagePackageTypes.getPackageTypeCount());

		//See what is displayed on the screen
		String[] packageTypeNames = pagePackageTypes.getPackageNames();
		Assert.assertEquals(packageTypeNames.length, countFilteredPackageTypes, "The footer does not have the right number of elements listed.");

		int filteredPackageCount = packageTypeNames.length;
		for (int i = 0; i < filteredPackageCount; i++) {
			Assert.assertTrue(packageTypeNames[i].toUpperCase().contains(filterPattern), "The package name " + packageTypeNames[i] + " does not match the filter pattern " + filterPattern);
		}

		pagePackageTypes.packageTypeFilterAppliedBtn.click();
		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.filterAcceptBtn);

		//clear the filters
		pagePackageTypes.filterClearBtn.click();
		pagePackageTypes.filterAcceptBtn.click();

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);

		Assert.assertTrue(pagePackageTypes.packageTypeFilterBtn.isDisplayed(), "Filter icon is not white");

		//find out how many package types there are unfiltered
		int countUnfilteredPackageTypes = Integer.parseInt(pagePackageTypes.getPackageTypeCount());

		Assert.assertEquals(countUnfilteredPackageTypes, countPackageTypes, "The element count in the footer is not the same as it was originally.");
	}

	@Test
	public void deletePackageType() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-6784", "This deletes the package type with name " + TEST_PACKAGE_NAME3 + ". This is a simple test to make sure delete works."
			+ " Delete related validations are tested in another test.");
		//Sort by Package Type Desc
		pagePackageTypes.packageTypeColumnHdr.click();
		Waiter.waitForElementToDisappear(driver,pagePackageTypes.loadSpinner);
		Assert.assertTrue(pagePackageTypes.packageTypeColumnHdrText.getAttribute("class").contains("sort-ASC"));
		pagePackageTypes.packageTypeColumnHdr.click();
		Waiter.waitForElementToDisappear(driver,pagePackageTypes.loadSpinner);
		Assert.assertTrue(pagePackageTypes.packageTypeColumnHdrText.getAttribute("class").contains("sort-DESC"));

		//See what is displayed on the screen
		String[] packageTypeNames = pagePackageTypes.getPackageNames();

		//Find the index for the row which holds the package name
		int packageRowIndex = -1;
		int pageNum = 1;
		Assert.assertTrue(pagePackageTypes.pageLimits.isDisplayed(), "Cannot capture the page limits");
		String strPageLimit = pagePackageTypes.pageLimits.getText();
		String[] temp = strPageLimit.split(" ");
		int pageLimit = Integer.parseInt(temp[1]);

		packageRowIndex = pagePackageTypes.getPackageTypeIndex(TEST_PACKAGE_NAME3);

		while ((packageRowIndex < 0) && (pageNum < pageLimit)) {
			pageNum++;
			pagePackageTypes.pageNumberTextbox.clear();
			pagePackageTypes.pageNumberTextbox.sendKeys(String.valueOf(pageNum));
			pagePackageTypes.pageNumberTextbox.sendKeys("\n");
			Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);
			packageRowIndex = pagePackageTypes.getPackageTypeIndex(TEST_PACKAGE_NAME3);
		}

		pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "Package Type").click();
		pagePackageTypes.getDeleteButton(packageRowIndex).click();

		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.deleteYesBtn);
		pagePackageTypes.deleteYesBtn.click();

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);

		packageRowIndex = pagePackageTypes.getPackageTypeIndex(TEST_PACKAGE_NAME3);
		Assert.assertEquals(packageRowIndex, -1, "Package Type " + TEST_PACKAGE_NAME3 + " is still on screen.");

		//validate Package Type in the DB

		Map<String, List<Object>> sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME3));

		Assert.assertEquals(sqlPackageDetails.get("PACKAGE_TYPE").size(), 0, "The package type " + TEST_PACKAGE_NAME3 + " still exists in the DB");
	}

	@Test
	public void deleteValidationReplicatedFlag() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-6784", "This updates the replication flag of the package type with name " + TEST_PACKAGE_NAME3 + " and sets it to 'Y'. " +
				"This is a simple test to make sure that once the replication flag is 'Y' the package type cannot be deleted.");

		//get details of TEST_PACKAGE_NAME
		Map<String, List<Object>> sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(
				TEST_PACKAGE_NAME3));
		String testPackage03PK = sqlPackageDetails.get("PACKAGE_PK").get(0).toString();

		//Filter for test data the screen
		String filterPattern = "AUTOPT";
		pagePackageTypes.packageTypeFilterBtn.click();
		pagePackageTypes.filterPackageNameTextbox.sendKeys(filterPattern);
		pagePackageTypes.filterAcceptBtn.click();

		//verify that a package_type with t_package_type.replicated_flag = 'Y' cannot be deleted
		//update a test package type with a replicated_flag = 'Y'

		DBMethodEMW.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPackageTypes.updateTableValueBasedOnPK("t_package_type", "replicated_flag", "Y", "package_pk",
				testPackage03PK));
		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME3));
		Assert.assertEquals(sqlPackageDetails.get("REPLICATED_FLAG").get(0), "Y", "The package type " + TEST_PACKAGE_NAME3 + " has a replicated flag of N");

		//Find the index for the row which holds the package name
		int packageRowIndex = -1;
		packageRowIndex = pagePackageTypes.getPackageTypeIndex(TEST_PACKAGE_NAME3);

		pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "Package Type").click();
		pagePackageTypes.getDeleteButton(packageRowIndex).click();

		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.deleteYesBtn);
		pagePackageTypes.deleteYesBtn.click();

		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.messageBox);

		Assert.assertEquals(pagePackageTypes.messageBox.getText(), "This Package Type has been replicated and cannot be deleted.");
		pagePackageTypes.messageBoxOK.click();

		packageRowIndex = pagePackageTypes.getPackageTypeIndex(TEST_PACKAGE_NAME3);
		Assert.assertNotEquals(packageRowIndex, -1, "Package Type " + TEST_PACKAGE_NAME3 + " has been deleted from the screen.");

		//validate Package Type in the DB
		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME3));
		Assert.assertNotEquals(sqlPackageDetails.get("PACKAGE_TYPE").size(), 0, "The package type " + TEST_PACKAGE_NAME3 + " still exists in the DB");
		Assert.assertEquals(sqlPackageDetails.get("REPLICATED_FLAG").get(0).toString(), "Y", "The package type was somehow altered during the test");

		//reset data to how it was before the test
		DBMethodEMW.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPackageTypes.updateTableValueBasedOnPK("t_package_type", "replicated_flag", "N", "package_pk",
				testPackage03PK));
		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME3));
		Assert.assertEquals(sqlPackageDetails.get("REPLICATED_FLAG").get(0).toString(), "N", "The package type was somehow altered during the test");

	}

	/* This is being used to test deletePackageValidations and we are right now not automating this because it involves using different environments for
	different tables
	 */
/*
	@Test (dataProvider = "deletePackageData")
	public void deleteValidations(String tableToCheck, String fieldToCheck, String pkName) throws Exception {

		ActionsEMW.setTestMethodDescription("ces-6789", "This tests the validations that need to be done when a user attempts to delete a package_type.");

		//get details of TEST_PACKAGE_NAME
		Map<String, List<Object>> sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(
				TEST_PACKAGE_NAME3));
		String testPackage03PK = sqlPackageDetails.get("PACKAGE_PK").get(0).toString();

		//Filter for test data the screen
		String filterPattern = "AUTOPT";
		pagePackageTypes.packageTypeFilterBtn.click();
		pagePackageTypes.filterPackageNameTextbox.sendKeys(filterPattern);
		pagePackageTypes.filterAcceptBtn.click();

		//verify that a t_package_type record with a dependent record in another table cannot be deleted
		//update a the dependent table with the package pk

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.selectTableMinPK(
				pkName, tableToCheck, "where " + fieldToCheck + " is not NULL"));

		Assert.assertNotNull(sqlPackageDetails.get("MIN_PK").get(0), "there is no " + tableToCheck + " record with a non-null " + fieldToCheck + " value.");

		String tableMinPK = sqlPackageDetails.get("MIN_PK").get(0).toString();
		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.selectValueBasedOnTablePK(fieldToCheck, tableToCheck,
				pkName, tableMinPK));
		String formerPK = sqlPackageDetails.get("RETURNED_VALUE").get(0).toString();
		DBMethodEMW.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPackageTypes.updateTableValueBasedOnPK(tableToCheck, fieldToCheck,
				testPackage03PK, pkName, tableMinPK));
		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.selectValueBasedOnTablePK(fieldToCheck, tableToCheck,
				pkName, tableMinPK));

		//Using a soft assert allows us to restore the data in the DB even if the assert fails
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertTrue(sqlPackageDetails.get("RETURNED_VALUE").get(0).toString().contentEquals(testPackage03PK),
				"The " + tableToCheck + "record has the wrong " + fieldToCheck);


		//Find the index for the row which holds the package name
		int packageRowIndex = -1;
		packageRowIndex = pagePackageTypes.getPackageTypeIndex(TEST_PACKAGE_NAME3);

		pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "Package Type").click();
		pagePackageTypes.getDeleteButton(packageRowIndex).click();

		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.deleteYesBtn);
		pagePackageTypes.deleteYesBtn.click();

		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.messageBox);

		softAssert.assertEquals(pagePackageTypes.messageBox.getText(), "This Package Type is in use in the application and cannot be deleted.");
		pagePackageTypes.messageBoxOK.click();

		packageRowIndex = pagePackageTypes.getPackageTypeIndex(TEST_PACKAGE_NAME3);
		softAssert.assertNotEquals(packageRowIndex, -1, "Package Type " + TEST_PACKAGE_NAME3 + " has been deleted from the screen.");

		//validate Package Type in the DB
		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME3));
		softAssert.assertNotEquals(sqlPackageDetails.get("PACKAGE_TYPE").size(), 0, "The package type " + TEST_PACKAGE_NAME3 + " still exists in the DB");

		//reset data to how it was before the test
		DBMethodEMW.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPackageTypes.updateTableValueBasedOnPK(tableToCheck, fieldToCheck,
				formerPK, pkName, tableMinPK));
		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.selectValueBasedOnTablePK(fieldToCheck, tableToCheck,
				pkName, tableMinPK));


		Assert.assertTrue(sqlPackageDetails.get("RETURNED_VALUE").get(0).toString().contentEquals(formerPK),  "The " + tableToCheck + " has the wrong " + fieldToCheck);
		softAssert.assertAll();
	}
*/

	@Test
	public void editPackageType() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-6781", "AND ces-6783 This tests editing both the package_description and the package classification checkboxes.");

		//Sort by Package Type Desc
		pagePackageTypes.packageTypeColumnHdr.click();
		Waiter.waitForElementToDisappear(driver,pagePackageTypes.loadSpinner);
		Assert.assertTrue(pagePackageTypes.packageTypeColumnHdrText.getAttribute("class").contains("sort-ASC"));
		pagePackageTypes.packageTypeColumnHdr.click();
		Waiter.waitForElementToDisappear(driver,pagePackageTypes.loadSpinner);
		Assert.assertTrue(pagePackageTypes.packageTypeColumnHdrText.getAttribute("class").contains("sort-DESC"));


		//See what is displayed on the screen
		String[] packageTypeNames = pagePackageTypes.getPackageNames();

		//Find the index for the row which holds the package name
		int packageRowIndex = -1;
		int pageNum = 1;
		Assert.assertTrue(pagePackageTypes.pageLimits.isDisplayed(), "Cannot capture the page limits");
		String strPageLimit = pagePackageTypes.pageLimits.getText();
		String[] temp = strPageLimit.split(" ");
		int pageLimit = Integer.parseInt(temp[1]);

		packageRowIndex = pagePackageTypes.getPackageTypeIndex(TEST_PACKAGE_NAME2);

		while ((packageRowIndex < 0) && (pageNum < pageLimit)) {
			pageNum++;
			pagePackageTypes.pageNumberTextbox.clear();
			pagePackageTypes.pageNumberTextbox.sendKeys(String.valueOf(pageNum));
			pagePackageTypes.pageNumberTextbox.sendKeys("\n");
			Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);
			packageRowIndex = pagePackageTypes.getPackageTypeIndex(TEST_PACKAGE_NAME2);
		}

		pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "Package Type").click();
		org.openqa.selenium.interactions.Actions cellAction = new org.openqa.selenium.interactions.Actions(driver);
		cellAction.doubleClick(pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "Package Type")).build().perform();

		//get package type details

		Map<String, List<Object>> sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME2));

		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.createPackageTypeTextbox);
		Assert.assertTrue(pagePackageTypes.createPackageTypeTextbox.getAttribute("value").contentEquals(sqlPackageDetails.get("PACKAGE_TYPE").get(0).toString()));
		Assert.assertTrue(pagePackageTypes.createPackageDescriptionTextbox.getAttribute("value").contentEquals(sqlPackageDetails.get("PACKAGE_DESC").get(0).toString()));
		if(sqlPackageDetails.get("PACKAGE_INV").get(0).toString().contentEquals("Y")){
			Assert.assertTrue(pagePackageTypes.createInventoryTypeCheckboxChecked.isDisplayed());
		}
		else{
			Assert.assertTrue(pagePackageTypes.createInventoryTypeCheckboxUnchecked.isDisplayed());
		}

		if(sqlPackageDetails.get("PACKAGE_RCP").get(0).toString().contentEquals("Y")){
			Assert.assertTrue(pagePackageTypes.createRecipeTypeCheckboxChecked.isDisplayed());
		}
		else{
			Assert.assertTrue(pagePackageTypes.createRecipeTypeCheckboxUnchecked.isDisplayed());
		}

		if(sqlPackageDetails.get("PACKAGE_PURCH").get(0).toString().contentEquals("Y")){
			Assert.assertTrue(pagePackageTypes.createPurchasingTypeCheckboxChecked.isDisplayed());
		}
		else{
			Assert.assertTrue(pagePackageTypes.createPurchasingTypeCheckboxUnchecked.isDisplayed());
		}



		pagePackageTypes.createPackageDescriptionTextbox.clear();
		pagePackageTypes.createPackageDescriptionTextbox.sendKeys("Altered In Test");
		pagePackageTypes.createRecipeTypeCheckboxChecked.click();
		pagePackageTypes.createUpdateAndSaveBtn.click();

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);

		//validate Package Type in the DB

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME2));

		Assert.assertEquals(sqlPackageDetails.get("PACKAGE_TYPE").size(), 1, "More than one package has the same name " + TEST_PACKAGE_NAME2);

		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_TYPE").get(0).toString().contentEquals(TEST_PACKAGE_NAME2), "The package type was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_TYPE").get(0).toString());
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_DESC").get(0).toString().contentEquals("Altered In Test"), "The package description was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_DESC").get(0).toString());
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_INV").get(0).toString().contentEquals("Y"), "The package inventory checkbox was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_INV").get(0).toString());
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_RCP").get(0).toString().contentEquals("N"), "The package recipe checkbox was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_RCP").get(0).toString());
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_PURCH").get(0).toString().contentEquals("Y"), "The package purchasing checkbox was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_PURCH").get(0).toString());
	}

	//commented out until CES-7842 is resolved
	/*This test covers the validations for editing a record from CES-6682

 */

	@Test
	public void editDupeRecordCheckboxes() throws Exception {

		ActionsEMW.setTestMethodDescription("ces-6682", "This tests editing the package type classifcation checkbox values from the summary screen when there is more than one"
			+ " package_type record with the same package type or package description. These sorts of records may already exist in client databases because the check for "
			+	"duplicates used to be case sensitive.");

		//Filter for test data the screen
		String filterPattern = "AUTOPT";
		pagePackageTypes.packageTypeFilterBtn.click();
		pagePackageTypes.filterPackageNameTextbox.sendKeys(filterPattern);
		pagePackageTypes.filterAcceptBtn.click();

		//Find the index for the row which holds the package name
		int packageRowIndex = -1;
		packageRowIndex = pagePackageTypes.getPackageTypeIndex(TEST_PACKAGE_NAME4_LC);

		//uncheck inventory
		pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "INVENTORY").click();

		Waiter.waitForElementToBeVisible(driver,pagePackageTypes.messageBox);
		Assert.assertTrue(pagePackageTypes.messageBox.getText().contentEquals("Package Type is already in use. Please enter another value."));
		pagePackageTypes.messageBoxOK.click();
		Assert.assertTrue(pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "INVENTORY").getAttribute("class").contains("checked"));

		Map<String, List<Object>>  sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME4_LC));
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_INV").get(0).toString().contentEquals("Y"), "The package inventory checkbox for " + TEST_PACKAGE_NAME4_LC +
				" was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_INV").get(0).toString());

		packageRowIndex = pagePackageTypes.getPackageTypeIndex(TEST_PACKAGE_NAME4_UC);
		//uncheck inventory
		pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "INVENTORY").click();
		Waiter.waitForElementToBeVisible(driver,pagePackageTypes.messageBox);
		Assert.assertTrue(pagePackageTypes.messageBox.getText().contentEquals("Package Type is already in use. Please enter another value."));
		pagePackageTypes.messageBoxOK.click();
		Assert.assertTrue(pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "INVENTORY").getAttribute("class").contains("checked"));

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME4_UC));
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_INV").get(0).toString().contentEquals("Y"), "The package inventory checkbox for " + TEST_PACKAGE_NAME4_UC +
				" was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_INV").get(0).toString());

		packageRowIndex = pagePackageTypes.getPackageTypeIndex(TEST_PACKAGE_NAME5);
		//uncheck inventory
		pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "INVENTORY").click();

		Waiter.waitForElementToBeVisible(driver,pagePackageTypes.messageBox);
		Assert.assertTrue(pagePackageTypes.messageBox.getText().contentEquals("Package Description is already in use. Please enter another value."));
		pagePackageTypes.messageBoxOK.click();
		Assert.assertTrue(pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "INVENTORY").getAttribute("class").contains("checked"));

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME5));
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_INV").get(0).toString().contentEquals("Y"), "The package inventory checkbox for " + TEST_PACKAGE_NAME5 +
				" was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_INV").get(0).toString());

		packageRowIndex = pagePackageTypes.getPackageTypeIndex(TEST_PACKAGE_NAME6);
		//uncheck inventory
		pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "INVENTORY").click();
		Waiter.waitForElementToBeVisible(driver,pagePackageTypes.messageBox);
		Assert.assertTrue(pagePackageTypes.messageBox.getText().contentEquals("Package Description is already in use. Please enter another value."));
		pagePackageTypes.messageBoxOK.click();
		Assert.assertTrue(pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "INVENTORY").getAttribute("class").contains("checked"));

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME6));
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_INV").get(0).toString().contentEquals("Y"), "The package inventory checkbox for " + TEST_PACKAGE_NAME6 +
				" was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_INV").get(0).toString());

		packageRowIndex = pagePackageTypes.getPackageTypeIndex(TEST_PACKAGE_NAME7_LC);
		//uncheck inventory
		pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "INVENTORY").click();

		Waiter.waitForElementToBeVisible(driver,pagePackageTypes.messageBox);
		Assert.assertTrue(pagePackageTypes.messageBox.getText().contentEquals("Package Type is already in use. Please enter another value."));
		pagePackageTypes.messageBoxOK.click();
		Assert.assertTrue(pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "INVENTORY").getAttribute("class").contains("checked"));

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME7_LC));
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_INV").get(0).toString().contentEquals("Y"), "The package inventory checkbox for " + TEST_PACKAGE_NAME7_LC +
				" was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_INV").get(0).toString());

		packageRowIndex = pagePackageTypes.getPackageTypeIndex(TEST_PACKAGE_NAME7_UC);
		//uncheck inventory
		pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "INVENTORY").click();
		Waiter.waitForElementToBeVisible(driver,pagePackageTypes.messageBox);
		Assert.assertTrue(pagePackageTypes.messageBox.getText().contentEquals("Package Type is already in use. Please enter another value."));
		pagePackageTypes.messageBoxOK.click();
		Assert.assertTrue(pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "INVENTORY").getAttribute("class").contains("checked"));

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME7_UC));
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_INV").get(0).toString().contentEquals("Y"), "The package inventory checkbox for " + TEST_PACKAGE_NAME7_UC +
				" was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_INV").get(0).toString());
	}

	@Test
	public void editBadRecordCheckboxes() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-7513", "This tests updating the package type classification checkboxes when the package type has a blank description.");

		//Filter for test data the screen
		String filterPattern = "AUTOPT";
		pagePackageTypes.packageTypeFilterBtn.click();
		pagePackageTypes.filterPackageNameTextbox.sendKeys(filterPattern);
		pagePackageTypes.filterAcceptBtn.click();

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);

		//Find the index for the row which holds the package name
		int packageRowIndex = -1;
		packageRowIndex = pagePackageTypes.getPackageTypeIndex(TEST_PACKAGE_NAME8);
		//uncheck inventory
		pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "INVENTORY").click();

		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.messageBox);
		Assert.assertTrue(pagePackageTypes.messageBox.getText().contentEquals("Package Description is required. Please update Package Type Description before making any changes."));
		pagePackageTypes.messageBoxOK.click();
		Assert.assertTrue(pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "INVENTORY").getAttribute("class").contains("checked"));

		Map<String, List<Object>> sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME8));
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_INV").get(0).toString().contentEquals("Y"), "The package inventory checkbox for " + TEST_PACKAGE_NAME8 +
				" was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_INV").get(0).toString());

		//uncheck purchasing
		pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "PURCHASING").click();

		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.messageBox);
		Assert.assertTrue(pagePackageTypes.messageBox.getText().contentEquals("Package Description is required. Please update Package Type Description before making any changes."));
		pagePackageTypes.messageBoxOK.click();
		Assert.assertTrue(pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "PURCHASING").getAttribute("class").contains("checked"));

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME8));
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_PURCH").get(0).toString().contentEquals("Y"), "The package purchasing checkbox for " + TEST_PACKAGE_NAME8 +
				" was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_PURCH").get(0).toString());

		//uncheck recipe
		pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "RECIPE").click();

		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.messageBox);
		Assert.assertTrue(pagePackageTypes.messageBox.getText().contentEquals("Package Description is required. Please update Package Type Description before making any changes."));
		pagePackageTypes.messageBoxOK.click();
		Assert.assertTrue(pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "RECIPE").getAttribute("class").contains("checked"));

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME8));
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_RCP").get(0).toString().contentEquals("Y"), "The package recipe checkbox for " + TEST_PACKAGE_NAME8 +
				" was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_RCP").get(0).toString());

		//set all checkboxes to unchecked

		DBMethodEMW.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPackageTypes.updateTableValueBasedOnPK("T_PACKAGE_TYPE", "PACKAGE_INVENTORY", "N", "PACKAGE_PK",
				sqlPackageDetails.get("PACKAGE_PK").get(0).toString()));
		DBMethodEMW.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPackageTypes.updateTableValueBasedOnPK("T_PACKAGE_TYPE", "PACKAGE_RECIPE", "N", "PACKAGE_PK",
				sqlPackageDetails.get("PACKAGE_PK").get(0).toString()));
		DBMethodEMW.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPackageTypes.updateTableValueBasedOnPK("T_PACKAGE_TYPE", "PACKAGE_PURCHASING", "N", "PACKAGE_PK",
				sqlPackageDetails.get("PACKAGE_PK").get(0).toString()));

		//Refresh the screen
		driver.navigate().refresh();

		//Filter screen again
		pagePackageTypes.packageTypeFilterBtn.click();
		pagePackageTypes.filterPackageNameTextbox.sendKeys(filterPattern);
		pagePackageTypes.filterAcceptBtn.click();

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);

		//Check to see that at least one of the checkboxes is unchecked so the test can proceed
		Assert.assertFalse(pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "INVENTORY").getAttribute("class").contains("checked"));

		//check inventory
		pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "INVENTORY").click();

		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.messageBox);
		Assert.assertTrue(pagePackageTypes.messageBox.getText().contentEquals("Package Description is required. Please update Package Type Description before making any changes."));
		pagePackageTypes.messageBoxOK.click();
		Assert.assertFalse(pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "INVENTORY").getAttribute("class").contains("checked"));

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME8));
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_INV").get(0).toString().contentEquals("N"), "The package inventory checkbox for " + TEST_PACKAGE_NAME8 +
				" was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_INV").get(0).toString());

		//check purchasing
		pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "PURCHASING").click();

		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.messageBox);
		Assert.assertTrue(pagePackageTypes.messageBox.getText().contentEquals("Package Description is required. Please update Package Type Description before making any changes."));
		pagePackageTypes.messageBoxOK.click();
		Assert.assertFalse(pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "PURCHASING").getAttribute("class").contains("checked"));

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME8));
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_PURCH").get(0).toString().contentEquals("N"), "The package purchasing checkbox for " + TEST_PACKAGE_NAME8 +
				" was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_PURCH").get(0).toString());

		//check recipe
		pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "RECIPE").click();

		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.messageBox);
		Assert.assertTrue(pagePackageTypes.messageBox.getText().contentEquals("Package Description is required. Please update Package Type Description before making any changes."));
		pagePackageTypes.messageBoxOK.click();
		Assert.assertFalse(pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "RECIPE").getAttribute("class").contains("checked"));;

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME8));
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_RCP").get(0).toString().contentEquals("N"), "The package recipe checkbox for " + TEST_PACKAGE_NAME8 +
				" was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_RCP").get(0).toString());


	}

	/*This test covers the validations for editing a record from CES-6682

	 */
	@Test
	public void editPackageTypeValidations() throws Exception {

		ActionsEMW.setTestMethodDescription("ces-6682", "This tests all the validations that are done when a user edits a package type from the popup.");

		//Filter for test data the screen
		String filterPattern = "AUTOPT";
		pagePackageTypes.packageTypeFilterBtn.click();
		pagePackageTypes.filterPackageNameTextbox.sendKeys(filterPattern);
		pagePackageTypes.filterAcceptBtn.click();

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);
		//Find the index for the row which holds the package name
		int packageRowIndex = -1;
		packageRowIndex = pagePackageTypes.getPackageTypeIndex(TEST_PACKAGE_NAME2);

		//double click that row to open edit popup
		pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "Package Type").click();
		org.openqa.selenium.interactions.Actions cellAction = new org.openqa.selenium.interactions.Actions(driver);
		cellAction.doubleClick(pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "Package Type")).build().perform();

		//Try to edit a package type - clearing its Package Type field
		//Will fail if the warning does not appear or if the screen actually closes upon clicking Save
		//Requirement 1)b)
		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.createPackageDescriptionTextbox);
		pagePackageTypes.createPackageTypeTextbox.click();
		pagePackageTypes.createPackageTypeTextbox.clear();
		Waiter.waitForOneSecond();
		pagePackageTypes.createPackageTypeTextbox.sendKeys(Keys.TAB);
		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.packageTypeErrorMessage);
		Assert.assertEquals(pagePackageTypes.packageTypeErrorMessage.getText(), "This field is required");
		pagePackageTypes.createPackageTypeTextbox.sendKeys("ABC");
		pagePackageTypes.createPackageDescriptionTextbox.click();
		pagePackageTypes.createPackageTypeTextbox.clear();
		Waiter.waitForOneSecond();
		pagePackageTypes.createUpdateAndSaveBtn.click();
		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.packageTypeErrorMessage);
		Assert.assertEquals(pagePackageTypes.packageTypeErrorMessage.getText(), "This field is required");
		pagePackageTypes.createXBtn.click();

		//reset
		//double click that row to open edit popup

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.createPackageDescriptionTextbox);
		pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "Package Type").click();
		Waiter.waitForOneSecond();
		org.openqa.selenium.interactions.Actions cellAction2 = new org.openqa.selenium.interactions.Actions(driver);
		cellAction.doubleClick(pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "Package Type")).build().perform();

		//Try to edit a package type - entering an already existing Package Type
		//Will fail if the warning does not appear or if the screen actually closes upon clicking Save
		//Requirement 2)b)

		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.createPackageDescriptionTextbox);
		pagePackageTypes.createPackageTypeTextbox.clear();
		pagePackageTypes.createPackageTypeTextbox.sendKeys(TEST_PACKAGE_NAME3);
		pagePackageTypes.createPackageTypeTextbox.click();
		pagePackageTypes.createUpdateAndSaveBtn.click();
		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.packageTypeErrorMessage);
		Assert.assertEquals(pagePackageTypes.packageTypeErrorMessage.getText(),"Package Type is already in use. Please enter another value.");
		pagePackageTypes.createXBtn.click();

		//reset
		//double click that row to open edit popup
		Waiter.waitForElementToDisappear(driver, pagePackageTypes.createPackageDescriptionTextbox);
		pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "Package Type").click();
		Waiter.waitForOneSecond();
		cellAction.doubleClick(pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "Package Type")).build().perform();

		//Try to edit a package type - clearing its Package Description field
		//Will fail if the warning does not appear or if the screen actually closes upon clicking Save
		//Requirement 3)b)

		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.createPackageDescriptionTextbox);
		pagePackageTypes.createPackageDescriptionTextbox.click();
		pagePackageTypes.createPackageDescriptionTextbox.clear();
		Waiter.waitForOneSecond();
		pagePackageTypes.createPackageDescriptionTextbox.sendKeys("\t");
		pagePackageTypes.createPackageDescriptionTextbox.click();
		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.packageDescErrorMessage);
		Assert.assertEquals(pagePackageTypes.packageDescErrorMessage.getText(), "This field is required");
		pagePackageTypes.createPackageDescriptionTextbox.sendKeys("ABC");
		pagePackageTypes.createPackageTypeTextbox.click();
		pagePackageTypes.createPackageDescriptionTextbox.click();
		pagePackageTypes.createPackageDescriptionTextbox.clear();
		pagePackageTypes.createPackageDescriptionTextbox.click();
		Waiter.waitForOneSecond();
		pagePackageTypes.createUpdateAndSaveBtn.click();
		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.packageDescErrorMessage);
		Assert.assertEquals(pagePackageTypes.packageDescErrorMessage.getText(),"This field is required");
		pagePackageTypes.createXBtn.click();

		//reset
		//double click that row to open edit popup
		Waiter.waitForElementToDisappear(driver, pagePackageTypes.createPackageDescriptionTextbox);
		pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "Package Type").click();
		Waiter.waitForOneSecond();
		cellAction.doubleClick(pagePackageTypes.getPackageTypeGridCellItem(packageRowIndex, "Package Type")).build().perform();

		//Try to edit a package type - entering an already existing Package Description
		//Will fail if the warning does not appear or if the screen actually closes upon clicking Save
		//Requirement 4)b)

		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.createPackageDescriptionTextbox);
		pagePackageTypes.createPackageDescriptionTextbox.clear();
		pagePackageTypes.createPackageDescriptionTextbox.sendKeys("Created in Setup 003");
		pagePackageTypes.createPackageDescriptionTextbox.click();
		pagePackageTypes.createUpdateAndSaveBtn.click();
		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.packageDescErrorMessage);
		Assert.assertEquals(pagePackageTypes.packageDescErrorMessage.getText(),"Package Description is already in use. Please enter another value.");
		pagePackageTypes.createXBtn.click();


	}
		@Test
	public void updatePackageTypeCheckboxesSingly() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-6783", "This tests updating package type classifications from the summary screen one at a time.");

		//Filter the screen
		String filterPattern = "AUTOPT0";
		pagePackageTypes.packageTypeFilterBtn.click();
		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.filterAcceptBtn);

		//Enter and apply Filter
		pagePackageTypes.filterPackageNameTextbox.sendKeys(filterPattern);
		pagePackageTypes.filterAcceptBtn.click();

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);

		//uncheck inventory
		pagePackageTypes.getPackageTypeGridCellItem(1, "INVENTORY").click();

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);

		//validate Package Type in the DB
		Map<String, List<Object>> sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME2));

		Assert.assertFalse(sqlPackageDetails.get("PACKAGE_TYPE").size() > 1, "More than one package has the same name " + TEST_PACKAGE_NAME2);
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_TYPE").size() > 0, "The package cannot be found.");

		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_INV").get(0).toString().contentEquals("N"), "The package inventory checkbox was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_INV").get(0).toString());

		//uncheck purchasing
		pagePackageTypes.getPackageTypeGridCellItem(1, "PURCHASING").click();

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME2));
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_PURCH").get(0).toString().contentEquals("N"), "The package purchasing checkbox was not saved correctly, the DB contains " +
				sqlPackageDetails.get("PACKAGE_PURCH").get(0).toString());

		//uncheck recipe and click cancel on alert
		pagePackageTypes.getPackageTypeGridCellItem(1, "RECIPE").click();

		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.messageBox);
		Assert.assertEquals(pagePackageTypes.messageBox.getText(), "Unchecking all classification options will make Package Type unavailable in the application.\n" +
				"\n" + "Do you wish to continue?");

		pagePackageTypes.messageBoxCancel.click();

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME2));
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_RCP").get(0).toString().contentEquals("Y"), "The package recipe checkbox was altered, the DB contains " +
				sqlPackageDetails.get("PACKAGE_RCP").get(0).toString());

		//uncheck recipe and click ok on alert

		pagePackageTypes.getPackageTypeGridCellItem(1, "RECIPE").click();

		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.messageBox);

		pagePackageTypes.messageBoxOK.click();

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME2));
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_RCP").get(0).toString().contentEquals("N"), "The package recipe checkbox was not altered in DB, the DB contains " +
				sqlPackageDetails.get("PACKAGE_RCP").get(0).toString());

		//recheck recipe
		pagePackageTypes.getPackageTypeGridCellItem(1, "RECIPE").click();

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME2));
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_RCP").get(0).toString().contentEquals("Y"), "The package recipe checkbox was not altered in DB, the DB contains " +
				sqlPackageDetails.get("PACKAGE_RCP").get(0).toString());

		//recheck inventory
		pagePackageTypes.getPackageTypeGridCellItem(1, "INVENTORY").click();

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME2));
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_INV").get(0).toString().contentEquals("Y"), "The package inventory checkbox was not altered in DB, the DB contains " +
				sqlPackageDetails.get("PACKAGE_INV").get(0).toString());

		//recheck purchasing
		pagePackageTypes.getPackageTypeGridCellItem(1, "PURCHASING").click();

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetPackageTypeDetails(TEST_PACKAGE_NAME2));
		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_PURCH").get(0).toString().contentEquals("Y"), "The package purchasing checkbox was not altered in DB, the DB contains " +
				sqlPackageDetails.get("PACKAGE_PURCH").get(0).toString());
	}

	@Test
	public void updatePackageTypeCheckboxesInBulk() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-6875", "This tests updating package type classifications from the summary screen in bulk.");

		//See what is displayed on the screen
		String[] packageTypeNames = pagePackageTypes.getPackageNames();

		//Sort by Package Type Asc
		pagePackageTypes.packageTypeColumnHdr.click();
		Waiter.waitForElementToDisappear(driver,pagePackageTypes.loadSpinner);
		Assert.assertTrue(pagePackageTypes.packageTypeColumnHdrText.getAttribute("class").contains("sort-ASC"));


		//Find the index for the row which holds the package name
		int packageRowIndex = -1;
		Assert.assertTrue(pagePackageTypes.pageLimits.isDisplayed(), "Cannot capture the page limits");
		String strPageLimit = pagePackageTypes.pageLimits.getText();
		String[] temp = strPageLimit.split(" ");
		int pageLimit = Integer.parseInt(temp[1]);
		int pageNum = pageLimit;

		packageRowIndex = pagePackageTypes.getPackageTypeIndex(TEST_PACKAGE_NAME32);

		while ((packageRowIndex <= pageLimit) && (pageNum > 0)){
			pageNum--;
			pagePackageTypes.pageNumberTextbox.clear();
			pagePackageTypes.pageNumberTextbox.sendKeys(String.valueOf(pageNum));
			pagePackageTypes.pageNumberTextbox.sendKeys("\n");
			Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);
			packageRowIndex = pagePackageTypes.getPackageTypeIndex(TEST_PACKAGE_NAME32);
			if (packageRowIndex > -1){
				break;
			}
		}

		packageTypeNames = pagePackageTypes.getPackageNames();


		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);

		String packageTypeTokens[] = packageTypeNames[packageTypeNames.length-1].split("T");
		int packageTypeNumOffPage = Integer.parseInt(packageTypeTokens[2])+ 5;

		String packageTypeOffPage = "AUTOPT000";
		if (packageTypeNumOffPage <= 100){
			packageTypeOffPage = "AUTOPT0" + String.valueOf(packageTypeNumOffPage);
		}
		else{
			packageTypeOffPage = "AUTOPT" + String.valueOf(packageTypeNumOffPage);
		}

		//uncheck Inventory
		pagePackageTypes.inventoryTypeColumnHdrCheckbox.click();
		pagePackageTypes.inventoryTypeColumnHdrCheckbox.click();

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);

		//validate Package Type in the DB
		Map<String, List<Object>> sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetAllPackageTypeDtlWithNameBetween(packageTypeNames[0], packageTypeNames[packageTypeNames.length-1]));
		for (int i = 0; i < sqlPackageDetails.get("PACKAGE_TYPE").size(); i++) {
			Assert.assertTrue(sqlPackageDetails.get("PACKAGE_INV").get(i).toString().contentEquals("N"), "The package inventory checkbox for a package type on the screen was not saved " +
					"correctly, the DB contains " +
					sqlPackageDetails.get("PACKAGE_INV").get(i).toString());
		}

		//Verify that package types not on the screen are unchanged.
		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetAllPackageTypeDtlWithNameLike(packageTypeOffPage));

		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_INV").get(0).toString().contentEquals("Y"), "he package inventory checkbox for a package type not on the screen was updated " +
				"and shouldn't have been, the DB contains " +
				sqlPackageDetails.get("PACKAGE_INV").get(0).toString());


		//uncheck Recipe
		pagePackageTypes.recipeTypeColumnHdrCheckbox.click();
		pagePackageTypes.recipeTypeColumnHdrCheckbox.click();

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetAllPackageTypeDtlWithNameBetween(packageTypeNames[0], packageTypeNames[packageTypeNames.length-1]));

		for (int i = 0; i < sqlPackageDetails.get("PACKAGE_TYPE").size(); i++) {
			Assert.assertTrue(sqlPackageDetails.get("PACKAGE_RCP").get(i).toString().contentEquals("N"), "The package recipe checkbox for a package type on the screen was not saved " +
					"correctly, the DB contains " +
					sqlPackageDetails.get("PACKAGE_RCP").get(i).toString());
		}

		//Verify that package types not on the screen are unchanged.
		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetAllPackageTypeDtlWithNameLike(packageTypeOffPage));

		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_RCP").get(0).toString().contentEquals("Y"), "The package recipe checkbox for a package type not on the screen was updated " +
				"and shouldn't have been, the DB contains " +
				sqlPackageDetails.get("PACKAGE_RCP").get(0).toString());

		//uncheck Purchasing and click Cancel on the alert
		pagePackageTypes.purchasingTypeColumnHdrCheckbox.click();
		pagePackageTypes.purchasingTypeColumnHdrCheckbox.click();

		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.messageBox);
		Assert.assertEquals(pagePackageTypes.messageBox.getText(), "Unchecking all classification options will make Package Type unavailable in the application.\n" +
				"\n" + "Do you wish to continue?");

		pagePackageTypes.messageBoxCancel.click();


		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetAllPackageTypeDtlWithNameBetween(packageTypeNames[0], packageTypeNames[packageTypeNames.length-1].toString()));;
		for (int i = 0; i < sqlPackageDetails.get("PACKAGE_TYPE").size(); i++) {
			Assert.assertTrue(sqlPackageDetails.get("PACKAGE_PURCH").get(i).toString().contentEquals("Y"), "The package purchasing checkbox for a package type on the screen was not saved " +
					"correctly, the DB contains " +
					sqlPackageDetails.get("PACKAGE_PURCH").get(i).toString());
		}

		//Verify that package types not on the screen are unchanged.
		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetAllPackageTypeDtlWithNameLike(packageTypeOffPage));

		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_PURCH").get(0).toString().contentEquals("Y"), "The package purchasing checkbox for a package type not on the screen was " +
				"updated and shouldn't have been, the DB contains " +
				sqlPackageDetails.get("PACKAGE_PURCH").get(0).toString());

		//uncheck Purchasing and click OK on the alert
		pagePackageTypes.purchasingTypeColumnHdrCheckbox.click();
		pagePackageTypes.purchasingTypeColumnHdrCheckbox.click();

		Waiter.waitForElementToBeVisible(driver, pagePackageTypes.messageBox);

		pagePackageTypes.messageBoxOK.click();

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetAllPackageTypeDtlWithNameBetween(packageTypeNames[0].toString(), packageTypeNames[packageTypeNames.length-1].toString()));;
		for (int i = 0; i < sqlPackageDetails.get("PACKAGE_TYPE").size(); i++) {
			Assert.assertTrue(sqlPackageDetails.get("PACKAGE_PURCH").get(i).toString().contentEquals("N"), "The package purchasing checkbox for a package type on the screen was not saved " +
					"correctly, the DB contains " +
					sqlPackageDetails.get("PACKAGE_PURCH").get(i).toString());
		}

		//Verify that package types not on the screen are unchanged.
		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetAllPackageTypeDtlWithNameLike(packageTypeOffPage));

		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_PURCH").get(0).toString().contentEquals("Y"), "The package purchasing checkbox for a package type not on the screen was " +
				"updated and shouldn't have been, the DB contains " +
				sqlPackageDetails.get("PACKAGE_PURCH").get(0).toString());

		//set the checkboxes for the offscreen PT to 'N'
		DBMethodEMW.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtSetCheckboxesToNForAPT(packageTypeOffPage));

		//recheck Inventory
		pagePackageTypes.inventoryTypeColumnHdrCheckbox.click();

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetAllPackageTypeDtlWithNameBetween(packageTypeNames[0].toString(), packageTypeNames[packageTypeNames.length-1].toString()));;
		for (int i = 0; i < sqlPackageDetails.get("PACKAGE_TYPE").size(); i++) {
			Assert.assertTrue(sqlPackageDetails.get("PACKAGE_INV").get(i).toString().contentEquals("Y"), "The package purchasing checkbox for a package type on the screen was not saved " +
					"correctly, the DB contains " +
					sqlPackageDetails.get("PACKAGE_INV").get(i).toString());
		}

		//Verify that package types not on the screen are unchanged.
		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetAllPackageTypeDtlWithNameLike(packageTypeOffPage));

		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_INV").get(0).toString().contentEquals("N"), "The package purchasing checkbox for a package type not on the screen was " +
				"updated and shouldn't have been, the DB contains " +
				sqlPackageDetails.get("PACKAGE_INV").get(0).toString());

		//recheck recipe
		pagePackageTypes.recipeTypeColumnHdrCheckbox.click();

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetAllPackageTypeDtlWithNameBetween(packageTypeNames[0].toString(), packageTypeNames[packageTypeNames.length-1].toString()));;
		for (int i = 0; i < sqlPackageDetails.get("PACKAGE_TYPE").size(); i++) {
			Assert.assertTrue(sqlPackageDetails.get("PACKAGE_RCP").get(i).toString().contentEquals("Y"), "The package purchasing checkbox for a package type on the screen was not saved " +
					"correctly, the DB contains " +
					sqlPackageDetails.get("PACKAGE_RCP").get(i).toString());
		}

		//Verify that package types not on the screen are unchanged.
		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetAllPackageTypeDtlWithNameLike(packageTypeOffPage));

		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_RCP").get(0).toString().contentEquals("N"), "The package purchasing checkbox for a package type not on the screen was " +
				"updated and shouldn't have been, the DB contains " +
				sqlPackageDetails.get("PACKAGE_RCP").get(0).toString());

		//recheck purch
		pagePackageTypes.purchasingTypeColumnHdrCheckbox.click();

		Waiter.waitForElementToDisappear(driver, pagePackageTypes.loadSpinner);

		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetAllPackageTypeDtlWithNameBetween(packageTypeNames[0].toString(), packageTypeNames[packageTypeNames.length-1].toString()));;
		for (int i = 0; i < sqlPackageDetails.get("PACKAGE_TYPE").size(); i++) {
			Assert.assertTrue(sqlPackageDetails.get("PACKAGE_PURCH").get(i).toString().contentEquals("Y"), "The package purchasing checkbox for a package type on the screen was not saved correctly, the DB contains " +
					sqlPackageDetails.get("PACKAGE_PURCH").get(i).toString());
		}

		//Verify that package types not on the screen are unchanged.
		sqlPackageDetails = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPackageTypes.getStmtGetAllPackageTypeDtlWithNameLike(packageTypeOffPage));

		Assert.assertTrue(sqlPackageDetails.get("PACKAGE_PURCH").get(0).toString().contentEquals("N"), "The package purchasing checkbox for a package type not on the screen was " +
				"updated and shouldn't have been, the DB contains " +
				sqlPackageDetails.get("PACKAGE_PURCH").get(0).toString());
	}
}