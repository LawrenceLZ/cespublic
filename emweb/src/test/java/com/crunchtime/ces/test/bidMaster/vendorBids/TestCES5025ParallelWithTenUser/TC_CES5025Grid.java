package com.crunchtime.ces.test.bidMaster.vendorBids.TestCES5025ParallelWithTenUser;

import com.crunchtime.ces.base.BaseTestEMW;
import com.crunchtime.ces.helper.ActionsEMW;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

public class TC_CES5025Grid extends BaseTestEMW {
	String testSiteName = "metz-fb-bidrewrite-em.net-chef.local";
	String userID = "griduser";
	String userPWD = "griduser";
	String testBidSheetName = "KTEST BID SHEET 004 (120 PRODUCTS)";
	String testVendorName = "ITR OF GEORGIA (AUTO VO Entry Point)";
	String testMarketName = "Atlanta";
	String testVendorProductNumber1 = "AutoContract005";
	String testVendorProductNumber2 = "AutoFuture006";
	String testVendorProductNumber3 = "AutoEditSpl007";
	private WebDriver driver;

	@BeforeClass
	@Parameters("URL")
	public void setup(String url) throws MalformedURLException {
		DesiredCapabilities capability = DesiredCapabilities.phantomjs();
		capability.setCapability("phantomjs.binary.path", "C:\\PhantomjsDriver\\phantomjs.exe");
		capability.setCapability("takesScreenshot", false);
		this.driver = new RemoteWebDriver(new URL(url + ":5555/wd/hub"), capability);

		basePageEMW.openTestSite(testSiteName);
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

	@BeforeMethod
	public void logIn() throws Exception {
		basePageEMW.openTestSite(testSiteName);
		pageEMWLogin.loginEMW(userID, userPWD, "");
	}

	@AfterMethod
	public void logOut() throws Exception {
		pageEMWLogout.logoutEMW();
	}

	@Test
	public void editVendorBidsRowGridUser() throws Exception {
		basePageEMW.navigateToPageOtherTasks();
		pageOtherTasks.navigateToVendorBidsScreen();
		Thread.sleep(2000);
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.elementToBeClickable(By.id("vendorBid-view-vendorBidContainer-panel-productParams-button-launchBidSheetsPopup-btnIconEl"))
				//ExpectedConditions.visibilityOf(pageVendorBids.bidSheetsBtn)
		);
		pageVendorBids.bidSheetsBtn.click();
		Thread.sleep(5000);
		new WebDriverWait(driver, 60, 1000).until(
				//to wait for select bid sheets rendered before further steps
				ExpectedConditions.presenceOfElementLocated(By.cssSelector("div[class*='x-window select-bid-sheet x-layer x-window-default x-closable']>div>div>div>div>table>tbody"))
		);
		Thread.sleep(1000);
		//select the testing bid sheet
		pageVendorBids.selectBidSheetsPopupSelectAllCheckbox.click();
		pageVendorBids.selectBidSheetsPopupFilterIcon.click();
		pageVendorBids.filterBidSheetTextbox.clear();
		pageVendorBids.filterBidSheetTextbox.sendKeys(testBidSheetName);
		pageVendorBids.filterBidSheetApplyBtn.click();
		new WebDriverWait(driver, 60, 1000).until(
				//to wait for select bid sheets rendered before further steps
				ExpectedConditions.presenceOfElementLocated(By.cssSelector("div[class*='x-window select-bid-sheet x-layer x-window-default x-closable']>div>div>div>div>table>tbody"))
		);
		Thread.sleep(1000);
		Thread.sleep(1000);
		pageVendorBids.selectBidSheetsPopupSelectAllCheckbox.click();
		pageVendorBids.selectBidSheetsPopupApplyBth.click();
		Thread.sleep(1000);
		//define vendor/market and retrieve
		pageVendorBids.vendorInputCombobox.click();
		pageVendorBids.vendorInputComboboxTrigger.click();
		Thread.sleep(1000);
		ActionsEMW.setComboBoxDropdownByValue(driver, pageVendorBids.vendorInputCombobox, testVendorName);
		Thread.sleep(1000);
		pageVendorBids.marketInputCombobox.click();
		pageVendorBids.marketInputCombobox.clear();
		pageVendorBids.marketInputCombobox.sendKeys(testMarketName);
		pageVendorBids.marketInputCombobox.sendKeys("\t");
		//String selectedVal = pageVendorBids.marketInputCombobox.getAttribute("Value");
		//System.out.println("market selected = " + selectedVal);
		Thread.sleep(10000);

		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.visibilityOf(pageVendorBids.effectiveDatePicker)
		);
		//set effective date = current date
		pageVendorBids.effectiveDatePicker.clear();
		pageVendorBids.effectiveDatePicker.sendKeys(ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 0));
		pageVendorBids.effectiveDatePicker.sendKeys("\t");
		Thread.sleep(1000);
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();

		//Filter testVendorProductNumber1 = AutoContract005 and Edit Bid Price
		pageVendorBids.vendorBidsFilterIcon.click();
		pageVendorBids.filterVendorProductNumberTextbox.click();
		pageVendorBids.filterVendorProductNumberTextbox.sendKeys(testVendorProductNumber1);
		pageVendorBids.vendorBidsFilterApplyBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		Thread.sleep(1000);
		pageVendorBids.vendorBidsGridCellItem(1, "Product #").click();
		pageVendorBids.waitFoVendorBidsEditFormReturn();
		pageVendorBids.bidEditFormPriceTextbox.clear();
		Random rand1 = new Random();
		double bidPrice1 = rand1.nextInt(10) + 0.50;
		//assign a random bid price to testProduct
		pageVendorBids.bidEditFormPriceTextbox.sendKeys(String.valueOf(bidPrice1));
		pageVendorBids.bidEditFormCollapseIcon.click();
		Thread.sleep(1000);
		System.out.println("For TC_CES5025Grid " + testVendorProductNumber1 + " EXPECTED edit bid price is " + bidPrice1);
		Reporter.log("For TC_CES5025Grid " + testVendorProductNumber1 + " EXPECTED edit bid price is " + bidPrice1);
		String vbidpNum1 = pageVendorBids.vendorBidsGridCellItem(1, "vendor #").getText();
		String updatedvbidpPrice1 = pageVendorBids.vendorBidsGridCellItem(1, "price").getText().trim();
		System.out.println("For TC_CES5025Grid " + vbidpNum1 + " ACTUAL edit bid price is " + updatedvbidpPrice1);
		Reporter.log("For TC_CES5025Grid " + vbidpNum1 + " ACTUAL edit bid price is " + updatedvbidpPrice1);

		//Filter testVendorProductNumber2 = AutoFuture006 and Edit Bid Price
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		Thread.sleep(1000);
		pageVendorBids.vendorBidsFilterIcon.click();
		pageVendorBids.filterVendorProductNumberTextbox.click();
		pageVendorBids.filterVendorProductNumberTextbox.clear();
		pageVendorBids.filterVendorProductNumberTextbox.sendKeys(testVendorProductNumber2);
		pageVendorBids.vendorBidsFilterApplyBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		Thread.sleep(1000);
		pageVendorBids.vendorBidsGridCellItem(1, "Product #").click();
		pageVendorBids.waitFoVendorBidsEditFormReturn();
		pageVendorBids.bidEditFormPriceTextbox.clear();
		Random rand2 = new Random();
		double bidPrice2 = rand2.nextInt(10) + 0.50;
		//assign a random bid price to testProduct
		pageVendorBids.bidEditFormPriceTextbox.sendKeys(String.valueOf(bidPrice2));
		pageVendorBids.bidEditFormCollapseIcon.click();
		Thread.sleep(1000);
		System.out.println("For TC_CES5025Grid " + testVendorProductNumber2 + " EXPECTED edit bid price is " + bidPrice2);
		Reporter.log("For TC_CES5025Grid " + testVendorProductNumber2 + " EXPECTED edit bid price is " + bidPrice2);
		String vbidpNum2 = pageVendorBids.vendorBidsGridCellItem(1, "vendor #").getText().trim();
		String updatedvbidpPrice2 = pageVendorBids.vendorBidsGridCellItem(1, "price").getText();
		System.out.println("For TC_CES5025Grid " + vbidpNum2 + " ACTUAL edit bid price is " + updatedvbidpPrice2);
		Reporter.log("For TC_CES5025Grid " + vbidpNum2 + " ACTUAL edit bid price is " + updatedvbidpPrice2);

		//Filter testVendorProductNumber3 = AutoEditSpl007 and Edit Bid Price
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		Thread.sleep(1000);
		pageVendorBids.vendorBidsFilterIcon.click();
		pageVendorBids.filterVendorProductNumberTextbox.click();
		pageVendorBids.filterVendorProductNumberTextbox.clear();
		pageVendorBids.filterVendorProductNumberTextbox.sendKeys(testVendorProductNumber3);
		pageVendorBids.vendorBidsFilterApplyBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		Thread.sleep(1000);
		pageVendorBids.vendorBidsGridCellItem(1, "Product #").click();
		pageVendorBids.waitFoVendorBidsEditFormReturn();
		pageVendorBids.bidEditFormPriceTextbox.clear();
		Random rand3 = new Random();
		double bidPrice3 = rand3.nextInt(10) + 0.50;
		//assign a random bid price to testProduct
		pageVendorBids.bidEditFormPriceTextbox.sendKeys(String.valueOf(bidPrice3));
		pageVendorBids.bidEditFormCollapseIcon.click();
		Thread.sleep(1000);
		System.out.println("For TC_CES5025Grid " + testVendorProductNumber3 + " EXPECTED edit bid price is " + bidPrice3);
		Reporter.log("For TC_CES5025Grid " + testVendorProductNumber3 + " EXPECTED edit bid price is " + bidPrice3);
		String vbidpNum3 = pageVendorBids.vendorBidsGridCellItem(1, "vendor #").getText();
		String updatedvbidpPrice3 = pageVendorBids.vendorBidsGridCellItem(1, "price").getText().trim();
		System.out.println("For TC_CES5025Grid " + vbidpNum3 + " ACTUAL edit bid price is " + updatedvbidpPrice3);
		Reporter.log("For TC_CES5025Grid " + vbidpNum3 + " ACTUAL edit bid price is " + updatedvbidpPrice3);

		System.out.println("Price edit complete for TC_CES5025Grid");
		Reporter.log("Price edit complete");
	}
}
