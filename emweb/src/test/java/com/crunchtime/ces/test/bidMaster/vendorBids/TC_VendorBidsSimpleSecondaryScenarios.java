package com.crunchtime.ces.test.bidMaster.vendorBids;

import com.crunchtime.ces.base.BaseTestEMW;
import com.crunchtime.ces.database.bidMaster.vendorBids.DBStmtVendorBids;
import com.crunchtime.ces.helper.ActionsEMW;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class TC_VendorBidsSimpleSecondaryScenarios extends BaseTestEMW {
	private static final String DB_CONNECTION_STRING = "AUTO/AUTO@CTAUTO24006";
	private static final String TEST_SITE_NAME = "auto-em.net-chef.local";
	private static final String USER_ID = "VBIDS";
	private static final String USER_PWD = "VBIDS";
	private static final String TEST_LOCATION_NAME = "Bubba Gump Shrimp Co";
	private static final String TEST_BID_SHEET_NAME = "TestVendorBids";
	private static final String TEST_VENDOR_NAME = "TestVendorBids Ven 1";
	private static final String TEST_MARKET_NAME = "TestVendorBids";
	private static final String TEST_COMPANYPRODUCT_NUMBER1 = "VBID006";
	private static final String TEST_COMPANYPRODUCT_NUMBER2 = "VBID007";
	private static final String TEST_COMPANYPRODUCT_NUMBER3 = "VBID008";
	private static final String TEST_COMPANYPRODUCT_NUMBER4 = "VBID009";
	private static final String TEST_COMPANYPRODUCT_NUMBER5 = "VBID010";
	private static final String TEST_COMPANYPRODUCT_NUMBER5_SECVBID_UNIT = "CA";

	@BeforeMethod(alwaysRun = true)
	public void login() throws Exception {
		basePageEMW.openTestSite(TEST_SITE_NAME);
		pageEMWLogin.loginEMW(USER_ID, USER_PWD, TEST_LOCATION_NAME);
		//Navigate to Vendor Bids detail screen with testing Bid Sheet/Vendor/Market combination
		basePageEMW.navigateToPageOtherTasks();
		pageOtherTasks.navigateToVendorBidsScreen();
		pageVendorBids.bidSheetsBtn.click();
		pageVendorBids.waitForSelectBidSheetsPopupReturn();
		//Select the testing Bid Sheet
		pageVendorBids.selectBidSheetsPopupSelectAllCheckbox.click(); //first to deselect all the bid sheets
		pageVendorBids.selectBidSheetsPopupFilterIcon.click();
		pageVendorBids.filterBidSheetTextbox.clear();
		pageVendorBids.filterBidSheetTextbox.sendKeys(TEST_BID_SHEET_NAME);
		pageVendorBids.filterBidSheetApplyBtn.click();
		pageVendorBids.waitForSelectBidSheetsPopupReturn(); //wait for filtered results to return
		pageVendorBids.selectBidSheetsPopupSelectAllCheckbox.click(); //now to select the filtered bid sheet(s)
		pageVendorBids.selectBidSheetsPopupApplyBth.click();
		pageVendorBids.waitForVendorInputCombobxEnable();
		Thread.sleep(1000);
		ActionsEMW.setComboBoxDropdownByValue(driver, pageVendorBids.vendorInputCombobox, TEST_VENDOR_NAME);
		Thread.sleep(1000);
		pageVendorBids.marketInputCombobox.click();
		pageVendorBids.marketInputCombobox.clear();
		pageVendorBids.marketInputCombobox.sendKeys(TEST_MARKET_NAME);
		pageVendorBids.marketInputCombobox.sendKeys("\t");
		boolean retrieveEnabled;
		do {
			Waiter.waitForOneSecond();
			retrieveEnabled = pageVendorBids.retrieveBtn.getAttribute("class").contains("disabled");
			int timer = 0;
			timer++;
			if (timer == 120) break;
		} while (retrieveEnabled);
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		boolean closeBtnExist = ActionsEMW.isElementPresent(pageVendorBids.secondaryBidsPopupCloseBtn);
		if (closeBtnExist) pageVendorBids.secondaryBidsPopupCloseBtn.click();
		pageEMWLogout.logoutEMW();
	}

	/**
	 * Test Case Scenario,
	 * Create a current Secondary Vendor Bid with end date = Bid Sheet End Date
	 */
	@Test
	public void editCurrentSecondaryVendorBidPriceAndEndDateEqualBidSheetEndDate() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-1427", "Create a current Secondary Vendor Bid with end date = Bid Sheet End Date");
//		ITestResult result = Reporter.getCurrentTestResult();
		//set effective date = current date
		String effDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 0);
		pageVendorBids.effectiveDatePicker.clear();
		pageVendorBids.effectiveDatePicker.sendKeys(effDate);
		pageVendorBids.effectiveDatePicker.sendKeys("\t");
		Thread.sleep(1000);
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();

		//Filter TEST_COMPANYPRODUCT_NUMBER1 and Edit Bid Price and End Date
		pageVendorBids.vendorBidsFilterIcon.click();
		pageVendorBids.filterProductNumberTextbox.click();
		pageVendorBids.filterProductNumberTextbox.sendKeys(TEST_COMPANYPRODUCT_NUMBER1);
		pageVendorBids.vendorBidsFilterApplyBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		//open Secondary Vendor Bid popup window
		pageVendorBids.vendorBidsGridCellItem(1, "Alt. Units").click();
		pageVendorBids.waitForSecondaryBidsPopupReturn();
		Random rand1 = new Random();
		double secVbidPrice1 = rand1.nextDouble();
		//assign the above random Secondary VBid Price to the testProduct
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(1, "Price"), String.valueOf(secVbidPrice1));
		//assign Bid Sheet End Date to bid End Date
		DBStmtVendorBids.getBidSheetEndDate(DB_CONNECTION_STRING, TEST_BID_SHEET_NAME);
		String[] bidSheetEndDate = ActionsEMW.readSqlResults("DBStmtVendorBids", "Find_Bid_Sheet_End_Date");
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(1, "End Date"), bidSheetEndDate[0]);
		pageVendorBids.secondaryBidsPopupSaveAndCloseBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		boolean editFormPresent = ActionsEMW.isElementPresent(pageVendorBids.bidEditFormCollapseIcon);
		if (editFormPresent) {
			pageVendorBids.bidEditFormCollapseIcon.click();
			new WebDriverWait(driver, 60, 1000).until(
					ExpectedConditions.visibilityOf(pageVendorBids.bidEditFormExpandIcon)
			);
		}
		//reopen Secondary Vendor Bid popup window to collect the prior edited values
		pageVendorBids.vendorBidsGridCellItem(1, "Alt. Units").click();
		pageVendorBids.waitForSecondaryBidsPopupReturn();
		String updatedSecVBidNum = pageVendorBids.secondaryBidsGridCellItem(1, "Vendor #").getText().trim();
		String updatedSecVBidPrice = pageVendorBids.secondaryBidsGridCellItem(1, "Price").getText().trim();
		String updatedSecVBidEndDate = pageVendorBids.secondaryBidsGridCellItem(1, "End Date").getText().trim();

		//When Bid End Date = Bid Sheet End Date, set the Bid End Date on screen + 1 to match database value
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = simpleDateFormat.parse(updatedSecVBidEndDate);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, 1);
		String actualUpdatedSecVBidpEndDate = simpleDateFormat.format(calendar.getTime());
//		Reporter.log("Test Method = " + result.getMethod().getMethodName() + ": " + updatedSecVBidNum + " ACTUAL edit bid price is " + updatedSecVBidPrice + "\n");
		//validate edited Bid Price to database saved value
		DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_bid_price", TEST_COMPANYPRODUCT_NUMBER1, updatedSecVBidNum, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbSecVendorBidPrice = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
		Assert.assertEquals(
				dbSecVendorBidPrice[0], updatedSecVBidPrice, "test FAILED, db value for Bid Price should be " + updatedSecVBidPrice);

		//validate edited Bid End Date to database saved value
		DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_end_Date", TEST_COMPANYPRODUCT_NUMBER1, updatedSecVBidNum, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbSecVendorBidEndDate = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
		//System.out.println(actualUpdatedSecVBidpEndDate);
		Assert.assertEquals(
				dbSecVendorBidEndDate[0], actualUpdatedSecVBidpEndDate, "Failed, db value for Bid End Date should be " + actualUpdatedSecVBidpEndDate);
	}

	/**
	 * Test Case Scenario,
	 * Create a current Secondary Vendor Bid with end date < Bid Sheet End Date
	 */
	@Test
	public void editCurrentSecondaryVendorBidPriceAndEndDateLessThanBidSheetEndDate() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-1427", "Create a current Secondary Vendor Bid with end date < Bid Sheet End Date");
//		ITestResult result = Reporter.getCurrentTestResult();
		//set effective date = current date
		String effDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 0);
		pageVendorBids.effectiveDatePicker.clear();
		pageVendorBids.effectiveDatePicker.sendKeys(effDate);
		pageVendorBids.effectiveDatePicker.sendKeys("\t");
		Thread.sleep(1000);
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();

		//Filter TEST_COMPANYPRODUCT_NUMBER2 and Edit Secondary Vendor Bid Price and End Date
		pageVendorBids.vendorBidsFilterIcon.click();
		pageVendorBids.filterProductNumberTextbox.click();
		pageVendorBids.filterProductNumberTextbox.sendKeys(TEST_COMPANYPRODUCT_NUMBER2);
		pageVendorBids.vendorBidsFilterApplyBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		//open Secondary Vendor Bid popup window
		pageVendorBids.vendorBidsGridCellItem(1, "Alt. Units").click();
		pageVendorBids.waitForSecondaryBidsPopupReturn();
		Random rand = new Random();
		double secVbidPrice = rand.nextDouble();
		//assign the above random Secondary VBid Price to the testProduct
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(1, "Price"), String.valueOf(secVbidPrice));
		//assign Bid Sheet End Date = today's date + 180 as a end date < bid sheet End Date
		String TEST_COMPANYPRODUCT_NUMBER2EndDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 180);
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(1, "End Date"), TEST_COMPANYPRODUCT_NUMBER2EndDate);
		pageVendorBids.secondaryBidsPopupSaveAndCloseBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		boolean editFormPresent = ActionsEMW.isElementPresent(pageVendorBids.bidEditFormCollapseIcon);
		if (editFormPresent) {
			pageVendorBids.bidEditFormCollapseIcon.click();
			new WebDriverWait(driver, 60, 1000).until(
					ExpectedConditions.visibilityOf(pageVendorBids.bidEditFormExpandIcon)
			);
		}
		//reopen Secondary Vendor Bid popup window to collect the prior edited values
		pageVendorBids.vendorBidsGridCellItem(1, "Alt. Units").click();
		pageVendorBids.waitForSecondaryBidsPopupReturn();
		String updatedSecVBidNum = pageVendorBids.secondaryBidsGridCellItem(1, "Vendor #").getText().trim();
		String updatedSecVBidPrice = pageVendorBids.secondaryBidsGridCellItem(1, "Price").getText().trim();
		String updatedSecVBidEndDate = pageVendorBids.secondaryBidsGridCellItem(1, "End Date").getText().trim();
//		Reporter.log("Test Method = " + result.getMethod().getMethodName() + ": " + updatedSecVBidNum + " ACTUAL edit bid price is " + updatedSecVBidPrice + "\n");

		//validate edited Bid Price to database saved value
		DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_bid_price", TEST_COMPANYPRODUCT_NUMBER2, updatedSecVBidNum, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbSecVendorBidPrice = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
		Assert.assertEquals(
				dbSecVendorBidPrice[0], updatedSecVBidPrice,"test FAILED, db value for Bid Price should be " + updatedSecVBidPrice);

		//validate edited Bid End Date to database saved value
		DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_end_Date", TEST_COMPANYPRODUCT_NUMBER2, updatedSecVBidNum, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbSecVendorBidEndDate = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
		//System.out.println(updatedSecVBidEndDate);
		Assert.assertEquals(
				dbSecVendorBidEndDate[0], updatedSecVBidEndDate, "Failed, db value for Bid End Date should be " + updatedSecVBidEndDate);
	}

	/**
	 * Test Case Scenario,
	 * Create a future Secondary Vendor Bid with end date = Bid Sheet End Date
	 */
	@Test
	public void editFutureSecondaryBidPriceAndEndDateEqualBidSheetEndDate() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-1427", "Create a future Secondary Vendor Bid with end date = Bid Sheet End Date");
//		ITestResult result = Reporter.getCurrentTestResult();

		//set effective date = current date + 30 as a Future Bid
		String effDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 30);
		pageVendorBids.effectiveDatePicker.clear();
		pageVendorBids.effectiveDatePicker.sendKeys(effDate);
		pageVendorBids.effectiveDatePicker.sendKeys("\t");
		Thread.sleep(1000);
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();

		//Filter TEST_COMPANYPRODUCT_NUMBER3 and Edit Bid Price and End Date
		pageVendorBids.vendorBidsFilterIcon.click();
		pageVendorBids.filterProductNumberTextbox.click();
		pageVendorBids.filterProductNumberTextbox.sendKeys(TEST_COMPANYPRODUCT_NUMBER3);
		pageVendorBids.vendorBidsFilterApplyBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		//open Secondary Vendor Bid popup window
		pageVendorBids.vendorBidsGridCellItem(1, "Alt. Units").click();
		pageVendorBids.waitForSecondaryBidsPopupReturn();
		Random rand = new Random();
		double secVbidPrice = rand.nextInt(10) + rand.nextDouble();
		//assign the above random Secondary VBid Price to the testProduct
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(1, "Price"), String.valueOf(secVbidPrice));
		//assign Bid Sheet End Date to bid End Date
		DBStmtVendorBids.getBidSheetEndDate(DB_CONNECTION_STRING, TEST_BID_SHEET_NAME);
		String[] bidSheetEndDate = ActionsEMW.readSqlResults("DBStmtVendorBids", "Find_Bid_Sheet_End_Date");
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(1, "End Date"), bidSheetEndDate[0]);
		pageVendorBids.secondaryBidsPopupSaveAndCloseBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		boolean editFormPresent = ActionsEMW.isElementPresent(pageVendorBids.bidEditFormCollapseIcon);
		if (editFormPresent) {
			pageVendorBids.bidEditFormCollapseIcon.click();
			new WebDriverWait(driver, 60, 1000).until(
					ExpectedConditions.visibilityOf(pageVendorBids.bidEditFormExpandIcon)
			);
		}
		//reopen Secondary Vendor Bid popup window to collect the prior edited values
		pageVendorBids.vendorBidsGridCellItem(1, "Alt. Units").click();
		pageVendorBids.waitForSecondaryBidsPopupReturn();
		String updatedSecVBidNum = pageVendorBids.secondaryBidsGridCellItem(1, "Vendor #").getText().trim();
		String updatedSecVBidPrice = pageVendorBids.secondaryBidsGridCellItem(1, "Price").getText().trim();
		String updatedSecVBidEndDate = pageVendorBids.secondaryBidsGridCellItem(1, "End Date").getText().trim();

		//When Bid End Date = Bid Sheet End Date, set the Bid End Date on screen + 1 to match database value
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = simpleDateFormat.parse(updatedSecVBidEndDate);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, 1);
		String actualUpdatedSecVBidpEndDate = simpleDateFormat.format(calendar.getTime());
//		Reporter.log("Test Method = " + result.getMethod().getMethodName() + ": " + updatedSecVBidNum + " ACTUAL edit bid price is " + updatedSecVBidPrice + "\n");
		//validate edited Bid Price to database saved value
		DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_bid_price", TEST_COMPANYPRODUCT_NUMBER3, updatedSecVBidNum, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbSecVendorBidPrice = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
		Assert.assertEquals(
				dbSecVendorBidPrice[0], updatedSecVBidPrice, "test FAILED, db value for Bid Price should be " + updatedSecVBidPrice);

		//validate edited Bid End Date to database saved value
		DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_end_Date", TEST_COMPANYPRODUCT_NUMBER3, updatedSecVBidNum, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbSecVendorBidEndDate = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
		//System.out.println(actualUpdatedSecVBidpEndDate);
		Assert.assertEquals(
				dbSecVendorBidEndDate[0], actualUpdatedSecVBidpEndDate, "Failed, db value for Bid End Date should be " + actualUpdatedSecVBidpEndDate);
	}

	/**
	 * Test Case Scenario,
	 * Create a future Secondary Vendor Bid with end date < Bid Sheet End Date
	 */
	@Test
	public void editFutureSecondryBidPriceAndEndDateLessThanBidSheetEndDate() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-1427", "Create a future Secondary Vendor Bid with end date < Bid Sheet End Date");
		//set effective date = current date + 30 as a Future Bid
		String effDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 30);
		pageVendorBids.effectiveDatePicker.clear();
		pageVendorBids.effectiveDatePicker.sendKeys(effDate);
		pageVendorBids.effectiveDatePicker.sendKeys("\t");
		Thread.sleep(1000);
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();

		//Filter TEST_COMPANYPRODUCT_NUMBER4 and Edit Secondary Vendor Bid Price and End Date
		pageVendorBids.vendorBidsFilterIcon.click();
		pageVendorBids.filterProductNumberTextbox.click();
		pageVendorBids.filterProductNumberTextbox.sendKeys(TEST_COMPANYPRODUCT_NUMBER4);
		pageVendorBids.vendorBidsFilterApplyBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		//open Secondary Vendor Bid popup window
		pageVendorBids.vendorBidsGridCellItem(1, "Alt. Units").click();
		pageVendorBids.waitForSecondaryBidsPopupReturn();
		Random rand = new Random();
		double secVbidPrice = rand.nextInt(15) + rand.nextDouble();
		//assign the above random Secondary VBid Price to the testProduct
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(1, "Price"), String.valueOf(secVbidPrice));
		//assign Bid Sheet End Date = today's date + 180 as a end date < bid sheet End Date
		String TEST_COMPANYPRODUCT_NUMBER4EndDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 180);
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(1, "End Date"), TEST_COMPANYPRODUCT_NUMBER4EndDate);
		pageVendorBids.secondaryBidsPopupSaveAndCloseBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		boolean editFormPresent = ActionsEMW.isElementPresent(pageVendorBids.bidEditFormCollapseIcon);
		if (editFormPresent) {
			pageVendorBids.bidEditFormCollapseIcon.click();
			new WebDriverWait(driver, 60, 1000).until(
					ExpectedConditions.visibilityOf(pageVendorBids.bidEditFormExpandIcon)
			);
		}
		//reopen Secondary Vendor Bid popup window to collect the prior edited values
		pageVendorBids.vendorBidsGridCellItem(1, "Alt. Units").click();
		pageVendorBids.waitForSecondaryBidsPopupReturn();
		String updatedSecVBidNum = pageVendorBids.secondaryBidsGridCellItem(1, "Vendor #").getText().trim();
		String updatedSecVBidPrice = pageVendorBids.secondaryBidsGridCellItem(1, "Price").getText().trim();
		String updatedSecVBidEndDate = pageVendorBids.secondaryBidsGridCellItem(1, "End Date").getText().trim();

		//validate edited Bid Price to database saved value
		DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_bid_price", TEST_COMPANYPRODUCT_NUMBER4, updatedSecVBidNum, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbSecVendorBidPrice = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
		Assert.assertEquals(
				dbSecVendorBidPrice[0], updatedSecVBidPrice, "FAILED, db value for Bid Price should be " + updatedSecVBidPrice);

		//validate edited Bid End Date to database saved value
		DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_end_Date", TEST_COMPANYPRODUCT_NUMBER4, updatedSecVBidNum, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbSecVendorBidEndDate = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
		//System.out.println(updatedSecVBidEndDate);
		Assert.assertEquals(
				dbSecVendorBidEndDate[0], updatedSecVBidEndDate, "Failed, db value for Bid End Date should be " + updatedSecVBidEndDate);
	}

	/**
	 * Test Case Scenario,
	 * Edit Secondary Vendor Bid End Dates sooner and later than Vendor Bid End Date, End Date=today’s date to delete a Vendor Bid
	 */
	@Test
	public void editCurrentSecondaryBidPriceAndEndDateEqualCurrentDate() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-1427", "Edit Secondary Vendor Bid End Dates sooner and later than Vendor Bid End Date, End Date=today’s date to delete a Vendor Bid");
//		ITestResult result = Reporter.getCurrentTestResult();
		//set effective date = current date
		String effDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 0);
		pageVendorBids.effectiveDatePicker.clear();
		pageVendorBids.effectiveDatePicker.sendKeys(effDate);
		pageVendorBids.effectiveDatePicker.sendKeys("\t");
		Thread.sleep(1000);
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();

		//Filter TEST_COMPANYPRODUCT_NUMBER5 and Edit Secondary Vendor Bid Price and End Date
		pageVendorBids.vendorBidsFilterIcon.click();
		pageVendorBids.filterProductNumberTextbox.click();
		pageVendorBids.filterProductNumberTextbox.sendKeys(TEST_COMPANYPRODUCT_NUMBER5);
		pageVendorBids.vendorBidsFilterApplyBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		//open Secondary Vendor Bid popup window
		pageVendorBids.vendorBidsGridCellItem(1, "Alt. Units").click();
		pageVendorBids.waitForSecondaryBidsPopupReturn();

		//create a new secondary vendor bid via Add button and fill in all the fields for later validation
		pageVendorBids.secondaryBidsPopupAddBtn.click();
		//stage the date to be used for Edit
		String timeStamp = ActionsEMW.setDateBasedOnToday("MM-dd-yy-HH-mm-ss", 0);
		String editSecVBidNumber = TEST_COMPANYPRODUCT_NUMBER5 + timeStamp;
		String editSecVBidName = TEST_COMPANYPRODUCT_NUMBER5 + timeStamp;
		String editSecVBidUnit = TEST_COMPANYPRODUCT_NUMBER5_SECVBID_UNIT;
		String editSecVBidBrand = timeStamp;
		String editSecVBidConv = "5.0000";
		Random rand = new Random();
		double secVbidPrice = rand.nextInt(15) + rand.nextDouble();
		String editSecVBidPrice = String.valueOf(secVbidPrice);
		String editSecVBidSplit = "Y";
		String editSecVBidTax = "EXC";
		String editSecVBidEndDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 80);
		//assign staged data to each secondary Vendor Bid field
		Integer addSecVBidsRowNum = ActionsEMW.getEMWGridRowCnt(pageVendorBids.secondaryBidsPopupGrid);
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(addSecVBidsRowNum, "Vendor #"), editSecVBidNumber);
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(addSecVBidsRowNum, "Vendor Product Name"), editSecVBidName);
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(addSecVBidsRowNum, "Vendor Unit"), editSecVBidUnit);
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(addSecVBidsRowNum, "Brand"), editSecVBidBrand);
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(addSecVBidsRowNum, "Conv."), editSecVBidConv);
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(addSecVBidsRowNum, "Price"), editSecVBidPrice);
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(addSecVBidsRowNum, "Split"), editSecVBidSplit);
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(addSecVBidsRowNum, "Tax Code"), editSecVBidTax);
		pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(addSecVBidsRowNum, "End Date"), editSecVBidEndDate);
		//save and close the secondary vendor bid popup
		pageVendorBids.secondaryBidsPopupSaveAndCloseBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		boolean editFormPresent = ActionsEMW.isElementPresent(pageVendorBids.bidEditFormCollapseIcon);
		if (editFormPresent) {
			pageVendorBids.bidEditFormCollapseIcon.click();
			new WebDriverWait(driver, 60, 1000).until(
					ExpectedConditions.visibilityOf(pageVendorBids.bidEditFormExpandIcon)
			);
		}

		//reopen Secondary Vendor Bid popup window to collect the prior edited values for Sec Vendor Product Num and Bid Price and End Date
		pageVendorBids.vendorBidsGridCellItem(1, "Alt. Units").click();
		pageVendorBids.waitForSecondaryBidsPopupReturn();
		//find the edited row to validate
		Integer rowNum = ActionsEMW.getEMWGridRowNumByCellValue(pageVendorBids.secondaryBidsPopupGrid, editSecVBidNumber);
		String updatedSecVBidNum = pageVendorBids.secondaryBidsGridCellItem(rowNum, "Vendor #").getText().trim();
		String updatedSecVBidPrice = pageVendorBids.secondaryBidsGridCellItem(rowNum, "Price").getText().trim();
		String updatedSecVBidEndDate = pageVendorBids.secondaryBidsGridCellItem(rowNum, "End Date").getText().trim();

		//validate edited Bid Price to database saved value
		DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_bid_price", TEST_COMPANYPRODUCT_NUMBER5, editSecVBidNumber, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbSecVendorBidPrice = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
		Assert.assertEquals(
				dbSecVendorBidPrice[0], updatedSecVBidPrice, "FAILED, db value for Bid Price should be " + updatedSecVBidPrice);

		//validate edited Bid End Date to database saved value
		DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_end_Date", TEST_COMPANYPRODUCT_NUMBER5, editSecVBidNumber, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbSecVendorBidEndDate = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
		//System.out.println(updatedSecVBidEndDate);
		Assert.assertEquals(
				dbSecVendorBidEndDate[0], updatedSecVBidEndDate, "Failed, db value for Bid End Date should be " + updatedSecVBidEndDate);

		//validate sec vendor bid product number
		DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_product_number", TEST_COMPANYPRODUCT_NUMBER5, editSecVBidNumber, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbSecVBidNumber = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
		Assert.assertEquals(
				dbSecVBidNumber[0], editSecVBidNumber, "Failed, db value for Secondary Vendor Bid Product Number should be " + editSecVBidNumber);

		//validate sec vendor bid product name
		DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_product_name", TEST_COMPANYPRODUCT_NUMBER5, editSecVBidNumber, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbSecVBidName = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
		Assert.assertEquals(
				dbSecVBidName[0], editSecVBidName, "Failed, db value for Secondary Vendor Bid Product Name should be " + editSecVBidName);

		//validate sec vendor bid product unit
		DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_product_unit", TEST_COMPANYPRODUCT_NUMBER5, editSecVBidNumber, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbSecVBidUnit = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
		Assert.assertEquals(
				dbSecVBidUnit[0], editSecVBidUnit, "Failed, db value for Secondary Vendor Bid Product Unit should be " + editSecVBidUnit);

		//validate sec vendor bid product brand
		DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_product_brand", TEST_COMPANYPRODUCT_NUMBER5, editSecVBidNumber, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbSecVBidBrand = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
		Assert.assertEquals(
				dbSecVBidBrand[0], editSecVBidBrand, "Failed, db value for Secondary Vendor Bid Product Brand should be " + editSecVBidBrand);

		//validate sec vendor bid product conversion
		DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_product_conv", TEST_COMPANYPRODUCT_NUMBER5, editSecVBidNumber, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbSecVBidConv = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
		Assert.assertEquals(
				dbSecVBidConv[0], editSecVBidConv, "Failed, db value for Secondary Vendor Bid Product Conversion should be " + editSecVBidConv);

		//validate sec vendor bid product split flag
		DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_split_flag", TEST_COMPANYPRODUCT_NUMBER5, editSecVBidNumber, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbSecVBidSplitFlag = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
		Assert.assertEquals(
				dbSecVBidSplitFlag[0], editSecVBidSplit, "Failed, db value for Secondary Vendor Bid Product Split Flag should be " + editSecVBidSplit);

		//validate sec vendor bid product tax code
		DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_tax_code", TEST_COMPANYPRODUCT_NUMBER5, editSecVBidNumber, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbSecVBidTax = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
		Assert.assertEquals(
				dbSecVBidTax[0], editSecVBidTax, "Failed, db value for Secondary Vendor Bid Product Tax Code should be " + editSecVBidTax);

		//validate sec vendor bid product end date
		DBStmtVendorBids.getSecVendorBid(DB_CONNECTION_STRING, "sec_ven_end_Date", TEST_COMPANYPRODUCT_NUMBER5, editSecVBidNumber, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbSecVBidEndDate = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Secondary_Vendor_Bid");
		Assert.assertEquals(
				dbSecVBidEndDate[0], editSecVBidEndDate, "Failed, db value for Secondary Vendor Bid Product Tax Code should be " + editSecVBidEndDate);

		//////////////////////////////////////////////////////////////
		//Logical Delete Secondary Vendor Bid (i.e. Begin Date = End Date)
		Integer secVBidsRowCnt = ActionsEMW.getEMWGridRowCnt(pageVendorBids.secondaryBidsPopupGrid);
		for (Integer i = 1; i <= secVBidsRowCnt; i++) {
			pageVendorBids.editSecondaryBidsGridCellItem(pageVendorBids.secondaryBidsGridCellItem(i, "End Date"), effDate);
		}
		//save and close the secondary vendor bid popup
		pageVendorBids.secondaryBidsPopupSaveAndCloseBtn.click();
		Waiter.waitForElementToDisappear(driver, pageVendorBids.loadSpinner);
		pageVendorBids.waitFoVendorBidsGridReturn();
		Waiter.waitForElementToDisappear(driver, pageVendorBids.loadSpinner);
		//Click Retrieve button to re-retrieve/refresh the results to make sure changes are updated
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();

		//validate sec vendor bid product is logically deleted
		String classString = pageVendorBids.vendorBidsGridCellItem(1, "Alt. Units").getAttribute("class");
		Assert.assertTrue(
				classString.contains("has-plus"), "Failed, secondary vendor bid is NOT logically deleted.");
	}
}