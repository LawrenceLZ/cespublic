package com.crunchtime.ces.test.bidMaster.bidSheet;

import com.crunchtime.ces.base.BaseTestEMW;
import org.openqa.selenium.Cookie;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Set;

public class TC_BidSheetSimpleWorkflow extends BaseTestEMW {
	private static final String DB_CONNECTION = "AUTO/AUTO@CTAUTO24006.WORLD";
	private static final String TEST_SITE_NAME = "auto-em.net-chef.local";
	private static final String USER_ID = "VBIDS";
	private static final String USER_PWD = "VBIDS";
	private static final String TEST_LOCATION_NAME = "Bubba Gump Shrimp Co";
	private static final String TEST_BID_SHEET_NAME = "TestVendorBids";
	private static final String TEST_COOKIE_NAME = "testCookieName";

	@BeforeClass(alwaysRun = true)
	public void sessionStartUp() throws Exception {
		basePageEMW.openTestSite(TEST_SITE_NAME);
		pageEMWLogin.loginEMW(USER_ID, USER_PWD, TEST_LOCATION_NAME);
//		Set<Cookie> allCookies = driver.manage().getCookies();
//		for (Cookie loadedCookie : allCookies) {
//			System.out.println(String.format("%s -> %s", loadedCookie.getName(), loadedCookie.getValue()));
//			Cookie cookie = new Cookie(loadedCookie.getName(), loadedCookie.getValue());
//			driver.manage().addCookie(cookie);
//		}
//		Object[] cookieArray = driver.manage().getCookies().toArray();
//		System.out.println(Arrays.toString(cookieArray));

	}

	@BeforeMethod(alwaysRun = true)
	public void login() throws Exception {
//		basePageEMW.openTestSite(TEST_SITE_NAME);
//		pageEMWLogin.loginEMW(USER_ID, USER_PWD, TEST_LOCATION_NAME);
//		Set<Cookie> allCookies = driver.manage().getCookies();
//		for (Cookie loadedCookie : allCookies) {
//			System.out.println(String.format("%s -> %s", loadedCookie.getName(), loadedCookie.getValue()));
//		}

		//Navigate to Vendor Bids detail screen with testing Bid Sheet/Vendor/Market combination
//		basePageEMW.navigateToPageOtherTasks();
//		pageOtherTasks.navigateToVendorBidsScreen();
//		pageVendorBids.bidSheetsBtn.click();
//		pageVendorBids.waitForSelectBidSheetsPopupReturn();
//		//Select the testing Bid Sheet
//		pageVendorBids.selectBidSheetsPopupSelectAllCheckbox.click(); //first to deselect all the bid sheets
//		pageVendorBids.selectBidSheetsPopupFilterIcon.click();
//		pageVendorBids.filterBidSheetTextbox.clear();
//		pageVendorBids.filterBidSheetTextbox.sendKeys(testBidSheetName);
//		pageVendorBids.filterBidSheetApplyBtn.click();
//		pageVendorBids.waitForSelectBidSheetsPopupReturn(); //wait for filtered results to return
//		pageVendorBids.selectBidSheetsPopupSelectAllCheckbox.click(); //now to select the filtered bid sheet(s)
//		pageVendorBids.selectBidSheetsPopupApplyBth.click();
//		pageVendorBids.waitForVendorInputCombobxEnable();
//		Thread.sleep(1000);
//		ActionsEMW.setComboBoxDropdownByValue(pageVendorBids.vendorInputCombobox, testVendorName);
//		Thread.sleep(1000);
//		pageVendorBids.marketInputCombobox.click();
//		pageVendorBids.marketInputCombobox.clear();
//		pageVendorBids.marketInputCombobox.sendKeys(testMarketName);
//		pageVendorBids.marketInputCombobox.sendKeys("\t");
//		boolean retrieveEnabled;
//		do {
//			WaiterStatic.waitForOneSecond();
//			retrieveEnabled = pageVendorBids.retrieveBtn.getAttribute("class").contains("disabled");
//			int timer = 0; timer++;
//			if (timer == 120) break;
//		} while (retrieveEnabled);
	}

//	@AfterMethod(alwaysRun = true)
//	public void logOut() throws Exception {
//		boolean closeBtnExist = ActionsEMW.isElementPresent(pageVendorBids.secondaryBidsPopupCloseBtn);
//		if (closeBtnExist) pageVendorBids.secondaryBidsPopupCloseBtn.click();
//		pageEMWLogout.logoutEMW();
//	}

	@Test
	public void demoTestCookie() {
		System.out.println("test is run***************");
		driver.get("http://auto-em.net-chef.local/navigation.do?method=otherTasks");
		System.out.println("test is run***demoTestCookie***************");
		Set<Cookie> allCookies = driver.manage().getCookies();
		for (Cookie loadedCookie : allCookies) {
			System.out.println(String.format("%s -> %s", loadedCookie.getName(), loadedCookie.getValue()));
		}
	}

	@Test
	public void demoTestCookie1() {
		driver.get("http://auto-em.net-chef.local/navigation.do?method=setup");
		System.out.println("test is run***************");
		System.out.println("test is run***demoTestCookie1***************");
		Set<Cookie> allCookies = driver.manage().getCookies();
		for (Cookie loadedCookie : allCookies) {
			System.out.println(String.format("%s -> %s", loadedCookie.getName(), loadedCookie.getValue()));
		}
	}

	@Test
	public void getName() throws Exception{
		ITestResult result = Reporter.getCurrentTestResult();
		String className = result.getTestClass().getName();
		System.out.println(className + "\n");
		System.out.println("demoTestCookie1\t" + "HIGH");
		try {

			String content = "This is the content to write into file";

			File file = new File("C:\\Users\\lzhao\\Downloads\\getName.txt");

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write("\n" + content + "\n");
			bw.write(className + "\n");
			bw.write("demoTestCookie1\t" + "HIGH" + "\n");
			bw.close();

			System.out.println("Done");

		} catch (IOException e) {
			e.printStackTrace();
		}

		Thread.sleep(1000);

	}
}