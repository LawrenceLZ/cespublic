package com.crunchtime.ces.test.bidMaster.vendorBids;

import com.crunchtime.ces.base.BaseTestEMW;
import com.crunchtime.ces.database.bidMaster.vendorBids.DBStmtVendorBids;
import com.crunchtime.ces.helper.ActionsEMW;
import com.crunchtime.ces.helper.DBMethodEMW;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.*;

public class TC_VendorBidsCES8054 extends BaseTestEMW {



	private static final String DB_CONNECTION_STRING = "AUTO/AUTO@CTAUTO24006.WORLD";
	private static final String TEST_SITE_NAME = "auto-em.net-chef.local";
	private static final String USER_ID = "AUTO2";
	private static final String USER_PWD = "AUTO2";
	private static final String TEST_LOCATION_NAME = "Bubba Gump Shrimp Co";
	private static final String TEST_BID_SHEET_NAME_WO = "BID SHEET WITHOUT T_VENDOR_BID RECORD";
	private static final String TEST_BID_SHEET_NAME_INCLUDES_TAX = "BID SHEET PRICE INCLUDES TAX";
	private static final String TEST_VENDOR_NAME_INCLUDES_TAX = "ALa Rocca Seaffod Inc";
	private static final String TEST_VENDOR_NAME = "Adventures By The Sea";
	private static final String TEST_MARKET_NAME = "Breckenridge";

/*
	private static final String DB_CONNECTION_STRING = "im_owner/prcorp@CTPRCORP11g_2.world";
	private static final String TEST_SITE_NAME = "prcorp-qa-em.net-chef.local";
	private static final String USER_ID = "ctsupport";
	private static final String USER_PWD = "password";
	private static final String TEST_LOCATION_NAME = "PRINCESS CRUISES";
	private static final String TEST_BID_SHEET_NAME_WO = "BID SHEET WITHOUT T_VENDOR_BID RECORD";
	private static final String TEST_BID_SHEET_NAME_INCLUDES_TAX = "BID SHEET PRICE INCLUDES TAX";
	private static final String TEST_VENDOR_NAME_INCLUDES_TAX = "American Pacific Corp";
	private static final String TEST_VENDOR_NAME = "American Pacific Corp";
	private static final String TEST_MARKET_NAME = "ACA-Acapulco";
	*/
	private static final String testCompanyProductNumber1 = "AUTOLAL001";
	private static final String testCompanyProductNumber2 = "AUTOLAL013";
	private static final String testCompanyProductNumber3 = "AUTOLAL003";

	@BeforeMethod(alwaysRun = true)
	public void login() throws Exception {
		DBMethodEMW.executeSqlStmt(DB_CONNECTION_STRING, DBStmtVendorBids.deleteVendorBidProductAuditForBidSheet(TEST_BID_SHEET_NAME_INCLUDES_TAX));
		DBMethodEMW.executeSqlStmt(DB_CONNECTION_STRING, DBStmtVendorBids.deleteVendorBidProductRecordsForBidSheet(TEST_BID_SHEET_NAME_INCLUDES_TAX));
		DBMethodEMW.executeSqlStmt(DB_CONNECTION_STRING, DBStmtVendorBids.getStmtDeleteVendorBidRecord(TEST_BID_SHEET_NAME_WO));

		basePageEMW.openTestSite(TEST_SITE_NAME);
		pageEMWLogin.loginEMW(USER_ID, USER_PWD, TEST_LOCATION_NAME);
		//Navigate to Vendor Bids detail screen with testing Bid Sheet/Vendor/Market combination
		basePageEMW.navigateToPageOtherTasks();
		pageOtherTasks.navigateToVendorBidsScreen();
		pageVendorBids.bidSheetsBtn.click();
		pageVendorBids.waitForSelectBidSheetsPopupReturn();

	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageEMWLogout.logoutEMW();
	}

	@Test
	public void vendorBidsRetrievalNoVendorBidRecord() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-8054", "When there are no t_vendor_bid records, products should display in the Vendor Bids" +
				"Screen");

		//Select the testing Bid Sheet
		pageVendorBids.selectBidSheetsPopupSelectAllCheckbox.click(); //first to deselect all the bid sheets
		pageVendorBids.selectBidSheetsPopupFilterIcon.click();
		pageVendorBids.filterBidSheetTextbox.clear();
		pageVendorBids.filterBidSheetTextbox.sendKeys(TEST_BID_SHEET_NAME_WO);
		pageVendorBids.filterBidSheetApplyBtn.click();
		pageVendorBids.waitForSelectBidSheetsPopupReturn(); //wait for filtered results to return
		pageVendorBids.selectBidSheetsPopupSelectAllCheckbox.click(); //now to select the filtered bid sheet(s)
		pageVendorBids.selectBidSheetsPopupApplyBth.click();
		//define vendor and market and then Retrieve
		pageVendorBids.vendorInputCombobox.click();
		pageVendorBids.vendorInputComboboxTrigger.click();
		Thread.sleep(1000);
		ActionsEMW.setComboBoxDropdownByValue(driver, pageVendorBids.vendorInputCombobox, TEST_VENDOR_NAME);
		Thread.sleep(1000);
		pageVendorBids.marketInputCombobox.click();
		pageVendorBids.marketInputCombobox.clear();
		pageVendorBids.marketInputCombobox.sendKeys(TEST_MARKET_NAME);
		pageVendorBids.marketInputCombobox.sendKeys("\t");
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.visibilityOf(pageVendorBids.effectiveDatePicker)
		);
		Thread.sleep(1000);
		String effDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 0);
		pageVendorBids.effectiveDatePicker.clear();
		pageVendorBids.effectiveDatePicker.sendKeys(effDate);
		pageVendorBids.effectiveDatePicker.sendKeys("\t");
		Thread.sleep(1000);
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();

		//check to make sure there is no t_vendor_bid record for the bid_sheet
		Map<String, List<Object>> vendorBidRecordPK = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING,DBStmtVendorBids.getStmtVendorBidRecord
				(TEST_BID_SHEET_NAME_WO, TEST_MARKET_NAME, TEST_VENDOR_NAME));
		Assert.assertTrue(vendorBidRecordPK.get("COUNT_ROWS").get(0).toString().contentEquals("0"), "Vendor Bid Record Exists, test cannot run.");

		try {
			Assert.assertTrue(pageVendorBids.vendorBidsGridCellItem(1, "PRODUCT NAME").isDisplayed(), "Cannot see any products in Vendor Bids Screen");
		}
		catch(NoSuchElementException e){
			Assert.fail("Cannot see any products in Vendor Bids Screen");
		}
	}

	@Test
	public void vendorBidsRetrievalWithVendorBidRecord() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-8054", "When there is a t_vendor_bid record with a tax flag of 'N', products should display in the Vendor Bids" +
				"Screen");

		//Select the testing Bid Sheet
		pageVendorBids.selectBidSheetsPopupSelectAllCheckbox.click(); //first to deselect all the bid sheets
		pageVendorBids.selectBidSheetsPopupFilterIcon.click();
		pageVendorBids.filterBidSheetTextbox.clear();
		pageVendorBids.filterBidSheetTextbox.sendKeys(TEST_BID_SHEET_NAME_WO);
		pageVendorBids.filterBidSheetApplyBtn.click();
		pageVendorBids.waitForSelectBidSheetsPopupReturn(); //wait for filtered results to return
		pageVendorBids.selectBidSheetsPopupSelectAllCheckbox.click(); //now to select the filtered bid sheet(s)
		pageVendorBids.selectBidSheetsPopupApplyBth.click();
		//define vendor and market and then Retrieve
		pageVendorBids.vendorInputCombobox.click();
		pageVendorBids.vendorInputComboboxTrigger.click();
		Thread.sleep(1000);
		ActionsEMW.setComboBoxDropdownByValue(driver, pageVendorBids.vendorInputCombobox, TEST_VENDOR_NAME);
		Thread.sleep(1000);
		pageVendorBids.marketInputCombobox.click();
		pageVendorBids.marketInputCombobox.clear();
		pageVendorBids.marketInputCombobox.sendKeys(TEST_MARKET_NAME);
		pageVendorBids.marketInputCombobox.sendKeys("\t");
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.visibilityOf(pageVendorBids.effectiveDatePicker)
		);
		Thread.sleep(1000);
		String effDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 0);
		pageVendorBids.effectiveDatePicker.clear();
		pageVendorBids.effectiveDatePicker.sendKeys(effDate);
		pageVendorBids.effectiveDatePicker.sendKeys("\t");
		Thread.sleep(1000);
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();

		//check to make sure there is a t_vendor_bid record for the bid_sheet
		DBMethodEMW.executeSqlStmt(DB_CONNECTION_STRING, DBStmtVendorBids.getStmtInsertVendorBidRecord(TEST_BID_SHEET_NAME_WO, TEST_VENDOR_NAME, TEST_MARKET_NAME));

		Map<String, List<Object>> vendorBidRecordPK= DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING,DBStmtVendorBids.getStmtVendorBidRecord
				(TEST_BID_SHEET_NAME_WO, TEST_MARKET_NAME, TEST_VENDOR_NAME));

		Assert.assertFalse(vendorBidRecordPK.get("COUNT_ROWS").get(0).toString().contentEquals("0"), "Vendor Bid Record does not exist, test cannot run.");

		try {
			Assert.assertTrue(pageVendorBids.vendorBidsGridCellItem(1, "PRODUCT NAME").isDisplayed(), "Cannot see any products in Vendor Bids Screen");
		}
		catch(NoSuchElementException e){
			Assert.fail("Cannot see any products in Vendor Bids Screen");
		}
	}

	@Test
	public void vendorBidsCreateVendorBidIncludesTax() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-7997", "Create a Vendor Bid when Tax Flag = Price Includes Tax");

		//Select the testing Bid Sheet
		pageVendorBids.selectBidSheetsPopupSelectAllCheckbox.click(); //first to deselect all the bid sheets
		pageVendorBids.selectBidSheetsPopupFilterIcon.click();
		pageVendorBids.filterBidSheetTextbox.clear();
		pageVendorBids.filterBidSheetTextbox.sendKeys(TEST_BID_SHEET_NAME_INCLUDES_TAX);
		pageVendorBids.filterBidSheetApplyBtn.click();
		pageVendorBids.waitForSelectBidSheetsPopupReturn(); //wait for filtered results to return
		pageVendorBids.selectBidSheetsPopupSelectAllCheckbox.click(); //now to select the filtered bid sheet(s)
		pageVendorBids.selectBidSheetsPopupApplyBth.click();
		//define vendor and market and then Retrieve
		pageVendorBids.vendorInputCombobox.click();
		pageVendorBids.vendorInputComboboxTrigger.click();
		Thread.sleep(1000);
		ActionsEMW.setComboBoxDropdownByValue(driver, pageVendorBids.vendorInputCombobox, TEST_VENDOR_NAME_INCLUDES_TAX);
		Thread.sleep(1000);
		pageVendorBids.marketInputCombobox.click();
		pageVendorBids.marketInputCombobox.clear();
		pageVendorBids.marketInputCombobox.sendKeys(TEST_MARKET_NAME);
		pageVendorBids.marketInputCombobox.sendKeys("\t");
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.visibilityOf(pageVendorBids.effectiveDatePicker)
		);
		Thread.sleep(1000);
		String effDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 0);
		pageVendorBids.effectiveDatePicker.clear();
		pageVendorBids.effectiveDatePicker.sendKeys(effDate);
		pageVendorBids.effectiveDatePicker.sendKeys("\t");
		Thread.sleep(1000);
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();

		pageVendorBids.vendorBidsGridCellItem(1, "Product #").click();
		pageVendorBids.waitFoVendorBidsEditFormReturn();
		pageVendorBids.bidEditFormPriceTextbox.clear();
		Random rand1 = new Random();
		double bidPrice1 = rand1.nextInt(10) + 0.50;
		//assign a random bid price to testProduct
		pageVendorBids.bidEditFormPriceTextbox.sendKeys(String.valueOf(bidPrice1));

		//assign a conversion of 1
		pageVendorBids.bidEditFormConversionTextbox.sendKeys("1");
		pageVendorBids.bidEditFormConversionTextbox.sendKeys(Keys.TAB);

		//select a tax code
		pageVendorBids.bidEditFormVendorProdTaxComboboxInput.click();
		pageVendorBids.bidEditFormVendorProdTaxComboboxTrigger.click();

		ActionsEMW.setComboBoxDropdownByValue(driver, pageVendorBids.bidEditFormVendorProdTaxComboboxInput, "BT");


		//Get bid sheet end date which should be the end date for the new bid

//		DBStmtVendorBids.getBidSheetEndDate(DB_CONNECTION_STRING, TEST_BID_SHEET_NAME_INCLUDES_TAX);
//		String[] bidSheetEndDate = ActionsEMW.readSqlResults("DBStmtVendorBids", "Find_Bid_Sheet_End_Date");
//		String[] bidSheetEndDateTokens = bidSheetEndDate[0].split("/");
		Map<String, List<Object>> map = DBMethodEMW.getSelectSqlMap(DB_CONNECTION_STRING,DBStmtVendorBids.getStmtFindBidSheetEndDate(TEST_BID_SHEET_NAME_INCLUDES_TAX));
		String[] bidSheetEndDateTokens = map.get("bid_sheet_end_date".toUpperCase()).get(0).toString().split("/");
		int year = Integer.parseInt(bidSheetEndDateTokens[2]);
		int month = Integer.parseInt(bidSheetEndDateTokens[0]);
		int day = Integer.parseInt(bidSheetEndDateTokens[1]);

		Calendar cal = new GregorianCalendar(year,month,day);
		cal.set(year, month - 1, day); //months start with 0 so we need to subtract one here

		SimpleDateFormat dateFormatDefined = new SimpleDateFormat("MM/dd/yyyy");
		String date = dateFormatDefined.format(new Date());
		date = dateFormatDefined.format(cal.getTime());


		Assert.assertTrue(pageVendorBids.bidEditFormEndDateTextbox.getAttribute("value").contentEquals(date), "The Vendor Bid Form is not defaulting the bid sheet end date.");

		pageVendorBids.bidEditFormCollapseIcon.click();

			new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.visibilityOf(pageVendorBids.bidEditFormExpandIcon)
		);
		Thread.sleep(1000);


		String updatedVbidpTaxedPrice1 = pageVendorBids.vendorBidsGridCellItem(1, "price").getText().trim();
		String updatedVbidpEndDate1 = pageVendorBids.vendorBidsGridCellItem(1, "Bid End Date").getText().trim();

		//validate screen end date to bid sheet end date
		Assert.assertTrue(updatedVbidpEndDate1.contentEquals(date), "End date in grid is not the correct end date. The end date should be " + date);

		//validate entered Bid Price to database saved value
		DBStmtVendorBids.getVendorBid(DB_CONNECTION_STRING, "ven_bid_price_taxed", testCompanyProductNumber2, TEST_VENDOR_NAME_INCLUDES_TAX, TEST_MARKET_NAME, effDate);
		String[] dbVendorBidPrice = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Vendor_Bid");
		Assert.assertTrue(dbVendorBidPrice[0].contentEquals(updatedVbidpTaxedPrice1), "Failed, db value for Bid Price should be " + updatedVbidpTaxedPrice1);

		//	cal.set(year, month, day);
		cal.add(cal.DAY_OF_MONTH, 1);

		// the db will hold a value of End Date + 1 for the end date
		String endDatePlusOne = dateFormatDefined.format(cal.getTime());

		//validate edited Bid End Date to database saved value
		DBStmtVendorBids.getVendorBid(DB_CONNECTION_STRING, "ven_end_Date", testCompanyProductNumber2, TEST_VENDOR_NAME_INCLUDES_TAX, TEST_MARKET_NAME, effDate);
		String[] dbVendorBidEndDate = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Vendor_Bid");

		Assert.assertTrue(dbVendorBidEndDate[0].contentEquals(endDatePlusOne), "Failed, db value for Bid End Date should be " + endDatePlusOne);


	}
}