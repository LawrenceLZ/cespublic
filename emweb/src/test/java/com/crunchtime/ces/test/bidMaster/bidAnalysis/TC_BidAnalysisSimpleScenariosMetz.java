package com.crunchtime.ces.test.bidMaster.bidAnalysis;

import com.crunchtime.ces.base.BaseTestEMW;
import com.crunchtime.ces.database.bidMaster.bidAnalysis.DBStmtBidAnalysis;
import com.crunchtime.ces.helper.ActionsEMW;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class TC_BidAnalysisSimpleScenariosMetz extends BaseTestEMW {
	private static final String TEST_SITE_NAME = "auto-em.net-chef.local";
	private static final String USER_ID = "AUTO";
	private static final String USER_PWD = "AUTO";
	private static final String TEST_LOCATION_NAME = "Bubba Gump Shrimp Co";
	private static final String TEST_BID_SHEET_NAME = "AUTOMATION TEST DO NOT USE - LAL";
	private static final String TEST_MARKET_NAME = "Breckenridge";
	private static final String DB_CONNECTION_STRING = "AUTO/AUTO@CTAUTO24006.WORLD";
	private static final String SUB_CATEGORY_NAME = "Food - Bakery";

	@DataProvider(name = "EffectiveDate")
	public Object[][] createEffectiveDate() {
		String futureEffDate = null;
		String currentEffDate = null;
		try {
			currentEffDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 0);
			futureEffDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 90);
		} catch (ParseException e) {
			Assert.fail("Couldn't set up effective dates");
		}
		return new Object[][]{
				{currentEffDate},
				{futureEffDate},
		};
	}

	//@BeforeMethod(alwaysRun = true)
	public void setup(String effDate) throws Exception {
		//data setup
		DBStmtBidAnalysis.setupData(DB_CONNECTION_STRING, effDate);

		basePageEMW.openTestSite(TEST_SITE_NAME);
		pageEMWLogin.loginEMW(USER_ID, USER_PWD, TEST_LOCATION_NAME);
		basePageEMW.navigateToPageOtherTasks();
		pageOtherTasks.navigateToBidAnalysisScreen();
		Thread.sleep(3000);
		pageBidAnalysis.bidSheetsBtn.click();
		pageBidAnalysis.waitForSelectBidSheetsPopupReturn();
		//Select the testing Bid Sheet
		pageBidAnalysis.selectBidSheetsPopupFilterIcon.click();
		pageBidAnalysis.filterBidSheetTextbox.clear();
		pageBidAnalysis.filterBidSheetTextbox.sendKeys(TEST_BID_SHEET_NAME);
		pageBidAnalysis.filterBidSheetApplyBtn.click();
		pageBidAnalysis.waitForSelectBidSheetsPopupReturn();
		pageBidAnalysis.selectBidSheetsPopupApplyBth.click();

		ActionsEMW.setComboBoxDropdownByValue(driver, pageBidAnalysis.marketInputCombobox, TEST_MARKET_NAME);

		Waiter.waitForElementToBeVisible(driver, pageBidAnalysis.effectiveDatePicker);

		pageBidAnalysis.effectiveDatePicker.clear();
		pageBidAnalysis.effectiveDatePicker.sendKeys(effDate);
		pageBidAnalysis.effectiveDatePicker.sendKeys("\t");

		// replace if we get a Waiter method for css mask for enabling button
		Waiter.waitForElementToBeVisible(driver, pageBidAnalysis.retrieveBtn);

		pageBidAnalysis.retrieveBtn.click();

		pageBidAnalysis.waitForBidAnalysisGridReturn();
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown() throws Exception {
		pageEMWLogout.logoutEMW();

		//check for dupe contracts
		DBStmtBidAnalysis.checkDupeContracts(DB_CONNECTION_STRING, TEST_BID_SHEET_NAME);
		String[] dupeContractList = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.checkDupeContractsFileName);
		Assert.assertTrue(dupeContractList.length == 1, "There are dupe contracts, see file Check_Dupe_contracts.txt for more information.");
	}


	@Test(dataProvider = "EffectiveDate")
	public void Filter(String effDate) throws Exception {

		ActionsEMW.setTestMethodDescription("ces-4089", "This tests filtering the Bid Analysis screen.");

		setup(effDate);

		//open the filter popup
		pageBidAnalysis.filterBidAnalysisGrid.click();

		pageBidAnalysis.filterSubCategoryRadioButton.click();
		//select a Category value
		ActionsEMW.setComboBoxDropdownByValue(driver, pageBidAnalysis.filterSubcategoryDropdown, SUB_CATEGORY_NAME);

		pageBidAnalysis.filterApplyBtn.click();

		Waiter.waitForElementToDisappear(driver, pageBidAnalysis.loadSpinner);

		// get the list of products on screen
		String[] testProductNumbers = pageBidAnalysis.getProductNumbers();

		//validate Contract Price to database saved value
		DBStmtBidAnalysis.getProductsFilteredByCategory(DB_CONNECTION_STRING, SUB_CATEGORY_NAME, TEST_BID_SHEET_NAME, effDate);
		String[] filteredProducts = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getProductsFilteredByCategoryFileName);

		for (int i = 0; i < filteredProducts.length; i++) {
			Assert.assertTrue(filteredProducts[i].equals(testProductNumbers[i]), "The products on the screen do not match the DB list of filtered products.");
		}
	}

	@Test(dataProvider = "EffectiveDate")
	public void bidAwardsSetup(String effDate) throws Exception {

		ActionsEMW.setTestMethodDescription("ces-4093", "This makes sure the the Bid Awards screen shows the correct vendors for the bids.");

		setup(effDate);
		// get vendornames that have contracts
		// get vendor and product list to use in test
		String[] testVendorNames = pageBidAnalysis.getVendorNames();
		String[] testProductNumbers = pageBidAnalysis.getProductNumbers();

		int productIndex, columnIndex;
		List<String> contractVendorList = new ArrayList<>();

		for (int i = 0; i < testVendorNames.length; i++) {
			for (int j = 0; j < testProductNumbers.length; j++) {
				if (pageBidAnalysis.bidAnalysisCheckIfGridCellItemSelected(j + 1, i + 1)) {
					contractVendorList.add(testVendorNames[i]);
					break;
				}
			}
		}

		String[] contractVendorNameList = contractVendorList.toArray(new String[contractVendorList.size()]);

		pageBidAnalysis.bidAwards.click();

		Waiter.waitForElementToDisappear(driver, pageBidAnalysis.loadSpinner);

		Assert.assertTrue(pageBidAnalysis.bidAwardsSelectPopup.isDisplayed(), "The Bid Awards Setup Popup is not visible");

		String[] bidAwardsVendorNames = pageBidAnalysis.getBidAwardsVendorNames();

		for (int i = 0; i < contractVendorNameList.length; i++) {
			Assert.assertEquals(contractVendorNameList[i], bidAwardsVendorNames[i], "The vendors are not all correct");
		}

		pageBidAnalysis.bidAwardsSelectPopupCloseButton.click();
	}

	@Test(dataProvider = "EffectiveDate")
	public void productDetailsPopup(String effDate) throws Exception {

		ActionsEMW.setTestMethodDescription("ces-4092", "This tests data in the Product Details popup from the Bid Analysis summary screen..");

		setup(effDate);
		pageBidAnalysis.clickFirstHyperlink();

		Waiter.waitForElementToBeVisible(driver, pageBidAnalysis.productDetailsPopup);

		String[] productDetails = pageBidAnalysis.getProductDetails();

		Assert.assertEquals(pageBidAnalysis.bidAnalysisLockedGridCellItem(1, 1).getText(), productDetails[0], "Product Names are not equal");
		Assert.assertEquals(pageBidAnalysis.bidAnalysisLockedGridCellItem(1, 2).getText(), productDetails[1], "Product Numbers are not equal");
		Assert.assertEquals(pageBidAnalysis.bidAnalysisLockedGridCellItem(1, 3).getText(), productDetails[2], "Inventory Units are not equal");
		Assert.assertEquals(TEST_MARKET_NAME, productDetails[3], "Markets are not equal");
		Assert.assertEquals(effDate, productDetails[4], "Effective Dates are not equal");

		pageBidAnalysis.productDetailsPopupCloseButton.click();
	}

	@Test(dataProvider = "EffectiveDate")
	public void createSingleContract(String effDate) throws Exception {

		ActionsEMW.setTestMethodDescription("ces-4090", "This tests creating an individual contract from the Bid Analysis Contract Selection section.");

		setup(effDate);

		// get vendor and product list to use in test
		String[] testVendorNames = pageBidAnalysis.getVendorNames();
		String[] testProductNumbers = pageBidAnalysis.getProductNumbers();

		pageBidAnalysis.waitForBidAnalysisGridReturn();

		//This assert checks to make sure there isn't already a contract for the product. There shouldn't be due to the setup script, so this is just a failsafe.
		Assert.assertFalse(pageBidAnalysis.bidAnalysisCheckIfGridCellItemSelected(1, 1), "Data setup is incorrect, test will fail.");

		//click the price for the first product, first vendor.
		pageBidAnalysis.bidAnalysisGridCellItem(1, 1).click();

		Waiter.waitForElementToDisappear(driver, pageBidAnalysis.loadSpinner);

		Assert.assertTrue(pageBidAnalysis.bidAnalysisCheckIfGridCellItemSelected(1, 1), "There is no checkmark for the contracted product.");

		//assign Bid Sheet End Date to bid End Date
		DBStmtBidAnalysis.getBidSheetEndDatePlusOne(DB_CONNECTION_STRING, TEST_BID_SHEET_NAME);
		String[] bidSheetEndDatePlusOne = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBidSheetEndDateFileName);

		String updatedContractPrice1 = pageBidAnalysis.bidAnalysisGridCellItem(1, 1).getText();

		//validate Contract Price to database saved value
		DBStmtBidAnalysis.getContract(DB_CONNECTION_STRING, "con_price", testProductNumbers[0], TEST_MARKET_NAME, effDate);
		String[] dbContractPrice = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getContractFileName);
		Assert.assertEquals(dbContractPrice[0], updatedContractPrice1, "Failed, db value for Contract Price should be " + updatedContractPrice1);

		//validate Contract Begin Date to database saved value
		DBStmtBidAnalysis.getContract(DB_CONNECTION_STRING, "con_begin_Date", testProductNumbers[0], TEST_MARKET_NAME, effDate);
		String[] dbContractBeginDate = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getContractFileName);

		Assert.assertEquals(dbContractBeginDate[0], effDate, "Failed, db value for Bid Begin Date should be " + effDate);

		//validate edited Contract End Date to database saved value
		DBStmtBidAnalysis.getContract(DB_CONNECTION_STRING, "con_end_Date", testProductNumbers[0], TEST_MARKET_NAME, effDate);
		String[] dbContractEndDate = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getContractFileName);

		Assert.assertEquals(dbContractEndDate[0], bidSheetEndDatePlusOne[0], "Failed, db value for Bid End Date should be " + bidSheetEndDatePlusOne[0]);

		//validate Contract Vendor to database saved value
		DBStmtBidAnalysis.getContract(DB_CONNECTION_STRING, "con_vendor", testProductNumbers[0], TEST_MARKET_NAME, effDate);
		String[] dbVendor = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getContractFileName);

		Assert.assertEquals(dbVendor[0], testVendorNames[0], "Failed, db value for vendor should be " + testVendorNames[0]);
	}

	@Test(dataProvider = "EffectiveDate")
	public void endSingleContract(String effDate) throws Exception {
		ActionsEMW.setTestMethodDescription("ces-4089", "This tests ending a single contract through the Bid Analysis Contract Selection screen.");

		setup(effDate);

		// get product list to use in test
		String[] testProductNumbers = pageBidAnalysis.getProductNumbers();

		pageBidAnalysis.waitForBidAnalysisGridReturn();

		//store the contract pk for the contract for the effective date
		DBStmtBidAnalysis.getContract(DB_CONNECTION_STRING, "con_price_pk", testProductNumbers[1], TEST_MARKET_NAME, effDate);
		String[] sqlResults = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getContractFileName);

		String dbPreviousContractPricePK = sqlResults[0];

		Assert.assertTrue(pageBidAnalysis.bidAnalysisCheckIfGridCellItemSelected(2, 3), "Data setup was incomplete, the contract to be ended does not exist");

		pageBidAnalysis.bidAnalysisGridCellItem(2, 3).click();

		Waiter.waitForElementToDisappear(driver, pageBidAnalysis.loadSpinner);

		//validate edited Bid End Date to database saved value
		DBStmtBidAnalysis.getContractBasedOnPK(DB_CONNECTION_STRING, "con_end_Date", dbPreviousContractPricePK);
		String[] dbContractEndDate = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getContractBasedOnPKFileName);

		Assert.assertEquals(dbContractEndDate[0], effDate, "Failed, db value for Bid End Date should be " + effDate);
	}

	@Test(dataProvider = "EffectiveDate")
	public void contractPageBest(String effDate) throws Exception {

		ActionsEMW.setTestMethodDescription("ces-4089", "This tests using Contract Page Best for the Bid Analysis screen.");

		//to do change sql which checks contracts to only look at those from the page
		setup(effDate);

		//assign Bid Sheet End Date to bid End Date
		DBStmtBidAnalysis.getBidSheetEndDatePlusOne(DB_CONNECTION_STRING, TEST_BID_SHEET_NAME);
		String[] bidSheetEndDatePlusOne = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBidSheetEndDateFileName);

		//get the pks of contracts already on the screen
		DBStmtBidAnalysis.getBidSheetContracts(DB_CONNECTION_STRING, "con_price_pk", TEST_BID_SHEET_NAME, TEST_MARKET_NAME, effDate);
		String[] contractPKs = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBidSheetContractsFileName);

		//get the list of the vendor with the best price per product
		DBStmtBidAnalysis.getBestPrices(DB_CONNECTION_STRING, effDate, TEST_BID_SHEET_NAME, TEST_MARKET_NAME);
		//	String[] bestProductsVendors = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBestPricesFileName);
		String[][] bestProductsVendors = DBStmtBidAnalysis.readSQLMultRecordsPerLine(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBestPricesFileName);

		int bestProductsVendorsLength = bestProductsVendors.length;
		String[] bestVendors = new String[bestProductsVendors.length];
		String[] bestProducts = new String[bestProductsVendors.length];

		for (int i = 0; i < bestProductsVendorsLength; i++) {
			bestProducts[i] = bestProductsVendors[i][0];
			bestVendors[i] = bestProductsVendors[i][1];
		}

		//click the contract page best button
		pageBidAnalysis.contractPageBest.click();

		//catch message for products with same price
		//to do  check colors

		Waiter.waitForElementToDisappear(driver, pageBidAnalysis.loadSpinner);

		//validate the vendors for the contracts
		DBStmtBidAnalysis.getBidSheetContracts(DB_CONNECTION_STRING, "con_vendor", TEST_BID_SHEET_NAME, TEST_MARKET_NAME, effDate);
		String[] contractVendors = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBidSheetContractsFileName);

		for (int i = 0; i < bestProductsVendorsLength; i++) {
			Assert.assertEquals(contractVendors[i], bestVendors[i], "Vendor is not correct for product " + bestProducts[i]);
		}

		//check to see that the best price for each product has a checkmark
		int productIndex, columnIndex;

		for (int i = 0; i < bestProductsVendorsLength; i++) {
			productIndex = pageBidAnalysis.bidAnalysisProductIndex(bestProducts[i]);
			columnIndex = pageBidAnalysis.bidAnalysisVendorIndex(bestVendors[i]);

			Assert.assertTrue(pageBidAnalysis.bidAnalysisCheckIfGridCellItemSelected(productIndex + 1, columnIndex + 1), "Checkmark is not displayed for product " +
					bestProducts[i] + "and vendor " + bestVendors[i]);
		}

		//check to see that the selected price for each product is the one that is in the UI defined as the best price (color should be green)

		for (int i = 0; i < bestProductsVendorsLength; i++) {
			productIndex = pageBidAnalysis.bidAnalysisProductIndex(bestProducts[i]);
			columnIndex = pageBidAnalysis.bidAnalysisVendorIndex(bestVendors[i]);

			Assert.assertTrue(pageBidAnalysis.bidAnalysisCheckIfGridCellItemBestPrice(productIndex + 1, columnIndex + 1), "The best price is not selected for product " +
					bestProducts[i] + "and vendor " + bestVendors[i]);
		}

		//validate the begin dates
		DBStmtBidAnalysis.getBidSheetContracts(DB_CONNECTION_STRING, "con_begin_Date", TEST_BID_SHEET_NAME, TEST_MARKET_NAME, effDate);
		String[] beginDates = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBidSheetContractsFileName);

		for (int i = 0; i < bestProductsVendorsLength; i++) {
			Assert.assertEquals(beginDates[i], effDate, "Begin Date is incorrect for product " + bestProducts[i]);
		}

		//validate the end dates
		DBStmtBidAnalysis.getBidSheetContracts(DB_CONNECTION_STRING, "con_end_Date", TEST_BID_SHEET_NAME, TEST_MARKET_NAME, effDate);
		String[] endDates = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBidSheetContractsFileName);

		for (int i = 0; i < bestProductsVendorsLength; i++) {
			Assert.assertEquals(endDates[i], bidSheetEndDatePlusOne[0], "End Date is incorrect for product " + bestProducts[i]);
		}

		//validate that any previously existing contracts were properly ended.
		for (int i = 0; i < contractPKs.length; i++) {
			DBStmtBidAnalysis.getContractBasedOnPK(DB_CONNECTION_STRING, "con_end_Date", contractPKs[i]);
			String[] endDate = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getContractBasedOnPKFileName);
			Assert.assertEquals(endDate[0], effDate, "The contract for " + contractPKs[i] + " was not properly ended. End date should be "
					+ effDate);
		}
	}

	@Test(dataProvider = "EffectiveDate")
	public void endPageContracts(String effDate) throws Exception {

		ActionsEMW.setTestMethodDescription("ces-4089", "This tests using End Page Contracts for the Bid Analysis screen.");

		//to do change sql which checks contracts to only look at those from the page
		setup(effDate);

		// get vendor and product list to use in test
		String[] testVendorNames = pageBidAnalysis.getVendorNames();
		String[] testProductNumbers = pageBidAnalysis.getProductNumbers();

		int numTestVendors = testVendorNames.length;
		int numTestProducts = testProductNumbers.length;

		//store the contract pk for the contract for the effective date
		DBStmtBidAnalysis.getBidSheetContracts(DB_CONNECTION_STRING, "con_price_pk", TEST_BID_SHEET_NAME, TEST_MARKET_NAME, effDate);
		String[] contractPKs = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBidSheetContractsFileName);

		//click the contract page best button
		pageBidAnalysis.endPageContracts.click();

		Waiter.waitForElementToDisappear(driver, pageBidAnalysis.loadSpinner);

		//check to see that no prices for any product has checkmarks

		for (int i = 0; i < numTestProducts; i++) {
			for (int j = 0; j < numTestVendors; j++) {
				Assert.assertFalse(pageBidAnalysis.bidAnalysisCheckIfGridCellItemSelected(i + 1, j + 1), "The checkmarks for product " + testProductNumbers[i]
						+ " and Vendor " + testVendorNames[j] + " have not disappeared.");
			}
		}

		//validate that any previously existing contracts were properly ended.
		for (int i = 0; i < contractPKs.length; i++) {
			DBStmtBidAnalysis.getContractBasedOnPK(DB_CONNECTION_STRING, "con_end_Date", contractPKs[i]);
			String[] endDate = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getContractBasedOnPKFileName);
			Assert.assertEquals(endDate[0], effDate, "A previously existing contract was not properly ended.");
		}
	}

	@Test(dataProvider = "EffectiveDate")
	public void contractAllBest(String effDate) throws Exception {

		ActionsEMW.setTestMethodDescription("ces-4089", "This tests using Contract All Best for the Bid Analysis screen. NOTE: at this point there is not enough data for this test"
		+ " to include bids on pages other than the current one.");

		setup(effDate);

		//assign Bid Sheet End Date to bid End Date
		DBStmtBidAnalysis.getBidSheetEndDatePlusOne(DB_CONNECTION_STRING, TEST_BID_SHEET_NAME);
		String[] bidSheetEndDatePlusOne = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBidSheetEndDateFileName);

		//get the pks of contracts already on the screen
		DBStmtBidAnalysis.getBidSheetContracts(DB_CONNECTION_STRING, "con_price_pk", TEST_BID_SHEET_NAME, TEST_MARKET_NAME, effDate);
		String[] contractPKs = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBidSheetContractsFileName);

		//get the list of the vendor with the best price per product
		DBStmtBidAnalysis.getBestPrices(DB_CONNECTION_STRING, effDate, TEST_BID_SHEET_NAME, TEST_MARKET_NAME);
		//	String[] bestProductsVendors = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBestPricesFileName);
		String[][] bestProductsVendors = DBStmtBidAnalysis.readSQLMultRecordsPerLine(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBestPricesFileName);

		int bestProductsVendorsLength = bestProductsVendors.length;
		String[] bestVendors = new String[bestProductsVendors.length];
		String[] bestProducts = new String[bestProductsVendors.length];

		for (int i = 0; i < bestProductsVendorsLength; i++) {
			bestProducts[i] = bestProductsVendors[i][0];
			bestVendors[i] = bestProductsVendors[i][1];
		}

		//click the contract page best button
		pageBidAnalysis.contractAllBest.click();

		//catch message for products with same price
		//to do  check colors

		Waiter.waitForElementToDisappear(driver, pageBidAnalysis.loadSpinner);

		//validate the vendors for the contracts
		DBStmtBidAnalysis.getBidSheetContracts(DB_CONNECTION_STRING, "con_vendor", TEST_BID_SHEET_NAME, TEST_MARKET_NAME, effDate);
		String[] contractVendors = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBidSheetContractsFileName);

		for (int i = 0; i < bestProductsVendorsLength; i++) {
			Assert.assertEquals(contractVendors[i], bestVendors[i], "Vendor is not correct for product " + bestProducts[i]);
		}

		//check to see that the best price for each product has a checkmark
		int productIndex, columnIndex;

		for (int i = 0; i < bestProductsVendorsLength; i++) {
			productIndex = pageBidAnalysis.bidAnalysisProductIndex(bestProducts[i]);
			columnIndex = pageBidAnalysis.bidAnalysisVendorIndex(bestVendors[i]);

			Assert.assertTrue(pageBidAnalysis.bidAnalysisCheckIfGridCellItemSelected(productIndex + 1, columnIndex + 1), "Checkmark is not displayed for product " +
					bestProducts[i] + "and vendor " + bestVendors[i]);
		}

		//check to see that the selected price for each product is the one that is in the UI defined as the best price (color should be green)

		for (int i = 0; i < bestProductsVendorsLength; i++) {
			productIndex = pageBidAnalysis.bidAnalysisProductIndex(bestProducts[i]);
			columnIndex = pageBidAnalysis.bidAnalysisVendorIndex(bestVendors[i]);

			Assert.assertTrue(pageBidAnalysis.bidAnalysisCheckIfGridCellItemBestPrice(productIndex + 1, columnIndex + 1), "The best price is not selected for product " +
					bestProducts[i] + "and vendor " + bestVendors[i]);
		}

		//validate the begin dates
		DBStmtBidAnalysis.getBidSheetContracts(DB_CONNECTION_STRING, "con_begin_Date", TEST_BID_SHEET_NAME, TEST_MARKET_NAME, effDate);
		String[] beginDates = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBidSheetContractsFileName);

		for (int i = 0; i < bestProductsVendorsLength; i++) {
			Assert.assertEquals(beginDates[i], effDate, "Begin Date is incorrect for product " + bestProducts[i]);
		}

		//validate the end dates
		DBStmtBidAnalysis.getBidSheetContracts(DB_CONNECTION_STRING, "con_end_Date", TEST_BID_SHEET_NAME, TEST_MARKET_NAME, effDate);
		String[] endDates = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBidSheetContractsFileName);

		for (int i = 0; i < bestProductsVendorsLength; i++) {
			Assert.assertEquals(endDates[i], bidSheetEndDatePlusOne[0], "End Date is incorrect for product " + bestProducts[i]);
		}

		//validate that any previously existing contracts were properly ended.
		for (int i = 0; i < contractPKs.length; i++) {
			DBStmtBidAnalysis.getContractBasedOnPK(DB_CONNECTION_STRING, "con_end_Date", contractPKs[i]);
			String[] endDate = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getContractBasedOnPKFileName);
			Assert.assertEquals(endDate[0], effDate, "The contract for " + contractPKs[i] + " was not properly ended. End date should be "
					+ effDate);
		}
	}

	@Test(dataProvider = "EffectiveDate")
	public void endAllContracts(String effDate) throws Exception {

		ActionsEMW.setTestMethodDescription("ces-4089", "This tests using End All Contracts for the Bid Analysis screen. NOTE: at this point there is not enough data for this test"
				+ " to include bids on pages other than the current one.");

		setup(effDate);

		// get vendor and product list to use in test
		String[] testVendorNames = pageBidAnalysis.getVendorNames();
		String[] testProductNumbers = pageBidAnalysis.getProductNumbers();

		int numTestVendors = testVendorNames.length;
		int numTestProducts = testProductNumbers.length;

		//store the contract pk for the contract for the effective date
		DBStmtBidAnalysis.getBidSheetContracts(DB_CONNECTION_STRING, "con_price_pk", TEST_BID_SHEET_NAME, TEST_MARKET_NAME, effDate);
		String[] contractPKs = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBidSheetContractsFileName);

		//click the contract page best button
		pageBidAnalysis.endAllContracts.click();

		Waiter.waitForElementToDisappear(driver, pageBidAnalysis.loadSpinner);

		//check to see that no prices for any product has checkmarks

		for (int i = 0; i < numTestProducts; i++) {
			for (int j = 0; j < numTestVendors; j++) {
				Assert.assertFalse(pageBidAnalysis.bidAnalysisCheckIfGridCellItemSelected(i + 1, j + 1), "The checkmarks for product " + testProductNumbers[i]
						+ " and Vendor " + testVendorNames[j] + " have not disappeared.");
			}
		}

		//validate that any previously existing contracts were properly ended.
		for (int i = 0; i < contractPKs.length; i++) {
			DBStmtBidAnalysis.getContractBasedOnPK(DB_CONNECTION_STRING, "con_end_Date", contractPKs[i]);
			String[] endDate = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getContractBasedOnPKFileName);
			Assert.assertEquals(endDate[0], effDate, "A previously existing contract was not properly ended.");
		}
	}

	@Test(dataProvider = "EffectiveDate")
	public void bulkContractByCheckbox(String effDate) throws Exception {

		ActionsEMW.setTestMethodDescription("ces-4089", "This tests using the checkboxes under the Vendor Names to contract all products for a given vendor.");

		setup(effDate);

		// get vendor and product list to use in test
		String[] testVendorNames = pageBidAnalysis.getVendorNames();
		String[] testProductNumbers = pageBidAnalysis.getProductNumbers();

		int testVendorIndex = testVendorNames.length - 1;
		int numTestProducts = testProductNumbers.length;

		//assign Bid Sheet End Date to bid End Date
		DBStmtBidAnalysis.getBidSheetEndDatePlusOne(DB_CONNECTION_STRING, TEST_BID_SHEET_NAME);
		String[] bidSheetEndDatePlusOne = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBidSheetEndDateFileName);

		//get the pks contracts already on the screen
		DBStmtBidAnalysis.getBidSheetContracts(DB_CONNECTION_STRING, "con_price_pk", TEST_BID_SHEET_NAME, TEST_MARKET_NAME, effDate);
		String[] contractPKs = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBidSheetContractsFileName);

		//Check the checkbox

		WebElement vendorCheckbox = pageBidAnalysis.getVendorCheckbox(testVendorNames[testVendorIndex]);
		vendorCheckbox.click();

		Waiter.waitForElementToDisappear(driver, pageBidAnalysis.loadSpinner);

		//check to make sure that checkmarks appear for all products and the given vendor
		for (int i = 1; i <= numTestProducts; i++) {
			Assert.assertTrue(pageBidAnalysis.bidAnalysisCheckIfGridCellItemSelected(i, testVendorIndex + 1),
					"Checkmark is not displayed for product " + testProductNumbers[i - 1] + " and vendor " + testVendorNames[testVendorIndex]);
		}

		//validate the vendors for the contracts
		DBStmtBidAnalysis.getBidSheetContracts(DB_CONNECTION_STRING, "con_vendor", TEST_BID_SHEET_NAME, TEST_MARKET_NAME, effDate);
		String[] vendors = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBidSheetContractsFileName);

		for (int i = 0; i < numTestProducts; i++) {
			Assert.assertEquals(vendors[i], testVendorNames[testVendorIndex],
					"The contract was not created for " + testVendorNames[testVendorIndex] + " for the product " + testProductNumbers[i]);
		}

		//validate the begin dates
		DBStmtBidAnalysis.getBidSheetContracts(DB_CONNECTION_STRING, "con_begin_Date", TEST_BID_SHEET_NAME, TEST_MARKET_NAME, effDate);
		String[] beginDates = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBidSheetContractsFileName);

		for (int i = 0; i < numTestProducts; i++) {
			Assert.assertEquals(beginDates[i], effDate, "Begin Date is incorrect for product " + testProductNumbers[i]);
		}

		//validate the end dates
		DBStmtBidAnalysis.getBidSheetContracts(DB_CONNECTION_STRING, "con_end_Date", TEST_BID_SHEET_NAME, TEST_MARKET_NAME, effDate);
		String[] endDates = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getBidSheetContractsFileName);

		for (int i = 0; i < numTestProducts; i++) {
			Assert.assertEquals(endDates[i], bidSheetEndDatePlusOne[0], "End Date is incorrect for the product in row" + testProductNumbers[i]);
		}

		//validate that any previously existing contracts were properly ended.
		for (int i = 0; i < contractPKs.length; i++) {
			DBStmtBidAnalysis.getContractBasedOnPK(DB_CONNECTION_STRING, "con_end_Date", contractPKs[i]);
			String[] endDate = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getContractBasedOnPKFileName);
			Assert.assertEquals(endDate[0], effDate, "A previously existing contract was not properly ended for pk " + contractPKs[i]);
		}
	}

	@Test(dataProvider = "EffectiveDate")
	public void taxCheckBoxToggle(String effDate) throws Exception {

		ActionsEMW.setTestMethodDescription("ces-4089", "This tests toggling the Price Includes Tax checkbox and makes sure that when it is checked/unchecked the appropriate "
			+ "prices are shown on the Bid Analysis screen.");

		setup(effDate);

		// get vendor and product list to use in test
		String[] testVendorNames = pageBidAnalysis.getVendorNames();
		String[] testProductNumbers = pageBidAnalysis.getProductNumbers();

		int numTestVendors = testVendorNames.length;
		int numTestProducts = testProductNumbers.length;

		pageBidAnalysis.includeTaxCheckbox.click();
		pageBidAnalysis.retrieveBtn.click();

		Waiter.waitForElementToDisappear(driver, pageBidAnalysis.loadSpinner);

		for (int i = 0; i < numTestProducts; i++) {
			for (int j = 0; j < numTestVendors; j++) {
				//get the taxed price from the db for product
				DBStmtBidAnalysis.getVendorBid(DB_CONNECTION_STRING, "ven_bid_price_taxed", testProductNumbers[i], testVendorNames[j],
						TEST_MARKET_NAME, effDate);
				String[] taxedPrice1 = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getVendorBidFileName);

				//read price from the screen for product
				String screenContractPrice1_taxed = pageBidAnalysis.bidAnalysisGridCellItem(i + 1, j + 1).getText();
				Assert.assertEquals(taxedPrice1[0], screenContractPrice1_taxed, "The screen does not show the taxed price");
			}
		}

		pageBidAnalysis.includeTaxCheckbox.click();
		pageBidAnalysis.retrieveBtn.click();

		Waiter.waitForElementToDisappear(driver, pageBidAnalysis.loadSpinner);

		for (int i = 0; i < numTestProducts; i++) {
			for (int j = 0; j < numTestVendors; j++) {
				//get the taxed price from the db for product
				DBStmtBidAnalysis.getVendorBid(DB_CONNECTION_STRING, "ven_bid_price", testProductNumbers[i], testVendorNames[j],
						TEST_MARKET_NAME, effDate);
				String[] bidPrice1 = ActionsEMW.readSqlResults(DBStmtBidAnalysis.class.getSimpleName(), DBStmtBidAnalysis.getVendorBidFileName);

				//read price from the screen for product
				String screenContractPrice1 = pageBidAnalysis.bidAnalysisGridCellItem(i + 1, j + 1).getText();
				Assert.assertEquals(bidPrice1[0], screenContractPrice1, "The screen does not show the untaxed price");
			}
		}
	}
}