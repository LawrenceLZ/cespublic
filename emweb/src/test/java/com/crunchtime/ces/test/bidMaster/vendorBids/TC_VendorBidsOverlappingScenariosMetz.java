package com.crunchtime.ces.test.bidMaster.vendorBids;

import com.crunchtime.ces.base.BaseTestEMW;
import com.crunchtime.ces.helper.ActionsEMW;
import com.crunchtime.ces.helper.Waiter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TC_VendorBidsOverlappingScenariosMetz extends BaseTestEMW {
	private static final String DB_CONNECTION_STRING = "AUTO/AUTO@CTAUTO24006.WORLD";
	private static final String TEST_SITE_NAME = "auto-em.net-chef.local";
	private static final String USEER_ID = "VBIDS";
	private static final String USER_PWD = "VBIDS";
	private static final String TEST_LOCATION_NAME = "Bubba Gump Shrimp Co";
	private static final String TEST_BID_SHEET_NAME = "TestVendorBids";
	private static final String TEST_VENDOR_NAME = "TestVendorBids Ven 1";
	private static final String TEST_MARKET_NAME = "TestVendorBids";
	private static final String TEST_ADDITIONAL_MKT_NAME = "TestVendorBidsAddiMkt";
	private static final String TEST_COMPANYPRODUCT_NUMBER1 = "VBID013";
	private static final String TEST_COMPANYPRODUCT_NUMBER2 = "VBID014";
	private static final String TEST_VBID_UNIT = "CA";
	private static final String TEST_SEC_VBID_UNIT = "Oz";
	private static final String TEST_VBID_TAX = "EXC";


	@BeforeMethod(alwaysRun = true)
	public void login() throws Exception {
		basePageEMW.openTestSite(TEST_SITE_NAME);
		pageEMWLogin.loginEMW(USEER_ID, USER_PWD, TEST_LOCATION_NAME);
		//Navigate to Vendor Bids detail screen with testing Bid Sheet/Vendor/Market combination
		basePageEMW.navigateToPageOtherTasks();
		pageOtherTasks.navigateToVendorBidsScreen();
		pageVendorBids.bidSheetsBtn.click();
		pageVendorBids.waitForSelectBidSheetsPopupReturn();
		//Select the testing Bid Sheet
		pageVendorBids.selectBidSheetsPopupSelectAllCheckbox.click(); //first to deselect all the bid sheets
		pageVendorBids.selectBidSheetsPopupFilterIcon.click();
		pageVendorBids.filterBidSheetTextbox.clear();
		pageVendorBids.filterBidSheetTextbox.sendKeys(TEST_BID_SHEET_NAME);
		pageVendorBids.filterBidSheetApplyBtn.click();
		pageVendorBids.waitForSelectBidSheetsPopupReturn(); //wait for filtered results to return
		pageVendorBids.selectBidSheetsPopupSelectAllCheckbox.click(); //now to select the filtered bid sheet(s)
		pageVendorBids.selectBidSheetsPopupApplyBth.click();
		pageVendorBids.waitForVendorInputCombobxEnable();
		//define vendor and market and then Retrieve
		pageVendorBids.vendorInputCombobox.click();
		pageVendorBids.vendorInputComboboxTrigger.click();
		Thread.sleep(1000);
		ActionsEMW.setComboBoxDropdownByValue(driver, pageVendorBids.vendorInputCombobox, TEST_VENDOR_NAME);
		Thread.sleep(1000);
		pageVendorBids.marketInputCombobox.click();
		pageVendorBids.marketInputCombobox.clear();
		pageVendorBids.marketInputCombobox.sendKeys(TEST_MARKET_NAME);
		pageVendorBids.marketInputCombobox.sendKeys("\t");
		boolean retrieveEnabled;
		do {
			Waiter.waitForOneSecond();
			retrieveEnabled = pageVendorBids.retrieveBtn.getAttribute("class").contains("disabled");
		} while (retrieveEnabled);
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageEMWLogout.logoutEMW();
	}

	/**
	 * Test Case Scenario,
	 * Create a current vendor bids that will overlap with a future vendor vendor bid, illustrated below
	 * ********************||Future Start---------------------------Future End
	 * ********************||
	 * ********************||
	 * ********************||
	 * ********************||cut-off point
	 * ********************||
	 * ********************||
	 * ********************||
	 * Current Start--------------------Current End
	 */
	@Test
	public void createCurrentVendorBidOverlapWithFutureVendorBid() throws Exception {

	}
}