package com.crunchtime.ces.test.bidMaster.vendorBids;

import com.crunchtime.ces.base.BaseTestEMW;
import com.crunchtime.ces.database.bidMaster.vendorBids.DBStmtVendorBids;
import com.crunchtime.ces.helper.ActionsEMW;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class TC_VendorBidsSimplePrimaryScenarios extends BaseTestEMW {
	private static final String DB_CONNECTION_STRING = "AUTO/AUTO@CTAUTO24006";
	private static final String TEST_SITE_NAME = "auto-em.net-chef.local";
	private static final String USER_ID = "VBIDS";
	private static final String USER_PWD = "VBIDS";
	private static final String TEST_LOCATION_NAME = "Bubba Gump Shrimp Co";
	private static final String TEST_BID_SHEET_NAME = "TestVendorBids";
	private static final String TEST_VENDOR_NAME = "TestVendorBids Ven 1";
	private static final String TEST_MARKET_NAME = "TestVendorBids";
	private static final String TEST_COMPANYPRODUCT_NUMBER1 = "VBID001";
	private static final String TEST_COMPANYPRODUCT_NUMBER2 = "VBID002";
	private static final String TEST_COMPANYPRODUCT_NUMBER3 = "VBID003";
	private static final String TEST_COMPANYPRODUCT_NUMBER4 = "VBID004";
	private static final String TEST_COMPANYPRODUCT_NUMBER5 = "VBID005";

	@BeforeMethod(alwaysRun = true)
	public void login() throws Exception {
		basePageEMW.openTestSite(TEST_SITE_NAME);
		pageEMWLogin.loginEMW(USER_ID, USER_PWD, TEST_LOCATION_NAME);
		//Navigate to Vendor Bids detail screen with testing Bid Sheet/Vendor/Market combination
		basePageEMW.navigateToPageOtherTasks();
		pageOtherTasks.navigateToVendorBidsScreen();
		pageVendorBids.bidSheetsBtn.click();
		pageVendorBids.waitForSelectBidSheetsPopupReturn();
		//Select the testing Bid Sheet
		pageVendorBids.selectBidSheetsPopupSelectAllCheckbox.click(); //first to deselect all the bid sheets
		pageVendorBids.selectBidSheetsPopupFilterIcon.click();
		pageVendorBids.filterBidSheetTextbox.clear();
		pageVendorBids.filterBidSheetTextbox.sendKeys(TEST_BID_SHEET_NAME);
		pageVendorBids.filterBidSheetApplyBtn.click();
		pageVendorBids.waitForSelectBidSheetsPopupReturn(); //wait for filtered results to return
		pageVendorBids.selectBidSheetsPopupSelectAllCheckbox.click(); //now to select the filtered bid sheet(s)
		pageVendorBids.selectBidSheetsPopupApplyBth.click();
		//define vendor and market and then Retrieve
		pageVendorBids.vendorInputCombobox.click();
		pageVendorBids.vendorInputComboboxTrigger.click();
		Thread.sleep(1000);
		ActionsEMW.setComboBoxDropdownByValue(driver, pageVendorBids.vendorInputCombobox, TEST_VENDOR_NAME);
		Thread.sleep(1000);
		pageVendorBids.marketInputCombobox.click();
		pageVendorBids.marketInputCombobox.clear();
		pageVendorBids.marketInputCombobox.sendKeys(TEST_MARKET_NAME);
		pageVendorBids.marketInputCombobox.sendKeys("\t");
		boolean retrieveEnabled;
		do {
			Waiter.waitForOneSecond();
			retrieveEnabled = pageVendorBids.retrieveBtn.getAttribute("class").contains("disabled");
			int timer = 0;
			timer++;
			if (timer == 120) break;
		} while (retrieveEnabled);
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageEMWLogout.logoutEMW();
	}

	/**
	 * Test Case Scenario,
	 * Create a current Primary Vendor Bid with end date = Bid Sheet End Date
	 */
	@Test
	public void editCurrentPrimaryVendorBidPriceAndEndDateEqualBidSheetEndDate() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-1427", "Create a current Primary Vendor Bid with end date = Bid Sheet End Date");
//		ITestResult result = Reporter.getCurrentTestResult();

		//set effective date = current date
		String effDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 0);
		pageVendorBids.effectiveDatePicker.clear();
		pageVendorBids.effectiveDatePicker.sendKeys(effDate);
		pageVendorBids.effectiveDatePicker.sendKeys("\t");
		Thread.sleep(1000);
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();

		//demo loop edit workflow
//        int rowCnt = pageEMWGeneric.getEMWGridRowCnt(pageVendorBids.vendorBidsGrid);
//        //System.out.println(rowCnt);
//        //Thread.sleep(3000);
//        for (int i = 1; i < rowCnt; i++) {
//            String testVenProdRow = pageVendorBids.vendorBidsGridCellItem(i, "Product #").getText();
//            //Thread.sleep(3000);
//            if (testVenProdRow.contains(TEST_COMPANYPRODUCT_NUMBER1) || testVenProdRow.contains(TEST_COMPANYPRODUCT_NUMBER2) || testVenProdRow.contains(TEST_COMPANYPRODUCT_NUMBER3)) {
//                pageVendorBids.vendorBidsGridCellItem(i, "Product #").click();
//                Thread.sleep(1000);
//                pageVendorBids.waitFoVendorBidsEditFormReturn();
//                pageVendorBids.bidPriceTextbox.clear();
//                Random rand = new Random();
//                double bidPrice = rand.nextInt(10) + 0.50;
//                //assign a random bid price to testProduct
//                pageVendorBids.bidPriceTextbox.sendKeys(String.valueOf(bidPrice));
//                pageVendorBids.bidEditFormCollapseIcon.click();
//                new WebDriverWait(driver, 60, 1000).until(
//                        ExpectedConditions.visibilityOf(pageVendorBids.bidEditFormExpandIcon)
//                );
//                Thread.sleep(1000);
//                System.out.println(testVenProdRow + " edited bid price is " + bidPrice);
//            }
//        }

		//Filter TEST_COMPANYPRODUCT_NUMBER1 = VBID001 and Edit Bid Price ONLY
		pageVendorBids.vendorBidsFilterIcon.click();
		pageVendorBids.filterProductNumberTextbox.click();
		pageVendorBids.filterProductNumberTextbox.sendKeys(TEST_COMPANYPRODUCT_NUMBER1);
		pageVendorBids.vendorBidsFilterApplyBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		pageVendorBids.vendorBidsGridCellItem(1, "Product #").click();
		pageVendorBids.waitFoVendorBidsEditFormReturn();
		pageVendorBids.bidEditFormPriceTextbox.clear();
		Random rand1 = new Random();
		double bidPrice1 = rand1.nextInt(10) + 0.50;
		//assign a random bid price to testProduct
		pageVendorBids.bidEditFormPriceTextbox.sendKeys(String.valueOf(bidPrice1));
		//assign Bid Sheet End Date to bid End Date
		DBStmtVendorBids.getBidSheetEndDate(DB_CONNECTION_STRING, TEST_BID_SHEET_NAME);
		String[] bidSheetEndDate = ActionsEMW.readSqlResults("DBStmtVendorBids", "Find_Bid_Sheet_End_Date");
		pageVendorBids.bidEditFormEndDateTextbox.clear();
		pageVendorBids.bidEditFormEndDateTextbox.sendKeys(bidSheetEndDate[0]);
		pageVendorBids.bidEditFormCollapseIcon.click();
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.visibilityOf(pageVendorBids.bidEditFormExpandIcon)
		);
		Thread.sleep(1000);
//		Reporter.log("Test Method = " + result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER1 + " EXPECTED edit bid price is " + bidPrice1 + "\n");
		String vbidpNum1 = pageVendorBids.vendorBidsGridCellItem(1, "vendor #").getText();
		String updatedvbidpPrice1 = pageVendorBids.vendorBidsGridCellItem(1, "price").getText().trim();
		String updatedvbidpEndDate1 = pageVendorBids.vendorBidsGridCellItem(1, "Bid End Date").getText().trim();
		//When Bid End Date = Bid Sheet End Date, set the Bid End Date on screen + 1 to match database value
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = simpleDateFormat.parse(updatedvbidpEndDate1);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, 1);
		String actualUpdatedvbidpEndDate1 = simpleDateFormat.format(calendar.getTime());
//		Reporter.log("Test Method = " + result.getMethod().getMethodName() + ": " + vbidpNum1 + " ACTUAL edit bid price is " + updatedvbidpPrice1);

		//validate edited Bid Price to database saved value
		DBStmtVendorBids.getVendorBid(DB_CONNECTION_STRING, "ven_bid_price", TEST_COMPANYPRODUCT_NUMBER1, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbVendorBidPrice = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Vendor_Bid");
		Assert.assertEquals(dbVendorBidPrice[0], updatedvbidpPrice1, "Failed, db value for Bid Price should be " + updatedvbidpPrice1);
		//validate edited Bid End Date to database saved value
		DBStmtVendorBids.getVendorBid(DB_CONNECTION_STRING, "ven_end_Date", TEST_COMPANYPRODUCT_NUMBER1, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbVendorBidEndDate = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Vendor_Bid");
		//System.out.println(actualUpdatedvbidpEndDate1);
		Assert.assertEquals(dbVendorBidEndDate[0], actualUpdatedvbidpEndDate1, "Failed, db value for Bid End Date should be " + actualUpdatedvbidpEndDate1);
	}

	/**
	 * Test Case Scenario,
	 * Create a current Primary Vendor Bid with end date < Bid Sheet End Date
	 */
	@Test
	public void editCurrentPrimaryVendorBidPriceAndEndDateLessThanBidSheetEndDate() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-1427", "Create a current Primary Vendor Bid with end date < Bid Sheet End Date");
//		ITestResult result = Reporter.getCurrentTestResult();

		//set effective date = current date
		String effDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 0);
		pageVendorBids.effectiveDatePicker.clear();
		pageVendorBids.effectiveDatePicker.sendKeys(effDate);
		pageVendorBids.effectiveDatePicker.sendKeys("\t");
		Thread.sleep(1000);
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();

		//Filter TEST_COMPANYPRODUCT_NUMBER2 = VBID002 and Edit Bid Price ONLY
		pageVendorBids.vendorBidsFilterIcon.click();
		pageVendorBids.filterProductNumberTextbox.click();
		pageVendorBids.filterProductNumberTextbox.sendKeys(TEST_COMPANYPRODUCT_NUMBER2);
		pageVendorBids.vendorBidsFilterApplyBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		pageVendorBids.vendorBidsGridCellItem(1, "Product #").click();
		pageVendorBids.waitFoVendorBidsEditFormReturn();
		pageVendorBids.bidEditFormPriceTextbox.clear();
		Random rand1 = new Random();
		double bidPrice1 = rand1.nextInt(10) + 0.50;
		//assign a random bid price to testProduct
		pageVendorBids.bidEditFormPriceTextbox.sendKeys(String.valueOf(bidPrice1));
		//assign bid End Date < Bid Sheet End Date
		String TEST_COMPANYPRODUCT_NUMBER2BidEndDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 180);
		pageVendorBids.bidEditFormEndDateTextbox.clear();
		pageVendorBids.bidEditFormEndDateTextbox.sendKeys(TEST_COMPANYPRODUCT_NUMBER2BidEndDate);
		pageVendorBids.bidEditFormCollapseIcon.click();
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.visibilityOf(pageVendorBids.bidEditFormExpandIcon)
		);
		Thread.sleep(1000);
//		Reporter.log("Test Method = " + result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER2 + " EXPECTED edit bid price is " + bidPrice1 + "\n");
		String vbidpNum2 = pageVendorBids.vendorBidsGridCellItem(1, "vendor #").getText();
		String updatedvbidpPrice2 = pageVendorBids.vendorBidsGridCellItem(1, "price").getText().trim();
		String updatedvbidpEndDate2 = pageVendorBids.vendorBidsGridCellItem(1, "Bid End Date").getText().trim();
//		Reporter.log("Test Method = " + result.getMethod().getMethodName() + ": " + vbidpNum2 + " ACTUAL edit bid price is " + updatedvbidpPrice2);

		//validate edited Bid Price to database saved value
		DBStmtVendorBids.getVendorBid(DB_CONNECTION_STRING, "ven_bid_price", TEST_COMPANYPRODUCT_NUMBER2, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbVendorBidPrice = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Vendor_Bid");
		Assert.assertEquals(dbVendorBidPrice[0], updatedvbidpPrice2, "Failed, db value for Bid Price should be " + updatedvbidpPrice2);
		//validate edited Bid End Date to database saved value
		DBStmtVendorBids.getVendorBid(DB_CONNECTION_STRING, "ven_end_Date", TEST_COMPANYPRODUCT_NUMBER2, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbVendorBidEndDate = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Vendor_Bid");
		Assert.assertEquals(dbVendorBidEndDate[0], updatedvbidpEndDate2, "Failed, db value for Bid End Date should be " + updatedvbidpEndDate2);
	}

	/**
	 * Test Case Scenario,
	 * Create a future Primary Vendor Bid with end date = Bid Sheet End Date
	 */
	@Test
	public void editFuturePrimaryBidPriceAndEndDateEqualBidSheetEndDate() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-1427", "Create a future Primary Vendor Bid with end date = Bid Sheet End Date");
//		ITestResult result = Reporter.getCurrentTestResult();

		//set effective date = current date + 30 as a Future Bid
		String effDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 30);
		pageVendorBids.effectiveDatePicker.clear();
		pageVendorBids.effectiveDatePicker.sendKeys(effDate);
		pageVendorBids.effectiveDatePicker.sendKeys("\t");
		Thread.sleep(1000);
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();

		//Filter TEST_COMPANYPRODUCT_NUMBER3 = VBID003 and Edit Bid Price ONLY
		pageVendorBids.vendorBidsFilterIcon.click();
		pageVendorBids.filterProductNumberTextbox.click();
		pageVendorBids.filterProductNumberTextbox.sendKeys(TEST_COMPANYPRODUCT_NUMBER3);
		pageVendorBids.vendorBidsFilterApplyBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		pageVendorBids.vendorBidsGridCellItem(1, "Product #").click();
		pageVendorBids.waitFoVendorBidsEditFormReturn();
		pageVendorBids.bidEditFormPriceTextbox.clear();
		Random rand1 = new Random();
		double bidPrice1 = rand1.nextInt(10) + 0.50;
		//assign a random bid price to testProduct
		pageVendorBids.bidEditFormPriceTextbox.sendKeys(String.valueOf(bidPrice1));
		//assign Bid Sheet End Date to bid End Date
		DBStmtVendorBids.getBidSheetEndDate(DB_CONNECTION_STRING, TEST_BID_SHEET_NAME);
		String[] bidSheetEndDateForProdNum3 = ActionsEMW.readSqlResults("DBStmtVendorBids", "Find_Bid_Sheet_End_Date");
		pageVendorBids.bidEditFormEndDateTextbox.clear();
		pageVendorBids.bidEditFormEndDateTextbox.sendKeys(bidSheetEndDateForProdNum3[0]);
		pageVendorBids.bidEditFormCollapseIcon.click();
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.visibilityOf(pageVendorBids.bidEditFormExpandIcon)
		);
		Thread.sleep(1000);
//		Reporter.log("Test Method = " + result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER3 + " EXPECTED edit bid price is " + bidPrice1 + "\n");
		String vbidpNum3 = pageVendorBids.vendorBidsGridCellItem(1, "vendor #").getText();
		String updatedvbidpPrice3 = pageVendorBids.vendorBidsGridCellItem(1, "price").getText().trim();
		String updatedvbidpEndDate3 = pageVendorBids.vendorBidsGridCellItem(1, "Bid End Date").getText().trim();
		//When Bid End Date = Bid Sheet End Date, set the Bid End Date on screen + 1 to match database value
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = simpleDateFormat.parse(updatedvbidpEndDate3);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, 1);
		String actualUpdatedvbidpEndDate3 = simpleDateFormat.format(calendar.getTime());
//		Reporter.log("Test Method = " + result.getMethod().getMethodName() + ": " + vbidpNum3 + " ACTUAL edit bid price is " + updatedvbidpPrice3);

		//validate edited Bid Price to database saved value
		DBStmtVendorBids.getVendorBid(DB_CONNECTION_STRING, "ven_bid_price", TEST_COMPANYPRODUCT_NUMBER3, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbVendorBidPrice = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Vendor_Bid");
		Assert.assertEquals(dbVendorBidPrice[0], updatedvbidpPrice3, "Failed, db value for Bid Price should be " + updatedvbidpPrice3);
		//validate edited Bid End Date to database saved value
		DBStmtVendorBids.getVendorBid(DB_CONNECTION_STRING, "ven_end_Date", TEST_COMPANYPRODUCT_NUMBER3, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbVendorBidEndDate = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Vendor_Bid");
		Assert.assertEquals(dbVendorBidEndDate[0], actualUpdatedvbidpEndDate3, "Failed, db value for Bid End Date should be " + actualUpdatedvbidpEndDate3);
	}

	/**
	 * Test Case Scenario,
	 * Create a future Primary Vendor Bid with end date < Bid Sheet End Date
	 */
	@Test
	public void editFuturePrimaryBidPriceAndEndDateLessThanBidSheetEndDate() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-1427", "Create a future Primary Vendor Bid with end date < Bid Sheet End Date");
//		ITestResult result = Reporter.getCurrentTestResult();

		//set effective date = current date + 30 as a Future Bid
		String effDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 30);
		pageVendorBids.effectiveDatePicker.clear();
		pageVendorBids.effectiveDatePicker.sendKeys(effDate);
		pageVendorBids.effectiveDatePicker.sendKeys("\t");
		Thread.sleep(1000);
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();

		//Filter TEST_COMPANYPRODUCT_NUMBER4 = VBID004 and Edit Bid Price ONLY
		pageVendorBids.vendorBidsFilterIcon.click();
		pageVendorBids.filterProductNumberTextbox.click();
		pageVendorBids.filterProductNumberTextbox.sendKeys(TEST_COMPANYPRODUCT_NUMBER4);
		pageVendorBids.vendorBidsFilterApplyBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		pageVendorBids.vendorBidsGridCellItem(1, "Product #").click();
		pageVendorBids.waitFoVendorBidsEditFormReturn();
		pageVendorBids.bidEditFormPriceTextbox.clear();
		Random rand1 = new Random();
		double bidPrice1 = rand1.nextInt(10) + 0.50;
		//assign a random bid price to testProduct
		pageVendorBids.bidEditFormPriceTextbox.sendKeys(String.valueOf(bidPrice1));
		//assign bid End Date < Bid Sheet End Date
		String TEST_COMPANYPRODUCT_NUMBER4BidEndDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 180);
		pageVendorBids.bidEditFormEndDateTextbox.clear();
		pageVendorBids.bidEditFormEndDateTextbox.sendKeys(TEST_COMPANYPRODUCT_NUMBER4BidEndDate);
		pageVendorBids.bidEditFormCollapseIcon.click();
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.visibilityOf(pageVendorBids.bidEditFormExpandIcon)
		);
		Thread.sleep(1000);
//		Reporter.log("Test Method = " + result.getMethod().getMethodName() + ": " + TEST_COMPANYPRODUCT_NUMBER4 + " EXPECTED edit bid price is " + bidPrice1 + "\n");
		String vbidpNum4 = pageVendorBids.vendorBidsGridCellItem(1, "vendor #").getText();
		String updatedvbidpPrice4 = pageVendorBids.vendorBidsGridCellItem(1, "price").getText().trim();
		String updatedvbidpEndDate4 = pageVendorBids.vendorBidsGridCellItem(1, "Bid End Date").getText().trim();
//		Reporter.log("Test Method = " + result.getMethod().getMethodName() + ": " + vbidpNum4 + " ACTUAL edit bid price is " + updatedvbidpPrice4);

		//validate edited Bid Price to database saved value
		DBStmtVendorBids.getVendorBid(DB_CONNECTION_STRING, "ven_bid_price", TEST_COMPANYPRODUCT_NUMBER4, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbVendorBidPrice = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Vendor_Bid");
		Assert.assertEquals(dbVendorBidPrice[0], updatedvbidpPrice4, "Failed, db value for Bid Price should be " + updatedvbidpPrice4);
		//validate edited Bid End Date to database saved value
		DBStmtVendorBids.getVendorBid(DB_CONNECTION_STRING, "ven_end_Date", TEST_COMPANYPRODUCT_NUMBER4, TEST_VENDOR_NAME, TEST_MARKET_NAME, effDate);
		String[] dbVendorBidEndDate = ActionsEMW.readSqlResults("DBStmtVendorBids", "Validate_Vendor_Bid");
		Assert.assertEquals(dbVendorBidEndDate[0], updatedvbidpEndDate4, "Failed, db value for Bid End Date should be " + updatedvbidpEndDate4);
	}

	/**
	 * Test Case Scenario,
	 * Edit End Dates sooner and later than Vendor Bid End Date, End Date=today’s date to delete a Vendor Bid
	 */
	@Test
	public void editCurrentPrimaryBidPriceAndEndDateEqualCurrentDate() throws Exception {
		ActionsEMW.setTestMethodDescription("ces-1427", "Edit End Dates sooner and later than Vendor Bid End Date, End Date=today’s date to delete a Vendor Bid");
//		ITestResult result = Reporter.getCurrentTestResult();

		//set effective date = current date, to create an ACTIVE Current Primary Vendor Bid
		String effDate = ActionsEMW.setDateBasedOnToday("MM/dd/yyyy", 0);
		pageVendorBids.effectiveDatePicker.clear();
		pageVendorBids.effectiveDatePicker.sendKeys(effDate);
		pageVendorBids.effectiveDatePicker.sendKeys("\t");
		Thread.sleep(1000);
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		//Pre-req setup, Filter TEST_COMPANYPRODUCT_NUMBER5 = VBID005 and Edit Bid Price and End Date = Bid Sheet End Date
		pageVendorBids.vendorBidsFilterIcon.click();
		pageVendorBids.filterProductNumberTextbox.click();
		pageVendorBids.filterProductNumberTextbox.sendKeys(TEST_COMPANYPRODUCT_NUMBER5);
		pageVendorBids.vendorBidsFilterApplyBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();
		pageVendorBids.vendorBidsGridCellItem(1, "Product #").click();
		pageVendorBids.waitFoVendorBidsEditFormReturn();
		//fill in Conversion = 1 to bypass validation
		pageVendorBids.bidEditFormConversionTextbox.clear();
		pageVendorBids.bidEditFormConversionTextbox.sendKeys("1");
		pageVendorBids.bidEditFormPriceTextbox.clear();
		Random rand1 = new Random();
		double bidPrice1 = rand1.nextInt(10) + 0.50;
		//assign a random bid price to testProduct
		pageVendorBids.bidEditFormPriceTextbox.sendKeys(String.valueOf(bidPrice1));
		//assign Bid Sheet End Date = Bid Sheet End Date
		DBStmtVendorBids.getBidSheetEndDate(DB_CONNECTION_STRING, TEST_BID_SHEET_NAME);
		String[] bidSheetEndDate = ActionsEMW.readSqlResults("DBStmtVendorBids", "Find_Bid_Sheet_End_Date");
		pageVendorBids.bidEditFormEndDateTextbox.clear();
		pageVendorBids.bidEditFormEndDateTextbox.sendKeys(bidSheetEndDate[0]);
		pageVendorBids.bidEditFormCollapseIcon.click();
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.visibilityOf(pageVendorBids.bidEditFormExpandIcon)
		);
		Thread.sleep(1000);

		//Logical Delete Vendor Bid (i.e. Begin Date = End Date)
		pageVendorBids.bidEditFormExpandIcon.click();
		pageVendorBids.waitFoVendorBidsEditFormReturn();
		pageVendorBids.bidEditFormPriceTextbox.clear();
		Random rand2 = new Random();
		double bidPrice2 = rand2.nextInt(10) + 0.50;
		//assign a random bid price to testProduct
		pageVendorBids.bidEditFormPriceTextbox.sendKeys(String.valueOf(bidPrice2));
		pageVendorBids.bidEditFormEndDateTextbox.clear();
		pageVendorBids.bidEditFormEndDateTextbox.sendKeys(effDate);
		pageVendorBids.bidEditFormCollapseIcon.click();
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.visibilityOf(pageVendorBids.bidEditFormExpandIcon)
		);
		Thread.sleep(1000);
		//Click Retrieve button to re-retrieve/refresh the results to make sure changes are updated
		pageVendorBids.retrieveBtn.click();
		pageVendorBids.waitFoVendorBidsGridReturn();

		//Validate that Vendor Bid Price and End Date are null for logically deleted bid
		String updatedvbidpPrice5 = pageVendorBids.vendorBidsGridCellItem(1, "price").getText().trim();
		String updatedvbidpEndDate5 = pageVendorBids.vendorBidsGridCellItem(1, "Bid End Date").getText().trim();
		Assert.assertEquals(updatedvbidpPrice5, "", "Fail, Bid Price should be null for logical delete bid");
//		Reporter.log("Test Method = " + result.getMethod().getMethodName() + ": " +
//				TEST_COMPANYPRODUCT_NUMBER5 + " EXPECTED edit Bid Price is null" + updatedvbidpPrice5 + " and End Date is " + updatedvbidpEndDate5);
	}
}