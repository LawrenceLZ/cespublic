package com.crunchtime.ces.driver;

import org.openqa.selenium.WebDriver;

public class DriverOfActiveThread {

	private static ThreadLocal<WebDriver> driver = new ThreadLocal<WebDriver>();

	public static WebDriver getDriver() {
		return DriverOfActiveThread.driver.get();
	}

	public static void setDriver(WebDriver driver) {
		DriverOfActiveThread.driver.set(driver);
	}
}