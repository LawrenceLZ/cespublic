package com.crunchtime.ces.database.generic;

public class DBStmtEMWGeneric {

	public static String getStmtUpdateUserGroupAccess(String accessFlag, String userId, String screenName, String controlName) {
		return "UPDATE t_user_group_access usrga\n" +
				"   SET usrga.access_allowed = upper('" + accessFlag + "')\n" +
				" WHERE (usrga.user_group_pk, usrga.user_access_pk) IN\n" +
				"       (SELECT usr.user_group_pk, usra.user_access_pk\n" +
				"          FROM t_user usr, t_user_access usra, t_screen scrn, t_control ctrl\n" +
				"         WHERE usra.screen_id = scrn.screen_id\n" +
				"           AND usra.control_id = ctrl.control_id\n" +
				"           AND upper(usr.user_id) = upper('" + userId + "')\n" +
				"           AND upper(scrn.screen_name) = upper('" + screenName + "')\n" +
				"           AND upper(ctrl.control_name) = upper('" + controlName + "'))\n";
	}
}