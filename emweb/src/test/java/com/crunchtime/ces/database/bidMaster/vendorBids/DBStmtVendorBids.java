package com.crunchtime.ces.database.bidMaster.vendorBids;

import org.testng.Assert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DBStmtVendorBids {
	private static final String root = System.getProperty("user.dir");
	//Base folder name co-relates to test package name
	private static final String dbResourcesBaseFolderName = "DBStmtVendorBids";
	//file name co-relates to database resource sql file name
	private static final String getVendorBidFileName = "Validate_Vendor_Bid";
	private static final String getSecVendorBidFileName = "Validate_Secondary_Vendor_Bid";
	private static final String getBidSheetEndDateFileName = "Find_Bid_Sheet_End_Date";

	//get the bid price based on Vendor Product #, Vendor, Market, Effective Date
	public static void getVendorBid(String dbConnection, String validateField, String companyProductNum, String vendorName, String marketName, String effectiveDate) throws IOException, InterruptedException {
		Process p;
		try {
			String line;
			//******ONLY need to define sqlMethodFileName******
			String sqlMethodFileName = getVendorBidFileName;

			//root, dbResourcesBasedir and basedir dir stays the same
			String basedir = root + "\\src\\test\\resources\\Database\\" + dbResourcesBaseFolderName + "\\" + sqlMethodFileName;
			//for debugging purpose
			String sqlCmd = "cmd /c exit|sqlplus -L " + dbConnection + " @" + basedir + " " + basedir + ".txt \"" + validateField + "\" \"" + companyProductNum + "\" \"" + vendorName + "\" \"" + marketName + "\" \"" + effectiveDate + "\"";
			p = Runtime.getRuntime().exec(sqlCmd);

			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = reader.readLine()) != null) {
				//print line is used for debugging
				System.out.println(line);
				boolean errOne;
				errOne = line.contains("ERROR");
				boolean errTwo;
				errTwo = line.contains("unable");
				Assert.assertFalse(errOne || errTwo, line);
			}
			reader.close();
			System.out.println("Done.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(1000);
	}

	//get the secondary vendor bid price based on Vendor Product #, Vendor, Market, Effective Date
	public static void getSecVendorBid(String dbConnection, String validateField, String companyProductNum, String secVenProductNum, String vendorName, String marketName, String effectiveDate) throws IOException, InterruptedException {
		Process p;
		try {
			String line;

			//******ONLY need to define sqlFolderFilePath******
			String sqlMethodFileName = getSecVendorBidFileName;

			//***DO NOT CHANGE*** root, dbResourcesBasedir and basedir dir stays the same
			String basedir = root + "\\src\\test\\resources\\Database\\" + dbResourcesBaseFolderName + "\\" + sqlMethodFileName;

			String sqlCmd = "cmd /c exit|sqlplus -L " + dbConnection + " @" + basedir + " " + basedir + ".txt \"" + validateField + "\" \"" + companyProductNum + "\" \"" + secVenProductNum + "\" \"" + vendorName + "\" \"" + marketName + "\" \"" + effectiveDate + "\"";
			//for debugging purpose
			//System.out.println(sqlCmd);

			p = Runtime.getRuntime().exec(sqlCmd);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = reader.readLine()) != null) {
				//print line is used for debugging
				System.out.println(line);
				boolean errOne;
				errOne = line.contains("ERROR");
				boolean errTwo;
				errTwo = line.contains("unable");
				Assert.assertFalse(errOne || errTwo, line);
			}
			reader.close();
			System.out.println("Done.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(1000);
	}

	//find the Bid Sheet End Date based on the given Bid Sheet Name
	public static void getBidSheetEndDate(String dbConnection, String bidSheetName) throws IOException, InterruptedException {
		Process p;
		try {
			String line;

			//******ONLY need to define sqlFolderFilePath******
			String sqlMethodFileName = getBidSheetEndDateFileName;
			//***DO NOT CHANGE*** root, dbResourcesBasedir and basedir dir stays the same
			String basedir = root + "\\src\\test\\resources\\Database\\" + dbResourcesBaseFolderName + "\\" + sqlMethodFileName;

			String sqlCmd = "cmd /c exit|sqlplus -L " + dbConnection + " @" + basedir + " " + basedir + ".txt \"" + bidSheetName + "\"";
			//for debugging purpose
			//System.out.println(sqlCmd);

			p = Runtime.getRuntime().exec(sqlCmd);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = reader.readLine()) != null) {
				//print line is used for debugging
				System.out.println(line);
				boolean errOne;
				errOne = line.contains("ERROR");
				boolean errTwo;
				errTwo = line.contains("unable");
				Assert.assertFalse(errOne || errTwo, line);
			}
			reader.close();
			System.out.println("Done.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(1000);
	}

	public static String getStmtInsertVendorBidRecord(String bidName, String vendorName, String marketName) {
		return "INSERT INTO T_VENDOR_BID \n" +
				"(VENDOR_BID_PK, \n" +
				" SUPPLY_ID, \n" +
				" MARKET_PK, \n" +
				" BID_PK, \n" +
				" EFFECTIVE_DATE, \n" +
				" PRICE_INCLUDES_TAX_FLAG, \n" +
				" REPLICATED_FLAG)\n" +
				"SELECT\n" +
				"      SEQ_T_VENDOR_BID.NEXTVAL, \n" +
				"      (SELECT SUP.SUPPLY_ID FROM T_SUPPLY SUP WHERE SUP.SUPPLY_NAME = '" + vendorName + "'),\n" +
				"      (SELECT MRKT.MARKET_PK FROM T_MARKET MRKT WHERE MARKET_NAME = '" + marketName + "'),\n" +
				"      (SELECT BID_PK FROM T_BID WHERE BID_NAME = '" + bidName + "'),\n" +
				"      SYSDATE, \n" +
				"      'N', \n" +
				"      'N' FROM DUAL\n" +
				"      WHERE NOT EXISTS (SELECT VB.BID_PK\n" +
				"                  		 FROM T_VENDOR_BID VB, T_BID BID\n" +
				"                  		 WHERE VB.BID_PK = BID.BID_PK\n" +
				"                            AND BID.BID_NAME = '" + bidName + "')";
	}

	public static String getStmtDeleteVendorBidRecord(String bidName) {
		return "DELETE T_VENDOR_BID\n" +
				"WHERE BID_PK =\n" +
				"  (SELECT BID_PK FROM T_BID\n" +
				"    where bid_name = '" + bidName + "')";
	}

	public static String getStmtVendorBidRecord(String bidName, String marketName, String vendorName){
		return "SELECT COUNT('X') COUNT_ROWS \n" +
				"FROM T_VENDOR_BID VB, T_BID BID, T_MARKET MRKT, T_SUPPLY SUP \n" +
				"WHERE BID.BID_PK = VB.BID_PK \n" +
				"  AND VB.MARKET_PK = MRKT.MARKET_PK \n" +
				"  AND VB.SUPPLY_ID = SUP.SUPPLY_ID  \n" +
				"  AND BID.BID_NAME = '" + bidName + "'\n" +
				"  AND MRKT.MARKET_NAME = '" + marketName + "'\n" +
				"  AND SUP.SUPPLY_NAME = '" + vendorName+ "'";

	}

	public static String deleteVendorBidProductAuditForBidSheet(String bidName) {
		return "delete from t_vendor_bid_audit vba\n" +
				"where vba.vendor_bid_product_pk in\n" +
				"(select vendor_bid_product_pk\n" +
				" from t_vendor_bid_product\n" +
				" where bid_pk = \n" +
				" (select bid_pk\n" +
				" FROM T_BID\n" +
				" where bid_name = '" + bidName + "'))";
	}
	public static String deleteVendorBidProductRecordsForBidSheet(String bidName) {
		return	"delete from t_vendor_bid_product vbp\n" +
				"where vbp.bid_pk =\n" +
				"(select bid_pk\n" +
				" from t_bid\n" +
				" where bid_name = '" + bidName + "')";
	}

	public static String getStmtFindBidSheetEndDate(String bidSheetName) {
		return "SELECT to_char(bid.contract_end_date, 'mm/dd/yyyy') bid_sheet_end_date\n" +
				"  FROM t_bid bid\n" +
				" WHERE upper(bid.bid_name) = upper('"+ bidSheetName +"')\n";
	}

}