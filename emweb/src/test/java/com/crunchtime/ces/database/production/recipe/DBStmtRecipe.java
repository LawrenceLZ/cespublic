package com.crunchtime.ces.database.production.recipe;

import org.testng.Assert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DBStmtRecipe {
	private static final String root = System.getProperty("user.dir");
	//Base folder name co-relates to test package name
	private static final String dbResourcesBaseFolderName = "DBStmtRecipe";
	//file name co-relates to database resource sql file name
	private static final String deleteRecipesFileName = "Delete_Recipe";

	//get the bid price based on Vendor Product #, Vendor, Market, Effective Date
	public static void deleteRecipes(String dbConnection, String recipeNamesPattern) throws IOException, InterruptedException {
		Process p;
		try {
			String line;

			//******ONLY need to define sqlFolderFilePath******
			String sqlMethodFileName = deleteRecipesFileName;

			//***DO NOT CHANGE*** root, dbResourcesBasedir and basedir dir stays the same
			String basedir = root + "\\src\\test\\resources\\Database\\" + dbResourcesBaseFolderName + "\\" + sqlMethodFileName;

			//build the sqlplus cmd
			String sqlCmd = "cmd /c exit|sqlplus -L " + dbConnection + " @" + basedir + " \"" + recipeNamesPattern + "\"";

			//System.out.println(basedir); //for debugging purpose
			System.out.println(sqlCmd);
			p = Runtime.getRuntime().exec(sqlCmd);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = reader.readLine()) != null) {
				//print line is used for debugging
				System.out.println(line);
				boolean errOne;
				errOne = line.contains("ERROR");
				boolean errTwo;
				errTwo = line.contains("unable");
				Assert.assertFalse(errOne || errTwo, line);
			}
			reader.close();
			System.out.println("Done.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(1000);
	}
}