package com.crunchtime.ces.database.packageType;

public class DBStmtPackageTypes {

	public static String getStmtSetupDelete() {
		return "DELETE T_PACKAGE_TYPE PT\n" +
				"WHERE EXISTS (SELECT PT2.PACKAGE_PK\n" +
				"              FROM T_PACKAGE_TYPE PT2\n" +
				"              WHERE PT.PACKAGE_TYPE in ('AUTOPT003', 'AUTOPT001')\n" +
				"                AND PT2.PACKAGE_TYPE = PT.PACKAGE_TYPE)";
	}

	public static String getStmtSetupUpdatePT002() {
		return "UPDATE T_PACKAGE_TYPE\n" +
				"SET PACKAGE_DESCRIPTION = 'Created in Setup 002',\n" +
				"    PACKAGE_INVENTORY = 'Y',\n" +
				"    PACKAGE_RECIPE = 'Y',\n" +
				"    PACKAGE_PURCHASING = 'Y'\n" +
				"WHERE PACKAGE_TYPE = 'AUTOPT002'";
	}

	public static String getStmtSetupUpdatePT008() {
		return "UPDATE T_PACKAGE_TYPE\n" +
				"SET PACKAGE_DESCRIPTION = null,\n" +
				"    PACKAGE_INVENTORY = 'Y',\n" +
				"    PACKAGE_RECIPE = 'Y',\n" +
				"    PACKAGE_PURCHASING = 'Y'\n" +
				"WHERE PACKAGE_TYPE = 'AUTOPT008'";
	}

	public static String getStmtSetupUpdateCheckboxes() {
		return "UPDATE T_PACKAGE_TYPE\n" +
				"SET PACKAGE_INVENTORY = 'Y',\n" +
				"    PACKAGE_RECIPE = 'Y',\n" +
				"    PACKAGE_PURCHASING = 'Y'\n" +
				"WHERE UPPER(PACKAGE_TYPE) like UPPER('AUTOPT%')";
	}

	public static String getStmtSetCheckboxesToNForAPT(String packageType) {
		return "UPDATE T_PACKAGE_TYPE\n" +
				"SET PACKAGE_INVENTORY = 'N',\n" +
				"    PACKAGE_RECIPE = 'N',\n" +
				"    PACKAGE_PURCHASING = 'N'\n" +
				"WHERE UPPER(PACKAGE_TYPE) like UPPER('" + packageType + "')";
	}

	public static String getStmtSetupUpdatePT003() {
		return "UPDATE T_PACKAGE_TYPE\n" +
				"SET REPLICATED_FLAG = 'N'\n" +
				"WHERE PACKAGE_TYPE = 'AUTOPT003'";
	}

	public static String getStmtSetupInsert() {
		return "INSERT INTO T_PACKAGE_TYPE\n" +
				"(PACKAGE_PK,\n" +
				" PACKAGE_TYPE,\n" +
				" PACKAGE_DESCRIPTION,\n" +
				" REPLICATED_FLAG,\n" +
				" PACKAGE_INVENTORY,\n" +
				" PACKAGE_RECIPE,\n" +
				" PACKAGE_PURCHASING)\n" +
				"SELECT\n" +
				" SEQ_T_PACKAGE_TYPE.NEXTVAL,\n" +
				" 'AUTOPT003',\n" +
				" 'Created in Setup 003',\n" +
				" 'N',\n" +
				" 'Y',\n" +
				" 'Y',\n" +
				" 'Y'\n" +
				"FROM DUAL\n" +
				"WHERE NOT EXISTS (SELECT NULL\n" +
				"                 FROM T_PACKAGE_TYPE\n" +
				"                 WHERE PACKAGE_TYPE = 'AUTOPT003')";
	}

	public static String getStmtGetPackageTypeDetails(String packageType) {
		return "SELECT PTYP.PACKAGE_PK             PACKAGE_PK,\n" +
				"       PTYP.PACKAGE_TYPE          PACKAGE_TYPE,\n" +
				"       PTYP.PACKAGE_DESCRIPTION   PACKAGE_DESC,\n" +
				"       PTYP.PACKAGE_INVENTORY     PACKAGE_INV,\n" +
				"       PTYP.PACKAGE_RECIPE        PACKAGE_RCP,\n" +
				"       PTYP.PACKAGE_PURCHASING    PACKAGE_PURCH,\n" +
				" 		PTYP.REPLICATED_FLAG 	   REPLICATED_FLAG\n" +
				"FROM T_PACKAGE_TYPE PTYP\n" +
				"WHERE PTYP.PACKAGE_TYPE = '" + packageType + "'";
	}

	public static String getStmtGetAllPackageTypeDtlWithNameLike(String packageType) {
		return "SELECT PTYP.PACKAGE_PK            PK,\n" +
				"       PTYP.PACKAGE_TYPE          PACKAGE_TYPE,\n" +
				"       PTYP.PACKAGE_DESCRIPTION   PACKAGE_DESC,\n" +
				"       PTYP.PACKAGE_INVENTORY     PACKAGE_INV,\n" +
				"       PTYP.PACKAGE_RECIPE        PACKAGE_RCP,\n" +
				"       PTYP.PACKAGE_PURCHASING    PACKAGE_PURCH\n" +
				"FROM T_PACKAGE_TYPE PTYP\n" +
				"WHERE PTYP.PACKAGE_TYPE like '" + packageType + "'";
	}

	public static String getStmtGetAllPackageTypeDtlWithNameBetween(String packageTypeFirst, String packageTypeLast) {
		return "SELECT PTYP.PACKAGE_PK            PK,\n" +
				"       PTYP.PACKAGE_TYPE          PACKAGE_TYPE,\n" +
				"       PTYP.PACKAGE_DESCRIPTION   PACKAGE_DESC,\n" +
				"       PTYP.PACKAGE_INVENTORY     PACKAGE_INV,\n" +
				"       PTYP.PACKAGE_RECIPE        PACKAGE_RCP,\n" +
				"       PTYP.PACKAGE_PURCHASING    PACKAGE_PURCH\n" +
				"FROM T_PACKAGE_TYPE PTYP\n" +
				"WHERE UPPER(PTYP.PACKAGE_TYPE) BETWEEN UPPER('" + packageTypeFirst + "') AND UPPER('" + packageTypeLast + "')";
	}

	public static String updateTableValueBasedOnPK(String tableName, String tableParameter, String targetValue, String tablePKName, String tablePKValue){
		return	"UPDATE " + tableName + "\n"+
				"SET " + tableParameter + " = '" + targetValue + "'\n"+
				"WHERE " + tablePKName + " = " + tablePKValue;
	}

	public static String selectTableMinPK(String tablePKName, String tableName, String whereClause){
		return "SELECT min(" + tablePKName + ") MIN_PK\n" +
				"FROM " + tableName + "\n" + whereClause;
	}

	public static String selectValueBasedOnTablePK(String returnedValue, String tableName, String tablePKName, String tablePKValue){
		return 	"SELECT " + returnedValue + " RETURNED_VALUE\n" +
				"FROM " + tableName + "\n" +
				"WHERE " + tablePKName + " = " + tablePKValue;
	}
}
