package com.crunchtime.ces.database.bidMaster.bidAnalysis;

import com.crunchtime.ces.helper.ActionsEMW;
import org.testng.Assert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DBStmtBidAnalysis {
	//file name co-relates to database resource sql file name
	public static final String getContractFileName = "Validate_Single_Contract";
	public static final String getContractBasedOnPKFileName = "Validate_Single_Contract_On_PK";
	public static final String getBidSheetEndDateFileName = "Find_Bid_Sheet_End_Date_Plus_One";
	public static final String getBidSheetContractsFileName = "Get_Bid_Sheet_Contracts";
	public static final String setupDataFileName = "Setup_Tests";
	public static final String getVendorBidFileName = "Validate_Vendor_Bid";
	public static final String getBestPricesFileName = "Get_Best_Vendor_Bid_Prices";
	public static final String checkDupeContractsFileName = "Check_Dupe_Contracts";
	public static final String getProductsFilteredByCategoryFileName = "Filter_By_Category";
	private static final String root = System.getProperty("user.dir");
	//Base folder name co-relates to test package name
	private static final String dbResourcesBaseFolderName = "DBStmtBidAnalysis";

	//get the bid price based on Vendor Product #, Vendor, Market, Effective Date
	public static void getVendorBid(String dbConnection, String validateField, String companyProductNum, String vendorName, String marketName, String effectiveDate) throws IOException, InterruptedException {
		Process p;
		try {
			String line;
			//******ONLY need to define sqlMethodFileName******
			String sqlMethodFileName = getVendorBidFileName;

			//root, dbResourcesBasedir and basedir dir stays the same
			String basedir = root + "\\src\\test\\resources\\Database\\" + dbResourcesBaseFolderName + "\\" + sqlMethodFileName;
			//for debugging purpose
			String sqlCmd = "cmd /c exit|sqlplus -L " + dbConnection + " @" + basedir + " " + basedir + ".txt \"" + validateField + "\" \"" + companyProductNum + "\" \"" + vendorName + "\" \"" + marketName + "\" \"" + effectiveDate + "\"";
			p = Runtime.getRuntime().exec(sqlCmd);

			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = reader.readLine()) != null) {
				//print line is used for debugging
				System.out.println(line);
				boolean errOne;
				errOne = line.contains("ERROR");
				boolean errTwo;
				errTwo = line.contains("unable");
				Assert.assertFalse(errOne || errTwo, line);
			}
			reader.close();
			System.out.println("Done.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(1000);
	}

	//get the bid sheet contracts based on Bid Sheet Name, Market, Effective Date
	public static void getBidSheetContracts(String dbConnection, String validateField, String bidSheet, String marketName, String effectiveDate) throws IOException, InterruptedException {
		Process p;
		try {
			String line;
			//******ONLY need to define sqlMethodFileName******
			String sqlMethodFileName = getBidSheetContractsFileName;

			//root, dbResourcesBasedir and basedir dir stays the same
			String basedir = root + "\\src\\test\\resources\\Database\\" + dbResourcesBaseFolderName + "\\" + sqlMethodFileName;

			String sqlCmd = "cmd /c exit|sqlplus -L " + dbConnection + " @" + basedir + " " + basedir + ".txt \"" + validateField + "\" \"" + bidSheet + "\" \"" + marketName + "\" \"" + effectiveDate + "\"";

			System.out.println(sqlCmd);

			p = Runtime.getRuntime().exec(sqlCmd);

			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = reader.readLine()) != null) {
				//print line is used for debugging
				System.out.println(line);
				boolean errOne;
				errOne = line.contains("ERROR");
				boolean errTwo;
				errTwo = line.contains("Unable");
				Assert.assertFalse(errOne || errTwo, line);
			}
			reader.close();
			System.out.println("Done.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(1000);
	}

	//get the bid sheet contracts based on Bid Sheet Name, Market, Effective Date
	public static void getBestPrices(String dbConnection, String effectiveDate, String bidSheet, String marketName) throws IOException, InterruptedException {
		Process p;
		try {
			String line;
			//******ONLY need to define sqlMethodFileName******
			String sqlMethodFileName = getBestPricesFileName;

			//root, dbResourcesBasedir and basedir dir stays the same
			String basedir = root + "\\src\\test\\resources\\Database\\" + dbResourcesBaseFolderName + "\\" + sqlMethodFileName;

			String sqlCmd = "cmd /c exit|sqlplus -L " + dbConnection + " @" + basedir + " " + basedir + ".txt \"" + effectiveDate + "\" \"" + bidSheet + "\" \"" + marketName + "\"";

			p = Runtime.getRuntime().exec(sqlCmd);

			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = reader.readLine()) != null) {
				//print line is used for debugging
				System.out.println(line);
				boolean errOne;
				errOne = line.contains("ERROR");
				boolean errTwo;
				errTwo = line.contains("unable");
				Assert.assertFalse(errOne || errTwo, line);
			}
			reader.close();
			System.out.println("Done.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(1000);
	}

	//get the contract based on Company Product #, Market, Effective Date
	public static void getContract(String dbConnection, String validateField, String companyProductNum, String marketName, String effectiveDate) throws IOException, InterruptedException {
		Process p;
		try {
			String line;
			//******ONLY need to define sqlMethodFileName******
			String sqlMethodFileName = getContractFileName;

			//root, dbResourcesBasedir and basedir dir stays the same
			String basedir = root + "\\src\\test\\resources\\Database\\" + dbResourcesBaseFolderName + "\\" + sqlMethodFileName;

			String sqlCmd = "cmd /c exit|sqlplus -L " + dbConnection + " @" + basedir + " " + basedir + ".txt \"" + validateField + "\" \"" + companyProductNum + "\" \"" + marketName + "\" \"" + effectiveDate + "\"";

			//for debugging purpose
			System.out.println(sqlCmd);

			p = Runtime.getRuntime().exec(sqlCmd);

			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = reader.readLine()) != null) {
				//print line is used for debugging
				System.out.println(line);
				boolean errOne;
				errOne = line.contains("ERROR");
				boolean errTwo;
				errTwo = line.contains("unable");
				Assert.assertFalse(errOne || errTwo, line);
			}
			reader.close();
			System.out.println("Done.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(1000);
	}

	//get the bid price based on Company Product #, Market, Effective Date
	public static void getContractBasedOnPK(String dbConnection, String validateField, String contractPricePK) throws IOException, InterruptedException {
		Process p;
		try {
			String line;
			//******ONLY need to define sqlMethodFileName******
			String sqlMethodFileName = getContractBasedOnPKFileName;

			//root, dbResourcesBasedir and basedir dir stays the same
			String basedir = root + "\\src\\test\\resources\\Database\\" + dbResourcesBaseFolderName + "\\" + sqlMethodFileName;

			//for debugging purpose
			System.out.println(basedir);

			String sqlCmd = "cmd /c exit|sqlplus -L " + dbConnection + " @" + basedir + " " + basedir + ".txt \"" + validateField + "\" \"" + contractPricePK + "\"";

			//for debugging purpose
			System.out.println(sqlCmd);

			p = Runtime.getRuntime().exec(sqlCmd);

			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = reader.readLine()) != null) {
				//print line is used for debugging
				System.out.println(line);
				boolean errOne;
				errOne = line.contains("ERROR");
				boolean errTwo;
				errTwo = line.contains("unable");
				Assert.assertFalse(errOne || errTwo, line);
			}
			reader.close();
			System.out.println("Done.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(1000);
	}

	//find the Bid Sheet End Date based on the given Bid Sheet Name
	public static void getBidSheetEndDatePlusOne(String dbConnection, String bidSheetName) throws IOException, InterruptedException {
		Process p;
		try {
			String line;

			//******ONLY need to define sqlFolderFilePath******
			String sqlMethodFileName = getBidSheetEndDateFileName;
			//***DO NOT CHANGE*** root, dbResourcesBasedir and basedir dir stays the same
			String basedir = root + "\\src\\test\\resources\\Database\\" + dbResourcesBaseFolderName + "\\" + sqlMethodFileName;

			String sqlCmd = "cmd /c exit|sqlplus -L " + dbConnection + " @" + basedir + " " + basedir + ".txt \"" + bidSheetName + "\"";
			//for debugging purpose
			//System.out.println(sqlCmd);

			p = Runtime.getRuntime().exec(sqlCmd);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = reader.readLine()) != null) {
				//print line is used for debugging
				System.out.println(line);
				boolean errOne;
				errOne = line.contains("ERROR");
				boolean errTwo;
				errTwo = line.contains("unable");
				Assert.assertFalse(errOne || errTwo, line);
			}
			reader.close();
			System.out.println("Done.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(1000);
	}

	//get the products that should be visible when filtered by category
	public static void getProductsFilteredByCategory(String dbConnection, String categoryName, String bidSheet, String effectiveDate) throws IOException, InterruptedException {

		Process p;
		try {
			String line;
			//******ONLY need to define sqlMethodFileName******
			String sqlMethodFileName = getProductsFilteredByCategoryFileName;

			//root, dbResourcesBasedir and basedir dir stays the same
			String basedir = root + "\\src\\test\\resources\\Database\\" + dbResourcesBaseFolderName + "\\" + sqlMethodFileName;

			String sqlCmd = "cmd /c exit|sqlplus -L " + dbConnection + " @" + basedir + " " + basedir + ".txt \""
					+ categoryName + "\" \"" + bidSheet + "\" \"" + effectiveDate + "\"";

			p = Runtime.getRuntime().exec(sqlCmd);

			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = reader.readLine()) != null) {
				//print line is used for debugging
				System.out.println(line);
				boolean errOne;
				errOne = line.contains("ERROR");
				boolean errTwo;
				errTwo = line.contains("unable");
				Assert.assertFalse(errOne || errTwo, line);
			}
			reader.close();
			System.out.println("Done.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(1000);
	}

	//Setup data needed for tests
	public static void setupData(String dbConnection, String effectiveDate) throws IOException, InterruptedException {
		Process p;
		try {
			String line;

			//******ONLY need to define sqlFolderFilePath******
			String sqlMethodFileName = setupDataFileName;
			//***DO NOT CHANGE*** root, dbResourcesBasedir and basedir dir stays the same
			String basedir = root + "\\src\\test\\resources\\Database\\" + dbResourcesBaseFolderName + "\\" + sqlMethodFileName;

			String sqlCmd = "cmd /c exit|sqlplus -L " + dbConnection + " @" + basedir + " " + effectiveDate;

			p = Runtime.getRuntime().exec(sqlCmd);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = reader.readLine()) != null) {
				//print line is used for debugging
				System.out.println(line);
				boolean errOne;
				errOne = line.contains("ERROR");
				boolean errTwo;
				errTwo = line.contains("unable");
				Assert.assertFalse(errOne || errTwo, line);
			}
			reader.close();
			System.out.println("Done.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(1000);
	}

	//Check for dupe contracts
	public static void checkDupeContracts(String dbConnection, String bidSheetName) throws IOException, InterruptedException {
		Process p;
		try {
			String line;

			//******ONLY need to define sqlFolderFilePath******
			String sqlMethodFileName = checkDupeContractsFileName;
			//***DO NOT CHANGE*** root, dbResourcesBasedir and basedir dir stays the same
			String basedir = root + "\\src\\test\\resources\\Database\\" + dbResourcesBaseFolderName + "\\" + sqlMethodFileName;

			String sqlCmd = "cmd /c exit|sqlplus -L " + dbConnection + " @" + basedir + " " + basedir + ".txt \"" + bidSheetName + "\"";

			p = Runtime.getRuntime().exec(sqlCmd);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = reader.readLine()) != null) {
				//print line is used for debugging
				System.out.println(line);
				boolean errOne;
				errOne = line.contains("ERROR");
				boolean errTwo;
				errTwo = line.contains("unable");
				Assert.assertFalse(errOne || errTwo, line);
			}
			reader.close();
			System.out.println("Done.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(1000);
	}

	//Method is used to read sql output files with more than one field per line the default delimiter that should be used for these files is ':del:'
	public static String[][] readSQLMultRecordsPerLine(String dbMethodClassName, String sqlFileName) throws Exception {

		String sqlResults[] = ActionsEMW.readSqlResults(dbMethodClassName, sqlFileName);

		String[][] sqlDataPerRecord = new String[sqlResults.length][];

		for (int i = 0; i < sqlResults.length; i++) {
			String[] temp = sqlResults[i].split(":del:");
			sqlDataPerRecord[i] = new String[temp.length];
			for (int j = 0; j < temp.length; j++) {
				sqlDataPerRecord[i][j] = temp[j].trim();
			}
		}
		return sqlDataPerRecord;
	}
}



