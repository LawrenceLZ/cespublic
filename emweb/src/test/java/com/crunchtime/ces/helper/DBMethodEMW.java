package com.crunchtime.ces.helper;

import com.crunchtime.ces.base.BaseDBConnectionEMW;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBMethodEMW extends BaseDBConnectionEMW {

	public static Map<String, List<Object>> getSelectSqlMap(String dbConnectionString, String selectSQL) throws SQLException {
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		Map<String, List<Object>> map = null;
		try {
			dbConnection = getConnection(dbConnectionString);
			preparedStatement = dbConnection.prepareStatement(selectSQL);
			ResultSet rs = preparedStatement.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			int columns = md.getColumnCount();
			map = new HashMap<>(columns);
			for (int i = 1; i <= columns; ++i) {
				map.put(md.getColumnName(i), new ArrayList<>());
			}
			while (rs.next()) {
				for (int i = 1; i <= columns; ++i) {
					map.get(md.getColumnName(i)).add(rs.getObject(i));
				}
			}
			return map;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
				System.out.println("******SQL Select Statement is executed.******");
			}
			if (dbConnection != null) {
				dbConnection.close();
				System.out.println("******DB Connection is closed.******");
			}
		}
		return map;
	}

	public static void executeSqlStmt(String dbConnectionString, String sqlStmt) throws SQLException {
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		try {
			dbConnection = getConnection(dbConnectionString);
			preparedStatement = dbConnection.prepareStatement(sqlStmt);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
				System.out.println("******SQL Statement is executed.******");
			}
			if (dbConnection != null) {
				dbConnection.close();
				System.out.println("******DB Connection is closed.******");
			}
		}
	}

//	public static Map<String, List<Object>> getSelectSqlMap(String dbConnectionString, String selectSQL, String setFirstVariable, String setSecondVariable, String setThirdVariable, String setFourthVariable, String setFifthVariable, String setSixthVariable, String setSeventhVariable) throws SQLException {
//		Connection dbConnection = null;
//		PreparedStatement preparedStatement = null;
//		Map<String, List<Object>> map = null;
//		try {
//			dbConnection = getConnection(dbConnectionString);
//			preparedStatement = dbConnection.prepareStatement(selectSQL);
//			if (setFirstVariable != null) {
//				if (!setFirstVariable.isEmpty()){
//					preparedStatement.setString(1, setFirstVariable);
//				}
//			}
//			if (setSecondVariable != null) {
//				if (!setSecondVariable.isEmpty()) {
//					preparedStatement.setString(2, setSecondVariable);
//				}
//			}
//			if (setThirdVariable != null) {
//				if (!setThirdVariable.isEmpty()) {
//					preparedStatement.setString(3, setThirdVariable);
//				}
//			}
//			if (setFourthVariable != null) {
//				if (!setFourthVariable.isEmpty()) {
//					preparedStatement.setString(4, setFourthVariable);
//				}
//			}
//			if (setFifthVariable != null) {
//				if (!setFifthVariable.isEmpty()) {
//					preparedStatement.setString(5, setFifthVariable);
//				}
//			}
//			if (setSixthVariable != null) {
//				if (!setSixthVariable.isEmpty()) {
//					preparedStatement.setString(6, setSixthVariable);
//				}
//			}
//			if (setSeventhVariable != null) {
//				if (!setSeventhVariable.isEmpty()) {
//					preparedStatement.setString(7, setSeventhVariable);
//				}
//			}
//			ResultSet rs = preparedStatement.executeQuery();
//			ResultSetMetaData md = rs.getMetaData();
//			int columns = md.getColumnCount();
//			map = new HashMap<>(columns);
//			for (int i = 1; i <= columns; ++i) {
//				map.put(md.getColumnName(i), new ArrayList<>());
//			}
//			while (rs.next()) {
//				for (int i = 1; i <= columns; ++i) {
//					map.get(md.getColumnName(i)).add(rs.getObject(i));
//				}
//			}
//			return map;
//		} catch (SQLException e) {
//			System.out.println(e.getMessage());
//		} finally {
//			if (preparedStatement != null) {
//				preparedStatement.close();
//				System.out.println("******SQL Select Statement is executed.******");
//			}
//			if (dbConnection != null) {
//				dbConnection.close();
//				System.out.println("******DB Connection is closed.******");
//			}
//		}
//		return map;
//	}

//	public static void executeSqlStmt(String dbConnectionString, String sqlStmt, String setFirstVariable, String setSecondVariable, String setThirdVariable, String setFourthVariable, String setFifthVariable, String setSixthVariable, String setSeventhVariable) throws SQLException {
//		Connection dbConnection = null;
//		PreparedStatement preparedStatement = null;
//		try {
//			dbConnection = getConnection(dbConnectionString);
//			preparedStatement = dbConnection.prepareStatement(sqlStmt);
//			if (setFirstVariable != null) {
//				if (!setFirstVariable.isEmpty()){
//					preparedStatement.setString(1, setFirstVariable);
//				}
//			}
//			if (setSecondVariable != null) {
//				if (!setSecondVariable.isEmpty()) {
//					preparedStatement.setString(2, setSecondVariable);
//				}
//			}
//			if (setThirdVariable != null) {
//				if (!setThirdVariable.isEmpty()) {
//					preparedStatement.setString(3, setThirdVariable);
//				}
//			}
//			if (setFourthVariable != null) {
//				if (!setFourthVariable.isEmpty()) {
//					preparedStatement.setString(4, setFourthVariable);
//				}
//			}
//			if (setFifthVariable != null) {
//				if (!setFifthVariable.isEmpty()) {
//					preparedStatement.setString(5, setFifthVariable);
//				}
//			}
//			if (setSixthVariable != null) {
//				if (!setSixthVariable.isEmpty()) {
//					preparedStatement.setString(6, setSixthVariable);
//				}
//			}
//			if (setSeventhVariable != null) {
//				if (!setSeventhVariable.isEmpty()) {
//					preparedStatement.setString(7, setSeventhVariable);
//				}
//			}
//			preparedStatement.executeUpdate();
//		} catch (SQLException e) {
//			System.out.println(e.getMessage());
//		} finally {
//			if (preparedStatement != null) {
//				preparedStatement.close();
//				System.out.println("******SQL Statement is executed.******");
//			}
//			if (dbConnection != null) {
//				dbConnection.close();
//				System.out.println("******DB Connection is closed.******");
//			}
//		}
//	}
}