package com.crunchtime.ces.helper;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.MoveTargetOutOfBoundsException;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ActionsEMW {
	private static final String root = System.getProperty("user.dir");

	public static String getBrowserName(WebDriver driver) {
		Capabilities capabilities = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = capabilities.getBrowserName();
		if (browserName.toUpperCase().contains("INTERNET")) return "InternetExplorer";
		if (browserName.toUpperCase().contains("FIREFOX")) return "Firefox";
		if (browserName.toUpperCase().contains("CHROME")) return "Chrome";
		else return browserName;
	}

	//check if Alert appears on screen
	public static boolean isAlertPresent(WebDriver driver) {
		boolean alertFlag = false;
		try {
			String alertMessage = driver.switchTo().alert().getText().trim();
			System.out.println(alertMessage);
			return true;
		} catch (Exception e) {
			//e.printStackTrace();
			e.getCause().getMessage();
		}
		return alertFlag;
	}

	//set select tag dropdown by number
	public static void setSelectDropdownByNum(WebElement element, int number) {
		Select dropdown = new Select(element);
		dropdown.selectByIndex(number);
	}

	//set select tag dropdown by value
	public static void setSelectDropdownByValue(WebElement element, String value) {
		Select dropdown = new Select(element);
		dropdown.selectByVisibleText(value);
	}

	//get select tag dropdown value
	public static String getSelectDropdownValue(WebElement element) {
		Select dropdownList = new Select(element);
		WebElement selectedElement = dropdownList.getFirstSelectedOption();
		return selectedElement.getText();
	}

	/**
	 * set the ExtJS implemented dropdown that with ul/li comboBox,
	 * by entering value to the input field.
	 * Also use JS injection to deal with the exception of
	 * MoveTargetOutOfBoundsException
	 */
	public static void setComboBoxDropdownByValue(WebDriver driver, WebElement comboBoxInputElement, String value) throws Exception {
		org.openqa.selenium.interactions.Actions comboBoxInputAction = new org.openqa.selenium.interactions.Actions(driver);
		Waiter.waitForOneSecond();
		comboBoxInputAction.click(comboBoxInputElement).perform();
		comboBoxInputElement.clear();
		Waiter.waitForOneSecond();
		comboBoxInputAction.sendKeys(comboBoxInputElement, value).sendKeys(Keys.ARROW_DOWN).perform();
		Waiter.waitForOneSecond();
		//Change xpath to css as Chrome and IE do not like xpath finder
		String cssSelectorPath = "div:not([style*='display: none']) div ul li";
		List<WebElement> liElements = driver.findElements(By.cssSelector(cssSelectorPath));
		String liElementValue;
		for (WebElement liElement : liElements) {
			liElementValue = liElement.getText();
			if (liElementValue.equals(value)) {
				try {
					comboBoxInputAction.click(liElement).build().perform();
				} catch (MoveTargetOutOfBoundsException e) {
					//use js click to deal with out of bound issue
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", liElement);
				}
			}
		}

		Waiter.waitForOneSecond();
	}

	//NOTE that the grid has to be a tag of tbody
	public static int getEMWGridRowCnt(WebElement grid) {
		return grid.findElements(By.cssSelector("tr")).size();
	}

	//NOTE that the grid has to be a tag of tbody
	public static int getEMWGridRowNumByCellValue(WebElement grid, String cellValue) {
		int rowCnt = grid.findElements(By.cssSelector("tr")).size();
		int rowNum = 0;
		for (int i = 1; i <= rowCnt; i++) {
			String evlString = grid.findElement(By.cssSelector("tr:nth-child(" + i + ")")).getText();
			if (evlString.contains(cellValue)) {
				rowNum = i;
				break;
			}
		}
		return rowNum;
	}

	//NOTE that the grid has to be a tag of tbody
	public static int getEMWGridColumnCntPerRowNum(WebElement grid, int rowNum) {
		return grid.findElements(By.cssSelector("tr:nth-child(" + rowNum + ")>td")).size();
	}

	public static void takeScreenshot(WebDriver driver, String nameOfOutputFile, WebElement elementCanBeNullForThisMethod) throws IOException, InterruptedException {
		Thread.sleep(5000);
		File scrFile;
		scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		if (elementCanBeNullForThisMethod != null) {
			//get the WebElement location
			Point p = elementCanBeNullForThisMethod.getLocation();
			//then get the WebElement's coordinates
			int width = elementCanBeNullForThisMethod.getSize().getWidth();
			int height = elementCanBeNullForThisMethod.getSize().getHeight();
			//resize the snapshot based on the WebElement
			BufferedImage img;
			img = ImageIO.read(scrFile);
			BufferedImage dest = img.getSubimage(p.getX(), p.getY(), width, height);
			ImageIO.write(dest, "png", scrFile);
		}
		String filePath = root + "\\src\\test\\resources\\ScreenShots\\Captured\\" + nameOfOutputFile + ".png";
		File destination = new File(filePath);
		System.out.println("Captured Screenshot is at " + destination.getAbsolutePath());
		FileUtils.copyFile(scrFile, destination);
		Thread.sleep(2000);
	}

	public static String setDateBasedOnToday(String dateFormat, Integer incrementTodayDateBy) throws ParseException {
		//set current date format
		SimpleDateFormat dateFormatDefined = new SimpleDateFormat(dateFormat);
		String date = dateFormatDefined.format(new Date());
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateFormatDefined.parse(date));
		cal.add(Calendar.DATE, incrementTodayDateBy);
		date = dateFormatDefined.format(cal.getTime());
		return date;
	}

	//boolean check to see if WebElement is present
	public static boolean isElementPresent(WebElement element) {
		try {
			//println is just for logging purpose
			element.isDisplayed();
			//String tagName = element.getTagName();
			//System.out.println("present element tagName is " + tagName);
			return true;
		} catch (NoSuchElementException e) {
			//System.out.println("==================print StackTrace for NoSuchElementException is merely for logging purpose==================");
			//e.getCause().printStackTrace();
			return false;
		}
	}

	//method is used to read sql spooled file result
	public static String[] readSqlResults(String dbMethodClassName, String sqlFileName) throws Exception {
		List<String> strLines = new ArrayList<>();
		try {
			// Open the file and read line
			String dbMethodSpoolFilePath = dbMethodClassName + "\\" + sqlFileName;
			String root = System.getProperty("user.dir");
			String dbResourcesBasedir = root + "\\src\\test\\resources\\Database\\";
			String basedir = dbResourcesBasedir + dbMethodSpoolFilePath;

			//print out the basedir for debugging purpose only
			//System.out.println("realsqlResults file dir is: " + basedir + ".txt");
			FileReader fileReader = new FileReader(basedir + ".txt");
			// Get the object of bufferReader
			BufferedReader br = new BufferedReader(fileReader);
			//List<String> strLines = new ArrayList<>();
			String strLine;
			//Read File Line By Line
			while ((strLine = br.readLine()) != null) {
				// Print the content on the console
				strLines.add(strLine);
			}
			//Close the bufferReader
			br.close();
			return strLines.toArray(new String[strLines.size()]);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Fail to get sql spooled result");
			return strLines.toArray(new String[strLines.size()]);
		}
	}

	//Image Diff Validation method
	public static void pDiff(String screenName) throws IOException, InterruptedException {
		try {
			File destination = new File(root + "\\src\\test\\resources\\ScreenShots\\Captured\\" + screenName + "_pDiff.png");
			if (destination.delete()) {
				System.out.println(destination + " of the prior test is deleted.");
			} else {
				System.out.println("No prior pDiff file to delete.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Process p;
		try {
			String pDiffString = root + "\\src\\test\\resources\\PDiff\\perceptualdiff " + root +
					"\\src\\test\\resources\\ScreenShots\\Captured\\" + screenName + ".png " + root + "\\src\\test\\resources\\ScreenShots\\ImageGold\\" + screenName + "_GOLD.png -verbose -threshold 100 -colorfactor 1.0 -gamma 1.0 -output " +
					root + "\\src\\test\\resources\\ScreenShots\\Captured\\" + screenName + "_pDiff.png";
			p = Runtime.getRuntime().exec(pDiffString);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line = reader.readLine();
			while (line != null) {
				line = reader.readLine();
				StringBuilder sb = new StringBuilder();
				String validateString = sb.append(line).toString();
				//print line is used for debugging
				System.out.println(validateString);
				boolean b = validateString.contains("FAIL");
				Assert.assertFalse(b, validateString);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		//Thread.sleep(15000);
	}

	public static void setTestMethodDescription(String jiraId, String testDescription) {
		String testName = Reporter.getCurrentTestResult().getMethod().getMethodName();
		String className = Reporter.getCurrentTestResult().getTestClass().getName();
		Reporter.log("<style type=\"text/css\">");
		Reporter.log(".tg {border-collapse:collapse;border-spacing:0;border-color:#999;table-layout:fixed;width:830px;}");
		Reporter.log(".tg td{font-family:Verdana;font-size:9px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;word-wrap:break-word;border-color:#999;color:#444;background-color:#F7FDFA;text-align:left;}");
		Reporter.log(".tg th{font-family:Verdana;font-size:10px;font-weight:bold;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}");
		Reporter.log("</style><table class=\"tg\"><tbody><col width=\"70px\"> <col width=\"260px\"> <col width=\"200px\"><col width=\"300px\">");
		String jiraIdUrl = "http://jira.crunchtime.local/browse/" + jiraId.toUpperCase();
		Reporter.log("<tr><td style=\"word-wrap:normal;\"><a target=\"_blank\" href=\""+ jiraIdUrl + "\" style=\"display:block;\">"+ jiraId.toUpperCase() +"</a></td>");
		Reporter.log("<td>"+ testDescription +"</td>");
		Reporter.log("<td>"+ testName +"</td>");
		Reporter.log("<td>" + className + "</td></tr></tbody></table>");
	}
}