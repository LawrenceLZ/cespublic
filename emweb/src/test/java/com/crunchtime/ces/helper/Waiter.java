package com.crunchtime.ces.helper;

import com.google.common.base.Predicate;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class Waiter {
	private static final int EXPLICIT_TIMEOUT_SECOND = 120;
	private static final int SLEEP_MILLI_SECOND = 1000;

	public static void waitForOneSecond() throws Exception {
		Thread.sleep(SLEEP_MILLI_SECOND);
	}

	public static void fluentWaitForElementToBeVisible(WebDriver driver, WebElement element) {
		driver.manage().timeouts().implicitlyWait(EXPLICIT_TIMEOUT_SECOND, TimeUnit.SECONDS);
		new FluentWait<WebElement>(element).withTimeout(EXPLICIT_TIMEOUT_SECOND, TimeUnit.SECONDS).pollingEvery(
				100, TimeUnit.MILLISECONDS
		).ignoring(NoSuchElementException.class).until(new Predicate<WebElement>() {
			@Override
			public boolean apply(WebElement webElement) {
				return webElement.isDisplayed();
			}
		});
	}

	public static void waitForElementToBeVisible(WebDriver driver, WebElement element) throws Exception {
		waitForOneSecond();
		new WebDriverWait(driver, EXPLICIT_TIMEOUT_SECOND, SLEEP_MILLI_SECOND).until(
				ExpectedConditions.visibilityOf(element)
		);
	}

	public static void waitForElementToBePresent(WebDriver driver, WebElement element) throws Exception {
		String idString = element.getAttribute("id");
		waitForOneSecond();
		new WebDriverWait(driver, EXPLICIT_TIMEOUT_SECOND, SLEEP_MILLI_SECOND).until(
				ExpectedConditions.presenceOfElementLocated(By.id(idString))
		);
	}

	public static void waitForElementToDisappear(WebDriver driver, WebElement element) throws Exception {
		waitForOneSecond();
		new WebDriverWait(driver, EXPLICIT_TIMEOUT_SECOND, SLEEP_MILLI_SECOND).until(
				ExpectedConditions.not(ExpectedConditions.visibilityOf(element))
		);
		waitForOneSecond();
	}

	/**
	 * wait until the element is NOT present on page
	 * unlike the waitForElementToDisappear, only wait until the element
	 * not visible.
	 */
	public static void waitForElementToVanish(WebElement element) throws Exception {
		int i;
		for (i = 0; i < EXPLICIT_TIMEOUT_SECOND; i++) {
			waitForOneSecond();
			try {
				element.isDisplayed();
			} catch (NoSuchElementException e) {
				//System.out.println("==================print StackTrace for NoSuchElementException is merely for logging purpose==================");
				//e.getCause().printStackTrace();
				break;
			}
		}
		if (i == EXPLICIT_TIMEOUT_SECOND) {
			throw new IllegalStateException("Timed out to wait for Element to vanish from page, after " + i + " seconds.");
		}
	}

	public static void waitUntilElementAttributeContain(WebElement element, String attribute, String containValue) throws Exception {
		waitForOneSecond();
		int i;
		for (i = 0; i < EXPLICIT_TIMEOUT_SECOND; i++) {
			waitForOneSecond();
			String attributeVal = element.getAttribute(attribute);
			if (attributeVal.contains(containValue)) break;
		}
		if (i == EXPLICIT_TIMEOUT_SECOND) {
			throw new IllegalStateException("Timed out to wait until Element's attribute contain value to be met, after " + i + " seconds.");
		}
	}

	public static void waitUntilElementAttributeNotContain(WebElement element, String attribute, String containeValue) throws Exception {
		waitForOneSecond();
		int i;
		for (i = 0; i < EXPLICIT_TIMEOUT_SECOND; i++) {
			waitForOneSecond();
			String attributeVal = element.getAttribute(attribute);
			if (!attributeVal.contains(containeValue)) break;
		}
		if (i == EXPLICIT_TIMEOUT_SECOND) {
			throw new IllegalStateException("Timed out to wait until Element's attribute NOT contain value to be met, after " + i + " seconds.");
		}
	}
}