package com.crunchtime.ces.pages.inventory.inventoryOverview.physicalInventory;

import com.crunchtime.ces.base.PageBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ViewPage extends PageBase {

	private final WebDriver driver;

	public ViewPage(WebDriver driver) {
		this.driver = driver;
	}

	private static final int DEFAULT_TIME_OUT = 10;

	@FindBy(xpath = "//span[@class='x-header-text x-panel-header-text x-panel-header-text-default']")
	public WebElement headerViewProcessPage;
	@FindBy(xpath = "//*[@ces-selenium-id='button']//span[contains(text(),'Close')]")
	public WebElement closeButton;
    @FindBy(xpath = "//img[@class='x-tool-img x-tool-expand-button-collapsed']")
    public WebElement collapsedIcon;
    @FindBy(xpath = "//img[@class='x-tool-img x-tool-filter-button']")
    public WebElement filterIcon;
	@FindBy(xpath = "//img[@class='x-tool-img x-tool-export']")
	public WebElement exportIcon;
}