package com.crunchtime.ces.pages.purchasing.purchasingOverview.widget;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageRecentCommissaryOrdersWidget extends PageBase {

	private final WebDriver driver;

	public PageRecentCommissaryOrdersWidget(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(xpath = "(//span[@class='x-header-text x-window-header-text x-window-header-text-default'])[last()]")
	private WebElement filtersDialogHeader;

	@FindBy(xpath = "(//input[@name='referenceNumber'])[last()]")
	private WebElement referenceNumberInput;

	@FindBy(xpath = "(//input[@name='inventoryValueStatus'])[last()]")
	private WebElement orderStatusCombobox;

	@FindBy(xpath = "(//span[text()='Apply'])[last()]")
	private WebElement applyButton;
	@FindBy(xpath = "(//span[text()='Cancel'])[last()]")
	private WebElement cancelButton;
	@FindBy(xpath = "(//span[text()='Clear'])[last()]")
	private WebElement clearButton;

	@FindBy(css = "[id^='nc-purchasing-commissary-orders'] .x-tool-img.x-tool-plus")
	private WebElement createCommissaryOrderLink;
	@FindBy(css = "input[value=' Close ']")
	private WebElement closeButton;

	public void typeReferenceNumber(String referenceNumberValue) throws Exception {
		Waiter.waitFor(visibilityOf(referenceNumberInput));
		referenceNumberInput.clear();
		referenceNumberInput.sendKeys(referenceNumberValue);
	}

	public void applyButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(applyButton));
		applyButton.click();
	}

	public void clearButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(clearButton));
		clearButton.click();
	}

	public void createCommissaryOrderLinkClick() throws Exception {
		Waiter.waitFor(visibilityOf(createCommissaryOrderLink));
		createCommissaryOrderLink.click();
	}

	public void closeButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(closeButton));
		closeButton.click();
	}
}