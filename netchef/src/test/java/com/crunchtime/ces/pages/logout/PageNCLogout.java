package com.crunchtime.ces.pages.logout;

import com.crunchtime.ces.helper.Waiter;
import com.crunchtime.ces.pages.login.PageNCLogin;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class PageNCLogout {
	private final WebDriver driver;
	private static final String logoutPageUrl = "/session.nc";

	public PageNCLogout(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "a[title='Logout']>img") private List<WebElement> logoutBtnList;
	@FindBy(css = "frame") private List<WebElement> frameList;
	@FindBy(css = "a[href ='/session.nc?method=logoutCheck'] img") private WebElement logoutBtnInFrame;
	@FindBy(css = "input[class='buttonStyle'],[type='submit'],[value=' OK ']") private List<WebElement> okBtnList;
	@FindBy(css = "input[name='cancelButton']") private List<WebElement> cancelBtnList;
	@FindBy(xpath = "//div[contains(text(),'You have successfully logged out of Net-Chef')]") private WebElement logoutStaticTitle;

	public PageNCLogin logoutNC() throws Exception {
		driver.switchTo().defaultContent();
		boolean logoutBtnExists = logoutBtnList.size() != 0;
		boolean logoutBtnFrameExists = frameList.size() != 0;
		if (logoutBtnExists) {
			for (WebElement logoutBtn : logoutBtnList) {
				logoutBtn.click();
			}
		} else if (logoutBtnFrameExists) {
			driver.switchTo().frame(0);
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			logoutBtnInFrame.click();
			driver.switchTo().defaultContent();
			Waiter.waitForOneSecond();
		}
		Waiter.waitForOneSecond();
		boolean okBtnExists = okBtnList.size() != 0;
		boolean cancelBtnExists = cancelBtnList.size() != 0;
		if (okBtnExists) {
			for (WebElement okBtn : okBtnList) {
				okBtn.click();
			}
		} else if (cancelBtnExists) {
			for (WebElement cancelBtn : cancelBtnList) {
				cancelBtn.click();
			}
			Waiter.waitForOneSecond();
			Waiter.waitForElementToBeVisible(driver, logoutStaticTitle);
			for (WebElement okBtn : okBtnList) {
				okBtn.click();
			}
		}
		if (!driver.getCurrentUrl().endsWith(logoutPageUrl)) {
			throw new IllegalStateException("Unable to logout NC to return to login page.");
		}
		return new PageNCLogin(driver);
	}
}