package com.crunchtime.ces.pages.administration.administrationOverview.widget;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageTemplatesWidget extends PageBase {

	private final WebDriver driver;

	public PageTemplatesWidget(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(xpath = "(//span[@class='x-header-text x-window-header-text x-window-header-text-default'])[last()]")
	private WebElement filtersDialogHeader;

	@FindBy(css = "[name='templateName']")
	public WebElement templateNameInput;

	@FindBy(css = "input[name='filter_by_status']")
	private WebElement typeCombobox;

	@FindBy(xpath = "(//span[text()='Apply'])[last()]")
	private WebElement applyButton;
	@FindBy(xpath = "(//span[text()='Cancel'])[last()]")
	private WebElement cancelButton;
	@FindBy(xpath = "(//span[text()='Clear'])[last()]")
	private WebElement clearButton;

	public void selectType(String typeValue) throws Exception {
		Waiter.waitFor(visibilityOf(typeCombobox));
		typeCombobox.click();
		Waiter.waitFor(visibilityOf(driver.findElement(By.xpath("//li[text()='" + typeValue + "']"))));
		driver.findElement(By.xpath("//li[text()='" + typeValue + "']")).click();
	}

	public void applyButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(applyButton));
		applyButton.click();
	}

	public void clearButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(clearButton));
		clearButton.click();
	}
}