package com.crunchtime.ces.pages.inventory.inventoryOverview.physicalInventory;

import com.crunchtime.ces.base.PageBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CountSheetPage extends PageBase {

	private final WebDriver driver;

	public CountSheetPage(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(id = "nc-physical-inventory-count-sheet_header")
	public WebElement headerCountSheetProcessPage;
	@FindBy(xpath = "//a[@ces-selenium-id='button']")
	public WebElement closeButton;
    @FindBy(id = "nc-physical-inventory-count-sheet-body")
    public WebElement gridBodyCountSheetProcessPage;
    @FindBy(xpath = "//img[@class='x-tool-img x-tool-expand-button-collapsed']")
    public WebElement collapsedIcon;
    @FindBy(xpath = "//img[@class='x-tool-img x-tool-filter-button']")
    public WebElement filterIcon;
    @FindBy(xpath = "//img[@class='x-tool-img x-tool-export']")
    public WebElement exportIcon;
}