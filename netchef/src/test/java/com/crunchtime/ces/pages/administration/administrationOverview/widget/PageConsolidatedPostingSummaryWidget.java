package com.crunchtime.ces.pages.administration.administrationOverview.widget;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageConsolidatedPostingSummaryWidget extends PageBase {

	private final WebDriver driver;

	public PageConsolidatedPostingSummaryWidget(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(xpath = "(//span[@class='x-header-text x-window-header-text x-window-header-text-default'])[last()]")
	private WebElement filtersDialogHeader;

	@FindBy(css = "input[name='locationCode']")
	public WebElement locationCodeInput;

	@FindBy(css = "input[name='hierarchycombo']")
	private WebElement hierarchyCombobox;

	@FindBy(xpath = "(//span[text()='Apply'])[last()]")
	private WebElement applyButton;
	@FindBy(xpath = "(//span[text()='Cancel'])[last()]")
	private WebElement cancelButton;
	@FindBy(xpath = "(//span[text()='Clear'])[last()]")
	private WebElement clearButton;

	public void typeLocationCode(String locationCodeValue) throws Exception {
		Waiter.waitFor(visibilityOf(locationCodeInput));
		locationCodeInput.clear();
		locationCodeInput.sendKeys(locationCodeValue);
	}

	public void selectHierarchy(String hierarchyValue) throws Exception {
		Waiter.waitFor(visibilityOf(hierarchyCombobox));
		hierarchyCombobox.click();
		Thread.sleep(100);
		Waiter.waitFor(visibilityOf(driver.findElement(By.xpath("//li[text()='" + hierarchyValue + "']"))));
		driver.findElement(By.xpath("//li[text()='" + hierarchyValue + "']")).click();
	}

	public void applyButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(applyButton));
		applyButton.click();
	}

	public void clearButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(clearButton));
		clearButton.click();
	}
}