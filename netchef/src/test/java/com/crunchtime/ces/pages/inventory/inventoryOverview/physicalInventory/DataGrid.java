package com.crunchtime.ces.pages.inventory.inventoryOverview.physicalInventory;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DataGrid extends PageBase {

	private final WebDriver driver;

	public DataGrid(WebDriver driver) {
		this.driver = driver;
	}

    @FindBy(xpath = "//span[text()='Audit Date']")
    public WebElement auditDateText;
    @FindBy(xpath = "//span[text()='User ID']")
    public WebElement userIDText;
    @FindBy(xpath = "//span[text()='Storage Location']")
    public WebElement storageLocationText;
    @FindBy(xpath = "//span[text()='Seq.']")
    public WebElement seqText;
    @FindBy(xpath = "//span[text()='Product #']")
    public WebElement productNumberText;
    @FindBy(xpath = "//span[text()='Product Name']")
    public WebElement productNameText;
    @FindBy(xpath = "//span[text()='Lot']")
    public WebElement lotText;
    @FindBy(xpath = "//span[text()='Inventory Unit']")
    public WebElement inventoryUnitText;
    @FindBy(xpath = "//span[text()='Unit Price']")
    public WebElement unitPriceText;
    @FindBy(xpath = "//span[text()='Physical Value']")
    public WebElement physicalValueText;
    @FindBy(xpath = "//span[text()='By Exception']")
    public WebElement byExceptionText;
    @FindBy(xpath = "//span[@id='nc-physical-inventory-count-sheet-grid-column-inventoryUnit-textEl']")
    public WebElement inventoryUnitDividedText;
    @FindBy(xpath = "//span[@id='nc-physical-inventory-count-sheet-grid-column-altUnit1-textEl']")
    public WebElement altUnit1Text;
    @FindBy(xpath = "//span[@id='nc-physical-inventory-count-sheet-grid-column-altUnit2-textEl']")
    public WebElement altUnit2Text;
    @FindBy(xpath = "//span[@id='nc-physical-inventory-count-sheet-grid-column-altUnit3-textEl']")
    public WebElement altUnit3Text;

    @FindBy(xpath = "//span[text()='Audit Date']//..//div")
    public WebElement auditDateArrow;
    @FindBy(xpath = "//span[text()='User ID']//..//div")
    public WebElement userIDArrow;
    @FindBy(xpath = "//span[text()='Storage Location']//..//div")
    public WebElement storageLocationArrow;
    @FindBy(xpath = "//span[text()='Seq.']//..//div")
    public WebElement seqArrow;
    @FindBy(xpath = "//span[text()='Product #']//..//div")
    public WebElement productNumberArrow;
    @FindBy(xpath = "//span[text()='Product Name']//..//div")
    public WebElement productNameArrow;
    @FindBy(xpath = "//span[text()='Lot']//..//div")
    public WebElement lotArrow;
    @FindBy(xpath = "//span[text()='Inventory Unit']//..//div")
    public WebElement inventoryUnitArrow;
    @FindBy(xpath = "//span[text()='Unit Price']//..//div")
    public WebElement unitPriceArrow;
    @FindBy(xpath = "//span[text()='Physical Value']//..//div")
    public WebElement physicalValueArrow;
    @FindBy(xpath = "//span[text()='By Exception']//..//div")
    public WebElement byExceptionArrow;
    @FindBy(xpath = "//span[@id='nc-physical-inventory-count-sheet-grid-column-inventoryUnit-textEl']//..//div")
    public WebElement inventoryUnitDividedArrow;
    @FindBy(xpath = "//span[@id='nc-physical-inventory-count-sheet-grid-column-altUnit1-textEl']//..//div")
    public WebElement altUnit1Arrow;
    @FindBy(xpath = "//span[@id='nc-physical-inventory-count-sheet-grid-column-altUnit2-textEl']//..//div")
    public WebElement altUnit2Arrow;
    @FindBy(xpath = "//span[@id='nc-physical-inventory-count-sheet-grid-column-altUnit3-textEl']//..//div")
    public WebElement altUnit3Arrow;

    @FindBy(xpath = "//span[text()='Sort Ascending']")
    public WebElement popupMenuSortAscending;
    @FindBy(xpath = "//span[text()='Sort Descending']")
    public WebElement popupMenuSortDescending;

    public void sorting(String columnName, String direction) throws Exception{
        Actions menuLinkMouseOverAction = new Actions(driver);
        switch (columnName)
        {
            case "Audit Date":
                menuLinkMouseOverAction.moveToElement(auditDateText).build().perform();
                Waiter.waitFor(visibilityOf(auditDateArrow));
                auditDateArrow.click();
                break;
            case "User ID":
                menuLinkMouseOverAction.moveToElement(userIDText).build().perform();
                Waiter.waitFor(visibilityOf(userIDArrow));
                userIDArrow.click();
                break;
            case "Storage Location":
                menuLinkMouseOverAction.moveToElement(storageLocationText).build().perform();
                Waiter.waitFor(visibilityOf(storageLocationArrow));
                storageLocationArrow.click();
                break;
            case "Seq.":
                menuLinkMouseOverAction.moveToElement(seqText).build().perform();
                Waiter.waitFor(visibilityOf(seqArrow));
                seqArrow.click();
                break;
            case "Product #":
                menuLinkMouseOverAction.moveToElement(productNumberText).build().perform();
                Waiter.waitFor(visibilityOf(productNumberArrow));
                productNumberArrow.click();
                break;
            case "Product Name":
                menuLinkMouseOverAction.moveToElement(productNameText).build().perform();
                Waiter.waitFor(visibilityOf(productNameArrow));
                productNameArrow.click();
                break;
            case "Lot":
                menuLinkMouseOverAction.moveToElement(lotText).build().perform();
                Waiter.waitFor(visibilityOf(lotArrow));
                lotArrow.click();
                break;
            case "Inventory Unit":
                menuLinkMouseOverAction.moveToElement(inventoryUnitText).build().perform();
                Waiter.waitFor(visibilityOf(inventoryUnitArrow));
                inventoryUnitArrow.click();
                break;
            case "Unit Price":
                menuLinkMouseOverAction.moveToElement(unitPriceText).build().perform();
                Waiter.waitFor(visibilityOf(unitPriceArrow));
                unitPriceArrow.click();
                break;
            case "Physical Value":
                menuLinkMouseOverAction.moveToElement(physicalValueText).build().perform();
                Waiter.waitFor(visibilityOf(physicalValueArrow));
                physicalValueArrow.click();
                break;
            case "By Exception":
                menuLinkMouseOverAction.moveToElement(byExceptionText).build().perform();
                Waiter.waitFor(visibilityOf(byExceptionArrow));
                byExceptionArrow.click();
                break;
            default:
                break;
        }
        Waiter.waitFor(visibilityOf(popupMenuSortAscending));
        if (direction.equals("Ascending")) {
            popupMenuSortAscending.click();
        }
        else if (direction.equals("Descending")) {
            popupMenuSortDescending.click();
        }
//        Thread.sleep(15000);
        Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
    }

    public int getListItemsCount(String columnName) {
        String id = "";
        List<WebElement> listItems = new ArrayList<WebElement>();
        switch (columnName) {
            case "Audit Date":
                id = auditDateText.getAttribute("id").substring(11, auditDateText.getAttribute("id").length() - 7);
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-datecolumn-" + id + " x-grid-cell-first']//div"));
                break;
            case "User ID":
                id = userIDText.getAttribute("id").substring(11, userIDText.getAttribute("id").length() - 7);
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-gridcolumn-" + id + "']//div"));
                break;
            case "Storage Location":
                id = storageLocationText.getAttribute("id").substring(11, storageLocationText.getAttribute("id").length() - 7);
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-gridcolumn-" + id + " wrappedColumn wrappedColumn']//div"));
                break;
            case "Seq.":
                id = seqText.getAttribute("id").substring(11, seqText.getAttribute("id").length() - 7);
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-gridcolumn-" + id + " x-grid-cell-first']//div"));
                break;
            case "Product #":
                id = productNumberText.getAttribute("id").substring(11, productNumberText.getAttribute("id").length() - 7);
                listItems = driver.findElement(By.xpath("(//table[contains (@class,'-table x-grid-table')])[1]")).findElements(By.xpath("//./td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-gridcolumn-" + id + " wrappedColumn wrappedColumn']//div"));
                break;
            case "Product Name":
                id = productNameText.getAttribute("id").substring(11, productNameText.getAttribute("id").length() - 7);
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-gridcolumn-" + id + " wrappedColumn wrappedColumn']//div"));
                break;
            case "Lot":
                id = lotText.getAttribute("id").substring(11, lotText.getAttribute("id").length() - 7);
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-gridcolumn-" + id + "']//div"));
                break;
            case "Inventory Unit":
                id = inventoryUnitText.getAttribute("id").substring(11, inventoryUnitText.getAttribute("id").length() - 7);
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-gridcolumn-" + id + "']//div"));
                break;
            case "Unit Price":
                id = unitPriceText.getAttribute("id").substring(11, unitPriceText.getAttribute("id").length() - 7);
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-gridcolumn-" + id + " wrappedColumn wrappedColumn']//div//a"));
                break;
            case "Physical Value":
                id = physicalValueText.getAttribute("id").substring(11, physicalValueText.getAttribute("id").length() - 7);
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-gridcolumn-" + id + "']//div"));
                break;
            case "By Exception":
                id = byExceptionText.getAttribute("id").substring(11, byExceptionText.getAttribute("id").length() - 7);
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-gridcolumn-" + id + " x-grid-cell-last']//div"));
                break;
            case "Inventory Unit Divided":
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-nc-physical-inventory-count-sheet-grid-column-inventoryUnit x-grid-cell-first']//div"));
                break;
            case "Alt Unit 1":
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-nc-physical-inventory-count-sheet-grid-column-altUnit1']//div"));
                break;
            case "Alt Unit 2":
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-nc-physical-inventory-count-sheet-grid-column-altUnit2']//div"));
                break;
            case "Alt Unit 3":
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-nc-physical-inventory-count-sheet-grid-column-altUnit3']//div"));
                break;
            default:
//                listItems = new ArrayList<WebElement>();
                break;
        }
        return listItems.size();
    }

    public void checkSorting(String columnName, String direction) {
        String id = "";
        List<WebElement> listItems = new ArrayList<WebElement>();
        List<String> listItemsString = new ArrayList<String>();
        List<Double> listItemsDouble = new ArrayList<Double>();
        List<String> cloneListItemsString = new ArrayList<String>();
        List<Double> cloneListItemsDouble = new ArrayList<Double>();
        List<String> listItemsString2 = new ArrayList<String>();
        List<Double> listItemsDouble2 = new ArrayList<Double>();
        List<String> cloneListItemsString2 = new ArrayList<String>();
        List<Double> cloneListItemsDouble2 = new ArrayList<Double>();
        boolean parseToDouble = false;
        int groupCount = 0;
        System.out.println("Column = '" + columnName + "', Direction = '" + direction + "'");
        switch (columnName)
        {
            case "Audit Date":
                id = auditDateText.getAttribute("id").substring(11, auditDateText.getAttribute("id").length()-7);
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-datecolumn-" + id + " x-grid-cell-first']//div"));
                break;
            case "User ID":
                id = userIDText.getAttribute("id").substring(11, userIDText.getAttribute("id").length()-7);
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-gridcolumn-" + id + "']//div"));
                break;
            case "Storage Location":
                id = storageLocationText.getAttribute("id").substring(11, storageLocationText.getAttribute("id").length()-7);
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-gridcolumn-" + id + " wrappedColumn wrappedColumn']//div"));
                break;
            case "Seq.":
                id = seqText.getAttribute("id").substring(11, seqText.getAttribute("id").length()-7);
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-gridcolumn-" + id + " x-grid-cell-first']//div"));
                break;
            case "Product #":
                id = productNumberText.getAttribute("id").substring(11, productNumberText.getAttribute("id").length()-7);
                listItems = driver.findElement(By.xpath("(//table[contains (@class,'-table x-grid-table')])[1]")).findElements(By.xpath("//./td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-gridcolumn-" + id + " wrappedColumn wrappedColumn']//div"));
                break;
            case "Product Name":
                id = productNameText.getAttribute("id").substring(11, productNameText.getAttribute("id").length()-7);
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-gridcolumn-" + id + " wrappedColumn wrappedColumn']//div"));
                break;
            case "Lot":
                id = lotText.getAttribute("id").substring(11, lotText.getAttribute("id").length()-7);
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-gridcolumn-" + id + "']//div"));
                break;
            case "Inventory Unit":
                id = inventoryUnitText.getAttribute("id").substring(11, inventoryUnitText.getAttribute("id").length()-7);
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-gridcolumn-" + id + "']//div"));
                break;
            case "Unit Price":
                id = unitPriceText.getAttribute("id").substring(11, unitPriceText.getAttribute("id").length()-7);
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-gridcolumn-" + id + " wrappedColumn wrappedColumn']//div"));
                parseToDouble = true;
                break;
            case "Physical Value":
                id = physicalValueText.getAttribute("id").substring(11, physicalValueText.getAttribute("id").length()-7);
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-gridcolumn-" + id + "']//div[not (contains (@class, 'otalValue'))]"));
                parseToDouble = true;
                break;
            case "By Exception":
                id = byExceptionText.getAttribute("id").substring(11, byExceptionText.getAttribute("id").length()-7);
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-gridcolumn-" + id + " x-grid-cell-last']//div"));
                break;
            case "Inventory Unit Divided":
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-nc-physical-inventory-count-sheet-grid-column-inventoryUnit x-grid-cell-first']//div"));
                break;
            case "Alt Unit 1":
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-nc-physical-inventory-count-sheet-grid-column-altUnit1']//div"));
                break;
            case "Alt Unit 2":
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-nc-physical-inventory-count-sheet-grid-column-altUnit2']//div"));
                break;
            case "Alt Unit 3":
                listItems = driver.findElements(By.xpath("//td[@class = 'x-grid-cell x-grid-td x-grid-cell-headerId-nc-physical-inventory-count-sheet-grid-column-altUnit3']//div"));
                break;
            default:
//                listItems = new ArrayList<WebElement>();
                break;
        }
        for (WebElement elem : listItems) {
            if (elem.getText().length() > 1 || (elem.findElements(By.className("subTotalValue")).size() < 1)) {
                if (groupCount == 1 || (groupCount == 3 && parseToDouble && columnName.equals("Unit Price"))) {
                    if (parseToDouble) {
                        try {
                            listItemsDouble.add(Double.parseDouble(elem.getText().replace(",", "")));
                        }
                        catch (NumberFormatException e) {
                            groupCount++;
                        }
                    } else {
                        listItemsString.add(elem.getText());
                    }
                }
                if (groupCount == 2 || (groupCount == 5 && parseToDouble && columnName.equals("Unit Price"))) {
                    if (parseToDouble) {
                        try {
                            listItemsDouble2.add(Double.parseDouble(elem.getText().replace(",", "")));
                        }
                        catch (NumberFormatException e) {
                            groupCount++;
                        }
                    } else {
                        listItemsString2.add(elem.getText());
                    }
                }
            }
            else {
                groupCount++;
            }
        }
        if (direction.equals("Ascending")) {
            if (parseToDouble) {
                cloneListItemsDouble = new ArrayList<Double>(listItemsDouble);
                Collections.sort(cloneListItemsDouble);
                if (cloneListItemsDouble.equals(listItemsDouble)) {
                    System.out.println("Sorting for first group is ok.");
                } else {
                    System.out.println("Error! Sorting for first group is NOT ok.");
                }
                if (listItemsDouble2.size() > 0) {
                    cloneListItemsDouble2 = new ArrayList<Double>(listItemsDouble2);
                    Collections.sort(cloneListItemsDouble2);
                    if (cloneListItemsDouble2.equals(listItemsDouble2)) {
                        System.out.println("Sorting for second group is ok.");
                    } else {
                        System.out.println("Error! Sorting for second group is NOT ok.");
                    }
                }
            }
            else {
                cloneListItemsString = new ArrayList<String>(listItemsString);
                Collections.sort(cloneListItemsString);
                if (cloneListItemsString.equals(listItemsString)) {
                    System.out.println("Sorting for first group is ok.");
                } else {
                    System.out.println("Error! Sorting for first group is NOT ok.");
                }
                if (listItemsString2.size() > 0) {
                    cloneListItemsString2 = new ArrayList<String>(listItemsString2);
                    Collections.sort(cloneListItemsString2);
                    if (cloneListItemsString2.equals(listItemsString2)) {
                        System.out.println("Sorting for second group is ok.");
                    } else {
                        System.out.println("Error! Sorting for second group is NOT ok.");
                    }
                }
            }
        }
        if (direction.equals("Descending")) {
            if (parseToDouble) {
                cloneListItemsDouble = new ArrayList<Double>(listItemsDouble);
                Collections.sort(cloneListItemsDouble, Collections.reverseOrder());
                if (cloneListItemsDouble.equals(listItemsDouble)) {
                    System.out.println("Sorting for first group is ok.");
                } else {
                    System.out.println("Error! Sorting for first group is NOT ok.");
                }
                if (listItemsDouble2.size() > 0) {
                    cloneListItemsDouble2 = new ArrayList<Double>(listItemsDouble2);
                    Collections.sort(cloneListItemsDouble2, Collections.reverseOrder());
                    if (cloneListItemsDouble2.equals(listItemsDouble2)) {
                        System.out.println("Sorting for second group is ok.");
                    } else {
                        System.out.println("Error! Sorting for second group is NOT ok.");
                    }
                }
            }
            else {
                cloneListItemsString = new ArrayList<String>(listItemsString);
                Collections.sort(cloneListItemsString, Collections.reverseOrder());
                if (cloneListItemsString.equals(listItemsString)) {
                    System.out.println("Sorting for first group is ok.");
                } else {
                    System.out.println("Error! Sorting for first group is NOT ok.");
                }
                if (listItemsString2.size() > 0) {
                    cloneListItemsString2 = new ArrayList<String>(listItemsString2);
                    Collections.sort(cloneListItemsString, Collections.reverseOrder());
                    if (cloneListItemsString2.equals(listItemsString2)) {
                        System.out.println("Sorting for second group is ok.");
                    } else {
                        System.out.println("Error! Sorting for second group is NOT ok.");
                    }
                }
            }
        }
    }
}