package com.crunchtime.ces.pages.administration.administrationOverview;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageAdministrationOverview {
	private final WebDriver driver;

	public PageAdministrationOverview(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "div[ces-selenium-id='nc-administration-panel_tabContentPanel']")
	public WebElement administrationPanel;
}
