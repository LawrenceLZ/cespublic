package com.crunchtime.ces.pages.purchasing.purchaseByInvoice;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageCreatePurchasebyInvoice {
	private final WebDriver driver;

	public PageCreatePurchasebyInvoice(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(id = "supplyId")
	public WebElement vendorDropdown;

	@FindBy(id = "invoiceNumber")
	public WebElement invoiceNumberTextBox;

	@FindBy(id = "lookupVendor")
	public WebElement lookupVendorBtn;

	@FindBy(id = "submitButton")
	public WebElement createBtn;

	@FindBy(css = "input[value='Cancel']")
	public WebElement cancelBtn;

	@FindBy(id = "deliveryDate")
	public WebElement deliveryDateTextBox;

	@FindBy(id = "invoiceDate")
	public WebElement invoiceDateTextBox;

	@FindBy(id = "invoiceTotal")
	public WebElement invoiceTotalTextBox;

	@FindBy(css = "input[class = 'ctDropdown '][name = 'null']")
	public WebElement categoryDropdown;
}
