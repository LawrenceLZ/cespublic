package com.crunchtime.ces.pages.inventory.createReviewInventories;

import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class PageCreateReviewInventories {
	private final WebDriver driver;

	public PageCreateReviewInventories(WebDriver driver) {
		this.driver = driver;
	}

	/**
	 * This method is used for ajax call back, so can be used for all the wait in this module
	 * where it shows spinner.
	 */
	@FindBy(css = "div[id*='loadmask'][class='x-mask-msg-text']") private List<WebElement> loadSpinnerMasks;
	public void waitForSpinnerToDisappear() throws Exception{
		Waiter.waitForOneSecond();
		//wait to make sure summary screen is returned
		for (WebElement loadSpinnerMask : loadSpinnerMasks){
			try {
				Waiter.waitForElementToDisappear(driver, loadSpinnerMask);
			} catch (Exception e){
				break;
			}
		}
		Waiter.waitForOneSecond();
	}

	/**
	 * Physical Inventory Summary screen
	 */
	@FindBy(css = "[ces-selenium-id='nc-physical-inventory-summary-grid_physicalInventorySummaryGrid']") public WebElement phyInvSummScrnGrid;
	@FindBy(css = "[ces-selenium-id='nc-physical-inventory-summary-grid_physicalInventorySummaryGrid'] input[name='postPeriod']")
	public WebElement phyInvSummScrnPostPeriodComboBox;
	@FindBy(css = "[ces-selenium-id='button_copyScheduledInventoryButton']") public WebElement copyScheduledInventoryBtn;
	@FindBy(css = "[ces-selenium-id='boundlist']:not([style*='display: none'])") public WebElement phyInvSummScrnPostPeriodComboBoxList;
	@FindBy(css = "[ces-selenium-id='button_newScheduledInventoryButton']") public WebElement newScheduledInventoryBtn;
	@FindBy(css = "[id*='nc-physical-inventory-summary-grid'] img[class*='restore']") public WebElement phyInvSummScrnMinIcon;
	@FindBy(css = "[id*='nc-physical-inventory-summary-grid'] img[class*='maximize']") public WebElement phyInvSummScrnMaxIcon;
	@FindBy(css = "[id*='nc-physical-inventory-summary-grid'] img[class*='collapse-top']") public WebElement phyInvSummScrnCollapseIcon;
	@FindBy(css = "[id*='nc-physical-inventory-summary-grid'] img[class*='expand-bottom']") public WebElement phyInvSummScrnExpandIcon;
	@FindBy(css = "[ces-selenium-id='datecolumn']") public WebElement phyInvSummScrnDateColumnHdr;
	@FindBy(css = "div[id*='datecolumn'][class*='trigger']") public WebElement phyInvSummScrnDateColumnHdrTrigger;
	@FindBy(css = "[ces-selenium-id='templatecolumn']") public WebElement phyInvSummScrnProcessColumnHdr;
	@FindBy(css = "[ces-selenium-id='menu'] div a") public List<WebElement> phyInvSummScrnColHdrActiveTriggerCtrls;
	@FindBy(css = "div[id*='nc-physical-inventory-summary-grid'] [ces-selenium-id='gridview'] tr") private List<WebElement> phyInvSummScrnGridRows;

	public WebElement processLinkForFinalForPost(String processLinkName){
		WebElement processLink = null;
			for (WebElement phyInvSummScrnGridRow : phyInvSummScrnGridRows){
				String finalForPostRow = phyInvSummScrnGridRow.getText();
				if (finalForPostRow.contains("Final for Post")){
					String processLinkType = null;
					switch (processLinkName.toLowerCase()){
						case "review":
							processLinkType = "a[actiontype='review']";
							break;
						case "view":
							processLinkType = "a[actiontype='view']";
							break;
						case "audit":
							processLinkType = "a[actiontype='audit']";
							break;
						case "count sheet":
							processLinkType = "a[actiontype='count_sheet']";
							break;
						case "data entry":
							processLinkType = "a[actiontype='data_entry']";
					}
					processLink = phyInvSummScrnGridRow.findElement(By.cssSelector(processLinkType));
				}
			}
		return processLink;
	}

	/**
	 * Overdue Tasks popup window (when clicking "Review" link)
	 */
	@FindBy(css = "[ces-selenium-id='window'] [ces-selenium-id='overdue-inventory-transactions']") public WebElement overdueInvTransPopupGrid;
	@FindBy(css = "[ces-selenium-id='window'] [ces-selenium-id='button']") private List<WebElement> overdueInvTransPopupCmdBtnList;
	public WebElement overdueInvTransPopupCmdBtn (String buttonLabel){
		WebElement button = null;
		for (WebElement cmdBtn : overdueInvTransPopupCmdBtnList){
			String btnLabelText = cmdBtn.getText();
			if (btnLabelText.toLowerCase().trim().equals(buttonLabel.toLowerCase())){
				button = cmdBtn;
			}
		}
		return button;
	}

	/**
	 * Physical Inventory > Copy Inventory screen
	 */
	@FindBy(css = "[ces-selenium-id='nc-physical-inventory-copy-postPeriod_selectPostPeriodGrid'] tbody") public WebElement phyInvCopyToGrid;
	@FindBy(css = "[ces-selenium-id='nc-physical-inventory-copy-scheduled_selectScheduledGrid'] tbody") public WebElement phyInvCopyFromGrid;
	@FindBy(css = "[ces-selenium-id='nc-physical-inventory-copy-scheduled-panel'] [ces-selenium-id='button_saveAndClose']") public WebElement phyInvCopySaveAndCloseBtn;
	@FindBy(css = "[ces-selenium-id='nc-physical-inventory-copy-scheduled-panel'] [ces-selenium-id='button']") public WebElement phyInvCopyCloseBtn;

	/**
	 * Physical Inventory > New Scheduled Inventory screen
	 */
	@FindBy(css = "[ces-selenium-id='nc-create-scheduled-physical-inventory_createScheduledPhysicalInventory'] [ces-selenium-id='combobox_postPeriod'] input")
	public WebElement newScheduledInvPostPeriodComboBox;
	@FindBy(css = "[ces-selenium-id='boundlist']:not([style*='display: none'])") public WebElement newScheduledInvPostPeriodComboBoxList;
	@FindBy(css = "[ces-selenium-id='nc-create-scheduled-physical-inventory_createScheduledPhysicalInventory'] div[class*='target'] [ces-selenium-id='header']")
	public List<WebElement> newScheduledInvGridPanelHeaders;
	@FindBy(css = "[ces-selenium-id='nc-create-scheduled-inventory-template-grid'] tbody") public WebElement newScheduledInvTemplateGrid;
	@FindBy(css = "[ces-selenium-id='nc-create-scheduled-physical-inventory_createScheduledPhysicalInventory'] [ces-selenium-id='button_saveAndCloseButton']")
	public WebElement newInvScheduleSaveAndCloseBtn;

	/**
	 * Physical Inventory Review screen
	 */
	@FindBy(css = "[ces-selenium-id='nc-physical-inventory-review'] img[class*='filter']") public WebElement phyInvReviewScrnFilterIcon;
	@FindBy(css = "div[id*='quantity-grid'] [ces-selenium-id='gridview']") public WebElement phyInvReviewScrnQtyTabGrid;
	@FindBy(css = "div[id*='value-grid'] [ces-selenium-id='gridview']") public WebElement phyInvReviewScrnValueTabGrid;
	@FindBy(css = "div[id*='variance-report-grid'] [ces-selenium-id='gridview']") public WebElement phyInvReviewScrnAnalysisTabGrid;
	@FindBy(css = "[ces-selenium-id='nc-physical-inventory-review'] [ces-selenium-id='tabbar'] a[data-qtip='Quantity']") public WebElement phyInvReviewScrnQtyTabLink;
	@FindBy(css = "[ces-selenium-id='nc-physical-inventory-review'] [ces-selenium-id='tabbar'] a[data-qtip='Value']") public WebElement phyInvReviewScrnValueTabLink;
	@FindBy(css = "[ces-selenium-id='nc-physical-inventory-review'] [ces-selenium-id='tabbar'] a[data-qtip='Analysis']") public WebElement phyInvReviewScrnAnalysisTabLink;
	@FindBy(css = "[ces-selenium-id='nc-physical-inventory-main'] [ces-selenium-id='button']") private List<WebElement> phyInvDtlScrnHdrCmdBtnList;
	public WebElement phyInvDtlScrnHdrCmdBtn (String buttonLabel) {
		WebElement hdrCmdBtn = null;
		for (WebElement cmdBtn : phyInvDtlScrnHdrCmdBtnList){
			String btnLabelText = cmdBtn.getText();
			if (btnLabelText.toLowerCase().trim().equals(buttonLabel.toLowerCase())){
				hdrCmdBtn = cmdBtn;
			}
		}
		return hdrCmdBtn;
	}

	/**
	 * Physical Inventory > Data Entry detail screen
	 */
	@FindBy(css = "[ces-selenium-id='nc-physical-inventory-data-entry'] [ces-selenium-id='tool_expandCollapseTool'] img")
	public WebElement phyInvDtlScrnCollapseExpandIcon;
	@FindBy(css = "[class*='data-entry-grid-column-primaryCount'] div") public List<WebElement> phyInvDtlScrnPrimaryCountCells;


	/**
	 * Physical Inventory Audit screen
	 */
	@FindBy(css = "[ces-selenium-id='nc-physical-inventory-audit'] [ces-selenium-id='gridview'] tbody") public WebElement phyInvDtlAuditScrnGrid;
	@FindBy(css = "[ces-selenium-id='nc-physical-inventory-audit'] img[class*='filter']") public WebElement phyInvDtlAuditScrnFilterIcon;
	@FindBy(css = "[class$='x-window-default-resizable'][ces-selenium-id='window']") public WebElement phyInvDtlAuditScrnFilterPopup;
	@FindBy(css = "[class$='x-window-default-resizable'] [ces-selenium-id='datefield_range-gt'] table td:first-child input") public WebElement phyInvDtlAuditScrnFilterAuditDateGreaterInputCell;
	@FindBy(css = "input[name='auditDate-gt']") public WebElement phyInvDtlAuditScrnFilterAuditDateGreaterInput;


	/**
	 * Physical Inventory screen ******generic****** Filters popup window
	 */
	@FindBy(css = "[class$='x-window-default-resizable'][ces-selenium-id='window']") public WebElement phyInvFilterPopup;
	@FindBy(css = "[class$='x-window-default-resizable'][ces-selenium-id='window'] table[ces-selenium-id='checkbox']")
	public WebElement excludeProductsWithoutInvActivityCheckbox;
	@FindBy(css = "[class$='x-window-default-resizable'][ces-selenium-id='window'] [ces-selenium-id='button']") private List<WebElement> phyInvFilterPopupCmdButtons;
	public WebElement phyInvFilterPopupCmdBtn(String buttonLabel){
		WebElement cmdBtn = null;
		for (WebElement phyInvDtlScrnFilterPopupCmdButton : phyInvFilterPopupCmdButtons){
			String button = phyInvDtlScrnFilterPopupCmdButton.getText().toLowerCase();
			if (buttonLabel.toLowerCase().equals(button)) cmdBtn = phyInvDtlScrnFilterPopupCmdButton;
		}
		return cmdBtn;
	}




}
