package com.crunchtime.ces.pages.dashboard;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.dialogs.ConfirmDialog;
import com.crunchtime.ces.dialogs.SelectLayoutDialog;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageDashboard {
	private final WebDriver driver;
	protected SelectLayoutDialog selectLayoutDialog;
	protected ConfirmDialog confirmDialog;

	public PageDashboard(WebDriver driver) {
		this.driver = driver;
		this.selectLayoutDialog = PageFactory.initElements(driver, SelectLayoutDialog.class);
		this.confirmDialog = PageFactory.initElements(driver, ConfirmDialog.class);
	}

	@FindBy(css = "div[ces-selenium-id='nc-dashboard-panel_tabContentPanel']")
	public WebElement dashboardPanel;
	@FindBy(id = "contentPanel-body")
	private WebElement contentPanel;
	@FindBy(xpath = "(//a[.='My Layout'])[last()]")
	private WebElement myLayoutBtn;
	@FindBy(xpath = "//a[.='Corporate Layout']")
	private WebElement corporateLayoutBtn;
	@FindBy(xpath = "(//a[.='Add New Gadgets'])[last()]")
	private WebElement addNewGadgetsBtn;
	@FindBy(xpath = "(//a[.='Edit Layout'])[last()]")
	private WebElement editLayoutBtn;
	@FindBy(xpath = "//a[.='Edit Layout']")
	private WebElement editLayoutForMyLayoutBtn;
	@FindBy(xpath = "(//a[.='Save Corporate Layout'])[last()]")
	private WebElement saveCorporateLayoutBtn;
	public static By saveCorporateLayoutButtonByLocator = By.xpath("(//a[.='Save Corporate Layout'])[last()]");
	@FindBy(xpath = "(//a[.='Restore Corporate Layout'])[last()]")
	private WebElement restoreCorporateLayoutBtn;
	@FindBy(xpath = "//a[.='Finish Editing']")
	public WebElement finishEditingBtn;
	@FindBy(css = "div[class='x-box-target']>div>img[class='x-tool-img x-tool-gear']")
	public List<WebElement> configureWidgetGearIcons;
	@FindBy(css = "div[class='x-box-target']>div>img[class='x-tool-img x-tool-maximize']")
	public List<WebElement> widgetMaxViewIcons;
	@FindBy(css = "div[class='x-box-target']>div>img[class='x-tool-img x-tool-restore']")
	public List<WebElement> widgetMinViewIcons;
	@FindBy(css = "div[class='x-box-target']>div>img[class='x-tool-img x-tool-expand-bottom']")
	public List<WebElement> widgetExpandBottomViewIcons;
	@FindBy(css = "div[class='x-box-target']>div>img[class='x-tool-img x-tool-collapse-top']")
	public List<WebElement> widgetExpandTopViewIcons;

	@FindBy(css = "div[class='x-box-target']>div[data-qtip='Move Up']>img[class='x-tool-img x-tool-toggle']")
	public List<WebElement> widgetMoveUpIcons;
	@FindBy(css = "div[class='x-box-target']>div[data-qtip='Move Down']>img[class='x-tool-img x-tool-toggle']")
	public List<WebElement> widgetMoveDownIcons;
	@FindBy(css = "div[class='x-box-target']>div[data-qtip='Add/Remove Links']>img[class='x-tool-img x-tool-configuration']")
	public List<WebElement> widgetAddRemoveLinksIcons;
	@FindBy(css = "div[class='x-box-target']>div[data-qtip='Remove Row']>img[class='x-tool-img x-tool-minus']")
	public List<WebElement> widgetRemoveRowIcons;

	@FindBy(xpath = "//a[.='Show Legend']")
	public WebElement widgetShowLegendBtn;
	@FindBy(css = "div [id *= 'nc-waste-trend-panel-'][class='x-box-target']>div>img[class='x-tool-img x-tool-maximize']")
	public WebElement wtdWasteTrendMaxBtn;
	@FindBy(css = "input[name='widgetName']")
	public WebElement selectWidgetComboBox;
	@FindBy(xpath = "//a[.='Save']")
	public WebElement configureWidgetSaveBtn;

	public PageDashboard legendNameClick(String legendNameText) {
		driver.findElement(By.cssSelector("text[text='" + legendNameText + "']")).click();
		return this;
	}

	public void corporateLayoutButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(corporateLayoutBtn));
		corporateLayoutBtn.click();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
	}

	public void myLayoutButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(myLayoutBtn));
		myLayoutBtn.click();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
	}

	public void editLayoutButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(editLayoutBtn));
		editLayoutBtn.click();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
	}

	public void editLayoutForMyLayoutButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(editLayoutForMyLayoutBtn));
		editLayoutForMyLayoutBtn.click();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
	}

	public void saveCorporateLayoutButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(saveCorporateLayoutBtn));
		saveCorporateLayoutBtn.click();
	}

	public void saveCorporateLayoutButtonClickAndConfirm() throws Exception {
		Waiter.waitFor(visibilityOf(saveCorporateLayoutBtn));
		saveCorporateLayoutBtn.click();
		confirmDialog.yesButtonClick();
		confirmDialog.noButtonClick();
	}

	public void restoreCorporateLayoutButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(restoreCorporateLayoutBtn));
		restoreCorporateLayoutBtn.click();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
	}

	public void finishEditingButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(finishEditingBtn));
		finishEditingBtn.click();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		try {
			finishEditingBtn.click();
			Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
			System.out.println("Second attempt to click Finish Editing button");
		} catch (Exception e) {
			//
			System.out.println("debug info: Finish Editing button is already clicked");
		}
	}

	public void addSingleWidgetBox() {
		Waiter.waitFor(visibilityOf(addNewGadgetsBtn));
		addNewGadgetsBtn.click();
		selectLayoutDialog.selectSingleWidgetBox();
	}

	public void scrollToBottomWidget() {
		while (driver.manage().window().getSize().getHeight() < configureWidgetGearIcons.get(configureWidgetGearIcons.size() - 1).getLocation().getY()) {
			contentPanel.sendKeys(Keys.PAGE_DOWN);
		}
	}
}