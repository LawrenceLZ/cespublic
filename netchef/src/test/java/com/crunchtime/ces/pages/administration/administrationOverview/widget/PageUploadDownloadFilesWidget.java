package com.crunchtime.ces.pages.administration.administrationOverview.widget;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageUploadDownloadFilesWidget extends PageBase {

	private final WebDriver driver;

	public PageUploadDownloadFilesWidget(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(xpath = "(//span[@class='x-header-text x-window-header-text x-window-header-text-default'])[last()]")
	private WebElement filtersDialogHeader;

	@FindBy(xpath = "(//input[@name='name'])[last()]")
	public WebElement fileNameInput;

	@FindBy(xpath = "(//span[text()='Apply'])[last()]")
	private WebElement applyButton;
	@FindBy(xpath = "(//span[text()='Cancel'])[last()]")
	private WebElement cancelButton;
	@FindBy(xpath = "(//span[text()='Clear'])[last()]")
	private WebElement clearButton;
	@FindBy(xpath = "(//span[text()='Save'])[last()]")
	private WebElement saveButton;

	@FindBy(xpath = "//td[@data-qtip='Click to Edit']")
	private List<WebElement> fileDescriptionCells;

	@FindBy(xpath = "//textarea[@name='messageText']")
	public WebElement messageTextArea;

	public void typeFileName(String fileNameValue) throws Exception {
		Waiter.waitFor(visibilityOf(fileNameInput));
		fileNameInput.clear();
		fileNameInput.sendKeys(fileNameValue);
	}

	public void applyButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(applyButton));
		applyButton.click();
	}

	public void clearButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(clearButton));
		clearButton.click();
	}

	public void saveButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(saveButton));
		saveButton.click();
	}

	public void fileDecriptionClick(int fileDescriptionNumber) throws Exception {
		Waiter.waitFor(visibilityOf(fileDescriptionCells.get(fileDescriptionNumber)));
		fileDescriptionCells.get(fileDescriptionNumber).click();
	}

	public void typeMessageText(String messageTextValue) throws Exception {
		Waiter.waitFor(visibilityOf(messageTextArea));
		messageTextArea.clear();
		messageTextArea.sendKeys(messageTextValue);
	}
}