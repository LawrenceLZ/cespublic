package com.crunchtime.ces.pages.purchasing.purchasingOverview;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PagePurchasingOverview {
	private final WebDriver driver;

	public PagePurchasingOverview(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "div[ces-selenium-id='nc-purchasing-panel_tabContentPanel']")
	public WebElement purchasingPanel;
}
