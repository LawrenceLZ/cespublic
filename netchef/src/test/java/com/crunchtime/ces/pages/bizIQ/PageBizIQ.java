package com.crunchtime.ces.pages.bizIQ;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageBizIQ {
	private final WebDriver driver;

	public PageBizIQ(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "div[ces-selenium-id='nc-bizIq-panel_tabContentPanel']")
	public WebElement bizIqPanel;
}
