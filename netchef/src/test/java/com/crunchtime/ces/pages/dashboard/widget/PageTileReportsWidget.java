package com.crunchtime.ces.pages.dashboard.widget;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageTileReportsWidget extends PageBase {

	private final WebDriver driver;

	public PageTileReportsWidget(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(xpath = "(//span[@class='x-header-text x-window-header-text x-window-header-text-default'])[last()]")
	private WebElement filtersDialogHeader;

	@FindBy(css = "div[ces-selenium-id='nc-dashboard-snapshot-panel'] .x-tool-img.x-tool-configuration")
	private WebElement optionsLink;

	@FindBy(xpath = "(//input[@name='tileId'])[1]")
	private WebElement tileId1Combobox;
	@FindBy(xpath = "(//input[@name='tileId'])[2]")
	private WebElement tileId2Combobox;
	@FindBy(xpath = "(//input[@name='tileId'])[3]")
	private WebElement tileId3Combobox;
	@FindBy(xpath = "(//input[@name='tileId'])[4]")
	private WebElement tileId4Combobox;

	@FindBy(xpath = "(//span[text()='Save'])[last()]")
	private WebElement saveButton;
	@FindBy(xpath = "(//span[text()='Reset'])[last()]")
	private WebElement resetButton;
	@FindBy(xpath = "(//span[text()='Cancel'])[last()]")
	private WebElement cancelButton;

	public void optionsLinkClick() throws Exception {
		Waiter.waitFor(visibilityOf(optionsLink));
		optionsLink.click();
	}

	public void selectTileId1(String tileId1Value) throws Exception {
		Waiter.waitFor(visibilityOf(tileId1Combobox));
		tileId1Combobox.click();
		Waiter.waitFor(visibilityOf(driver.findElement(By.xpath("(//li[text()=\"" + tileId1Value + "\"])[1]"))));
		driver.findElement(By.xpath("(//li[text()=\"" + tileId1Value + "\"])[1]")).click();
	}

	public void selectTileId2(String tileId2Value) throws Exception {
		Waiter.waitFor(visibilityOf(tileId2Combobox));
		tileId2Combobox.click();
		Waiter.waitFor(visibilityOf(driver.findElement(By.xpath("(//li[text()=\"" + tileId2Value + "\"])[2]"))));
		driver.findElement(By.xpath("(//li[text()=\"" + tileId2Value + "\"])[2]")).click();
	}

	public void selectTileId3(String tileId3Value) throws Exception {
		Waiter.waitFor(visibilityOf(tileId3Combobox));
		tileId3Combobox.click();
		Waiter.waitFor(visibilityOf(driver.findElement(By.xpath("(//li[text()=\"" + tileId3Value + "\"])[3]"))));
		driver.findElement(By.xpath("(//li[text()=\"" + tileId3Value + "\"])[3]")).click();
	}

	public void selectTileId4(String tileId4Value) throws Exception {
		Waiter.waitFor(visibilityOf(tileId4Combobox));
		tileId4Combobox.click();
		Waiter.waitFor(visibilityOf(driver.findElement(By.xpath("(//li[text()=\"" + tileId4Value + "\"])[4]"))));
		driver.findElement(By.xpath("(//li[text()=\"" + tileId4Value + "\"])[4]")).click();
	}

	public void saveButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(saveButton));
		saveButton.click();
	}

	public void resetButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(resetButton));
		resetButton.click();
	}

	public void cancelButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(cancelButton));
		cancelButton.click();
	}
}