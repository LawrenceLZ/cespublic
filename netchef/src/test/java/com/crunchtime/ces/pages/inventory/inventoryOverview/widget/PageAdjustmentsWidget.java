package com.crunchtime.ces.pages.inventory.inventoryOverview.widget;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageAdjustmentsWidget extends PageBase {

	private final WebDriver driver;

	public PageAdjustmentsWidget(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(xpath = "(//span[@class='x-header-text x-window-header-text x-window-header-text-default'])[last()]")
	private WebElement filtersDialogHeader;

	@FindBy(xpath = "(//input[@name='transactionNumber'])[last()]")
	private WebElement transactionNumberInput;

	@FindBy(xpath = "(//input[@name='adjustmentType'])[last()]")
	private WebElement adjustmentTypeCombobox;

	@FindBy(xpath = "(//span[text()='Apply'])[last()]")
	private WebElement applyButton;
	@FindBy(xpath = "(//span[text()='Cancel'])[last()]")
	private WebElement cancelButton;
	@FindBy(xpath = "(//span[text()='Clear'])[last()]")
	private WebElement clearButton;

	public void selectAdjustmentType(String adjustmentTypeValue) throws Exception {
		Waiter.waitFor(visibilityOf(adjustmentTypeCombobox));
		adjustmentTypeCombobox.click();
		Waiter.waitFor(visibilityOf(driver.findElement(By.xpath("//li[text()='" + adjustmentTypeValue + "']"))));
		driver.findElement(By.xpath("//li[text()='" + adjustmentTypeValue + "']")).click();
	}

	public void applyButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(applyButton));
		applyButton.click();
	}

	public void clearButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(clearButton));
		clearButton.click();
	}
}