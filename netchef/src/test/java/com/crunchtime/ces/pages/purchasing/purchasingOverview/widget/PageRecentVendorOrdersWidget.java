package com.crunchtime.ces.pages.purchasing.purchasingOverview.widget;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageRecentVendorOrdersWidget extends PageBase {

	private final WebDriver driver;

	public PageRecentVendorOrdersWidget(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(xpath = "(//span[@class='x-header-text x-window-header-text x-window-header-text-default'])[1]")
	private WebElement filtersDialogHeader;

	@FindBy(xpath = "(//input[@name='vendor'])[last()]")
	private WebElement vendorInput;
	@FindBy(xpath = "(//input[@name='referenceNumber'])[last()]")
	private WebElement referenceNumberInput;
	@FindBy(css = "input[name='purchaseOrderNumber']")
	private WebElement purchaseOrderNumberInput;
	@FindBy(css = "input[name='vendorInvoice']")
	private WebElement vendorInvoiceNumberInput;

	@FindBy(css = "input[name='returnFlag']")
	private WebElement orderTypeCombobox;
	@FindBy(css = "input[name='inventoryStatusName']")
	private WebElement orderStatusInput;

	@FindBy(xpath = "(//span[text()='Apply'])[last()]")
	private WebElement applyButton;
	@FindBy(xpath = "(//span[text()='Cancel'])[last()]")
	private WebElement cancelButton;
	@FindBy(xpath = "(//span[text()='Clear'])[last()]")
	private WebElement clearButton;

	@FindBy(css = "[id^='nc-vendor-order-summary-grid'] .x-tool-img.x-tool-plus")
	private WebElement createVendorOrderLink;
	@FindBy(css = "input[value='Cancel']")
	private WebElement createVendorOrderCancelLink;

	public void typeVendor(String vendorValue) throws Exception {
		Waiter.waitFor(visibilityOf(vendorInput));
		vendorInput.clear();
		vendorInput.sendKeys(vendorValue);
	}

	public void typeReferenceNumber(String referenceNumberValue) throws Exception {
		Waiter.waitFor(visibilityOf(referenceNumberInput));
		referenceNumberInput.clear();
		referenceNumberInput.sendKeys(referenceNumberValue);
	}

	public void applyButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(applyButton));
		applyButton.click();
	}

	public void clearButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(clearButton));
		clearButton.click();
	}

	public void createVendorOrderClick() throws Exception {
		Waiter.waitFor(visibilityOf(createVendorOrderLink));
		createVendorOrderLink.click();
	}

	public void createVendorOrderCancelClick() throws Exception {
		Waiter.waitFor(visibilityOf(createVendorOrderCancelLink));
		createVendorOrderCancelLink.click();
	}
}