package com.crunchtime.ces.pages.reports.reportsOverview;

import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageReportsOverview {
	private final WebDriver driver;

	public PageReportsOverview(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "div[ces-selenium-id='nc-reports-panel_tabContentPanel']")
	public WebElement reportsPanel;
	@FindBy(css = "[ces-selenium-id='button']")
	private WebElement expandAllButton;
	@FindBy(css = "div[ces-selenium-id='component'] a[href='/ncext/index.ct#purchasingMenu~purchasesByGL?parentModule=purchasingMenu']")
	private WebElement purchasesByGlLink;
	@FindBy(css = "div[ces-selenium-id='component'] a[href='/ncext/index.ct#purchasingMenu~consolidatedPurchasesByGL?parentModule=purchasingMenu']")
	private WebElement consolidatedPurchasesByGlLink;

	public void expandAllButtonClick() {
		Waiter.waitFor(visibilityOf(expandAllButton));
		expandAllButton.click();
		Waiter.waitFor(visibilityOf(purchasesByGlLink));
	}

	public void purchasesByGlLinkClick() {
		purchasesByGlLink.click();
	}

	public void consolidatedPurchasesByGlLinkClick() {
		consolidatedPurchasesByGlLink.click();
	}
}
