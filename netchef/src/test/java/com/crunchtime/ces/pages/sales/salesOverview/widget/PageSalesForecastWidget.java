package com.crunchtime.ces.pages.sales.salesOverview.widget;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageSalesForecastWidget extends PageBase {

	private final WebDriver driver;

	public PageSalesForecastWidget(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(xpath = "(//span[@class='x-header-text x-window-header-text x-window-header-text-default'])[last()]")
	private WebElement filtersDialogHeader;

	@FindBy(xpath = "//input[@name='fiscalYear']//..//..//div[@class='x-trigger-index-0 x-form-trigger x-form-arrow-trigger x-form-trigger-first']")
	private WebElement fiscalYearCombobox;
	@FindBy(xpath = "//input[@name='status']//..//..//div[@class='x-trigger-index-0 x-form-trigger x-form-arrow-trigger x-form-trigger-first']")
	private WebElement statusCombobox;
	@FindBy(xpath = "(//input[contains (@class,'x-form-field x-form-checkbox x-form-cb')])[1]")
	private WebElement includeGuestCountsCheckbox;
	@FindBy(xpath = "(//input[contains (@class,'x-form-field x-form-checkbox x-form-cb')])[2]")
	private WebElement includeCheckCountsCheckbox;

	public void selectFiscalYear(String fiscalYearValue) throws Exception {
		Waiter.waitFor(visibilityOf(fiscalYearCombobox));
		fiscalYearCombobox.click();
		Waiter.waitFor(visibilityOf(driver.findElement(By.xpath("//li[text()='" + fiscalYearValue + "']"))));
		driver.findElement(By.xpath("//li[text()='" + fiscalYearValue + "']")).click();
	}

	public void selectStatus(String statusValue) throws Exception {
		Waiter.waitFor(visibilityOf(statusCombobox));
		statusCombobox.click();
		Waiter.waitFor(visibilityOf(driver.findElement(By.xpath("//li[text()='" + statusValue + "']"))));
		driver.findElement(By.xpath("//li[text()='" + statusValue + "']")).click();
	}

	public void checkIncludeGuestCountsCheckbox() throws Exception {
		Waiter.waitFor(visibilityOf(includeGuestCountsCheckbox));
		if (!includeCheckCountsCheckbox.isSelected()) {
			includeCheckCountsCheckbox.click();
		}
	}

	public void checkIncludeCheckCountsCheckbox() throws Exception {
		Waiter.waitFor(visibilityOf(includeCheckCountsCheckbox));
		if (!includeCheckCountsCheckbox.isSelected()) {
			includeCheckCountsCheckbox.click();
		}
	}

	public void uncheckIncludeGuestCountsCheckbox() throws Exception {
		Waiter.waitFor(visibilityOf(includeGuestCountsCheckbox));
		if (includeCheckCountsCheckbox.isSelected()) {
			includeCheckCountsCheckbox.click();
		}
	}

	public void uncheckIncludeCheckCountsCheckbox() throws Exception {
		Waiter.waitFor(visibilityOf(includeCheckCountsCheckbox));
		if (includeCheckCountsCheckbox.isSelected()) {
			includeCheckCountsCheckbox.click();
		}
	}
}