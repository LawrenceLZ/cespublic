package com.crunchtime.ces.pages.labor.laborOverview;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageLaborOverview {
	private final WebDriver driver;

	public PageLaborOverview(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "div[ces-selenium-id='nc-labor-panel_tabContentPanel']")
	public WebElement laborPanel;
}
