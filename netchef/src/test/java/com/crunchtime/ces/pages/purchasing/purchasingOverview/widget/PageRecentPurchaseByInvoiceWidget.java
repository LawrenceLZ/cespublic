package com.crunchtime.ces.pages.purchasing.purchasingOverview.widget;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageRecentPurchaseByInvoiceWidget extends PageBase {

	private final WebDriver driver;

	public PageRecentPurchaseByInvoiceWidget(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(xpath = "(//span[@class='x-header-text x-window-header-text x-window-header-text-default'])[last()]")
	private WebElement filtersDialogHeader;

	@FindBy(xpath = "(//input[@name='vendor'])[last()]")
	private WebElement vendorInput;
	@FindBy(css = "input[name='entryUser']")
	private WebElement entryUserInput;

	@FindBy(xpath = "input[name='status']")
	private WebElement statusCombobox;

	@FindBy(xpath = "(//span[text()='Apply'])[last()]")
	private WebElement applyButton;
	@FindBy(xpath = "(//span[text()='Cancel'])[last()]")
	private WebElement cancelButton;
	@FindBy(xpath = "(//span[text()='Clear'])[last()]")
	private WebElement clearButton;

	public void typeVendor(String vendorValue) throws Exception {
		Waiter.waitFor(visibilityOf(vendorInput));
		vendorInput.clear();
		vendorInput.sendKeys(vendorValue);
	}

	public void applyButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(applyButton));
		applyButton.click();
	}

	public void clearButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(clearButton));
		clearButton.click();
	}
}