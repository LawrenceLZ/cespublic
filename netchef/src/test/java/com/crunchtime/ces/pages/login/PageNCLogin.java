package com.crunchtime.ces.pages.login;

import com.crunchtime.ces.base.BasePageNC;
import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.ActionsNC;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class PageNCLogin extends PageBase {
	private final WebDriver driver;
    private static final String TITLE = "CrunchTime! Information Systems | Net-Chef";
	private static final String basePageUrl = "/ncext/index.ct";

	public PageNCLogin(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(id = "username") private WebElement userName;
	@FindBy(id = "password") private WebElement userPwd;
	@FindBy(id = "login") private WebElement firstSignInBtn;
	@FindBy(id = "locationId") private WebElement selectLocationDropdown;
	@FindBy(id = "loginButton") private WebElement secondSignInBtn;
	@FindBy(css = "#loginDiv") private WebElement firstLogInForm;
	@FindBy(css = "[ces-selenium-id='nc-dashboard-top-panel_topPanel'] a[ces-selenium-id='splitbutton']") private WebElement pageDashboardSnapshotWaiter;
	@FindBy(css = "div[class='x-window x-layer x-window-default x-closable x-window-closable x-window-default-closable nc-top-dock-exists x-border-box']") private WebElement pageDashboardCorpDailyNewsPopupForWaiter;

	public BasePageNC loginNC(String userId, String userPassword, String location) throws Exception {
		userName.clear();
		userName.sendKeys(userId);
		userPwd.clear();
		userPwd.sendKeys(userPassword);
		firstSignInBtn.submit();
		Waiter.waitForElementToVanish(firstLogInForm);
		Select locName = new Select(selectLocationDropdown);
		locName.selectByVisibleText(location);
		secondSignInBtn.submit();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		Waiter.waitForElementToBeVisible(driver, pageDashboardSnapshotWaiter);
		boolean corpNewsPopupExists = ActionsNC.isElementPresent(pageDashboardCorpDailyNewsPopupForWaiter);
		if (corpNewsPopupExists) {
			new WebDriverWait(driver, 60, 10000).until(
					ExpectedConditions.elementToBeClickable(By.cssSelector("[ces-selenium-id='window'] img[class='x-tool-img x-tool-close']"))
			);
			driver.findElement(By.cssSelector("[ces-selenium-id='window'] img[class='x-tool-img x-tool-close']")).click();
		}
		if (!driver.getCurrentUrl().endsWith(basePageUrl)) {
			throw new IllegalStateException("Unable to login to NC Home page.");
		}
		Waiter.waitForOneSecond();
		return new BasePageNC(driver);
	}

    public void shouldAppear () {
        shouldAppear(TITLE);
    }
}