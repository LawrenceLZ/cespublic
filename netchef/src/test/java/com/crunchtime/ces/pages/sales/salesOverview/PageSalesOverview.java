package com.crunchtime.ces.pages.sales.salesOverview;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageSalesOverview {
	private final WebDriver driver;

	public PageSalesOverview(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "div[ces-selenium-id='nc-sales-panel_tabContentPanel']")
	public WebElement salesPanel;
}
