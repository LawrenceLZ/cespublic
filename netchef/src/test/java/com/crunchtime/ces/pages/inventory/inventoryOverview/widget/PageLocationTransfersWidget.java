package com.crunchtime.ces.pages.inventory.inventoryOverview.widget;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageLocationTransfersWidget extends PageBase {

	private final WebDriver driver;

	public PageLocationTransfersWidget(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(xpath = "(//span[@class='x-header-text x-window-header-text x-window-header-text-default'])[last()]")
	private WebElement filtersDialogHeader;

	@FindBy(css = "input[name='transferDate-gt']")
	private WebElement transferDateGtInput;
	@FindBy(css = "input[name='transferDate-lt']")
	private WebElement transferDateLtInput;

	@FindBy(css = "input[name='type']")
	private WebElement typeCombobox;

	@FindBy(xpath = "(//span[text()='Apply'])[last()]")
	private WebElement applyButton;
	@FindBy(xpath = "(//span[text()='Cancel'])[last()]")
	private WebElement cancelButton;
	@FindBy(xpath = "(//span[text()='Clear'])[last()]")
	private WebElement clearButton;

	public void typeTransferDateGt(String transferDateGtValue) throws Exception {
		Waiter.waitFor(visibilityOf(transferDateGtInput));
		transferDateGtInput.clear();
		transferDateGtInput.sendKeys(transferDateGtValue);
	}

	public void typeTransferDateLt(String transferDateLtValue) throws Exception {
		Waiter.waitFor(visibilityOf(transferDateLtInput));
		transferDateLtInput.clear();
		transferDateLtInput.sendKeys(transferDateLtValue);
	}

	public void selectAdjustmentType(String typeValue) throws Exception {
		Waiter.waitFor(visibilityOf(typeCombobox));
		typeCombobox.click();
		Waiter.waitFor(visibilityOf(driver.findElement(By.xpath("//li[text()='" + typeValue + "']"))));
		driver.findElement(By.xpath("//li[text()='" + typeValue + "']")).click();
	}

	public void applyButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(applyButton));
		applyButton.click();
	}

	public void clearButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(clearButton));
		clearButton.click();
	}
}