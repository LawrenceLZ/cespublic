package com.crunchtime.ces.pages.labor.laborOverview.widget;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageLaborSummaryWidget extends PageBase {

	private final WebDriver driver;

	public PageLaborSummaryWidget(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(xpath = "(//span[@class='x-header-text x-window-header-text x-window-header-text-default'])[last()]")
	public WebElement filtersDialogHeader;

	@FindBy(css = "[ces-selenium-id='nc-labor-actuals-grid'] [role=combobox]")
	private WebElement fiscalYearCombobox;

	@FindBy(css = "input[name='displayBy']")
	private WebElement displayByCombobox;

	@FindBy(xpath = "(//span[text()='Apply'])[last()]")
	private WebElement applyButton;
	@FindBy(xpath = "(//span[text()='Cancel'])[last()]")
	private WebElement cancelButton;
	@FindBy(xpath = "(//span[text()='Clear'])[last()]")
	private WebElement clearButton;

	public void selectFiscalYear(String fiscalYearValue) throws Exception {
		Waiter.waitFor(visibilityOf(fiscalYearCombobox));
		fiscalYearCombobox.click();
		Waiter.waitFor(visibilityOf(driver.findElement(By.xpath("(//li[text()='" + fiscalYearValue + "'])[2]"))));
		driver.findElement(By.xpath("(//li[text()='" + fiscalYearValue + "'])[2]")).click();
	}

	public void selectDisplayBy(String displayByValue) throws Exception {
		Waiter.waitFor(visibilityOf(displayByCombobox));
		displayByCombobox.click();
		Waiter.waitFor(visibilityOf(driver.findElement(By.xpath("(//li[text()='" + displayByValue + "'])[2]"))));
		driver.findElement(By.xpath("(//li[text()='" + displayByValue + "'])[2]")).click();
	}

	public void applyButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(applyButton));
		applyButton.click();
	}

	public void clearButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(clearButton));
		clearButton.click();
	}
}