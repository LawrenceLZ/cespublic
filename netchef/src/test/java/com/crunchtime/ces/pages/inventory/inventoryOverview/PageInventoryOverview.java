package com.crunchtime.ces.pages.inventory.inventoryOverview;

import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageInventoryOverview {

	private final WebDriver driver;

	public PageInventoryOverview(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "div[ces-selenium-id='nc-inventory-panel_tabContentPanel']")
	public WebElement inventoryPanel;

	@FindBy(id = "nc-physical-inventory-summary-grid_header_hd")
	public WebElement headerPhysicalInventory;

	@FindBy(xpath = "//div[@class='x-window x-message-box nc-top-dock-exists x-layer x-window-default x-closable x-window-closable x-window-default-closable x-border-box physical-inventory-cancel-confirm']")
	public WebElement headerCancelProcessPage;

	@FindBy(css = "a[actiontype='review']")
	public WebElement reviewProcessLink;
	@FindBy(css = "a[actiontype='view']")
	public WebElement viewProcessLink;
	@FindBy(css = "a[actiontype='audit']")
	public WebElement auditProcessLink;

	@FindBy(xpath = "//input[@value='Close']")
	public WebElement closeButton;

	@FindBy(css = "a[actiontype='count_sheet']")
	public WebElement countSheetProcessLink;
	@FindBy(css = "a[linktype='cancelAction']")
	public WebElement cancelProcessLink;

	@FindBy(xpath = "(//td[contains (@class, 'x-grid-cell x-grid-td x-grid-cell-headerId-nc-physical-inventory-data-entry-grid-column-primaryCount')]/div)[1]")
	public WebElement editCell;
	@FindBy(xpath = "(//td[contains (@class, 'x-grid-cell x-grid-td x-grid-cell-headerId-nc-physical-inventory-data-entry-grid-column-primaryCount')]/div)[2]")
	public WebElement editCell2;
	@FindBy(xpath = "//input[contains (@class, 'x-form-field x-form-text x-form-focus x-field-form-focus x-field-default-form-focus')]")
	public WebElement inputCell;

	@FindBy(css = "input[name='postPeriod']")
	public WebElement postPeriodComboBox;

	@FindBy(css = "a[ces-selenium-id='button_copyScheduledInventoryButton']")
	public WebElement copyScheduledInventoryButton;

	@FindBy(xpath = "(//a[@class='x-btn x-btn-default-toolbar-small btnLegend pressed x-unselectable x-btn-default-small x-noicon x-btn-noicon x-btn-default-small-noicon'])[1]")
	public WebElement myLayoutButton;
	@FindBy(xpath = "(//a[@class='x-btn x-btn-default-toolbar-small btnLegend x-unselectable x-btn-default-small x-noicon x-btn-noicon x-btn-default-small-noicon'])[1]")
	public WebElement corporateLayoutButton;
	@FindBy(xpath = "(//a[@class='x-btn x-btn-default-toolbar-small btnLegend x-unselectable x-btn-default-small x-noicon x-btn-noicon x-btn-default-small-noicon'])[2]")
	public WebElement editLayoutButton;

	@FindBy(xpath = "//*[text()='Final for Post']")
	public WebElement FinalForPost;

	public void selectPostPeriod(String postPeriod) {
		postPeriodComboBox.click();
		Waiter.waitFor(visibilityOf(driver.findElement(By.xpath("//div[text()='" + postPeriod + "']"))));
		driver.findElement(By.xpath("//div[text()='" + postPeriod + "']")).click();
	}
}
