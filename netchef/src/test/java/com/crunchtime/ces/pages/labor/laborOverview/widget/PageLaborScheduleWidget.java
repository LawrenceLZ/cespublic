package com.crunchtime.ces.pages.labor.laborOverview.widget;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageLaborScheduleWidget extends PageBase {

	private final WebDriver driver;

	public PageLaborScheduleWidget(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(xpath = "(//span[@class='x-header-text x-window-header-text x-window-header-text-default'])[last()]")
	private WebElement filtersDialogHeader;

	@FindBy(css = "div[ces-selenium-id='nc-labor-schedule-grid'] input[role=combobox]")
	private WebElement fiscalYearCombobox;

	@FindBy(css = "input[name='displayByCombo']")
	private WebElement displayByCombobox;

	@FindBy(xpath = "(//span[text()='Apply'])[last()]")
	private WebElement applyButton;
	@FindBy(xpath = "(//span[text()='Cancel'])[last()]")
	private WebElement cancelButton;
	@FindBy(xpath = "(//span[text()='Clear'])[last()]")
	private WebElement clearButton;

	public void selectFiscalYear(String fiscalYearValue) throws Exception {
		Waiter.waitFor(visibilityOf(fiscalYearCombobox));
		fiscalYearCombobox.click();
		Waiter.waitFor(visibilityOf(driver.findElement(By.xpath("//li[text()='" + fiscalYearValue + "']"))));
		driver.findElement(By.xpath("//li[text()='" + fiscalYearValue + "']")).click();
	}

	public void selectDisplayBy(String displayByValue) throws Exception {
		Waiter.waitFor(visibilityOf(displayByCombobox));
		displayByCombobox.click();
		Waiter.waitFor(visibilityOf(driver.findElement(By.xpath("//li[text()='" + displayByValue + "']"))));
		driver.findElement(By.xpath("//li[text()='" + displayByValue + "']")).click();
	}

	public void applyButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(applyButton));
		applyButton.click();
	}

	public void clearButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(clearButton));
		clearButton.click();
	}
}