package com.crunchtime.ces.pages.inventory.inventoryOverview.physicalInventory;

import com.crunchtime.ces.base.PageBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AuditPage extends PageBase {

	private final WebDriver driver;

	public AuditPage(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(xpath = "//span[@class='x-header-text x-panel-header-text x-panel-header-text-default']")
	public WebElement headerAuditProcessPage;

	@FindBy(xpath = "(//a[@ces-selenium-id='button'])[1]")
	public WebElement closeButton;
    @FindBy(xpath = "//img[@class='x-tool-img x-tool-filter-applied-button']")
    public WebElement filterIcon;
    @FindBy(xpath = "(//img[@class='x-tool-img x-tool-export'])")
    public WebElement exportIcon;
}