package com.crunchtime.ces.pages.purchasing.vendorOrder;

import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class PageRecentVendorOrders {
	private final WebDriver driver;

	public PageRecentVendorOrders(WebDriver driver) {
		this.driver = driver;
	}


	/**
	 * This method is used for ajax call back, so can be used for all the wait in this module
	 * where it shows spinner.
	 */
	@FindBy(css = "div[id*='loadmask'][class='x-mask-msg-text']") private List<WebElement> loadSpinnerMasks;
	public void waitForSpinnerToDisappear() throws Exception{
		Waiter.waitForOneSecond();
		//wait to make sure summary screen is returned
		for (WebElement loadSpinnerMask : loadSpinnerMasks){
			try {
				Waiter.waitForElementToDisappear(driver, loadSpinnerMask);
			} catch (Exception e){
				break;
			}
		}
		Waiter.waitForOneSecond();
	}

	@FindBy(css = "img[class = 'x-tool-img x-tool-restore']")
	public List<WebElement> minimizeBtn;

	@FindBy(css = "img[class = 'x-tool-img x-tool-filter-button']")
	public List<WebElement> filterBtn;

	@FindBy(name = "purchaseOrderNumber")
	public WebElement purchaseOrderNumberTextbox;

	@FindBy(xpath = "//span[.='Apply']")
	public WebElement filterApplyBtn;

	@FindBy(css = "[ces-selenium-id='nc-vendor-order-summary-grid'] [ces-selenium-id='headercontainer'] [class*='x-column-header-inner'] span")
	public List<WebElement> headerColumns;

	@FindBy(css = "div>div[id*='headercontainer'][class='x-box-target']>div>div>div")
	public List<WebElement> headerColumnTriggers;

	public void clickHeaderColumnTrigger(int columnNumber) throws InterruptedException {
		Actions clickHeaderColumnTriggerAction = new Actions(driver);
		clickHeaderColumnTriggerAction.moveToElement(driver.findElements(By.cssSelector("div>div[id*='headercontainer'][class='x-box-target']>div")).get(columnNumber)).build().perform();
		driver.findElements(By.cssSelector("div>div[id*='headercontainer'][class='x-box-target']>div>div>div")).get(columnNumber).click();
	}

	@FindBy(xpath = "//span[.='Reset Columns']")
	public WebElement resetColumnsLink;

	@FindBy(css = "div[id*='nc-vendor-order-summary-grid']>div>table>tbody[id*='gridview-']")
	public WebElement recentVendorOrdersGrid;

	@FindBy(css = "a[href*='type=R&class=reconcile']")
	public WebElement reconcileLink;

	public WebElement recentVOgridCellItem(int rowNum, String columnHdrLabel) throws Exception {
		int columnNum = 0;
		String cssSelectorPath;
		switch (columnHdrLabel.toUpperCase()) {
			case "VENDOR":
				columnNum = 1;
				break;
			case "REFERENCE #":
				columnNum = 2;
				break;
			case "PO #":
				columnNum = 3;
				break;
			case "INVOICE #":
				columnNum = 4;
				break;
			case "ORDER DATE":
				columnNum = 5;
				break;
			case "EXP. DEL. DATE":
				columnNum = 6;
				break;
			case "ACT. DEL. DATE":
				columnNum = 7;
				break;
			case "TYPE":
				columnNum = 8;
				break;
			case "STATUS":
				columnNum = 9;
				break;
			case "ACTIONS":
				columnNum = 10;
				break;
		}
		cssSelectorPath = "div[id*='nc-vendor-order-summary-grid']>div>table>tbody[id*='gridview-']>tr:nth-child(" + rowNum + ")>td:nth-child(" + columnNum + ")";
		return driver.findElement(By.cssSelector(cssSelectorPath));
	}

	@FindBy(name = "vendor_invoice_number")
	public WebElement vendorInvoiceNumberTextbox;

	@FindBy(css = "input[name='invoice_total']")
	public WebElement totalInvoiceValueTextbox;

	@FindBy(name = "continue")
	public WebElement continueBtn;

	@FindBy(css = "frame:nth-of-type(2)")
	public WebElement reconcileDetailScrnBottomFrame;

	public WebElement reconcileVODtlgridCellItem(int rowNum, String columnHdrLabel) throws Exception {
		int columnNum = 0;
		String cssSelectorPath;
		if (!columnHdrLabel.isEmpty()) {
			switch (columnHdrLabel.toUpperCase()) {
				case "PRODUCT NUMBER":
					columnNum = 1;
					break;
				case "PRODUCT NAME":
					columnNum = 2;
					break;
				case "ORDER QUANTITY":
					columnNum = 3;
					break;
				case "UNIT PRICE":
					columnNum = 4;
					break;
				case "INVOICE QUANTITY":
					columnNum = 5;
					break;
				case "PHYSICAL QUANTITY":
					columnNum = 6;
					break;
				case "UNIT":
					columnNum = 7;
					break;
				case "CONVERSION":
					columnNum = 8;
					break;
				case "CONTRACT PRICE":
					columnNum = 9;
					break;
				case "INVOICE PRICE":
					columnNum = 10;
					break;
				case "INVOICE EXTENDED VALUE":
					columnNum = 11;
					break;
				case "TAX VALUE":
					columnNum = 12;
					break;
				case "TAX CODE":
					columnNum = 13;
					break;
				case "GROSS EXTENDED VALUE":
					columnNum = 14;
					break;
			}
			if (columnNum == 1 || columnNum == 2) {
				cssSelectorPath = "div[id='orderData']>form>table>tbody>tr:nth-child(" + rowNum + ")>td:nth-child(" + columnNum + ")>a";
				return driver.findElement(By.cssSelector(cssSelectorPath));
			} else {
				if (columnNum == 5 || columnNum == 6 || columnNum == 8 || columnNum == 9 || columnNum == 10) {
					cssSelectorPath = "div[id='orderData']>form>table>tbody>tr:nth-child(" + rowNum + ")>td:nth-child(" + columnNum + ")>input";
					return driver.findElement(By.cssSelector(cssSelectorPath));
				} else {
					cssSelectorPath = "div[id='orderData']>form>table>tbody>tr:nth-child(" + rowNum + ")>td:nth-child(" + columnNum + ")";
					return driver.findElement(By.cssSelector(cssSelectorPath));
				}
			}
		} else {
			cssSelectorPath = "div[id='orderData']>form>table>tbody>tr:nth-child(" + rowNum + ")>td";
			return driver.findElement(By.cssSelector(cssSelectorPath));
		}
	}

	@FindBy(xpath = "//input[@name='invoice_total']")
	public WebElement invoiceTotalTextbox;

	@FindBy(xpath = "//div[@id='summaryData']//tbody//th[.='Invoice Total:']/following-sibling::td[@class='but']")
	public WebElement defaultInvoiceTotal;

	@FindBy(css = "input[type='submit'][name='save']")
	public WebElement reconcileBtn;

	@FindBy(css = "input[type='submit'][name='ok']")
	public WebElement okBtn;
}