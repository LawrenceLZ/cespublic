package com.crunchtime.ces.pages.purchasing.commissaryOrder;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageCreateCommissaryOrder {
	private final WebDriver driver;

	public PageCreateCommissaryOrder(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(name = "supply_location_id")
	public WebElement supplyLocationDropdown;

	@FindBy(id = "delivery_date")
	public WebElement deliveryDateTextBox;

	@FindBy(name = "reference_number")
	public WebElement referenceNumberTextBox;

	@FindBy(name = "consumption_days")
	public WebElement consumptionDaysTextBox;

	@FindBy(name = "submit_button")
	public WebElement continueBtn;

	@FindBy(name = "close")
	public WebElement closeBtn;

	@FindBy(css = "input[value = ' Retrieve ']")
	public WebElement retrieveBtn;

	@FindBy(name = "use_last_order")
	public WebElement useLastOrderBtn;

	@FindBy(id = "category")
	public WebElement filterByCategoryDropdown;

	@FindBy(id = "div_category_level")
	public WebElement categoryRadioBtn;

	@FindBy(css = "span[id='div_subcategory_level']>input")
	public WebElement subcategoryRadioBtn;

	@FindBy(css = "span[id='div_microcategory_level']>input")
	public WebElement microcategoryRadioBtn;

	@FindBy(name = "template")
	public WebElement selectTemplateDropdown;

	@FindBy(name = "search_string")
	public WebElement prodSearchTextBox;

	@FindBy(css = "input[value='name'][name='search_by']")
	public WebElement searchByNameRadioBtn;

	@FindBy(css = "input[value='number'][name='search_by']")
	public WebElement searchByNumberRadioBtn;

	@FindBy(name = "four_wk_avg")
	public WebElement useFourWkAvgBtn;

	@FindBy(name = "zero_order_qty")
	public WebElement zeroOrderQtyBtn;

	@FindBy(name = "use_suggested_order")
	public WebElement useSuggestedOrderBtn;

	@FindBy(name = "continue")
	public WebElement addSelectedProdBtn;

	@FindBy(name = "update_quantity")
	public WebElement updateQtyBtn;

	@FindBy(name = "save")
	public WebElement saveBtn;

	@FindBy(name = "continue")
	public WebElement submitBtn;
}
