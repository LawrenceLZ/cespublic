package com.crunchtime.ces.pages.dashboard.widget;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageManagerLogWidget extends PageBase {

	private final WebDriver driver;

	public PageManagerLogWidget(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(xpath = "(//span[@class='x-header-text x-window-header-text x-window-header-text-default'])[last()]")
	private WebElement filtersDialogHeader;

	@FindBy(css = "[ces-selenium-id=nc-manager-logs-grid] .x-tool-img.x-tool-plus")
	private WebElement addEntryLink;

	@FindBy(css = "textarea[name='message']")
	private WebElement textareaMessage;

	@FindBy(xpath = "(//span[text()='Save'])[last()]")
	private WebElement saveButton;
	@FindBy(xpath = "(//span[text()='Close'])[last()]")
	private WebElement closeButton;

	public void addEntryLinkClick() throws Exception {
		Waiter.waitFor(visibilityOf(addEntryLink));
		addEntryLink.click();
	}

	public void typeMessageText(String messageTextValue) throws Exception {
		Waiter.waitFor(visibilityOf(textareaMessage));
		textareaMessage.clear();
		textareaMessage.sendKeys(messageTextValue);
		Thread.sleep(200);
	}

	public void saveButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(saveButton));
		saveButton.click();
	}

	public void closeButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(closeButton));
		closeButton.click();
	}
}