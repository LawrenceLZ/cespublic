package com.crunchtime.ces.pages.inventory.inventoryOverview.physicalInventory;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class ReviewPage extends PageBase {

	private final WebDriver driver;

	public ReviewPage(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(xpath = "//a[@ces-selenium-id='tab'][1]")
	public WebElement quantityTab;
	@FindBy(xpath = "//a[@ces-selenium-id='tab'][2]")
	public WebElement valueTab;
	@FindBy(xpath = "//a[@ces-selenium-id='tab'][3]")
	public WebElement analysisTab;
	@FindBy(xpath = "//span[text()='Edit']")
	public WebElement editLink;
	@FindBy(xpath = "//span[text()='Save and Review']")
	public WebElement saveAndReviewLink;
	@FindBy(xpath = "//span[text()='Save and Close']")
	public WebElement saveAndCloseLink;
	@FindBy(xpath = "//span[text()='Close']")
	public WebElement closeButton;
	@FindBy(xpath = "//input[@name='groupBy']")
	public WebElement groupByCombobox;
    @FindBy(xpath = "(//img[@class='x-tool-img x-tool-expand-button-collapsed'])[last()]")
    public WebElement collapsedIcon;
    @FindBy(xpath = "(//img[@class='x-tool-img x-tool-filter-button'])[last()]")
    public WebElement filterIcon;
    @FindBy(xpath = "(//img[@class='x-tool-img x-tool-export'])[last()]")
    public WebElement exportIcon;


    public void selectGroupBy(String groupByValue) {
        groupByCombobox.click();
        Waiter.waitFor(visibilityOf(driver.findElement(By.xpath("//li[text()='" + groupByValue + "']"))));
        driver.findElement(By.xpath("//li[text()='" + groupByValue + "']")).click();
    }
}