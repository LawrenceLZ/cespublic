package com.crunchtime.ces.pages.dashboard.widget;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PagePerformanceMetricsWidget extends PageBase {

	private final WebDriver driver;

	public PagePerformanceMetricsWidget(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(xpath = "(//span[@class='x-header-text x-window-header-text x-window-header-text-default'])[last()]")
	private WebElement filtersDialogHeader;

	@FindBy(css = "div[id^=nc-performance-metrics] a[ces-selenium-id=component]")
	private WebElement selectPerformanceMetricsLink;

	@FindBy(xpath = "//label[text()='Actual Net Sales']/..//input")
	private WebElement actualNetSalesCheckbox;
	@FindBy(xpath = "//label[text()='Last Year Same Day Sales']/..//input")
	private WebElement lastYearSameDaySalesCheckbox;
	@FindBy(xpath = "//label[text()='Forecasted Sales']/..//input")
	private WebElement forecastedSalesCheckbox;

	@FindBy(xpath = "(//span[text()='Save and Close'])[last()]")
	private WebElement saveAndCloseButton;
	@FindBy(xpath = "(//span[text()='Restore Corporate Defaults'])[last()]")
	private WebElement restoreCorporateDefaultsButton;
	@FindBy(xpath = "(//span[text()='Close'])[last()]")
	private WebElement closeButton;

	public void selectPerformanceMetricsLinkClick() throws Exception {
		Waiter.waitFor(visibilityOf(selectPerformanceMetricsLink));
		selectPerformanceMetricsLink.click();
//        Thread.sleep(1000);
	}

	public void checkActualNetSalesCheckbox() throws Exception {
		Waiter.waitFor(visibilityOf(actualNetSalesCheckbox));
		if (!actualNetSalesCheckbox.isSelected()) {
			actualNetSalesCheckbox.click();
		}
	}

	public void checkLastYearSameDaySalesCheckbox() throws Exception {
		Waiter.waitFor(visibilityOf(lastYearSameDaySalesCheckbox));
		if (!lastYearSameDaySalesCheckbox.isSelected()) {
			lastYearSameDaySalesCheckbox.click();
		}
	}

	public void checForecastedSalesCheckbox() throws Exception {
		Waiter.waitFor(visibilityOf(forecastedSalesCheckbox));
		if (!forecastedSalesCheckbox.isSelected()) {
			forecastedSalesCheckbox.click();
		}
	}

	public void uncheckActualNetSalesCheckbox() throws Exception {
		Waiter.waitFor(visibilityOf(actualNetSalesCheckbox));
		if (actualNetSalesCheckbox.isSelected()) {
			actualNetSalesCheckbox.click();
		}
	}

	public void uncheckLastYearSameDaySalesCheckbox() throws Exception {
		Waiter.waitFor(visibilityOf(lastYearSameDaySalesCheckbox));
		if (lastYearSameDaySalesCheckbox.isSelected()) {
			lastYearSameDaySalesCheckbox.click();
		}
	}

	public void unchecForecastedSalesCheckbox() throws Exception {
		Waiter.waitFor(visibilityOf(forecastedSalesCheckbox));
		if (forecastedSalesCheckbox.isSelected()) {
			forecastedSalesCheckbox.click();
		}
	}

	public void saveAndCloseButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(saveAndCloseButton));
		saveAndCloseButton.click();
//        Thread.sleep(100);
	}

	public void restoreCorporateDefaultsButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(restoreCorporateDefaultsButton));
		restoreCorporateDefaultsButton.click();
//        Thread.sleep(100);
	}

	public void closeButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(closeButton));
		closeButton.click();
//        Thread.sleep(100);
	}
}