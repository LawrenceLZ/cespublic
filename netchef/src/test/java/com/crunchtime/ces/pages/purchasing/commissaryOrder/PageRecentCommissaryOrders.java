package com.crunchtime.ces.pages.purchasing.commissaryOrder;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class PageRecentCommissaryOrders {
	private final WebDriver driver;

	public PageRecentCommissaryOrders(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "img[class = 'x-tool-img x-tool-restore']")
	public List<WebElement> minimizeBtn;
}
