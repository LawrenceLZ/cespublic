package com.crunchtime.ces.pages.production.productionOverview;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageProductionOverview {
	private final WebDriver driver;

	public PageProductionOverview(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "div[ces-selenium-id='nc-production-panel_tabContentPanel']")
	public WebElement productionPanel;
}
