package com.crunchtime.ces.pages.labor.laborOverview.widget;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageEmployeeInformationWidget extends PageBase {

	private final WebDriver driver;

	public PageEmployeeInformationWidget(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(xpath = "(//span[@class='x-header-text x-window-header-text x-window-header-text-default'])[last()]")
	public WebElement filtersDialogHeader;

	@FindBy(css = "input[name='filter_by_status']")
	public WebElement statusCombobox;

	@FindBy(xpath = "(//span[text()='Apply'])[last()]")
	public WebElement applyButton;
	@FindBy(xpath = "(//span[text()='Cancel'])[last()]")
	public WebElement cancelButton;
	@FindBy(xpath = "(//span[text()='Clear'])[last()]")
	public WebElement clearButton;

	public void selectStatus(String statusValue) throws Exception {
		Waiter.waitFor(visibilityOf(statusCombobox));
		statusCombobox.click();
		Thread.sleep(100); // it necessary for correct work of script
		Waiter.waitFor(visibilityOf(driver.findElement(By.xpath("(//li[text()='" + statusValue + "'])[3]"))));
		driver.findElement(By.xpath("(//li[text()='" + statusValue + "'])[3]")).click();
	}

	public void applyButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(applyButton));
		applyButton.click();
	}

	public void clearButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(clearButton));
		clearButton.click();
	}
}