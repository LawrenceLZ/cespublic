package com.crunchtime.ces.pages.purchasing.vendorOrder;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageCreateVendorOrder {
	private final WebDriver driver;

	public PageCreateVendorOrder(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(id = "supplyId")
	public WebElement vendorDropdown;

	@FindBy(id = "delivery_date")
	public WebElement expectedDeliveryDateTextbox;

	@FindBy(name = "consumption_days")
	public WebElement consumptionDaysTextbox;

	@FindBy(name = "continue")
	public WebElement continueBtn;

	@FindBy(name = "cancel")
	public WebElement cancelBtn;

	@FindBy(name = "retrieve")
	public WebElement retrieveBtn;

	@FindBy(css = "table[cellpadding='1']")
	public WebElement createVOgrid;

	@FindBy(name = "add_selected")
	public WebElement addSelectedProductsBtn;

	@FindBy(name = "prepare_order")
	public WebElement prepareOrderBtn;

	@FindBy(name = "save_submit")
	public WebElement submitBtn;

	@FindBy(css = "font>nobr")
	public WebElement submittedTransNumber;

	@FindBy(name = "exit")
	public WebElement exitBtn;

	public WebElement createVOgridCellItem(int rowNum, String columnHdrLabel) throws Exception {
		int columnNum = 0;
		String cssSelectorPath;
		if (!columnHdrLabel.isEmpty()) {
			switch (columnHdrLabel.toUpperCase()) {
				case "FLAG":
					columnNum = 1;
					break;
				case "PRODUCT NUMBER":
					columnNum = 2;
					break;
				case "PRODUCT NAME":
					columnNum = 3;
					break;
				case "LAST ORDER":
					columnNum = 4;
					break;
				case "4-WEEK AVERAGE":
					columnNum = 5;
					break;
				case "UNIT PRICE":
					columnNum = 6;
					break;
				case "ORDER QUANTITY":
					columnNum = 7;
					break;
				case "SUGGESTED ORDER":
					columnNum = 8;
					break;
				case "MIN. PAR":
					columnNum = 9;
					break;
				case "MAX. PAR":
					columnNum = 10;
					break;
				case "AVAILABLE":
					columnNum = 11;
					break;
				case "ON VENDOR ORDER":
					columnNum = 12;
					break;
				case "VENDOR CONV.":
					columnNum = 13;
					break;
				case "VENDOR UNIT":
					columnNum = 14;
					break;
			}
			if (columnNum == 2) {
				cssSelectorPath = "table[cellpadding='1']>tbody>tr:nth-child(" + rowNum + ")>td:nth-child(" + columnNum + ")>font>a";
				return driver.findElement(By.cssSelector(cssSelectorPath));
			} else {
				if (columnNum == 7) {
					cssSelectorPath = "table[cellpadding='1']>tbody>tr:nth-child(" + rowNum + ")>td:nth-child(" + columnNum + ")>input";
					return driver.findElement(By.cssSelector(cssSelectorPath));
				} else {
					cssSelectorPath = "table[cellpadding='1']>tbody>tr:nth-child(" + rowNum + ")>td:nth-child(" + columnNum + ")";
					return driver.findElement(By.cssSelector(cssSelectorPath));
				}
			}
		} else {
			cssSelectorPath = "table[cellpadding='1']>tbody>tr:nth-child(" + rowNum + ")>td";
			return driver.findElement(By.cssSelector(cssSelectorPath));
		}
	}
}