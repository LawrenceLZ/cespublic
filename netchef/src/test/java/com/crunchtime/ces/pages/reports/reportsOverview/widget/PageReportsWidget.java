package com.crunchtime.ces.pages.reports.reportsOverview.widget;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageReportsWidget extends PageBase {

	private final WebDriver driver;

	public PageReportsWidget(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "[ces-selenium-id='button']")
	private WebElement expandCollapseButton;

	public void expandAllClick() throws Exception {
		Waiter.waitFor(visibilityOf(expandCollapseButton));
		expandCollapseButton.click();
		Thread.sleep(200);
	}

	public void collapseAllClick() throws Exception {
		Waiter.waitFor(visibilityOf(expandCollapseButton));
		expandCollapseButton.click();
		Thread.sleep(200);
	}
}