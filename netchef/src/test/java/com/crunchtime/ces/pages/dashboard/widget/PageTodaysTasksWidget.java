package com.crunchtime.ces.pages.dashboard.widget;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageTodaysTasksWidget extends PageBase {

	private final WebDriver driver;

	public PageTodaysTasksWidget(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(xpath = "(//span[@class='x-header-text x-window-header-text x-window-header-text-default'])[last()]")
	private WebElement filtersDialogHeader;

	@FindBy(css = "#TodayTasksGrid_header-body a[ces-selenium-id='component']")
	private WebElement taskAuditReportLink;

	@FindBy(xpath = "//input[@name='taskTypeFilter']/../..//div[contains (@class, 'x-trigger-index-0 x-form-trigger x-form-arrow-trigger x-form-trigger-first')]")
	private WebElement taskTypeFilterCombobox;

	@FindBy(css = ".x-tool-img.x-tool-close")
	private WebElement closeButton;

	public void taskAuditReportLinkClick() throws Exception {
		Waiter.waitFor(visibilityOf(taskAuditReportLink));
		taskAuditReportLink.click();
		Thread.sleep(25000);
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
	}

	public void selectTaskTypeFilter(String taskTypeFilterValue) throws Exception {
		Waiter.waitFor(visibilityOf(taskTypeFilterCombobox));
		taskTypeFilterCombobox.click();
		Waiter.waitFor(visibilityOf(driver.findElement(By.xpath("(//li[text()=\"" + taskTypeFilterValue + "\"])[1]"))));
		driver.findElement(By.xpath("(//li[text()=\"" + taskTypeFilterValue + "\"])[1]")).click();
		Thread.sleep(5000);
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
	}

	public void closeButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(closeButton));
		closeButton.click();
//        Thread.sleep(100);
	}
}