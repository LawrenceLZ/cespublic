package com.crunchtime.ces.pages.reports.reportsPurchasing;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageReportsPurchasingConsolidatedPurchasesByGL {
	private final WebDriver driver;

	@FindBy(css = "div[ces-selenium-id='toolbar_filtersBar'] [name='from_date']")
	private WebElement startDateInputField;
	@FindBy(css = "div[ces-selenium-id='toolbar_filtersBar'] [name='to_date']")
	private WebElement endDateInputField;
	@FindBy(css = "div[ces-selenium-id='toolbar_filtersBar'] input[name='hierarchycombo']")
	private WebElement hierarchyCombobox;
	@FindBy(css = "div[ces-selenium-id='toolbar_filtersBar'] a[ces-selenium-id='button']")
	private WebElement retrieveButton;
	@FindBy(css = "div[ces-selenium-id='header'] img[class^='x-tool-img x-tool-filter']")
	private WebElement filterButton;
	@FindBy(css = "div[ces-selenium-id='header'] a[ces-selenium-id='button']")
	private WebElement closeButton;
	@FindBy(css = ".x-tool-img.x-tool-export")
	private WebElement exportButton;
	@FindBy(css = "#gtGrid_header img[id^='tool']")
	private WebElement grandTotalExpandCollapseTool;
	@FindBy(css = "#purchasesbyglsummarygrid_header img[id^='tool']")
	private WebElement locationsDetailsExpandCollapseTool;
	//Filter elements
	@FindBy(css = "input[name='locationCode']")
	private WebElement locationCodeInputField;
	@FindBy(css = "input[name='locationName']")
	private WebElement locationNameInputField;
	@FindBy(css = "input[name='glCode']")
	private WebElement glCodeInputField;
	@FindBy(css = "input[name='glDescription']")
	private WebElement glDescriptionInputField;
	@FindBy(css = "input[name='amount-gt']")
	private WebElement startPurchaseAmount;
	@FindBy(css = "input[name='amount-lt']")
	private WebElement endPurchaseAmount;
	@FindBy(xpath = "(//span[text()='Apply'])")
	private WebElement applyFilterButton;
	@FindBy(xpath = "(//span[text()='Cancel'])")
	private WebElement cancelFilterButton;
	@FindBy(xpath = "(//span[text()='Clear'])")
	private WebElement clearFilterButton;
	private By grandTotalGridColumn = By.cssSelector("#gtGrid [ces-selenium-id='gridcolumn']");
	private By locationDetailsGridColumn = By.cssSelector("#purchasesbyglsummarygrid [ces-selenium-id='gridcolumn']");

	public PageReportsPurchasingConsolidatedPurchasesByGL(WebDriver driver) {
		this.driver = driver;
	}

	public void enterStartDate(String startDate) {
		Waiter.waitFor(visibilityOf(startDateInputField));
		startDateInputField.clear();
		startDateInputField.sendKeys(startDate);
	}

	public void enterEndDate(String endDate) {
		Waiter.waitFor(visibilityOf(endDateInputField));
		endDateInputField.clear();
		endDateInputField.sendKeys(endDate);
	}

	public void retrieveData() {
		retrieveButton.click();
	}

	public void openFilter() {
		Waiter.waitFor(visibilityOf(filterButton));
		filterButton.click();
	}

	public void filterByLocationCode(String locationCode) {
		filterButton.click();
		Waiter.waitFor(visibilityOf(locationCodeInputField));
		locationCodeInputField.sendKeys(locationCode);
		applyFilterButton.click();
	}

	public void clearConsolidatedPurchasesByGlFilter() {
		filterButton.click();
		clearFilterButton.click();
		applyFilterButton.click();
	}

	public void collapseGrandTotal() {
		grandTotalExpandCollapseTool.click();
		Waiter.waitFor(invisibilityOfElementLocated(grandTotalGridColumn));
	}

	public void expandGrandTotal() {
		grandTotalExpandCollapseTool.click();
		Waiter.waitFor(visibilityOf(driver.findElement(grandTotalGridColumn)));
	}

	public void collapseLocationDetails() {
		locationsDetailsExpandCollapseTool.click();
		Waiter.waitFor(invisibilityOfElementLocated(locationDetailsGridColumn));
	}

	public void expandLocationDetails() {
		locationsDetailsExpandCollapseTool.click();
		Waiter.waitFor(visibilityOf(driver.findElement(locationDetailsGridColumn)));
	}

	public void selectHierarchy(String hierarchy) {
		hierarchyCombobox.click();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.loadMask));
		Waiter.waitFor(visibilityOf(driver.findElement(By.xpath("//*[contains(text(),'" + hierarchy + "')]"))));
		driver.findElement(By.xpath("//*[text()='" + hierarchy + "']")).click();
	}

	public void closeConsolidatedPurchasesByGl() {
		closeButton.click();
	}
}