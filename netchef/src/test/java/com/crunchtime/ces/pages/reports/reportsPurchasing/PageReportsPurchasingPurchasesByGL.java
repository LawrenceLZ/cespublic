package com.crunchtime.ces.pages.reports.reportsPurchasing;

import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageReportsPurchasingPurchasesByGL {
	private final WebDriver driver;

	@FindBy(css = "#startDate-inputEl")
	private WebElement startDateInputField;
	@FindBy(css = "#endDate-inputEl")
	private WebElement endDateInputField;
	@FindBy(css = "div[ces-selenium-id='toolbar_filtersBar'] a[ces-selenium-id='button']")
	private WebElement retrieveButton;
	@FindBy(css = "div[ces-selenium-id='header'] img[class^='x-tool-img x-tool-filter']")
	private WebElement filterButton;
	@FindBy(css = "div[ces-selenium-id='header'] a[ces-selenium-id='button']")
	private WebElement closeButton;
	@FindBy(css = ".x-tool-img.x-tool-export")
	private WebElement exportButton;
	@FindBy(css = "#gtGrid_header img[id^='tool']")
	private WebElement locationSummaryExpandCollapseTool;
	@FindBy(css = "#purchasesbygllocationview_header img[id^='tool']")
	private WebElement locationsDetailsExpandCollapseTool;

	//Filter Elements
	@FindBy(css = "input[name='supplier']")
	private WebElement supplierInputField;
	@FindBy(css = "input[name='supplierCode']")
	private WebElement supplierCodeInputField;
	@FindBy(css = "input[name='glDescription']")
	private WebElement glDescriptionInputField;
	@FindBy(css = "input[name='invoiceNumber']")
	private WebElement invoiceNumberInputField;
	@FindBy(css = "input[name='glCode']")
	private WebElement glCodeInputField;
	@FindBy(css = "input[name='reconcileDate-gt']")
	private WebElement startReconcileDate;
	@FindBy(css = "input[name='reconcileDate-lt']")
	private WebElement endReconcileDate;
	@FindBy(css = "deliveryDate-gt")
	private WebElement startDeliveryDate;
	@FindBy(css = "deliveryDate-lt")
	private WebElement endDeliveryDate;
	@FindBy(xpath = "(//span[text()='Apply'])")
	private WebElement applyFilterButton;
	@FindBy(xpath = "(//span[text()='Cancel'])")
	private WebElement cancelFilterButton;
	@FindBy(xpath = "(//span[text()='Clear'])")
	private WebElement clearFilterButton;
	private By locationSummaryGridColumn = By.cssSelector("#gtGrid [ces-selenium-id='gridcolumn']");
	private By locationDetailsGridColumn = By.cssSelector("#purchasesbygllocationview [ces-selenium-id='gridcolumn']");

	//ExportElements
	@FindBy()
	private WebElement cancelExportButton;

	public PageReportsPurchasingPurchasesByGL(WebDriver driver) {
		this.driver = driver;
	}

	public void enterStartDate(String startDate) {
		Waiter.waitFor(visibilityOf(startDateInputField));
		startDateInputField.clear();
		startDateInputField.sendKeys(startDate);
	}

	public void enterEndDate(String endDate) {
		Waiter.waitFor(visibilityOf(endDateInputField));
		endDateInputField.clear();
		endDateInputField.sendKeys(endDate);
	}

	public void retrieveData() {
		retrieveButton.click();
	}

	public void openFilter() {
		Waiter.waitFor(visibilityOf(filterButton));
		filterButton.click();
	}

	public void filterBySupplier(String supplier) {
		filterButton.click();
		Waiter.waitFor(visibilityOf(supplierInputField));
		supplierInputField.sendKeys(supplier);
		applyFilterButton.click();
	}

	public void clearPurchasesByGlFilter() {
		filterButton.click();
		clearFilterButton.click();
		applyFilterButton.click();
	}

	public void collapseLocationSummary() {
		locationSummaryExpandCollapseTool.click();
		Waiter.waitFor(invisibilityOfElementLocated(locationSummaryGridColumn));
	}

	public void expandLocationSummary() {
		locationSummaryExpandCollapseTool.click();
		Waiter.waitFor(visibilityOf(driver.findElement(locationSummaryGridColumn)));
	}

	public void collapseLocationDetails() {
		locationsDetailsExpandCollapseTool.click();
		Waiter.waitFor(invisibilityOfElementLocated(locationDetailsGridColumn));
	}

	public void expandLocationDetails() {
		locationsDetailsExpandCollapseTool.click();
		Waiter.waitFor(visibilityOf(driver.findElement(locationDetailsGridColumn)));
	}

	public void closePurchasesByGl() {
		closeButton.click();
	}
}
