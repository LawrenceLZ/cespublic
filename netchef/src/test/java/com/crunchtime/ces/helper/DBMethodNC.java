package com.crunchtime.ces.helper;

import com.crunchtime.ces.base.BaseDBConnectionNC;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBMethodNC extends BaseDBConnectionNC {

	public static Map<String, List<Object>> getSelectSqlMap(String dbConnectionString, String selectSQL) throws SQLException {
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		Map<String, List<Object>> map = null;
		try {
			dbConnection = getConnection(dbConnectionString);
			preparedStatement = dbConnection.prepareStatement(selectSQL);
			ResultSet rs = preparedStatement.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			int columns = md.getColumnCount();
			map = new HashMap<>(columns);
			for (int i = 1; i <= columns; ++i) {
				map.put(md.getColumnName(i), new ArrayList<>());
			}
			while (rs.next()) {
				for (int i = 1; i <= columns; ++i) {
					map.get(md.getColumnName(i)).add(rs.getObject(i));
				}
			}
			return map;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
				System.out.println("******SQL Select Statement is executed.******");
			}
			if (dbConnection != null) {
				dbConnection.close();
				System.out.println("******DB Connection is closed.******");
			}
		}
		return map;
	}

	public static void executeSqlStmt(String dbConnectionString, String sqlStmt) throws SQLException {
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		try {
			dbConnection = getConnection(dbConnectionString);
			preparedStatement = dbConnection.prepareStatement(sqlStmt);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
				System.out.println("******SQL Statement is executed.******");
			}
			if (dbConnection != null) {
				dbConnection.close();
				System.out.println("******DB Connection is closed.******");
			}
		}
	}
}