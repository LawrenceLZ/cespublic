package com.crunchtime.ces.helper;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;
import ru.yandex.qatools.allure.annotations.Attach;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.model.AttachmentType;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ActionsNC {
	private static final String root = System.getProperty("user.dir");


	public static String getBrowserName(WebDriver driver) {
		Capabilities capabilities = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = capabilities.getBrowserName();
		if (browserName.toUpperCase().contains("INTERNET")) return "InternetExplorer";
		if (browserName.toUpperCase().contains("FIREFOX")) return "Firefox";
		if (browserName.toUpperCase().contains("CHROME")) return "Chrome";
		else return browserName;
	}

	//check if Alert appears on screen
	public static boolean isAlertPresent(WebDriver driver) {
		boolean alertFlag = false;
		try {
			String alertMessage = driver.switchTo().alert().getText().trim();
			System.out.println(alertMessage);
			return true;
		} catch (Exception e) {
			//e.printStackTrace();
			e.getCause().getMessage();
		}
		return alertFlag;
	}

	//get List of WebElements count
	public static int getElementsListCount(List<WebElement> elements) {
		return elements.size();
	}

	//set select tag dropdown by number
	public static void setSelectDropdownByNum(WebElement element, int number) {
		Select dropdown = new Select(element);
		dropdown.selectByIndex(number);
	}

	//set select tag dropdown by value
	public static void setSelectDropdownByValue(WebElement element, String value) {
		Select dropdown = new Select(element);
		dropdown.selectByVisibleText(value);
	}

	//get select tag dropdown value
	public static String getSelectDropdownValue(WebElement element) {
		Select dropdownList = new Select(element);
		WebElement selectedElement = dropdownList.getFirstSelectedOption();
		return selectedElement.getText();
	}

	//get select tag dropdown list values count
	public static int getSelectDropdownCount(WebElement element) {
		Select dropdownList = new Select(element);
		List<WebElement> values = dropdownList.getOptions();
		return values.size();
	}

	//set the extjs implemented dropdown that with ul/li combo box
	public static void setComboBoxDropdownByValue(WebDriver driver, WebElement element, String value) throws Exception {
		Actions comboBoxInputAction = new Actions(driver);
		Waiter.waitForOneSecond();
		comboBoxInputAction.click(element).build().perform();
		String liXpathValue = "//li[.='" + value + "']";
		WebElement liElement = driver.findElement(By.xpath(liXpathValue));
		comboBoxInputAction.click(liElement).build().perform();
		Waiter.waitForOneSecond();
	}

	//NOTE that the grid has to be a tag of tbody
	public static int getGridRowCnt(WebElement grid) {
		return grid.findElements(By.cssSelector("tr")).size();
	}

	//NOTE that the grid has to be a tag of tbody
	public static int getGridRowNumByCellValue(WebElement grid, String cellValue) {
		int rowCnt = grid.findElements(By.cssSelector("tr")).size();
		int rowNum = 0;
		for (int i = 1; i <= rowCnt; i++) {
			String evlString = grid.findElement(By.cssSelector("tr:nth-child(" + i + ")")).getText();
			if (evlString.contains(cellValue)) {
				rowNum = i;
				break;
			}
		}
		return rowNum;
	}

	//NOTE that the grid has to be a tag of tbody
	public static int getGridColumnCntPerRowNum(WebElement grid, int rowNum) {
		return grid.findElements(By.cssSelector("tr:nth-child(" + rowNum + ")>td")).size();
	}

	public static void takeScreenshot(WebDriver driver, String nameOfOutputFile, WebElement elementCanBeNullForThisMethod) throws IOException, InterruptedException {
		Thread.sleep(5000);
		File scrFile;
		scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		if (elementCanBeNullForThisMethod != null) {
			//get the WebElement location
			Point p = elementCanBeNullForThisMethod.getLocation();
			//then get the WebElement's coordinates
			int width = elementCanBeNullForThisMethod.getSize().getWidth();
			int height = elementCanBeNullForThisMethod.getSize().getHeight();
			//resize the snapshot based on the WebElement
			BufferedImage img;
			img = ImageIO.read(scrFile);
			BufferedImage dest = img.getSubimage(p.getX(), p.getY(), width, height);
			ImageIO.write(dest, "png", scrFile);
		}
		String filePath = root + "\\src\\test\\resources\\ScreenShots\\Captured\\" + nameOfOutputFile + ".png";
		File destination = new File(filePath);
		System.out.println("Captured Screenshot is at " + destination.getAbsolutePath());
		FileUtils.copyFile(scrFile, destination);
		Thread.sleep(2000);
	}

	public static String setDateBasedOnToday(String dateFormat, Integer incrementTodayDateBy) throws ParseException {
		//set current date format
		SimpleDateFormat dateFormatDefined = new SimpleDateFormat(dateFormat);
		String date = dateFormatDefined.format(new Date());
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateFormatDefined.parse(date));
		cal.add(Calendar.DATE, incrementTodayDateBy);
		date = dateFormatDefined.format(cal.getTime());
		return date;
	}

	//boolean check to see if WebElement is present
	public static boolean isElementPresent(WebElement element) {
		try {
			//println is just for logging purpose
			element.isDisplayed();
			//String tagName = element.getTagName();
			//System.out.println("present element tagName is " + tagName);
			return true;
		} catch (NoSuchElementException e) {
			//System.out.println("==================print StackTrace for NoSuchElementException is merely for logging purpose==================");
			//e.getCause().printStackTrace();
			return false;
		}
	}

	//method is used to read sql spooled file result
	public static String[] readSqlResults(String dbMethodClassName, String sqlFileName) throws Exception {
		List<String> strLines = new ArrayList<>();
		try {
			// Open the file and read line
			String dbMethodSpoolFilePath = dbMethodClassName + "\\" + sqlFileName;
			String root = System.getProperty("user.dir");
			String dbResourcesBasedir = root + "\\src\\test\\resources\\Database\\";
			String basedir = dbResourcesBasedir + dbMethodSpoolFilePath;

			//print out the basedir for debugging purpose only
			//System.out.println("realsqlResults file dir is: " + basedir + ".txt");
			FileReader fileReader = new FileReader(basedir + ".txt");
			// Get the object of bufferReader
			BufferedReader br = new BufferedReader(fileReader);
			//List<String> strLines = new ArrayList<>();
			String strLine;
			//Read File Line By Line
			while ((strLine = br.readLine()) != null) {
				// Print the content on the console
				strLines.add(strLine);
			}
			//Close the bufferReader
			br.close();
			return strLines.toArray(new String[strLines.size()]);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Fail to get sql spooled result");
			return strLines.toArray(new String[strLines.size()]);
		}
	}

	//Image Diff Validation method
	public static void pDiff(String screenName) throws IOException, InterruptedException {
		try {
			File destination = new File(root + "\\src\\test\\resources\\ScreenShots\\Captured\\" + screenName + "_pDiff.png");
			if (destination.delete()) {
				System.out.println(destination + " of the prior test is deleted.");
			} else {
				System.out.println("No prior pDiff file to delete.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Process p;
		try {
			String pDiffString = root + "\\src\\test\\resources\\PDiff\\perceptualdiff " + root +
					"\\src\\test\\resources\\ScreenShots\\Captured\\" + screenName + ".png " + root + "\\src\\test\\resources\\ScreenShots\\ImageGold\\" + screenName + "_GOLD.png -verbose -threshold 100 -colorfactor 1.0 -gamma 1.0 -output " +
					root + "\\src\\test\\resources\\ScreenShots\\Captured\\" + screenName + "_pDiff.png";
			p = Runtime.getRuntime().exec(pDiffString);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line = reader.readLine();
			while (line != null) {
				line = reader.readLine();
				StringBuilder sb = new StringBuilder();
				String validateString = sb.append(line).toString();
				//print line is used for debugging
				System.out.println(validateString);
				boolean b = validateString.contains("FAIL");
				Assert.assertFalse(b, validateString);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		//Thread.sleep(15000);
	}

	/**
	 * Moves the mouse to the top-right corner of the element.
	 * @param columnHeader element to move to.
	 * @param xOffset Offset from the top-right corner. A negative value means coordinates left from
	 * the element.
	 */
	public static void resizeGridColumnByOffset(WebDriver driver, WebElement columnHeader, int xOffset)throws Exception{
		org.openqa.selenium.interactions.Actions columnResizeAction = new org.openqa.selenium.interactions.Actions(driver);
		columnResizeAction.moveToElement(columnHeader).perform();
		Waiter.waitForOneSecond();
		int x = columnHeader.getLocation().getX();
		int width = columnHeader.getSize().getWidth();
		columnResizeAction.moveToElement(columnHeader, width, 0).clickAndHold().moveByOffset(x+width+xOffset, 0).release().perform();
		Waiter.waitForOneSecond();
	}

	public static String getDBSessionStringNC(WebDriver driver){
		Set<Cookie> allCookies = driver.manage().getCookies();
		String dbSessionString = null;
		StringBuilder sb = new StringBuilder();
		for (Cookie loadedCookie : allCookies) {
//			System.out.println(String.format("%s -> %s", loadedCookie.getName(), loadedCookie.getValue()));
			String cookieName = loadedCookie.getName();
			if (cookieName.equals("session_idUTF")){
				String[] dbSessionStrEncrypted = loadedCookie.getValue().split("%");
				//int dbSessionStrSplLen = dbSessionStrOri.length;
				for (String dbSessionSpl : dbSessionStrEncrypted){
					dbSessionString = sb.append(dbSessionSpl.charAt(dbSessionSpl.length()-1)).toString();
				}
			}
		}
		return dbSessionString;
	}

//	Letter	Description	Examples
//	y	Year	2013
//	M	Month in year	July, 07, 7
//	d	Day in month	1-31
//	E	Day name in week	Friday, Sunday
//	a	Am/pm marker	AM, PM
//	H	Hour in day	0-23
//	h	Hour in am/pm	1-12
//	m	Minute in hour	0-60
//	s	Second in minute	0-60
	public static Date getDateByString(String dateString, String format){
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		Date date = null;
		try {
			date = dateFormat.parse(dateString);
		} catch (ParseException e){
			e.printStackTrace();
		}
		return date;
	}

    @Attachment(value = "JIRA ID", type = "text/html")
	public static String setTestMethodDescription(String jiraId, String testDescription) {
		String testName = Reporter.getCurrentTestResult().getMethod().getMethodName();
		String className = Reporter.getCurrentTestResult().getTestClass().getName();
		Reporter.log("<style type=\"text/css\">");
		Reporter.log(".tg {border-collapse:collapse;border-spacing:0;border-color:#999;table-layout:fixed;width:830px;}");
		Reporter.log(".tg td{font-family:Verdana;font-size:9px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;word-wrap:break-word;border-color:#999;color:#444;background-color:#F7FDFA;text-align:left;}");
		Reporter.log(".tg th{font-family:Verdana;font-size:10px;font-weight:bold;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}");
		Reporter.log("</style><table class=\"tg\"><tbody><col width=\"70px\"> <col width=\"260px\"> <col width=\"200px\"><col width=\"300px\">");
		String jiraIdUrl = "http://jira.crunchtime.local/browse/" + jiraId.toUpperCase();
		Reporter.log("<tr><td style=\"word-wrap:normal;\"><a target=\"_blank\" href=\""+ jiraIdUrl + "\" style=\"display:block;\">"+ jiraId.toUpperCase() +"</a></td>");
		Reporter.log("<td>"+ testDescription +"</td>");
		Reporter.log("<td>"+ testName +"</td>");
		Reporter.log("<td>" + className + "</td></tr></tbody></table>");
        return "<tr><td style=\"word-wrap:normal;\"><a target=\"_blank\" href=\""+ jiraIdUrl + "\" style=\"display:block;\">"+ jiraId.toUpperCase() +"</a></td>";
	}

    @Attachment(value = "Test Failure Screenshot", type = "image/png")
    public static byte[] setTestFailureScreenshot(File file) throws IOException {
        BufferedImage bufferedImage = ImageIO.read(file);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "png", baos);
        baos.flush();
        byte[] imageInByte = baos.toByteArray();
        baos.close();
        return imageInByte;
    }
}