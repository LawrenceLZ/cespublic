package com.crunchtime.ces.helper;

import com.crunchtime.ces.base.SmokeTestBase;
import com.google.common.base.Predicate;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class Waiter {
	private static final long EXPLICIT_TIME_OUT_SECONDS = 120;
	private static final long SLEEP_MILLI_SECONDS = 1000;
	private static final int DEFAULT_TIME_OUT = 120;

	public static void waitForOneSecond() throws Exception {
		Thread.sleep(SLEEP_MILLI_SECONDS);
	}

	public static void waitFor(final ExpectedCondition condition) {
		getWaiter().until(condition);
	}

	public static void fluentWaitForElementToBeVisible(WebDriver driver, WebElement element) {
		driver.manage().timeouts().implicitlyWait(EXPLICIT_TIME_OUT_SECONDS, TimeUnit.SECONDS);
		new FluentWait<WebElement>(element).withTimeout(EXPLICIT_TIME_OUT_SECONDS, TimeUnit.SECONDS).pollingEvery(
				100, TimeUnit.MILLISECONDS
		).ignoring(NoSuchElementException.class).until(new Predicate<WebElement>() {
			@Override
			public boolean apply(WebElement webElement) {
				return webElement.isDisplayed();
			}
		});
	}

	public static void waitForElementToBeVisible(WebDriver driver, WebElement element) throws Exception {
		waitForOneSecond();
		new WebDriverWait(driver, EXPLICIT_TIME_OUT_SECONDS, SLEEP_MILLI_SECONDS).until(
				ExpectedConditions.visibilityOf(element)
		);
	}

	public static void waitForElementToDisappear(WebDriver driver, WebElement element) throws Exception {
		waitForOneSecond();
		new WebDriverWait(driver, EXPLICIT_TIME_OUT_SECONDS, SLEEP_MILLI_SECONDS).until(
				ExpectedConditions.not(ExpectedConditions.visibilityOf(element))
		);
		waitForOneSecond();
	}

	public static void waitForElementToBeClickable(WebDriver driver, WebElement element) throws Exception {
		waitForOneSecond();
		new WebDriverWait(driver, EXPLICIT_TIME_OUT_SECONDS, SLEEP_MILLI_SECONDS).until(
				ExpectedConditions.elementToBeClickable(element)
		);
		waitForOneSecond();
	}

	public static void waitForElementToVanish(WebElement element) throws Exception {
		int i;
		for (i = 0; i < EXPLICIT_TIME_OUT_SECONDS; i++) {
			waitForOneSecond();
			try {
				element.isDisplayed();
			} catch (NoSuchElementException e) {
				//System.out.println("==================print StackTrace for NoSuchElementException is merely for logging purpose==================");
				//e.getCause().printStackTrace();
				break;
			}
		}
		if (i == EXPLICIT_TIME_OUT_SECONDS) {
			throw new IllegalStateException("Timed out to wait for Element to vanish from page, after " + i + " seconds.");
		}
	}

	public static void waitForPageTitle(String title) {
		getWaiter().until(ExpectedConditions.titleIs(title));
	}

	private static WebDriverWait getWaiter() {
		WebDriver driver = SmokeTestBase.getPhysicalInventoryTestBaseDriver();
		return new WebDriverWait(driver, DEFAULT_TIME_OUT);
	}
}