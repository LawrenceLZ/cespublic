package com.crunchtime.ces.dialogs;

import com.crunchtime.ces.base.PageBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ExportDialog extends PageBase {

	private final WebDriver driver;

	public ExportDialog(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(xpath = "//*[@ces-selenium-id='header']//span[contains(text(),'Export')]")
	public WebElement exportDialogHeader;

    @FindBy(xpath = "(//table[@class='x-field x-table-plain x-form-item x-form-type-text x-box-item x-field-default x-vbox-form-item x-form-dirty']//label[@class='x-form-item-label x-unselectable x-form-item-label-left'])[1]")
    public WebElement fileFormatLabel;
    @FindBy(xpath = "(//table[@class='x-field x-table-plain x-form-item x-form-type-text x-box-item x-field-default x-vbox-form-item x-form-dirty']//label[@class='x-form-item-label x-unselectable x-form-item-label-left'])[2]")
    public WebElement pageFormatLabel;
    @FindBy(xpath = "(//table[@class='x-field x-table-plain x-form-item x-form-type-text x-box-item x-field-default x-vbox-form-item x-form-dirty']//label[@class='x-form-item-label x-unselectable x-form-item-label-left'])[3]")
    public WebElement orientationLabel;

    @FindBy(xpath = "//input[@name='fileFormat']")
    public WebElement fileFormatCombobox;
    @FindBy(xpath = "//input[@name='pageFormat']")
    public WebElement pageFormatCombobox;
    @FindBy(xpath = "//input[@name='orientation']")
    public WebElement orientationCombobox;

    @FindBy(xpath = "(//a[@class='x-btn x-unselectable x-btn-toolbar x-box-item x-toolbar-item x-btn-default-toolbar-small x-noicon x-btn-noicon x-btn-default-toolbar-small-noicon'])[1]")
    public WebElement exportButton;
    @FindBy(xpath = "(//a[@class='x-btn x-unselectable x-btn-toolbar x-box-item x-toolbar-item x-btn-default-toolbar-small x-noicon x-btn-noicon x-btn-default-toolbar-small-noicon'])[2]")
    public WebElement cancelButton;
}