package com.crunchtime.ces.dialogs;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class SelectLayoutDialog extends PageBase {

	private final WebDriver driver;

	public SelectLayoutDialog(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "a[title='Single Widget']")
	private WebElement singleWidgetBox;
	@FindBy(css = "a[title='Two 1:1 Widgets']")
	public WebElement two11WidgetsBox;

	public void selectSingleWidgetBox() {
		Waiter.waitFor(visibilityOf(singleWidgetBox));
		singleWidgetBox.click();
	}
}