package com.crunchtime.ces.dialogs;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class CreateQuickLinkDialog extends PageBase {

	private final WebDriver driver;

	public CreateQuickLinkDialog(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "input[name='linkUrl']")
	private WebElement urlForContent;
	@FindBy(css = "input[name='linkLabel']")
	private WebElement title;
	@FindBy(css = "a[ces-selenium-id=button_saveButton]")
	private WebElement saveButton;

	public void typeUrlForContent(String urlForContentValue) throws Exception {
		Waiter.waitFor(visibilityOf(urlForContent));
		urlForContent.clear();
		urlForContent.sendKeys(urlForContentValue);
	}

	public void typeTitle(String titleValue) throws Exception {
		Waiter.waitFor(visibilityOf(title));
		title.clear();
		title.sendKeys(titleValue);
		Thread.sleep(100);
	}

	public void saveButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(saveButton));
		Waiter.waitFor(elementToBeClickable(saveButton));
		saveButton.click();
	}
}