package com.crunchtime.ces.dialogs;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class AddRemoveLinksDialog extends PageBase {

	private final WebDriver driver;

	public AddRemoveLinksDialog(WebDriver driver) {
		this.driver = driver;
        this.confirmDialog = PageFactory.initElements(driver, ConfirmDialog.class);
	}


    @FindBy(xpath = "//label[text()='Purchasing Overview']/..//input")
    private WebElement purchasingOverviewCheckbox;
    @FindBy(xpath = "//label[text()='New CrunchTime']/..//input")
    private WebElement newCrunchTimeCheckbox;
    @FindBy(xpath = "//label[text()='New CrunchTime']/../../../../../..//img")
    private WebElement newCrunchTimeDeleteLink;
    @FindBy(xpath = "(//span[text()='Purchasing'])[last()]/../../../../../../../../..")
    private WebElement dialogBodyDiv;
    @FindBy(xpath = "//div[@data-qtip='Create Custom Link']//img[@class='x-tool-img x-tool-configuration']")
    private WebElement createCustomLinkIcon;

    @FindBy(xpath = "//span[text()='Save']")
    private WebElement saveButton;

    protected ConfirmDialog confirmDialog;

    public void scrollToBottomDialog() throws Exception{
        Waiter.waitFor(visibilityOf(dialogBodyDiv));
        Actions action = new Actions(driver);
        action.moveToElement(dialogBodyDiv, 0, 0).perform();
        dialogBodyDiv.click();
        dialogBodyDiv.sendKeys(Keys.PAGE_DOWN);
        dialogBodyDiv.sendKeys(Keys.PAGE_DOWN);
        dialogBodyDiv.sendKeys(Keys.PAGE_DOWN);
        dialogBodyDiv.sendKeys(Keys.PAGE_DOWN);
        Thread.sleep(500);
    }

    public void scrollToTopDialog() throws Exception{
        Waiter.waitFor(visibilityOf(dialogBodyDiv));
        Actions action = new Actions(driver);
        action.moveToElement(dialogBodyDiv, 0, 0).perform();
        dialogBodyDiv.click();
        dialogBodyDiv.sendKeys(Keys.PAGE_UP);
        dialogBodyDiv.sendKeys(Keys.PAGE_UP);
        dialogBodyDiv.sendKeys(Keys.PAGE_UP);
        dialogBodyDiv.sendKeys(Keys.PAGE_UP);
        Thread.sleep(500);
    }

    public void newCrunchTimeDeleteLinkClick() throws Exception{
        try {
            Waiter.waitFor(visibilityOf(newCrunchTimeCheckbox));
            newCrunchTimeDeleteLink.click();
            confirmDialog.yesButtonClick();
        }
        catch (Exception e) {
            System.out.println("Exception occurred: " + e.getMessage());
        }
    }

    public void createCustomLinkIconClick() throws Exception{
        Waiter.waitFor(visibilityOf(createCustomLinkIcon));
        createCustomLinkIcon.click();
    }

    public void checkNewCrunchTimeCheckbox() {
        Waiter.waitFor(visibilityOf(newCrunchTimeCheckbox));
        if (!newCrunchTimeCheckbox.isSelected()) {
            newCrunchTimeCheckbox.click();
        }
    }

    public void checkPurchasingOverviewCheckbox() {
        Waiter.waitFor(visibilityOf(purchasingOverviewCheckbox));
        if (!purchasingOverviewCheckbox.isSelected()) {
            purchasingOverviewCheckbox.click();
        }
    }

    public void saveButtonClick() throws Exception{
        Waiter.waitFor(visibilityOf(saveButton));
        saveButton.click();
    }

}