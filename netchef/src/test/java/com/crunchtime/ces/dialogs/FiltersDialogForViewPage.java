package com.crunchtime.ces.dialogs;

import com.crunchtime.ces.base.PageBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FiltersDialogForViewPage extends PageBase {

	private final WebDriver driver;

	public FiltersDialogForViewPage(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(xpath = "(//span[@class='x-header-text x-window-header-text x-window-header-text-default'])[last()]")
	public WebElement filtersDialogHeader;

    @FindBy(xpath = "(//input[@name='productName'])[last()]")
    public WebElement productNameInput;
    @FindBy(xpath = "(//input[@name='productNumber'])[last()]")
    public WebElement productNumberInput;
    @FindBy(xpath = "(//input[@name='productUpcNumber'])[last()]")
    public WebElement productUpcNumberInput;
    @FindBy(xpath = "(//input[@name='productType'])[last()]")
    public WebElement productTypeCombobox;
    @FindBy(xpath = "(//input[@name='storageName'])[last()]")
    public WebElement storageLocationCombobox;
    @FindBy(xpath = "(//input[@name='userDefinedCategory'])[last()]")
    public WebElement userDefinedCategoryCombobox;
    @FindBy(xpath = "(//input[@name='category'])[last()]")
    public WebElement categoryCombobox;
    @FindBy(xpath = "(//input[@class='x-form-field x-form-radio x-form-cb'])[1]")
    public WebElement categoryRadiobox;
    @FindBy(xpath = "(//input[@class='x-form-field x-form-radio x-form-cb'])[2]")
    public WebElement subcategoryRadiobox;
    @FindBy(xpath = "(//input[@class='x-form-field x-form-radio x-form-cb'])[3]")
    public WebElement microcategoryRadiobox;

    @FindBy(xpath = "(//div[@class='x-toolbar x-docked x-toolbar-default x-docked-bottom x-toolbar-docked-bottom x-toolbar-default-docked-bottom x-box-layout-ct'])[last()]//a[@class='x-btn x-unselectable x-btn-toolbar x-box-item x-toolbar-item x-btn-default-toolbar-small x-noicon x-btn-noicon x-btn-default-toolbar-small-noicon'][1]")
    public WebElement applyButton;
    @FindBy(xpath = "(//div[@class='x-toolbar x-docked x-toolbar-default x-docked-bottom x-toolbar-docked-bottom x-toolbar-default-docked-bottom x-box-layout-ct'])[last()]//a[@class='x-btn x-unselectable x-btn-toolbar x-box-item x-toolbar-item x-btn-default-toolbar-small x-noicon x-btn-noicon x-btn-default-toolbar-small-noicon'][2]")
    public WebElement cancelButton;
    @FindBy(xpath = "(//div[@class='x-toolbar x-docked x-toolbar-default x-docked-bottom x-toolbar-docked-bottom x-toolbar-default-docked-bottom x-box-layout-ct'])[last()]//a[@class='x-btn x-unselectable x-btn-toolbar x-box-item x-toolbar-item x-btn-default-toolbar-small x-noicon x-btn-noicon x-btn-default-toolbar-small-noicon'][3]")
    public WebElement clearButton;
}