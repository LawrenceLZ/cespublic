package com.crunchtime.ces.dialogs;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class ConfigureWidgetDialog extends PageBase {

	private final WebDriver driver;

	public ConfigureWidgetDialog(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "input[name='widgetName']")
	private WebElement widgetNameCombobox;
	@FindBy(xpath = "//label[text()='Allow Reordering']/../..//input[contains (@class,'x-form-field x-form-checkbox x-form-cb')]")
	private WebElement allowReorderingCheckbox;
	@FindBy(xpath = "//label[text()='Allow Remove']/../..//input[contains (@class,'x-form-field x-form-checkbox x-form-cb')]")
	private WebElement allowRemoveCheckbox;
	@FindBy(xpath = "//span[text()='Save']")
	private WebElement saveButton;

	public void selectWidgetName(String widgetName) {
		Waiter.waitFor(visibilityOf(widgetNameCombobox));
		widgetNameCombobox.click();
		Waiter.waitFor(visibilityOf(driver.findElement(By.xpath("//li[text()='" + widgetName + "']"))));
		driver.findElement(By.xpath("//li[text()='" + widgetName + "']")).click();
	}

	public void checkAllowReorderingCheckbox() {
		Waiter.waitFor(visibilityOf(allowReorderingCheckbox));
		if (!allowReorderingCheckbox.isSelected()) {
			allowReorderingCheckbox.click();
		}
	}

	public void checkAllowRemoveCheckbox() {
		Waiter.waitFor(visibilityOf(allowRemoveCheckbox));
		if (!allowRemoveCheckbox.isSelected()) {
			allowRemoveCheckbox.click();
		}
	}

	public void saveButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(saveButton));
		saveButton.click();
	}
}