package com.crunchtime.ces.dialogs;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.helper.Waiter;
import com.crunchtime.ces.pages.dashboard.PageDashboard;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class ConfirmDialog extends PageBase {

	private final WebDriver driver;

	public ConfirmDialog(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(css = "a[ces-selenium-id=button_yes]")
	private WebElement yesButton;
	@FindBy(css = "a[ces-selenium-id=button_no]")
	private WebElement noButton;
	@FindBy(css = "a[ces-selenium-id=button_cancel]")
	private WebElement cancelButton;

	private static By saveCorporateLayoutButton = By.xpath("(//a[.='Save Corporate Layout'])[last()]");

	public void yesButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(yesButton));
		yesButton.click();
	}

	public void noButtonClick() throws Exception {
		Waiter.waitFor(visibilityOf(noButton));
		noButton.click();
		Waiter.waitFor(invisibilityOfElementLocated(PageDashboard.saveCorporateLayoutButtonByLocator));
	}
}