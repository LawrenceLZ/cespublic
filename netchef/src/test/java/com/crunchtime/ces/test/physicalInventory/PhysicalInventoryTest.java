package com.crunchtime.ces.test.physicalInventory;


import com.crunchtime.ces.base.ScreenshotOnFailureListenerNC;
import com.crunchtime.ces.base.SmokeTestBase;
import com.crunchtime.ces.helper.Waiter;
import com.crunchtime.ces.pages.inventory.inventoryOverview.PageInventoryOverview;
import com.crunchtime.ces.pages.inventory.inventoryOverview.physicalInventory.AuditPage;
import com.crunchtime.ces.pages.inventory.inventoryOverview.physicalInventory.CountSheetPage;
import com.crunchtime.ces.pages.inventory.inventoryOverview.physicalInventory.ReviewPage;
import com.crunchtime.ces.pages.inventory.inventoryOverview.physicalInventory.ViewPage;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;
import static org.testng.Assert.*;

@Listeners(ScreenshotOnFailureListenerNC.class)
@Test(groups = "NCPhysicalInventory")
public class PhysicalInventoryTest extends SmokeTestBase {

	protected PageInventoryOverview pageNCInventoryOverview;
	protected AuditPage auditPage;
	protected ViewPage viewPage;
	protected ReviewPage reviewPage;
	protected CountSheetPage countSheetPage;
	private static String comboboxValueForCountSheet = "12/29/2013";

	@BeforeMethod
	public void setUp() throws Exception {
		super.setUp();
		this.pageNCInventoryOverview = PageFactory.initElements(driver, PageInventoryOverview.class);
		this.auditPage = PageFactory.initElements(driver, AuditPage.class);
		this.viewPage = PageFactory.initElements(driver, ViewPage.class);
		this.reviewPage = PageFactory.initElements(driver, ReviewPage.class);
		this.countSheetPage = PageFactory.initElements(driver, CountSheetPage.class);
	}

	@Test(groups = "NCPhysicalInventory")
	public void checkInventoryLinkTest() throws Exception {
		assertTrue(basePageNC.inventoryMainMenuLink.isDisplayed(), "Inventory main menu link is not displayed, so test failed.");
	}

	@Test(groups = "NCPhysicalInventory")
	public void checkPhysicalInventoryTest() throws Exception {
		basePageNC.inventoryMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.headerPhysicalInventory));
		assertTrue(pageNCInventoryOverview.headerPhysicalInventory.isDisplayed(), "Physical Inventory table is not displayed, so test failed.");
	}

	@Test(groups = "NCPhysicalInventory")
	public void clickInventoryOverviewMenuTest() throws Exception {
		Actions menuLinkMouseOverAction = new Actions(driver);
		menuLinkMouseOverAction.moveToElement(basePageNC.inventoryMainMenuLink).build().perform();
		Waiter.waitFor(visibilityOf(basePageNC.inventoryOverviewMenuLink));
		basePageNC.inventoryOverviewMenuLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.headerPhysicalInventory));
		assertTrue(pageNCInventoryOverview.headerPhysicalInventory.isDisplayed(), "Physical Inventory table is not displayed, so test failed.");
	}

	@Test(groups = "NCPhysicalInventory")
	public void checkReviewProcessTest() throws Exception {
		basePageNC.inventoryMainMenuLink.click();
		Thread.sleep(2000);
		pageNCInventoryOverview.reviewProcessLink.click();
		Thread.sleep(2000);
		assertTrue(viewPage.headerViewProcessPage.isDisplayed(), "Physical Inventory for Post Period X is not displayed, so test failed.");
		assertTrue(reviewPage.quantityTab.isDisplayed(), "Quantity link is not displayed, so test failed.");
		assertTrue(reviewPage.valueTab.isDisplayed(), "Value link is not displayed, so test failed.");
		assertTrue(reviewPage.analysisTab.isDisplayed(), "Analysis link is not displayed, so test failed.");
	}

	// todo: this test doesn't work properly in Firefox
//	@Test(groups = "NCPhysicalInventory")
//	public void checkCountSheetProcessTest() throws Exception {
//		basePageNC.inventoryMainMenuLink.click();
//		pageNCInventoryOverview.selectPostPeriod(comboboxValueForCountSheet);
//		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.countSheetProcessLink));
//		pageNCInventoryOverview.countSheetProcessLink.click();
//		Waiter.waitFor(visibilityOf(countSheetPage.headerCountSheetProcessPage));
//		assertTrue(countSheetPage.headerCountSheetProcessPage.isDisplayed(), "Physical Inventory Count Sheet for Post Period Ending X is not displayed, so test failed.");
//	}

	// todo: this test doesn't work properly in Firefox
//	@Test(groups = "NCPhysicalInventory")
//	public void checkCountSheetAndCloseTest() throws Exception {
//		basePageNC.inventoryMainMenuLink.click();
//		pageNCInventoryOverview.selectPostPeriod(comboboxValueForCountSheet);
//		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.countSheetProcessLink));
//		pageNCInventoryOverview.countSheetProcessLink.click();
//		Waiter.waitFor(visibilityOf(countSheetPage.closeButton));
//		countSheetPage.closeButton.click();
//		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.headerPhysicalInventory));
//		assertTrue(pageNCInventoryOverview.headerPhysicalInventory.isDisplayed(), "Physical Inventory table is not displayed, so test failed.");
//	}

	@Test(groups = "NCPhysicalInventory")
	public void checkCancelProcessTest() throws Exception {
		basePageNC.inventoryMainMenuLink.click();
		pageNCInventoryOverview.selectPostPeriod(comboboxValueForCountSheet);
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.cancelProcessLink));
		pageNCInventoryOverview.cancelProcessLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.headerCancelProcessPage));
		assertTrue(pageNCInventoryOverview.headerCancelProcessPage.isDisplayed(), "Cancel Physical Inventory dialog box is not displayed, so test failed.");
	}

	@Test(groups = "NCPhysicalInventory")
	public void checkFinalForPostInventoryTest() throws Exception {
		basePageNC.inventoryMainMenuLink.click();
		pageNCInventoryOverview.selectPostPeriod(comboboxValueForCountSheet);
		//Waiter.waitFor(visibilityOf(pageNCInventoryOverview.reviewProcessLink));
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.FinalForPost));
		assertTrue(pageNCInventoryOverview.FinalForPost.isDisplayed(), "Final for Post Inventory is not displayed, so test failed.");
	}

	// todo: this test doesn't work properly in Firefox
//    @Test (groups = "NCPhysicalInventory")
//    public void checkCountSheetColumnsTest () throws Exception {
//        basePageNC.inventoryMainMenuLink.click();
//        pageNCInventoryOverview.selectPostPeriod(comboboxValueForCountSheet);
//        Waiter.waitFor(visibilityOf(pageNCInventoryOverview.countSheetProcessLink));
//        pageNCInventoryOverview.countSheetProcessLink.click();
//        Waiter.waitFor(visibilityOf(pageNCInventoryOverview.CountAltUnit3));
//        boolean testPassed = true;
//        if (!pageNCInventoryOverview.CountSeq.isDisplayed()) {
//            testPassed = false;
//            System.out.println("Seq. column is not displayed");
//        }
//        if (!pageNCInventoryOverview.CountProductNum.isDisplayed()) {
//            testPassed = false;
//            System.out.println("Product # column is not displayed");
//        }if (!pageNCInventoryOverview.CountProductName.isDisplayed()) {
//            testPassed = false;
//            System.out.println("Product Name column is not displayed");
//        }if (!pageNCInventoryOverview.CountInventoryUnit.isDisplayed()) {
//            testPassed = false;
//            System.out.println("Alt Unit 1 column is not displayed");
//        }if (!pageNCInventoryOverview.CountAltUnit1.isDisplayed()) {
//            testPassed = false;
//            System.out.println("Alt Unit 2 column is not displayed");
//        }if (!pageNCInventoryOverview.CountAltUnit2.isDisplayed()) {
//            testPassed = false;
//            System.out.println("Alt Unit 3 column is not displayed");
//        }if (!pageNCInventoryOverview.CountAltUnit3.isDisplayed()) {
//            testPassed = false;
//            System.out.println("Seq. column is not displayed");
//        }
//        assertTrue(testPassed, "Some column(s) is (are) not displayed, so test failed.");
//    }
}