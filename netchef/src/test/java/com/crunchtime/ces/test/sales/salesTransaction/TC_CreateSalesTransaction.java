package com.crunchtime.ces.test.sales.salesTransaction;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.database.generic.DBStmtNCGeneric;
import com.crunchtime.ces.helper.ActionsNC;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TC_CreateSalesTransaction extends BaseTestNC {
	String dbConnectionString = "autorpt/autorpt@CTAUTO24006";
	String testSiteName = "autorpt.net-chef.local";
	String userID = "autorpt";
	String userPWD = "autorpt";
	String testLocationName = "#CCI_REPORT#";

	@BeforeMethod(alwaysRun = true)
	public void setup() throws Exception {
		basePageNC.openNCTestSite(testSiteName);
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown() throws Exception {
		pageNCLogout.logoutNC();
	}

	@Test(groups = {"userGroupAccess"})
	public void userGroupAccessForCreateSalesTransaction() throws Exception {
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Enter Sales Transactions", "Create");
		pageNCLogout.logoutNC();
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
		pageNavigationGeneric.menuLinkMouseOver("Sales", "");
		//check if it is existed
		boolean existFlag = ActionsNC.isElementPresent(basePageNC.createSalesTransactionMenuLink);
		if (existFlag) {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Enter Sales Transactions", "Create");
			Assert.assertFalse(existFlag, "Create Sales Transaction Link Should be DISABLED if control is OFF, so test failed.");
		} else {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Enter Sales Transactions", "Create");
			pageNCLogout.logoutNC();
			pageNCLogin.loginNC(userID, userPWD, testLocationName);
			pageNavigationGeneric.menuLinkMouseOver("Sales", "");
			boolean existFlagUpdated = ActionsNC.isElementPresent(basePageNC.createSalesTransactionMenuLink);
			Assert.assertTrue(existFlagUpdated, "Create Sales Transaction Link Should be ENABLED if control is ON, so test failed.");
		}
	}
}
