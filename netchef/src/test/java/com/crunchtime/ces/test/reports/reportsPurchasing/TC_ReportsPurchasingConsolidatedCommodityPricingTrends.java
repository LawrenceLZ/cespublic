package com.crunchtime.ces.test.reports.reportsPurchasing;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.database.generic.DBStmtNCGeneric;
import com.crunchtime.ces.helper.ActionsNC;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TC_ReportsPurchasingConsolidatedCommodityPricingTrends extends BaseTestNC {
	private String dbConnectionString = "autorpt/autorpt@CTAUTO24006";
	private String testSiteName = "autorpt.net-chef.local";
	private String userID = "autorpt";
	private String userPWD = "autorpt";
	private String testLocationName = "#CCI_REPORT#";

	@BeforeMethod(alwaysRun = true)
	public void setup() throws Exception {
		basePageNC.openNCTestSite(testSiteName);
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}

	@Test(groups = {"userGroupAccess"})
	public void userGroupAccessForReportsPurchasingConsolidatedCommodityPricingTrends() throws Exception {
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Consolidated Reports", "Consolidated Commodity Pricing Trends");
		pageNCLogout.logoutNC();
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
		pageNavigationGeneric.menuLinkMouseOver("Purchasing", "Reports");
		//check if it is existed
		boolean existFlag1 = ActionsNC.isElementPresent(basePageNC.reportsPurchasingConsolidatedCommodityPricingTrendsMenuLink);
		pageNavigationGeneric.menuLinkMouseOver("Reports", "Purchasing");
		boolean existFlag2 = ActionsNC.isElementPresent(basePageNC.reportsPurchasingConsolidatedCommodityPricingTrendsMenuLink);
		if (existFlag1 || existFlag2) {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Consolidated Reports", "Consolidated Commodity Pricing Trends");
			Assert.assertFalse(existFlag1, "Purchasing --> Reports --> Consolidated Commodity Pricing Trends Link Should be DISABLED if control is OFF, so test failed.");
			Assert.assertFalse(existFlag2, "Reports --> Purchasing --> Consolidated Commodity Pricing Trends Link Should be DISABLED if control is OFF, so test failed.");
		} else {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Consolidated Reports", "Consolidated Commodity Pricing Trends");
			pageNCLogout.logoutNC();
			pageNCLogin.loginNC(userID, userPWD, testLocationName);
			pageNavigationGeneric.menuLinkMouseOver("Purchasing", "Reports");
			boolean existFlagUpdated1 = ActionsNC.isElementPresent(basePageNC.reportsPurchasingConsolidatedCommodityPricingTrendsMenuLink);
			Assert.assertTrue(existFlagUpdated1, "Purchasing --> Reports --> Consolidated Commodity Pricing Trends Link Should be ENABLED if control is ON, so test failed.");
			pageNavigationGeneric.menuLinkMouseOver("Reports", "Purchasing");
			boolean existFlagUpdated2 = ActionsNC.isElementPresent(basePageNC.reportsPurchasingConsolidatedCommodityPricingTrendsMenuLink);
			Assert.assertTrue(existFlagUpdated2, "Reports --> Purchasing --> Consolidated Commodity Pricing Trends Link Should be ENABLED if control is ON, so test failed.");
		}
	}
}
