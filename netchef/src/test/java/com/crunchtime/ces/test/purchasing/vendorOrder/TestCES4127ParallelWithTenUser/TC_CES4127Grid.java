package com.crunchtime.ces.test.purchasing.vendorOrder.TestCES4127ParallelWithTenUser;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.database.purchasing.vendorOrder.DBStmtVendorOrder;
import com.crunchtime.ces.helper.ActionsNC;
import com.crunchtime.ces.helper.DBMethodNC;
import com.crunchtime.ces.helper.Waiter;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.*;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class TC_CES4127Grid extends BaseTestNC {
	private static final String DB_CONNECTION_STRING = "AUTO/AUTO@CTAUTO24006.WORLD";
	private static final String TEST_SITE_NAME = "auto.net-chef.local/";
//	private static final String USER_ID = "grid";
//	private static final String USER_PWD = "grid";
	private static final String TEST_LOCATION_NAME = "Chicago #332";
	private static final String TEST_VENDOR_NAME = "Alpha Baking Company";
	private static final String TEST_PRODUCT_NAME_ONE = "Baguettes";
	private static final String TEST_PRODUCT_NAME_TWO = "Croutons";

	@Parameters({"USER_ID", "USER_PWD"})
	@BeforeMethod
	public void logIn(@Optional("auto") String userID, @Optional("auto") String userPwd) throws Exception {
		basePageNC.openNCTestSite(TEST_SITE_NAME);
		pageNCLogin.loginNC(userID, userPwd, TEST_LOCATION_NAME);
	}

	@AfterMethod
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}

	@Test
	public void createAndReconcileVendorOrderGrid(Method method) throws Exception {
		basePageNC.navigateToCreateVendorOrderScreen();
		//select the test vendor and go to detail page
		ActionsNC.setSelectDropdownByValue(pageCreateVendorOrder.vendorDropdown, TEST_VENDOR_NAME);
		pageCreateVendorOrder.continueBtn.click();
		//switch to vo detail grid (second frame) to enter order qty
		driver.switchTo().frame(1);
		//int rowCnt = pageGeneric.getGridRowCnt(pageCreateVendorOrder.createVOgrid) - 2;
		for (int i = 1; i < 5; i++) {
			Random rand = new Random();
			double orderQty = rand.nextInt(10) + 0.50;
			int columnCnt = ActionsNC.getGridColumnCntPerRowNum(pageCreateVendorOrder.createVOgrid, i);
//			System.out.println(columnCnt);
			if (columnCnt > 1) {
//				Thread.sleep(2000);
//				driver.switchTo().defaultContent();
//				driver.switchTo().frame(1);
//				Thread.sleep(2000);
				pageCreateVendorOrder.createVOgridCellItem(i, "Order Quantity").clear();
				pageCreateVendorOrder.createVOgridCellItem(i, "Order Quantity").sendKeys(String.valueOf(orderQty));
			}
		}
		pageCreateVendorOrder.addSelectedProductsBtn.click();
		pageCreateVendorOrder.prepareOrderBtn.click();
		driver.switchTo().defaultContent();
		//switch to second frame again to submit order
		driver.switchTo().frame(1);
		pageCreateVendorOrder.submitBtn.click();
		//update VO transaction number
		String fromTransNum = pageCreateVendorOrder.submittedTransNumber.getText().trim();
		String toTransNum = "AUTO-" + fromTransNum;
		DBStmtVendorOrder.updateVOTransNumber(DB_CONNECTION_STRING, toTransNum, fromTransNum);
		System.out.println(method.getName() + " Tran# is " + toTransNum);
		Reporter.log(method.getName() + " Tran# is " + toTransNum);
		pageCreateVendorOrder.exitBtn.click();
		//go to recent vendor orders grid
		pageCreateVendorOrder.cancelBtn.click();
		//start to reconcile vo
		pageRecentVendorOrders.filterBtn.get(0).click();
		pageRecentVendorOrders.purchaseOrderNumberTextbox.sendKeys(toTransNum);
		pageRecentVendorOrders.filterApplyBtn.click();
		//after filter to find the trans and then reset column order if needed
		pageRecentVendorOrders.waitForSpinnerToDisappear();
		//new WebDriverWait(driver,60,1000).until(
		//        ExpectedConditions.visibilityOfAllElements(pageRecentVendorOrders.recentVendorOrdersGrid.findElements(By.cssSelector("tr")))
		//);
		String defaultFirstVOgridColumn = pageRecentVendorOrders.headerColumns.get(0).getText().trim();
		System.out.println(defaultFirstVOgridColumn);
//		if (!defaultFirstVOgridColumn.equals("Vendor")) {
//			pageRecentVendorOrders.clickHeaderColumnTrigger(0);
//			pageRecentVendorOrders.resetColumnsLink.click();
//		}
//		pageRecentVendorOrders.recentVOgridCellItem(1, "Actions").findElement(By.linkText("Reconcile")).click();
		pageRecentVendorOrders.reconcileLink.click();
		Waiter.waitForElementToBeVisible(driver, pageRecentVendorOrders.vendorInvoiceNumberTextbox);
		pageRecentVendorOrders.vendorInvoiceNumberTextbox.sendKeys(toTransNum);
		pageRecentVendorOrders.totalInvoiceValueTextbox.clear();
		pageRecentVendorOrders.totalInvoiceValueTextbox.sendKeys("1.00");
		pageRecentVendorOrders.continueBtn.click();
		//driver.switchTo().frame(pageGeneric.cgiFrame.get(1));
		Waiter.waitForElementToBeVisible(driver, pageRecentVendorOrders.reconcileDetailScrnBottomFrame);
		driver.switchTo().frame(1);
		String defaultInvoiceTotal = pageRecentVendorOrders.defaultInvoiceTotal.getText();
		System.out.println(defaultInvoiceTotal);
		pageRecentVendorOrders.invoiceTotalTextbox.clear();
		pageRecentVendorOrders.invoiceTotalTextbox.sendKeys(defaultInvoiceTotal);
		pageRecentVendorOrders.continueBtn.click();
		//insert 50 rows to t_vendor_bid_queue to emulate multiple vendor bids waiting for update during reconcile process
		DBStmtVendorOrder.createRowsInVenBidQueueCES4127(DB_CONNECTION_STRING, 50, TEST_PRODUCT_NAME_ONE, TEST_VENDOR_NAME, TEST_LOCATION_NAME);
		DBStmtVendorOrder.createRowsInVenBidQueueCES4127(DB_CONNECTION_STRING, 50, TEST_PRODUCT_NAME_TWO, TEST_VENDOR_NAME, TEST_LOCATION_NAME);
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		//driver.switchTo().frame(pageGeneric.cgiFrame.get(1));
		driver.switchTo().frame(1);
		Thread.sleep(1000);
		pageRecentVendorOrders.reconcileBtn.click();
		pageRecentVendorOrders.okBtn.click();
		System.out.println("Reconcile complete");
		//Validate the row counts on t_vendor_bid_queue table for 3 minutes
		int i;
		for (i=0; i<180; i++){
			Waiter.waitForOneSecond();
			Map<String, List<Object>> map = DBMethodNC.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtVendorOrder.getStmtSelectRowCntFromVenBidQueueTbl());
			int rowCntOnVenBidQueueTbl = Integer.parseInt(map.get("row_count".toUpperCase()).get(0).toString().trim());
			if (rowCntOnVenBidQueueTbl==0){
				break;
			}
		}
		Assert.assertFalse(i==180, "Failed or timed-out, as T_VENDOR_BID_QUEUE table row count is NOT zero after 3 minutes.");
	}
}