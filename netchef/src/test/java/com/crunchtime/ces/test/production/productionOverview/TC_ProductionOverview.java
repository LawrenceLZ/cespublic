package com.crunchtime.ces.test.production.productionOverview;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.database.generic.DBStmtNCGeneric;
import com.crunchtime.ces.helper.ActionsNC;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TC_ProductionOverview extends BaseTestNC {
	String dbConnectionString = "autorpt/autorpt@CTAUTO24006";
	String testSiteName = "autorpt.net-chef.local";
	String userID = "autorpt";
	String userPWD = "autorpt";
	String testLocationName = "#CCI_REPORT#";

	@BeforeMethod(alwaysRun = true)
	public void logIn() throws Exception {
		basePageNC.openNCTestSite(testSiteName);
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}

	@Test(groups = {"userGroupAccess"})
	public void userGroupAccessForProductionOverview() throws Exception {
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Daily Store Tasks", "Production");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Daily Store Tasks", "Create Daily Prep");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Daily Store Tasks", "Daily Prep");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Daily Store Tasks", "Buffet Consumption");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Administrative Options", "Menu Matrix");
		//user group access for each report under Production page module
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-View Reports", "7-Day Pre-Production Summary");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-View Reports", "Recipe Card");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-View Reports", "Recipe Margin");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Administrative Options", "Scale Recipes");

		pageNCLogout.logoutNC();
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
		//pageNavigationGeneric.menuLinkMouseOver("Inventory", "");
		//check if it is existed
		boolean existFlag = ActionsNC.isElementPresent(pageNavigationGeneric.productionMenuLink);
		if (existFlag) {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Daily Store Tasks", "Production");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Daily Store Tasks", "Create Daily Prep");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Daily Store Tasks", "Daily Prep");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Daily Store Tasks", "Buffet Consumption");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Administrative Options", "Menu Matrix");
			//user group access for each report under Production page module
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "7-Day Pre-Production Summary");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Recipe Card");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Recipe Margin");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Administrative Options", "Scale Recipes");

			Assert.assertFalse(existFlag, "Production menu Link Should be DISABLED if control is OFF, so test failed.");
		} else {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Daily Store Tasks", "Production");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Daily Store Tasks", "Create Daily Prep");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Daily Store Tasks", "Daily Prep");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Daily Store Tasks", "Buffet Consumption");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Administrative Options", "Menu Matrix");
			//user group access for each report under Production page module
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "7-Day Pre-Production Summary");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Recipe Card");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Recipe Margin");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Administrative Options", "Scale Recipes");

			pageNCLogout.logoutNC();
			pageNCLogin.loginNC(userID, userPWD, testLocationName);
			//pageNavigationGeneric.menuLinkMouseOver("Inventory", "");
			boolean existFlagUpdated = ActionsNC.isElementPresent(pageNavigationGeneric.productionMenuLink);
			Assert.assertTrue(existFlagUpdated, "Production menu Link Should be ENABLED if control is ON, so test failed.");
		}
	}
}
