package com.crunchtime.ces.test.reports.reportsPurchasing;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.base.SmokeTestBase;
import com.crunchtime.ces.helper.Waiter;
import com.crunchtime.ces.pages.reports.reportsOverview.PageReportsOverview;
import com.crunchtime.ces.pages.reports.reportsPurchasing.PageReportsPurchasingConsolidatedPurchasesByGL;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class TC_ReportsConsolidatedPurchasesByGlSmoke extends SmokeTestBase {
	private static final String START_DATE = "06/15/2013";

	private PageReportsPurchasingConsolidatedPurchasesByGL pageReportsPurchasingConsolidatedPurchasesByGL;
	private PageReportsOverview pageReportsOverview;

	@BeforeMethod
	public void setUp() throws Exception {
		super.setUp();
		pageReportsPurchasingConsolidatedPurchasesByGL = PageFactory.initElements(driver, PageReportsPurchasingConsolidatedPurchasesByGL.class);
		pageReportsOverview = PageFactory.initElements(driver, PageReportsOverview.class);

		Waiter.waitFor(visibilityOf(basePageNC.reportsMainMenuLink));
		basePageNC.reportsMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageReportsOverview.reportsPanel));
		pageReportsOverview.expandAllButtonClick();
		pageReportsOverview.consolidatedPurchasesByGlLinkClick();
	}

	@Test(groups = "smoke")
	public void consolidatedPurchasesByGlLinkSmokeTest() {
		pageReportsPurchasingConsolidatedPurchasesByGL.enterStartDate(START_DATE);
		pageReportsPurchasingConsolidatedPurchasesByGL.selectHierarchy("Hierarchy 02-1-Corporate");
		pageReportsPurchasingConsolidatedPurchasesByGL.retrieveData();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.loadMask));
		pageReportsPurchasingConsolidatedPurchasesByGL.filterByLocationCode("abc");
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.loadMask));
		pageReportsPurchasingConsolidatedPurchasesByGL.clearConsolidatedPurchasesByGlFilter();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.loadMask));
		pageReportsPurchasingConsolidatedPurchasesByGL.collapseGrandTotal();
		pageReportsPurchasingConsolidatedPurchasesByGL.collapseLocationDetails();
		pageReportsPurchasingConsolidatedPurchasesByGL.expandGrandTotal();
		pageReportsPurchasingConsolidatedPurchasesByGL.expandLocationDetails();
		pageReportsPurchasingConsolidatedPurchasesByGL.closeConsolidatedPurchasesByGl();
	}

	@AfterMethod
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}
}