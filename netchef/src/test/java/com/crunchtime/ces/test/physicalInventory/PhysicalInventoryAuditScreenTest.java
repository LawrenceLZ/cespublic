package com.crunchtime.ces.test.physicalInventory;

import com.crunchtime.ces.base.SmokeTestBase;
import com.crunchtime.ces.dialogs.ExportDialog;
import com.crunchtime.ces.helper.Waiter;
import com.crunchtime.ces.pages.inventory.inventoryOverview.PageInventoryOverview;
import com.crunchtime.ces.pages.inventory.inventoryOverview.physicalInventory.AuditPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;
import static org.testng.Assert.assertTrue;

public class PhysicalInventoryAuditScreenTest extends SmokeTestBase {

	protected PageInventoryOverview pageNCInventoryOverview;
	protected AuditPage auditPage;
    protected ExportDialog exportDialog;

	@BeforeMethod
	public void setUp() throws Exception {
		super.setUp();
		this.pageNCInventoryOverview = PageFactory.initElements(driver, PageInventoryOverview.class);
		this.auditPage = PageFactory.initElements(driver, AuditPage.class);
        this.exportDialog = PageFactory.initElements(driver, ExportDialog.class);
	}

	@Test(groups = "NCPhysicalInventory")
	public void checkAuditProcessTest() throws Exception {
		basePageNC.inventoryMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.auditProcessLink));
		pageNCInventoryOverview.auditProcessLink.click();
		Waiter.waitFor(visibilityOf(auditPage.headerAuditProcessPage));
		assertTrue(auditPage.headerAuditProcessPage.isDisplayed(), "Physical Inventory Audit table is not displayed, so test failed.");
	}

	@Test(groups = "NCPhysicalInventory")
	public void checkAuditAndCloseTest() throws InterruptedException {
		basePageNC.inventoryMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.auditProcessLink));
		pageNCInventoryOverview.auditProcessLink.click();
		Waiter.waitFor(visibilityOf(auditPage.headerAuditProcessPage));
		auditPage.closeButton.click();
		assertTrue(pageNCInventoryOverview.headerPhysicalInventory.isDisplayed());
	}

    @Test(groups = "NCPhysicalInventory")
    public void checkExportOnAuditTest() throws Exception {
        basePageNC.inventoryMainMenuLink.click();
        Waiter.waitFor(visibilityOf(pageNCInventoryOverview.reviewProcessLink));
        pageNCInventoryOverview.auditProcessLink.click();
        Waiter.waitFor(visibilityOf(auditPage.exportIcon));
        auditPage.exportIcon.click();
        Waiter.waitFor(visibilityOf(exportDialog.exportDialogHeader));
        assertTrue(exportDialog.exportDialogHeader.isDisplayed(), "Export dialog box is not displayed, so test failed.");
    }
}