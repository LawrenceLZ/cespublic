package com.crunchtime.ces.test.reports.reportsPurchasing;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.database.generic.DBStmtNCGeneric;
import com.crunchtime.ces.helper.ActionsNC;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TC_ReportsPurchasingPurchaseDetail extends BaseTestNC {
	String dbConnectionString = "autorpt/autorpt@CTAUTO24006";
	String testSiteName = "autorpt.net-chef.local";
	String userID = "autorpt";
	String userPWD = "autorpt";
	String testLocationName = "#CCI_REPORT#";

	@BeforeMethod(alwaysRun = true)
	public void setup() throws Exception {
		basePageNC.openNCTestSite(testSiteName);
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}

	@Test(groups = {"userGroupAccess"})
	public void userGroupAccessForReportsPurchasingPurchaseDetail() throws Exception {
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-View Reports", "Purchase Detail");
		pageNCLogout.logoutNC();
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
		pageNavigationGeneric.menuLinkMouseOver("Purchasing", "Reports");
		//check if it is existed
		boolean existFlag1 = ActionsNC.isElementPresent(basePageNC.reportsPurchasingPurchaseDetailMenuLink);
		pageNavigationGeneric.menuLinkMouseOver("Reports", "Purchasing");
		boolean existFlag2 = ActionsNC.isElementPresent(basePageNC.reportsPurchasingPurchaseDetailMenuLink);
		if (existFlag1 || existFlag2) {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Purchase Detail");
			Assert.assertFalse(existFlag1, "Purchasing --> Reports --> Purchase Detail Link Should be DISABLED if control is OFF, so test failed.");
			Assert.assertFalse(existFlag2, "Reports --> Purchasing --> Purchase Detail Link Should be DISABLED if control is OFF, so test failed.");
		} else {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Purchase Detail");
			pageNCLogout.logoutNC();
			pageNCLogin.loginNC(userID, userPWD, testLocationName);
			pageNavigationGeneric.menuLinkMouseOver("Purchasing", "Reports");
			boolean existFlagUpdated1 = ActionsNC.isElementPresent(basePageNC.reportsPurchasingPurchaseDetailMenuLink);
			Assert.assertTrue(existFlagUpdated1, "Purchasing --> Reports --> Purchase Detail Link Should be ENABLED if control is ON, so test failed.");
			pageNavigationGeneric.menuLinkMouseOver("Reports", "Purchasing");
			boolean existFlagUpdated2 = ActionsNC.isElementPresent(basePageNC.reportsPurchasingPurchaseDetailMenuLink);
			Assert.assertTrue(existFlagUpdated2, "Reports --> Purchasing --> Purchase Detail Link Should be ENABLED if control is ON, so test failed.");
		}
	}
}
