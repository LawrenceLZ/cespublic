package com.crunchtime.ces.test.smoke;

import com.crunchtime.ces.base.BaseWidgetNC;
import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.base.SmokeTestBase;
import com.crunchtime.ces.dialogs.*;
import com.crunchtime.ces.helper.Waiter;
import com.crunchtime.ces.pages.administration.administrationOverview.PageAdministrationOverview;
import com.crunchtime.ces.pages.administration.administrationOverview.widget.PageConsolidatedPostingSummaryWidget;
import com.crunchtime.ces.pages.administration.administrationOverview.widget.PageTemplatesWidget;
import com.crunchtime.ces.pages.administration.administrationOverview.widget.PageUploadDownloadFilesWidget;
import com.crunchtime.ces.pages.bizIQ.PageBizIQ;
import com.crunchtime.ces.pages.dashboard.PageDashboard;
import com.crunchtime.ces.pages.inventory.inventoryOverview.PageInventoryOverview;
import com.crunchtime.ces.pages.inventory.inventoryOverview.widget.PageAdjustmentsWidget;
import com.crunchtime.ces.pages.inventory.inventoryOverview.widget.PageLocationTransfersWidget;
import com.crunchtime.ces.pages.labor.laborOverview.PageLaborOverview;
import com.crunchtime.ces.pages.labor.laborOverview.widget.PageEmployeeInformationWidget;
import com.crunchtime.ces.pages.labor.laborOverview.widget.PageLaborScheduleWidget;
import com.crunchtime.ces.pages.labor.laborOverview.widget.PageLaborSummaryWidget;
import com.crunchtime.ces.pages.production.productionOverview.PageProductionOverview;
import com.crunchtime.ces.pages.production.productionOverview.widget.PageDailyPrepWidget;
import com.crunchtime.ces.pages.purchasing.purchasingOverview.PagePurchasingOverview;
import com.crunchtime.ces.pages.purchasing.purchasingOverview.widget.PageRecentCommissaryOrdersWidget;
import com.crunchtime.ces.pages.purchasing.purchasingOverview.widget.PageRecentPurchaseByInvoiceWidget;
import com.crunchtime.ces.pages.purchasing.purchasingOverview.widget.PageRecentVendorOrdersWidget;
import com.crunchtime.ces.pages.reports.reportsOverview.PageReportsOverview;
import com.crunchtime.ces.pages.reports.reportsOverview.widget.PageReportsWidget;
import com.crunchtime.ces.pages.sales.salesOverview.PageSalesOverview;
import com.crunchtime.ces.pages.sales.salesOverview.widget.PageRecentSalesTransactionsWidget;
import com.crunchtime.ces.pages.sales.salesOverview.widget.PageSalesForecastWidget;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class TC_DashboardsSmoke extends SmokeTestBase {
	private PageBase pageBase;
	private BaseWidgetNC baseWidgetNC;
	private PageDashboard pageDashboard;
	private PageBizIQ pageBizIQ;
	private PagePurchasingOverview pagePurchasingOverview;
	private PageInventoryOverview pageInventoryOverview;
	private PageSalesOverview pageSalesOverview;
	private PageProductionOverview pageProductionOverview;
	private PageLaborOverview pageLaborOverview;
	private PageReportsOverview pageReportsOverview;
	private PageAdministrationOverview pageAdministrationOverview;
	private SelectLayoutDialog selectLayoutDialog;
	private ConfigureWidgetDialog configureWidgetDialog;
	private ConfirmDialog confirmDialog;
	private AddRemoveLinksDialog addRemoveLinksDialog;
	private CreateQuickLinkDialog createQuickLinkDialog;
	private PageRecentVendorOrdersWidget recentVendorOrdersWidget;
	private PageRecentPurchaseByInvoiceWidget recentPurchaseByInvoiceWidget;
	private PageRecentCommissaryOrdersWidget recentCommissaryOrdersWidget;
	private PageAdjustmentsWidget adjustmentsWidget;
	private PageLocationTransfersWidget locationTransfersWidget;
	private PageSalesForecastWidget salesForecastWidget;
	private PageRecentSalesTransactionsWidget recentSalesTransactionsWidget;
	private PageDailyPrepWidget dailyPrepWidget;
	private PageLaborScheduleWidget laborScheduleWidget;
	private PageLaborSummaryWidget laborSummaryWidget;
	private PageEmployeeInformationWidget employeeInformationWidget;
	private PageReportsWidget reportsWidget;
	private PageTemplatesWidget templatesWidget;
	private PageUploadDownloadFilesWidget uploadDownloadFilesWidget;
	private PageConsolidatedPostingSummaryWidget consolidatedPostingSummaryWidget;

	private static final String WIDGET_NAME = "Quick Links";
	private static final String URL_FOR_CONTENT = "http://www.crunchtime.com";
	private static final String TITLE = "New CrunchTime";
	private static final int WIDGET_NUMBER = 0;
	private static final int WIDGET_NUMBER_FOR_DELETING = 1;
	private static final int FIRST_WIDGET_NUMBER = 0;
	private static final String RECENT_VENDOR_ORDER_WIDGET_NAME = "Recent Vendor Orders";
	private static final String RECENT_PURCHASE_BY_INVOICE_WIDGET_NAME = "Recent Purchase by Invoice";
	private static final String RECENT_COMMISSARY_ORDERS_WIDGET_NAME = "Recent Commissary Orders";
	private static final String PHYSICAL_INVENTORY_WIDGET_NAME = "Physical Inventory";
	private static final String ADJUSTMENTS_WIDGET_NAME = "Adjustments";
	private static final String LOCATION_TRANSFERS_WIDGET_NAME = "Location Transfers";
	private static final String SALES_FORECAST_WIDGET_NAME = "Sales Forecast";
	private static final String RECENT_SALES_TRANSACTIONS_WIDGET_NAME = "Recent Sales Transactions";
	private static final String DAILY_PREP_WIDGET_NAME = "Daily Prep";
	private static final String LABOR_SCHEDULE_WIDGET_NAME = "Labor Schedule";
	private static final String LABOR_SUMMARY_WIDGET_NAME = "Labor Summary";
	private static final String EMPLOYEE_INFORMATION_WIDGET_NAME = "Employee Information";
	private static final String TEMPLATES_WIDGET_NAME = "Templates";
	private static final String UPLOAD_DOWNLOAD_FILES_WIDGET_NAME = "Upload/Download Files";
	private static final String CONSOLIDATED_POSTING_SUMMARY_WIDGET_NAME = "Consolidated Posting Summary";
	private static int recentVendorOrdersWidgetNumber;
	private static int recentPurchaseByInvoiceWidgetNumber;
	private static int recentCommissaryOrdersWidgetNumber;
	private static int physicalInventoryWidgetNumber;
	private static int adjustmentsWidgetNumber;
	private static int locationTransfersWidgetNumber;
	private static int salesForecastWidgetNumber;
	private static int recentSalesTransactionsWidgetNumber;
	private static int dailyPrepWidgetNumber;
	private static int laborScheduleWidgetNumber;
	private static int laborSummaryWidgetNumber;
	private static int employeeInformationWidgetNumber;
	private static int templatesWidgetNumber;
	private static int uploadDownloadFilesWidgetNumber;
	private static int consolidatedPostingSummaryWidgetNumber;
	private static final String VENDOR_VALUE_FOR_RECENT_VENDOR_ORDERS_WIDGET = "food";
	private static final String VENDOR_VALUE_FOR_RECENT_PURCHASE_BY_INVOICE_WIDGET = "general";
	private static final String REFERENCE_NUMBER_VALUE_FOR_RECENT_COMMISSARY_ORDERS_WIDGET = "123";
	private static final String POST_PERIOD_VALUE_FOR_PHYSICAL_INVENTORY_WIDGET = "09/25/2011"; //"04/22/2012";
	private static final String ADJUSTMENT_TYPE_VALUE_FOR_ADJUSTMENTS_WIDGET = "Recipe";
	private static final String TRANSFER_DATE_GT_VALUE_FOR_LOCATION_TRANSFERS_WIDGET = "01/01/2011";
	private static final String DESCRIPTION_VALUE_FOR_DAILY_PREP_WIDGET = "description";
	private static final String FISCAL_YEAR_VALUE_FOR_LABOR_SCHEDULE_WIDGET = "2013";
	private static final String FISCAL_YEAR_VALUE_FOR_LABOR_SUMMARY_WIDGET = "2012";
	private static final String FISCAL_YEAR_VALUE_FOR_SALES_FORECAST_WIDGET = "2012";
	private static final String DISPLAY_BY_VALUE_FOR_LABOR_SCHEDULE_WIDGET = "All";
	private static final String STATUS_VALUE_FOR_EMPLOYEE_INFORMATION_WIDGET = "All";
	private static final String STATUS_VALUE_FOR_SALES_FORECAST_WIDGET = "All";
	private static final String TYPE_VALUE_FOR_TEMPLATES_WIDGET = "Corporate";
	private static final String FILE_NAME_VALUE_FOR_UPLOAD_DOWNLOAD_FILES_WIDGET = "Auto";
	private static final String HIERARCHY_VALUE_FOR_CONSOLIDATED_POSTING_SUMMARY_WIDGET = "Hierarchy 04-2-East";
	private static final String LOCATION_CODE_VALUE_FOR_CONSOLIDATED_POSTING_SUMMARY_WIDGET = "0001";
	private static final String MESSAGE_TEXT_VALUE_FOR_UPLOAD_DOWNLOAD_FILES_WIDGET = "message text";
	private static final String REGISTER_VALUE_FOR_RECENT_SALES_TRANSACTIONS_WIDGET = "321";
	private static final String BIZ_IQ_PASSWORD_VALUE = "anyvalue";

	@BeforeMethod
	public void setUp() throws Exception {
		super.setUp();
		pageBase = PageFactory.initElements(driver, PageBase.class);
		baseWidgetNC = PageFactory.initElements(driver, BaseWidgetNC.class);
		pageDashboard = PageFactory.initElements(driver, PageDashboard.class);
		pageBizIQ = PageFactory.initElements(driver, PageBizIQ.class);
		pagePurchasingOverview = PageFactory.initElements(driver, PagePurchasingOverview.class);
		pageInventoryOverview = PageFactory.initElements(driver, PageInventoryOverview.class);
		pageSalesOverview = PageFactory.initElements(driver, PageSalesOverview.class);
		pageProductionOverview = PageFactory.initElements(driver, PageProductionOverview.class);
		pageLaborOverview = PageFactory.initElements(driver, PageLaborOverview.class);
		pageReportsOverview = PageFactory.initElements(driver, PageReportsOverview.class);
		pageAdministrationOverview = PageFactory.initElements(driver, PageAdministrationOverview.class);
		selectLayoutDialog = PageFactory.initElements(driver, SelectLayoutDialog.class);
		configureWidgetDialog = PageFactory.initElements(driver, ConfigureWidgetDialog.class);
		confirmDialog = PageFactory.initElements(driver, ConfirmDialog.class);
		addRemoveLinksDialog = PageFactory.initElements(driver, AddRemoveLinksDialog.class);
		createQuickLinkDialog = PageFactory.initElements(driver, CreateQuickLinkDialog.class);
		recentVendorOrdersWidget = PageFactory.initElements(driver, PageRecentVendorOrdersWidget.class);
		recentPurchaseByInvoiceWidget = PageFactory.initElements(driver, PageRecentPurchaseByInvoiceWidget.class);
		recentCommissaryOrdersWidget = PageFactory.initElements(driver, PageRecentCommissaryOrdersWidget.class);
		adjustmentsWidget = PageFactory.initElements(driver, PageAdjustmentsWidget.class);
		locationTransfersWidget = PageFactory.initElements(driver, PageLocationTransfersWidget.class);
		salesForecastWidget = PageFactory.initElements(driver, PageSalesForecastWidget.class);
		recentSalesTransactionsWidget = PageFactory.initElements(driver, PageRecentSalesTransactionsWidget.class);
		dailyPrepWidget = PageFactory.initElements(driver, PageDailyPrepWidget.class);
		laborScheduleWidget = PageFactory.initElements(driver, PageLaborScheduleWidget.class);
		laborSummaryWidget = PageFactory.initElements(driver, PageLaborSummaryWidget.class);
		employeeInformationWidget = PageFactory.initElements(driver, PageEmployeeInformationWidget.class);
		reportsWidget = PageFactory.initElements(driver, PageReportsWidget.class);
		templatesWidget = PageFactory.initElements(driver, PageTemplatesWidget.class);
		uploadDownloadFilesWidget = PageFactory.initElements(driver, PageUploadDownloadFilesWidget.class);
		consolidatedPostingSummaryWidget = PageFactory.initElements(driver, PageConsolidatedPostingSummaryWidget.class);
	}

	@Test(groups = "smoke")
	public void dashboardsNavigationSmokeTest() throws Exception {
		basePageNC.bizIqMainMenuLinkClick();
		basePageNC.purchasingMainMenuLinkClick();
		basePageNC.inventoryMainMenuLinkClick();
		basePageNC.salesMainMenuLinkClick();
		basePageNC.productionMainMenuLinkClick();
		basePageNC.laborMainMenuLinkClick();
		basePageNC.reportsMainMenuLinkClick();
		basePageNC.administrationMainMenuLinkClick();
		basePageNC.dashboardMainMenuLinkClick();
	}

	@Test(groups = "smoke")
	public void editLayoutsSmokeTest() throws Exception {
		pageDashboard.corporateLayoutButtonClick();
		pageDashboard.editLayoutButtonClick();
		pageDashboard.addSingleWidgetBox();
		pageDashboard.scrollToBottomWidget();
		baseWidgetNC.configureWidgetGearIconsClick(baseWidgetNC.getConfigureWidgetGearIconsCount()); // * click 'Configure Widget' gear icon for last widget on the page
		configureWidgetDialog.selectWidgetName(WIDGET_NAME);
		configureWidgetDialog.checkAllowReorderingCheckbox();
		configureWidgetDialog.checkAllowRemoveCheckbox();
		configureWidgetDialog.saveButtonClick();
		baseWidgetNC.moveWidgetToTop();
		pageDashboard.saveCorporateLayoutButtonClickAndConfirm();
		baseWidgetNC.widgetAddRemoveLinksIconsClick(WIDGET_NUMBER);
		addRemoveLinksDialog.scrollToBottomDialog();
		addRemoveLinksDialog.newCrunchTimeDeleteLinkClick(); // * preparation step - delete 'New CrunchTime' link if exist
		addRemoveLinksDialog.createCustomLinkIconClick();
		createQuickLinkDialog.typeUrlForContent(URL_FOR_CONTENT);
		createQuickLinkDialog.typeTitle(TITLE);
		createQuickLinkDialog.saveButtonClick();
		addRemoveLinksDialog.checkNewCrunchTimeCheckbox();
		addRemoveLinksDialog.scrollToTopDialog();
		addRemoveLinksDialog.checkPurchasingOverviewCheckbox();
		addRemoveLinksDialog.saveButtonClick();
		pageDashboard.myLayoutButtonClick();
		pageDashboard.editLayoutForMyLayoutButtonClick();
		pageDashboard.restoreCorporateLayoutButtonClick();
		pageDashboard.finishEditingButtonClick();
		pageDashboard.editLayoutForMyLayoutButtonClick();
		baseWidgetNC.widgetMoveDownIconsClick(WIDGET_NUMBER);
		pageDashboard.finishEditingButtonClick();
		pageDashboard.editLayoutForMyLayoutButtonClick();
		baseWidgetNC.widgetRemoveRowIconsClick(WIDGET_NUMBER_FOR_DELETING);
		pageDashboard.finishEditingButtonClick();
		pageDashboard.editLayoutForMyLayoutButtonClick();
		pageDashboard.restoreCorporateLayoutButtonClick();
		pageDashboard.finishEditingButtonClick();
		baseWidgetNC.widgetMaximizeClick(WIDGET_NUMBER);
		baseWidgetNC.widgetMinimizeClick(WIDGET_NUMBER);
		pageDashboard.corporateLayoutButtonClick();
		pageDashboard.editLayoutButtonClick();
		baseWidgetNC.widgetRemoveRowIconsClick(WIDGET_NUMBER);
		pageDashboard.saveCorporateLayoutButtonClickAndConfirm();
	}

	@Test(groups = "smoke")
	public void purchasingWidgetsSmokeTest() throws Exception {
		Waiter.waitFor(visibilityOf(basePageNC.purchasingMainMenuLink));
		basePageNC.purchasingMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pagePurchasingOverview.purchasingPanel));
		recentVendorOrdersWidgetNumber = baseWidgetNC.getWidgetExpandBottomViewIconsNumber(RECENT_VENDOR_ORDER_WIDGET_NAME);
		baseWidgetNC.widgetExpandClick(recentVendorOrdersWidgetNumber);
		recentVendorOrdersWidgetNumber = baseWidgetNC.getWidgetMaxViewIconsNumber(RECENT_VENDOR_ORDER_WIDGET_NAME);
		baseWidgetNC.widgetMaximizeClick(recentVendorOrdersWidgetNumber);
		baseWidgetNC.widgetFiltersIconsClick(FIRST_WIDGET_NUMBER);
		recentVendorOrdersWidget.typeVendor(VENDOR_VALUE_FOR_RECENT_VENDOR_ORDERS_WIDGET);
		recentVendorOrdersWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetFiltersAppliedIconsClick(FIRST_WIDGET_NUMBER);
		recentVendorOrdersWidget.clearButtonClick();
		recentVendorOrdersWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetMinimizeClick(FIRST_WIDGET_NUMBER);
		baseWidgetNC.widgetCollapseClick(recentVendorOrdersWidgetNumber);
		recentPurchaseByInvoiceWidgetNumber = baseWidgetNC.getWidgetExpandBottomViewIconsNumber(RECENT_PURCHASE_BY_INVOICE_WIDGET_NAME);
		baseWidgetNC.widgetExpandClick(recentPurchaseByInvoiceWidgetNumber);
		recentPurchaseByInvoiceWidgetNumber = baseWidgetNC.getWidgetMaxViewIconsNumber(RECENT_PURCHASE_BY_INVOICE_WIDGET_NAME);
		baseWidgetNC.widgetMaximizeClick(recentPurchaseByInvoiceWidgetNumber);
		baseWidgetNC.widgetFiltersIconsClick(FIRST_WIDGET_NUMBER);
		recentPurchaseByInvoiceWidget.typeVendor(VENDOR_VALUE_FOR_RECENT_PURCHASE_BY_INVOICE_WIDGET);
		recentPurchaseByInvoiceWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetFiltersAppliedIconsClick(FIRST_WIDGET_NUMBER);
		recentPurchaseByInvoiceWidget.clearButtonClick();
		recentPurchaseByInvoiceWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetMinimizeClick(FIRST_WIDGET_NUMBER);
		recentPurchaseByInvoiceWidgetNumber = baseWidgetNC.getWidgetExpandTopViewIconsNumber(RECENT_PURCHASE_BY_INVOICE_WIDGET_NAME);
		baseWidgetNC.widgetCollapseClick(recentPurchaseByInvoiceWidgetNumber);
		recentCommissaryOrdersWidgetNumber = baseWidgetNC.getWidgetExpandBottomViewIconsNumber(RECENT_COMMISSARY_ORDERS_WIDGET_NAME);
		baseWidgetNC.widgetExpandClick(recentCommissaryOrdersWidgetNumber);
		recentCommissaryOrdersWidgetNumber = baseWidgetNC.getWidgetMaxViewIconsNumber(RECENT_COMMISSARY_ORDERS_WIDGET_NAME);
		baseWidgetNC.widgetMaximizeClick(recentCommissaryOrdersWidgetNumber);
		baseWidgetNC.widgetFiltersAppliedIconsClick(FIRST_WIDGET_NUMBER);
		recentCommissaryOrdersWidget.typeReferenceNumber(REFERENCE_NUMBER_VALUE_FOR_RECENT_COMMISSARY_ORDERS_WIDGET);
		recentCommissaryOrdersWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetFiltersAppliedIconsClick(FIRST_WIDGET_NUMBER);
		recentCommissaryOrdersWidget.clearButtonClick();
		recentCommissaryOrdersWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetMinimizeClick(FIRST_WIDGET_NUMBER);
		recentCommissaryOrdersWidgetNumber = baseWidgetNC.getWidgetExpandTopViewIconsNumber(RECENT_COMMISSARY_ORDERS_WIDGET_NAME);
		baseWidgetNC.widgetCollapseClick(recentCommissaryOrdersWidgetNumber);
	}

	@Test(groups = "smoke")
	public void inventoryWidgetsSmokeTest() throws Exception {
		Waiter.waitFor(visibilityOf(basePageNC.inventoryMainMenuLink));
		basePageNC.inventoryMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageInventoryOverview.inventoryPanel));
		physicalInventoryWidgetNumber = baseWidgetNC.getWidgetMaxViewIconsNumber(PHYSICAL_INVENTORY_WIDGET_NAME);
		baseWidgetNC.widgetMaximizeClick(physicalInventoryWidgetNumber);
		pageInventoryOverview.selectPostPeriod(POST_PERIOD_VALUE_FOR_PHYSICAL_INVENTORY_WIDGET);
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetMinimizeClick(FIRST_WIDGET_NUMBER);
		baseWidgetNC.scrollToWidget(physicalInventoryWidgetNumber);
		baseWidgetNC.widgetCollapseClick(physicalInventoryWidgetNumber);
		adjustmentsWidgetNumber = baseWidgetNC.getWidgetExpandBottomViewIconsNumber(ADJUSTMENTS_WIDGET_NAME);
		baseWidgetNC.scrollToWidgetByExpandBottomIcon(adjustmentsWidgetNumber);
		baseWidgetNC.widgetExpandClick(adjustmentsWidgetNumber);
		adjustmentsWidgetNumber = baseWidgetNC.getWidgetMaxViewIconsNumber(ADJUSTMENTS_WIDGET_NAME);
		baseWidgetNC.widgetMaximizeClick(adjustmentsWidgetNumber);
		baseWidgetNC.widgetFiltersAppliedIconsClick(FIRST_WIDGET_NUMBER);
		adjustmentsWidget.selectAdjustmentType(ADJUSTMENT_TYPE_VALUE_FOR_ADJUSTMENTS_WIDGET);
		adjustmentsWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetFiltersAppliedIconsClick(FIRST_WIDGET_NUMBER);
		adjustmentsWidget.clearButtonClick();
		adjustmentsWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetMinimizeClick(FIRST_WIDGET_NUMBER);
		adjustmentsWidgetNumber = baseWidgetNC.getWidgetExpandTopViewIconsNumber(ADJUSTMENTS_WIDGET_NAME);
		baseWidgetNC.scrollToWidgetByExpandTopIcon(adjustmentsWidgetNumber);
		baseWidgetNC.widgetCollapseClick(adjustmentsWidgetNumber);
		locationTransfersWidgetNumber = baseWidgetNC.getWidgetExpandBottomViewIconsNumber(LOCATION_TRANSFERS_WIDGET_NAME);
		baseWidgetNC.scrollToWidgetByExpandBottomIcon(locationTransfersWidgetNumber);
		baseWidgetNC.widgetExpandClick(locationTransfersWidgetNumber);
		locationTransfersWidgetNumber = baseWidgetNC.getWidgetMaxViewIconsNumber(LOCATION_TRANSFERS_WIDGET_NAME);
		baseWidgetNC.widgetMaximizeClick(locationTransfersWidgetNumber);
		baseWidgetNC.widgetFiltersAppliedIconsClick(FIRST_WIDGET_NUMBER);
		locationTransfersWidget.typeTransferDateGt(TRANSFER_DATE_GT_VALUE_FOR_LOCATION_TRANSFERS_WIDGET);
		locationTransfersWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetFiltersAppliedIconsClick(FIRST_WIDGET_NUMBER);
		locationTransfersWidget.clearButtonClick();
		locationTransfersWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetMinimizeClick(FIRST_WIDGET_NUMBER);
		locationTransfersWidgetNumber = baseWidgetNC.getWidgetExpandTopViewIconsNumber(LOCATION_TRANSFERS_WIDGET_NAME);
		baseWidgetNC.scrollToWidgetByExpandTopIcon(locationTransfersWidgetNumber);
		baseWidgetNC.widgetCollapseClick(locationTransfersWidgetNumber);
	}

	@Test(groups = "smoke")
	public void salesWidgetsSmokeTest() throws Exception {
		Waiter.waitFor(visibilityOf(basePageNC.salesMainMenuLink));
		basePageNC.salesMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageSalesOverview.salesPanel));
		salesForecastWidgetNumber = baseWidgetNC.getWidgetExpandBottomViewIconsNumber(SALES_FORECAST_WIDGET_NAME);
		baseWidgetNC.scrollToWidget(salesForecastWidgetNumber);
		baseWidgetNC.widgetExpandClick(salesForecastWidgetNumber);
		salesForecastWidgetNumber = baseWidgetNC.getWidgetMaxViewIconsNumber(SALES_FORECAST_WIDGET_NAME);
		baseWidgetNC.widgetMaximizeClick(salesForecastWidgetNumber);
		salesForecastWidget.selectFiscalYear(FISCAL_YEAR_VALUE_FOR_SALES_FORECAST_WIDGET);
		salesForecastWidget.selectStatus(STATUS_VALUE_FOR_SALES_FORECAST_WIDGET);
		salesForecastWidget.checkIncludeCheckCountsCheckbox();
		salesForecastWidget.uncheckIncludeGuestCountsCheckbox();
		salesForecastWidget.uncheckIncludeCheckCountsCheckbox();
		salesForecastWidget.checkIncludeGuestCountsCheckbox();
		baseWidgetNC.widgetMinimizeClick(FIRST_WIDGET_NUMBER);
		salesForecastWidgetNumber = baseWidgetNC.getWidgetExpandTopViewIconsNumber(SALES_FORECAST_WIDGET_NAME);
		baseWidgetNC.scrollToWidget(salesForecastWidgetNumber);
		baseWidgetNC.widgetCollapseClick(salesForecastWidgetNumber);
		recentSalesTransactionsWidgetNumber = baseWidgetNC.getWidgetMaxViewIconsNumber(RECENT_SALES_TRANSACTIONS_WIDGET_NAME);
		baseWidgetNC.widgetMaximizeClick(recentSalesTransactionsWidgetNumber);
		baseWidgetNC.widgetFiltersAppliedIconsClick(FIRST_WIDGET_NUMBER);
		recentSalesTransactionsWidget.typeRegister(REGISTER_VALUE_FOR_RECENT_SALES_TRANSACTIONS_WIDGET);
		recentSalesTransactionsWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetFiltersAppliedIconsClick(FIRST_WIDGET_NUMBER);
		recentSalesTransactionsWidget.clearButtonClick();
		recentSalesTransactionsWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetMinimizeClick(FIRST_WIDGET_NUMBER);
		recentSalesTransactionsWidgetNumber = baseWidgetNC.getWidgetExpandTopViewIconsNumber(RECENT_SALES_TRANSACTIONS_WIDGET_NAME);
		baseWidgetNC.scrollToWidget(recentSalesTransactionsWidgetNumber);
		baseWidgetNC.widgetCollapseClick(recentSalesTransactionsWidgetNumber);
	}

	@Test(groups = "smoke")
	public void productionWidgetsSmokeTest() throws Exception {
		Waiter.waitFor(visibilityOf(basePageNC.productionMainMenuLink));
		basePageNC.productionMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageProductionOverview.productionPanel));
		dailyPrepWidgetNumber = baseWidgetNC.getWidgetMaxViewIconsNumber(DAILY_PREP_WIDGET_NAME);
		baseWidgetNC.widgetMaximizeClick(dailyPrepWidgetNumber);
		baseWidgetNC.widgetFiltersAppliedIconsClick(FIRST_WIDGET_NUMBER);
		dailyPrepWidget.typeDescription(DESCRIPTION_VALUE_FOR_DAILY_PREP_WIDGET);
		dailyPrepWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetFiltersAppliedIconsClick(FIRST_WIDGET_NUMBER);
		dailyPrepWidget.clearButtonClick();
		dailyPrepWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetMinimizeClick(FIRST_WIDGET_NUMBER);
		baseWidgetNC.scrollToWidget(dailyPrepWidgetNumber);
		baseWidgetNC.widgetCollapseClick(dailyPrepWidgetNumber);
	}

	@Test(groups = "smoke")
	public void laborWidgetsSmokeTest() throws Exception {
		Waiter.waitFor(visibilityOf(basePageNC.laborMainMenuLink));
		basePageNC.laborMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageLaborOverview.laborPanel));
		laborScheduleWidgetNumber = baseWidgetNC.getWidgetExpandBottomViewIconsNumber(LABOR_SCHEDULE_WIDGET_NAME);
		baseWidgetNC.scrollToWidget(laborScheduleWidgetNumber);
		baseWidgetNC.widgetExpandClick(laborScheduleWidgetNumber);
		laborScheduleWidgetNumber = baseWidgetNC.getWidgetMaxViewIconsNumber(LABOR_SCHEDULE_WIDGET_NAME);
		baseWidgetNC.widgetMaximizeClick(laborScheduleWidgetNumber);
		laborScheduleWidget.selectFiscalYear(FISCAL_YEAR_VALUE_FOR_LABOR_SCHEDULE_WIDGET);
		baseWidgetNC.widgetFiltersAppliedIconsClick(FIRST_WIDGET_NUMBER);
		laborScheduleWidget.selectDisplayBy(DISPLAY_BY_VALUE_FOR_LABOR_SCHEDULE_WIDGET);
		laborScheduleWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetFiltersAppliedIconsClick(FIRST_WIDGET_NUMBER);
		laborScheduleWidget.clearButtonClick();
		laborScheduleWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetMinimizeClick(FIRST_WIDGET_NUMBER);
		laborScheduleWidgetNumber = baseWidgetNC.getWidgetExpandTopViewIconsNumber(LABOR_SCHEDULE_WIDGET_NAME);
		baseWidgetNC.scrollToWidget(laborScheduleWidgetNumber);
		baseWidgetNC.widgetCollapseClick(laborScheduleWidgetNumber);
		laborSummaryWidgetNumber = baseWidgetNC.getWidgetExpandBottomViewIconsNumber(LABOR_SUMMARY_WIDGET_NAME);
		baseWidgetNC.scrollToWidget(laborSummaryWidgetNumber);
		baseWidgetNC.widgetExpandClick(laborSummaryWidgetNumber);
		laborSummaryWidgetNumber = baseWidgetNC.getWidgetMaxViewIconsNumber(LABOR_SUMMARY_WIDGET_NAME);
		baseWidgetNC.widgetMaximizeClick(laborSummaryWidgetNumber);
		laborSummaryWidget.selectFiscalYear(FISCAL_YEAR_VALUE_FOR_LABOR_SUMMARY_WIDGET);
		baseWidgetNC.widgetFiltersAppliedIconsClick(FIRST_WIDGET_NUMBER);
		laborSummaryWidget.selectDisplayBy(DISPLAY_BY_VALUE_FOR_LABOR_SCHEDULE_WIDGET);
		laborSummaryWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetFiltersAppliedIconsClick(FIRST_WIDGET_NUMBER);
		laborSummaryWidget.clearButtonClick();
		laborSummaryWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetMinimizeClick(FIRST_WIDGET_NUMBER);
		laborSummaryWidgetNumber = baseWidgetNC.getWidgetExpandTopViewIconsNumber(LABOR_SUMMARY_WIDGET_NAME);
		baseWidgetNC.scrollToWidget(laborSummaryWidgetNumber);
		baseWidgetNC.widgetCollapseClick(laborSummaryWidgetNumber);
		employeeInformationWidgetNumber = baseWidgetNC.getWidgetExpandBottomViewIconsNumber(EMPLOYEE_INFORMATION_WIDGET_NAME);
		baseWidgetNC.scrollToWidget(employeeInformationWidgetNumber);
		baseWidgetNC.widgetExpandClick(employeeInformationWidgetNumber);
		employeeInformationWidgetNumber = baseWidgetNC.getWidgetMaxViewIconsNumber(EMPLOYEE_INFORMATION_WIDGET_NAME);
		baseWidgetNC.widgetMaximizeClick(employeeInformationWidgetNumber);
		baseWidgetNC.widgetFiltersAppliedIconsClick(FIRST_WIDGET_NUMBER);
		employeeInformationWidget.selectStatus(STATUS_VALUE_FOR_EMPLOYEE_INFORMATION_WIDGET);
		employeeInformationWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetFiltersAppliedIconsClick(FIRST_WIDGET_NUMBER);
		employeeInformationWidget.clearButtonClick();
		employeeInformationWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetMinimizeClick(FIRST_WIDGET_NUMBER);
		employeeInformationWidgetNumber = baseWidgetNC.getWidgetExpandTopViewIconsNumber(EMPLOYEE_INFORMATION_WIDGET_NAME);
		baseWidgetNC.scrollToWidget(employeeInformationWidgetNumber);
		baseWidgetNC.widgetCollapseClick(employeeInformationWidgetNumber);
	}

	@Test(groups = "smoke")
	public void reportsWidgetsSmokeTest() throws Exception {
		Waiter.waitFor(visibilityOf(basePageNC.reportsMainMenuLink));
		basePageNC.reportsMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageReportsOverview.reportsPanel));
		reportsWidget.expandAllClick();
		reportsWidget.collapseAllClick();
		baseWidgetNC.widgetExpandClick(FIRST_WIDGET_NUMBER);
		reportsWidget.expandAllClick();
		baseWidgetNC.widgetCollapseClick(FIRST_WIDGET_NUMBER);
		reportsWidget.collapseAllClick();
	}

	@Test(groups = "smoke")
	public void administrationWidgetsSmokeTest() throws Exception {
		Waiter.waitFor(visibilityOf(basePageNC.administrationMainMenuLink));
		basePageNC.administrationMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageAdministrationOverview.administrationPanel));
		templatesWidgetNumber = baseWidgetNC.getWidgetExpandBottomViewIconsNumber(TEMPLATES_WIDGET_NAME);
		baseWidgetNC.scrollToWidget(templatesWidgetNumber);
		baseWidgetNC.widgetExpandClick(templatesWidgetNumber);
		templatesWidgetNumber = baseWidgetNC.getWidgetMaxViewIconsNumber(TEMPLATES_WIDGET_NAME);
		baseWidgetNC.widgetMaximizeClick(templatesWidgetNumber);
		baseWidgetNC.widgetFiltersIconsClick(FIRST_WIDGET_NUMBER);
		templatesWidget.selectType(TYPE_VALUE_FOR_TEMPLATES_WIDGET);
		templatesWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetFiltersAppliedIconsClick(FIRST_WIDGET_NUMBER);
		templatesWidget.clearButtonClick();
		templatesWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetMinimizeClick(FIRST_WIDGET_NUMBER);
		templatesWidgetNumber = baseWidgetNC.getWidgetExpandTopViewIconsNumber(TEMPLATES_WIDGET_NAME);
		baseWidgetNC.scrollToWidget(templatesWidgetNumber);
		baseWidgetNC.widgetCollapseClick(templatesWidgetNumber);
		uploadDownloadFilesWidgetNumber = baseWidgetNC.getWidgetExpandBottomViewIconsNumber(UPLOAD_DOWNLOAD_FILES_WIDGET_NAME);
		baseWidgetNC.scrollToWidget(laborSummaryWidgetNumber);
		baseWidgetNC.widgetExpandClick(uploadDownloadFilesWidgetNumber);
		uploadDownloadFilesWidgetNumber = baseWidgetNC.getWidgetMaxViewIconsNumber(UPLOAD_DOWNLOAD_FILES_WIDGET_NAME);
		baseWidgetNC.widgetMaximizeClick(uploadDownloadFilesWidgetNumber);
		baseWidgetNC.widgetFiltersIconsClick(FIRST_WIDGET_NUMBER);
		uploadDownloadFilesWidget.typeFileName(FILE_NAME_VALUE_FOR_UPLOAD_DOWNLOAD_FILES_WIDGET);
		uploadDownloadFilesWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetFiltersAppliedIconsClick(FIRST_WIDGET_NUMBER);
		uploadDownloadFilesWidget.clearButtonClick();
		uploadDownloadFilesWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		uploadDownloadFilesWidget.fileDecriptionClick(FIRST_WIDGET_NUMBER);
		uploadDownloadFilesWidget.typeMessageText(MESSAGE_TEXT_VALUE_FOR_UPLOAD_DOWNLOAD_FILES_WIDGET);
		uploadDownloadFilesWidget.saveButtonClick();
		baseWidgetNC.widgetMinimizeClick(FIRST_WIDGET_NUMBER);
		uploadDownloadFilesWidgetNumber = baseWidgetNC.getWidgetExpandTopViewIconsNumber(UPLOAD_DOWNLOAD_FILES_WIDGET_NAME);
		baseWidgetNC.scrollToWidget(uploadDownloadFilesWidgetNumber);
		baseWidgetNC.widgetCollapseClick(uploadDownloadFilesWidgetNumber);
		consolidatedPostingSummaryWidgetNumber = baseWidgetNC.getWidgetExpandBottomViewIconsNumber(CONSOLIDATED_POSTING_SUMMARY_WIDGET_NAME);
		baseWidgetNC.scrollToWidget(consolidatedPostingSummaryWidgetNumber);
		baseWidgetNC.widgetExpandClick(consolidatedPostingSummaryWidgetNumber);
		consolidatedPostingSummaryWidgetNumber = baseWidgetNC.getWidgetMaxViewIconsNumber(CONSOLIDATED_POSTING_SUMMARY_WIDGET_NAME);
		baseWidgetNC.widgetMaximizeClick(consolidatedPostingSummaryWidgetNumber);
		consolidatedPostingSummaryWidget.selectHierarchy(HIERARCHY_VALUE_FOR_CONSOLIDATED_POSTING_SUMMARY_WIDGET);
		baseWidgetNC.widgetFiltersIconsClick(FIRST_WIDGET_NUMBER);
		consolidatedPostingSummaryWidget.typeLocationCode(LOCATION_CODE_VALUE_FOR_CONSOLIDATED_POSTING_SUMMARY_WIDGET);
		consolidatedPostingSummaryWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetFiltersAppliedIconsClick(FIRST_WIDGET_NUMBER);
		consolidatedPostingSummaryWidget.clearButtonClick();
		consolidatedPostingSummaryWidget.applyButtonClick();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		baseWidgetNC.widgetMinimizeClick(FIRST_WIDGET_NUMBER);
		consolidatedPostingSummaryWidgetNumber = baseWidgetNC.getWidgetExpandTopViewIconsNumber(CONSOLIDATED_POSTING_SUMMARY_WIDGET_NAME);
		baseWidgetNC.scrollToWidget(consolidatedPostingSummaryWidgetNumber);
		baseWidgetNC.widgetCollapseClick(consolidatedPostingSummaryWidgetNumber);
	}

	@Test(groups = "smoke")
	public void bizIQPasswordSmokeTest() throws Exception {
		basePageNC.enterBizIQPasswordMenuLink();
		basePageNC.typeBizIQPassword(BIZ_IQ_PASSWORD_VALUE);
		basePageNC.saveBizIQPasswordClick();
		basePageNC.logoutClick();
	}

	@AfterMethod
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}
}