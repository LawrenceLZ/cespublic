package com.crunchtime.ces.test.inventory.createReviewInventories;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.helper.ActionsNC;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.*;

public class TC_CreateReviewInvAuditScrn extends BaseTestNC {
	private static final String DB_CONNECTION_STRING = "AUTO/AUTO@CTAUTO24006.WORLD";
	private static final String TEST_SITE_NAME = "auto.net-chef.local";
	private static final String USER_ID = "INVEN";
	private static final String USER_PWD = "INVEN";
	private static final String TEST_LOCATION_NAME = "Chicago #332";

	@Parameters({"SMOKE"})
	@BeforeMethod(alwaysRun = true)
	public void logIn(@Optional(TEST_SITE_NAME) String siteName) throws Exception {
		basePageNC.openNCTestSite(siteName);
		pageNCLogin.loginNC(USER_ID, USER_PWD, TEST_LOCATION_NAME);
		basePageNC.navigateToCreateReviewInvSummScreen();
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}

	@Test
	public void testCreatePhyInvAuditRecords() throws Exception{
		ActionsNC.setTestMethodDescription("ces-7231", "Make a change to qty change to make sure audit record is created.");
		pageCreateReviewInventories.processLinkForFinalForPost("Audit").click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		pageCreateReviewInventories.phyInvDtlAuditScrnFilterIcon.click();
		Waiter.waitForElementToBeVisible(driver, pageCreateReviewInventories.phyInvDtlAuditScrnFilterPopup);
		String todayDateStr = ActionsNC.setDateBasedOnToday("MM/dd/yyyy", 0);
		pageCreateReviewInventories.phyInvDtlAuditScrnFilterAuditDateGreaterInputCell.click();
		WebElement inputElement = driver.switchTo().activeElement();
		inputElement.clear();
		inputElement.sendKeys(todayDateStr);
		pageCreateReviewInventories.phyInvFilterPopupCmdBtn("Apply").click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		int auditScrnRowCntBeforeEdit = ActionsNC.getGridRowCnt(pageCreateReviewInventories.phyInvDtlAuditScrnGrid);
		pageCreateReviewInventories.phyInvDtlScrnHdrCmdBtn("Close").click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		pageCreateReviewInventories.processLinkForFinalForPost("Review").click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		try {
			boolean overdueTasksPopupExists = ActionsNC.isElementPresent(pageCreateReviewInventories.overdueInvTransPopupGrid);
			if (overdueTasksPopupExists){
				pageCreateReviewInventories.overdueInvTransPopupCmdBtn("Continue").click();
			}
		} catch (Exception e){
			e.printStackTrace();
		}
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		pageCreateReviewInventories.phyInvDtlScrnHdrCmdBtn("Edit").click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		//comment out the steps for clicking expand/callapse icon as the first row (first group) is always expanded
//		String iconStatus = pageCreateReviewInventories.phyInvDtlScrnCollapseExpandIcon.getAttribute("class");
//		if (iconStatus.contains("collapsed")){
//			pageCreateReviewInventories.phyInvDtlScrnCollapseExpandIcon.click();
//			Waiter.waitForElementToBeVisible(driver, pageCreateReviewInventories.phyInvDtlScrnPrimaryCountCells.get(0));
//		}
		//For ease of testing, only edit the first row's Primary Count
		String valueBeforeEdit = pageCreateReviewInventories.phyInvDtlScrnPrimaryCountCells.get(0).getText();
		String editVal = Double.toString(Double.parseDouble(valueBeforeEdit.replaceAll(",", ""))+1);
		pageCreateReviewInventories.phyInvDtlScrnPrimaryCountCells.get(0).click();
		WebElement inputCell = driver.switchTo().activeElement();
		inputCell.clear();
		inputCell.sendKeys(editVal + Keys.ENTER);
		pageCreateReviewInventories.phyInvDtlScrnHdrCmdBtn("Save and Review").click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		try {
			boolean overdueTasksPopupExists = ActionsNC.isElementPresent(pageCreateReviewInventories.overdueInvTransPopupGrid);
			if (overdueTasksPopupExists){
				pageCreateReviewInventories.overdueInvTransPopupCmdBtn("Continue").click();
			}
		} catch (Exception e){
			e.printStackTrace();
		}
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		pageCreateReviewInventories.phyInvDtlScrnHdrCmdBtn("Save and Close").click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		pageCreateReviewInventories.processLinkForFinalForPost("Audit").click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		pageCreateReviewInventories.phyInvDtlAuditScrnFilterIcon.click();
		Waiter.waitForElementToBeVisible(driver, pageCreateReviewInventories.phyInvDtlAuditScrnFilterPopup);
		pageCreateReviewInventories.phyInvDtlAuditScrnFilterAuditDateGreaterInputCell.click();
		WebElement newInputElement = driver.switchTo().activeElement();
		newInputElement.clear();
		newInputElement.sendKeys(todayDateStr);
		pageCreateReviewInventories.phyInvFilterPopupCmdBtn("Apply").click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		int auditScrnRowCntAfterEdit = ActionsNC.getGridRowCnt(pageCreateReviewInventories.phyInvDtlAuditScrnGrid);
		Assert.assertTrue((auditScrnRowCntAfterEdit - auditScrnRowCntBeforeEdit) == 1, "Failed, as the one new edited record NOT audited.");
	}
}