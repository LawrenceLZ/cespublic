package com.crunchtime.ces.test.inventory.createReviewInventories;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.database.generic.DBStmtNCGeneric;
import com.crunchtime.ces.helper.ActionsNC;
import com.crunchtime.ces.helper.DBMethodNC;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.*;

public class TC_CreateReviewInventories extends BaseTestNC {

	private static final String DB_CONNECTION_STRING = "AUTO/AUTO@CTAUTO24006.WORLD";
	private static final String TEST_SITE_NAME = "auto.net-chef.local";
	private static final String USER_ID = "INVEN";
	private static final String USER_PWD = "INVEN";
	private static final String TEST_LOCATION_NAME = "Chicago #332";

	@Parameters({"SMOKE"})
	@BeforeMethod(alwaysRun = true)
	public void logIn(@Optional(TEST_SITE_NAME) String siteName) throws Exception {
		basePageNC.openNCTestSite(siteName);
		pageNCLogin.loginNC(USER_ID, USER_PWD, TEST_LOCATION_NAME);
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}

	@Test(groups = {"userGroupAccess"})
	public void userGroupAccessForCreateReviewInventories() throws Exception {
		ActionsNC.setTestMethodDescription("ces-5690", "Verify 'Create/Review Inventories' menu link based on user group control.");
		DBStmtNCGeneric.updateUserGroupAccess(DB_CONNECTION_STRING, "N", USER_ID, "NC-Inventory", "Physical Inventory");
		pageNCLogout.logoutNC();
		pageNCLogin.loginNC(USER_ID, USER_PWD, TEST_LOCATION_NAME);
		pageNavigationGeneric.menuLinkMouseOver("Inventory", "");
		//check if it is existed
		boolean existFlag = ActionsNC.isElementPresent(basePageNC.createReviewInventoriesMenuLink);
		if (existFlag) {
			DBStmtNCGeneric.updateUserGroupAccess(DB_CONNECTION_STRING, "Y", USER_ID, "NC-Inventory", "Physical Inventory");
			Assert.assertFalse(existFlag, "Create/Review Inventories Link Should be DISABLED if control is OFF, so test failed.");
		} else {
			DBStmtNCGeneric.updateUserGroupAccess(DB_CONNECTION_STRING, "Y", USER_ID, "NC-Inventory", "Physical Inventory");
			pageNCLogout.logoutNC();
			pageNCLogin.loginNC(USER_ID, USER_PWD, TEST_LOCATION_NAME);
			pageNavigationGeneric.menuLinkMouseOver("Inventory", "");
			boolean existFlagUpdated = ActionsNC.isElementPresent(basePageNC.createReviewInventoriesMenuLink);
			Assert.assertTrue(existFlagUpdated, "Create/Review Inventories Link Should be ENABLED if control is ON, so test failed.");
		}
	}

	/**
	 * ces-6365
	 */
	@Test(groups = {"userGroupAccess"})
	public void userGroupAccessForPhyInvAuditLink() throws Exception {
		ActionsNC.setTestMethodDescription("ces-5472", "Verify 'Audit' link based on user group control.");
		DBMethodNC.executeSqlStmt(DB_CONNECTION_STRING, DBStmtNCGeneric.getStmtUpdateUserGroupAccess("N", USER_ID, "NC-Inventory", "Audit"));
		pageNCLogout.logoutNC();
		pageNCLogin.loginNC(USER_ID, USER_PWD, TEST_LOCATION_NAME);
		basePageNC.navigateToCreateReviewInvSummScreen();
		boolean reviewLink = ActionsNC.isElementPresent(pageCreateReviewInventories.processLinkForFinalForPost("Review"));
		if (!reviewLink){
			pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBox.click();
			Waiter.waitForElementToBeVisible(driver, pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBoxList);
			pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBoxList.findElement(By.cssSelector("[class*='x-boundlist-item']:first-child")).click();
			pageCreateReviewInventories.waitForSpinnerToDisappear();
		}
		boolean auditLink = false;
		try {
			pageCreateReviewInventories.processLinkForFinalForPost("Audit").click();
			auditLink = true;
		} catch (NoSuchElementException e){
//			System.out.println(e.getMessage());
		}
		if (auditLink){
			DBMethodNC.executeSqlStmt(DB_CONNECTION_STRING, DBStmtNCGeneric.getStmtUpdateUserGroupAccess("Y", USER_ID, "NC-Inventory", "Audit"));
			Assert.assertFalse(auditLink, "Failed, as Audit link should be DISABLED if control is OFF");
		} else {
			DBMethodNC.executeSqlStmt(DB_CONNECTION_STRING, DBStmtNCGeneric.getStmtUpdateUserGroupAccess("Y", USER_ID, "NC-Inventory", "Audit"));
			pageNCLogout.logoutNC();
			pageNCLogin.loginNC(USER_ID, USER_PWD, TEST_LOCATION_NAME);
			basePageNC.navigateToCreateReviewInvSummScreen();
			boolean auditLinkUpdated = ActionsNC.isElementPresent(pageCreateReviewInventories.processLinkForFinalForPost("Audit"));
			Assert.assertTrue(auditLinkUpdated, "Failed, as Audit link should be ENABLED if control is ON.");
		}

	}
	
}
