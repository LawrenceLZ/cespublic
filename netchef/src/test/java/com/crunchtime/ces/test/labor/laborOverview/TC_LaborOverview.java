package com.crunchtime.ces.test.labor.laborOverview;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.database.generic.DBStmtNCGeneric;
import com.crunchtime.ces.helper.ActionsNC;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TC_LaborOverview extends BaseTestNC {
	String dbConnectionString = "autorpt/autorpt@CTAUTO24006";
	String testSiteName = "autorpt.net-chef.local";
	String userID = "autorpt";
	String userPWD = "autorpt";
	String testLocationName = "#CCI_REPORT#";

	@BeforeMethod(alwaysRun = true)
	public void logIn() throws Exception {
		basePageNC.openNCTestSite(testSiteName);
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}

	@Test(groups = {"userGroupAccess"})
	public void userGroupAccessForLaborOverview() throws Exception {
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Other Store Tasks", "Review/Edit Labor Information");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Labor Schedule", "New Labor Schedule");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Administrative Options", "Labor Schedule");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Emp. Maint. Summary Screen", "Add Employees");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Administrative Options", "Employee Maintenance");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Administrative Options", "Staffing Level Templates");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Stations", "View");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Stations", "Add/Delete/Save");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Positions", "View");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Positions", "Add/Delete/Save");
		//user group access for each report under Production page module
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-View Reports", "Employee Breaks");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-View Reports", "Employee Changes");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-View Reports", "Employee Contact Information");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-View Reports", "Employee Payroll Information Summary");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Consolidated Reports", "Consolidated Employee Breaks");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Consolidated Reports", "Consolidated Employee Time Detail");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Consolidated Reports", "Consolidated Labor Productivity");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Consolidated Reports", "Consolidated Payroll Control");

		pageNCLogout.logoutNC();
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
		//pageNavigationGeneric.menuLinkMouseOver("Inventory", "");
		//check if it is existed
		boolean existFlag = ActionsNC.isElementPresent(pageNavigationGeneric.laborMenuLink);
		if (existFlag) {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Other Store Tasks", "Review/Edit Labor Information");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Labor Schedule", "New Labor Schedule");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Administrative Options", "Labor Schedule");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Emp. Maint. Summary Screen", "Add Employees");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Administrative Options", "Employee Maintenance");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Administrative Options", "Staffing Level Templates");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Stations", "View");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Stations", "Add/Delete/Save");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Positions", "View");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Positions", "Add/Delete/Save");
			//user group access for each report under Production page module
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Employee Breaks");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Employee Changes");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Employee Contact Information");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Employee Payroll Information Summary");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Consolidated Reports", "Consolidated Employee Breaks");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Consolidated Reports", "Consolidated Employee Time Detail");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Consolidated Reports", "Consolidated Labor Productivity");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Consolidated Reports", "Consolidated Payroll Control");

			Assert.assertFalse(existFlag, "Labor menu Link Should be DISABLED if control is OFF, so test failed.");
		} else {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Other Store Tasks", "Review/Edit Labor Information");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Labor Schedule", "New Labor Schedule");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Administrative Options", "Labor Schedule");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Emp. Maint. Summary Screen", "Add Employees");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Administrative Options", "Employee Maintenance");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Administrative Options", "Staffing Level Templates");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Stations", "View");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Stations", "Add/Delete/Save");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Positions", "View");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Positions", "Add/Delete/Save");
			//user group access for each report under Production page module
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Employee Breaks");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Employee Changes");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Employee Contact Information");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Employee Payroll Information Summary");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Consolidated Reports", "Consolidated Employee Breaks");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Consolidated Reports", "Consolidated Employee Time Detail");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Consolidated Reports", "Consolidated Labor Productivity");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Consolidated Reports", "Consolidated Payroll Control");

			pageNCLogout.logoutNC();
			pageNCLogin.loginNC(userID, userPWD, testLocationName);
			//pageNavigationGeneric.menuLinkMouseOver("Inventory", "");
			boolean existFlagUpdated = ActionsNC.isElementPresent(pageNavigationGeneric.laborMenuLink);
			Assert.assertTrue(existFlagUpdated, "Labor menu Link Should be ENABLED if control is ON, so test failed.");
		}
	}
}
