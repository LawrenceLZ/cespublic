package com.crunchtime.ces.test.physicalInventory;

import com.crunchtime.ces.base.PageBase;

import com.crunchtime.ces.base.SmokeTestBase;
import com.crunchtime.ces.dialogs.ExportDialog;
import com.crunchtime.ces.helper.Waiter;
import com.crunchtime.ces.pages.inventory.inventoryOverview.PageInventoryOverview;
import com.crunchtime.ces.pages.inventory.inventoryOverview.physicalInventory.DataGrid;
import com.crunchtime.ces.pages.inventory.inventoryOverview.physicalInventory.ReviewPage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;
import static org.testng.Assert.*;

public class PhysicalInventoryReviewScreenTest extends SmokeTestBase {

	protected ReviewPage reviewPage;
	protected PageInventoryOverview pageNCInventoryOverview;
    protected DataGrid dataGrid;
    protected ExportDialog exportDialog;

	private static String inputValueForEdit = "9.87";

	@BeforeMethod
	public void setUp() throws Exception {
		super.setUp();
		this.pageNCInventoryOverview = PageFactory.initElements(driver, PageInventoryOverview.class);
		this.reviewPage = PageFactory.initElements(driver, ReviewPage.class);
        this.dataGrid = PageFactory.initElements(driver, DataGrid.class);
        this.exportDialog = PageFactory.initElements(driver, ExportDialog.class);
	}

	@Test(groups = "NCPhysicalInventory")
	public void checkEditButtonTest() throws Exception {
		basePageNC.inventoryMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.reviewProcessLink));
		pageNCInventoryOverview.reviewProcessLink.click();
		Waiter.waitFor(visibilityOf(reviewPage.editLink));
		assertTrue(reviewPage.editLink.isDisplayed(), "Edit button is not displayed, so test failed.");
	}

	@Test(groups = "NCPhysicalInventory")
	public void checkEditOptionTest() throws Exception {
		basePageNC.inventoryMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.reviewProcessLink));
		pageNCInventoryOverview.reviewProcessLink.click();
		Waiter.waitFor(visibilityOf(reviewPage.editLink));
		reviewPage.editLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.editCell));
		pageNCInventoryOverview.editCell2.click(); // for correct work of the test
		pageNCInventoryOverview.editCell.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.inputCell));
//        pageNCInventoryOverview.inputCell.clear();
		pageNCInventoryOverview.inputCell.sendKeys(Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE);
		pageNCInventoryOverview.inputCell.sendKeys(inputValueForEdit, Keys.ENTER);
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.editCell));
		assertEquals(pageNCInventoryOverview.editCell.getText(), inputValueForEdit, "Entered data is not displayed correctly, so test failed.");
	}

	@Test(groups = "NCPhysicalInventory")
	public void checkSaveAndReviewButtonTest() throws Exception {
		basePageNC.inventoryMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.reviewProcessLink));
		pageNCInventoryOverview.reviewProcessLink.click();
		Waiter.waitFor(visibilityOf(reviewPage.editLink));
		reviewPage.editLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.editCell));
		String textBeforeEditing = pageNCInventoryOverview.editCell.getText();
		pageNCInventoryOverview.editCell2.click(); // for correct work of the test
		pageNCInventoryOverview.editCell.click();
//        pageNCInventoryOverview.inputCell.clear();
		pageNCInventoryOverview.inputCell.sendKeys(Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE);
		pageNCInventoryOverview.inputCell.sendKeys(inputValueForEdit, Keys.ENTER);
		reviewPage.saveAndReviewLink.click();
		assertTrue(reviewPage.quantityTab.isDisplayed(), "Review screen is not displayed, so test failed.");
		assertTrue(reviewPage.valueTab.isDisplayed(), "Review screen is not displayed, so test failed.");
		assertTrue(reviewPage.analysisTab.isDisplayed(), "Review screen is not displayed, so test failed.");
		Waiter.waitFor(visibilityOf(reviewPage.editLink));
		reviewPage.editLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.editCell));
		pageNCInventoryOverview.editCell2.click(); // for correct work of the test
		pageNCInventoryOverview.editCell.click();
//        pageNCInventoryOverview.editCell.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.inputCell));
//        pageNCInventoryOverview.inputCell.clear();
		pageNCInventoryOverview.inputCell.sendKeys(Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE);
		pageNCInventoryOverview.inputCell.sendKeys(textBeforeEditing, Keys.ENTER);
		reviewPage.saveAndReviewLink.click();
		Waiter.waitFor(visibilityOf(reviewPage.editLink));
	}

	@Test(groups = "NCPhysicalInventory")
	public void checkSaveAndReviewDataTest() throws Exception {
		basePageNC.inventoryMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.reviewProcessLink));
		pageNCInventoryOverview.reviewProcessLink.click();
		Waiter.waitFor(visibilityOf(reviewPage.editLink));
		reviewPage.editLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.editCell));
		String textBeforeEditing = pageNCInventoryOverview.editCell.getText();
		pageNCInventoryOverview.editCell2.click(); // for correct work of the test
		pageNCInventoryOverview.editCell.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.inputCell));
//        pageNCInventoryOverview.inputCell.clear();
		pageNCInventoryOverview.inputCell.sendKeys(Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE);
		pageNCInventoryOverview.inputCell.sendKeys(inputValueForEdit, Keys.ENTER);
		reviewPage.saveAndReviewLink.click();
		Waiter.waitFor(visibilityOf(reviewPage.editLink));
		reviewPage.editLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.editCell));
		assertEquals(pageNCInventoryOverview.editCell.getText(), inputValueForEdit, "Entered data was not saved correctly, so test failed.");
		pageNCInventoryOverview.editCell2.click(); // for correct work of the test
		pageNCInventoryOverview.editCell.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.inputCell));
//        pageNCInventoryOverview.inputCell.clear();
		pageNCInventoryOverview.inputCell.sendKeys(Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE);
		pageNCInventoryOverview.inputCell.sendKeys(textBeforeEditing, Keys.ENTER);
		reviewPage.saveAndReviewLink.click();
		Waiter.waitFor(visibilityOf(reviewPage.editLink));
	}

	@Test(groups = "NCPhysicalInventory")
	public void checkSaveAndCloseButtonDisplayedTest() throws Exception {
		basePageNC.inventoryMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.reviewProcessLink));
		pageNCInventoryOverview.reviewProcessLink.click();
//      "Save and Close" button should not be displayed on Review tab in future. Test will be changed.
		assertTrue(reviewPage.saveAndCloseLink.isDisplayed(), "Save and Close button is not presents, test failed.");
	}

	//DRAFT checkSaveAndCloseDataSavedTest
	@Test(groups = "NCPhysicalInventory")
	public void checkSaveAndCloseDataSavedTest() throws Exception {
		basePageNC.inventoryMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.reviewProcessLink));
		pageNCInventoryOverview.reviewProcessLink.click();
		Waiter.waitFor(visibilityOf(reviewPage.editLink));
		reviewPage.editLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.editCell));
		String textBeforeEditing = pageNCInventoryOverview.editCell.getText();
		pageNCInventoryOverview.editCell2.click(); // for correct work of the test
		pageNCInventoryOverview.editCell.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.inputCell));
		pageNCInventoryOverview.inputCell.sendKeys(Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE);
		pageNCInventoryOverview.inputCell.sendKeys(inputValueForEdit, Keys.ENTER);
		reviewPage.saveAndCloseLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.reviewProcessLink));
		pageNCInventoryOverview.reviewProcessLink.click();
		Waiter.waitFor(visibilityOf(reviewPage.editLink));
		reviewPage.editLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.editCell));
		assertEquals(pageNCInventoryOverview.editCell.getText(), inputValueForEdit, "Entered data was not saved correctly, so test failed.");
		pageNCInventoryOverview.editCell2.click();
		pageNCInventoryOverview.editCell.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.inputCell));
		pageNCInventoryOverview.inputCell.sendKeys(Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE);
		pageNCInventoryOverview.inputCell.sendKeys(textBeforeEditing, Keys.ENTER);
		reviewPage.saveAndReviewLink.click();
		Waiter.waitFor(visibilityOf(reviewPage.editLink));
	}

	@Test(groups = "NCPhysicalInventory")
	public void checkReviewAndCloseTest() throws Exception {
		basePageNC.inventoryMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.reviewProcessLink));
		pageNCInventoryOverview.reviewProcessLink.click();
		Waiter.waitFor(visibilityOf(reviewPage.closeButton));
		reviewPage.closeButton.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.headerPhysicalInventory));
		assertTrue(pageNCInventoryOverview.headerPhysicalInventory.isDisplayed(), "Physical Inventory table is not displayed, so test failed.");
	}

	@Test(groups = "NCPhysicalInventory")
	public void checkDefaultValueOfGroupByTest() throws Exception {
		basePageNC.inventoryMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.reviewProcessLink));
		pageNCInventoryOverview.reviewProcessLink.click();
		Waiter.waitFor(visibilityOf(reviewPage.groupByCombobox));
		assertEquals(reviewPage.groupByCombobox.getAttribute("value"), "Storage Location", "'Group by' combobox default value is not equal 'Storage Location'");
	}

    @Test(groups = "NCPhysicalInventory")
    public void checkExportOnReviewQuantityTabTest() throws Exception {
        basePageNC.inventoryMainMenuLink.click();
        Waiter.waitFor(visibilityOf(pageNCInventoryOverview.reviewProcessLink));
        pageNCInventoryOverview.reviewProcessLink.click();
        Waiter.waitFor(visibilityOf(reviewPage.quantityTab));
        reviewPage.quantityTab.click();
        Waiter.waitFor(visibilityOf(reviewPage.exportIcon));
        reviewPage.exportIcon.click();
        Waiter.waitFor(visibilityOf(exportDialog.exportDialogHeader));
        assertTrue(exportDialog.exportDialogHeader.isDisplayed(), "Export dialog box is not displayed, so test failed.");
    }

    @Test(groups = "NCPhysicalInventory")
    public void checkExportOnReviewValueTabTest() throws Exception {
        basePageNC.inventoryMainMenuLink.click();
        Waiter.waitFor(visibilityOf(pageNCInventoryOverview.reviewProcessLink));
        pageNCInventoryOverview.reviewProcessLink.click();
        Waiter.waitFor(visibilityOf(reviewPage.valueTab));
        reviewPage.valueTab.click();
        Waiter.waitFor(visibilityOf(reviewPage.exportIcon));
        reviewPage.exportIcon.click();
        Waiter.waitFor(visibilityOf(exportDialog.exportDialogHeader));
        assertTrue(exportDialog.exportDialogHeader.isDisplayed(), "Export dialog box is not displayed, so test failed.");
    }

    @Test(groups = "NCPhysicalInventory")
    public void checkExportOnReviewAnalysisTabTest() throws Exception {
        basePageNC.inventoryMainMenuLink.click();
        Waiter.waitFor(visibilityOf(pageNCInventoryOverview.reviewProcessLink));
        pageNCInventoryOverview.reviewProcessLink.click();
        Waiter.waitFor(visibilityOf(reviewPage.analysisTab));
        reviewPage.analysisTab.click();
        Waiter.waitFor(visibilityOf(reviewPage.exportIcon));
        reviewPage.exportIcon.click();
        Waiter.waitFor(visibilityOf(exportDialog.exportDialogHeader));
        assertTrue(exportDialog.exportDialogHeader.isDisplayed(), "Export dialog box is not displayed, so test failed.");
    }

    // todo: DRAFT
    @Test(groups = "NCPhysicalInventory")
    public void checkReviewSortingQuantityTabTest() throws Exception {
        boolean testPassed = true;
        int countBeforeSorting = 0;
        int countAfterSorting = 0;
        basePageNC.inventoryMainMenuLink.click();
        Waiter.waitFor(visibilityOf(pageNCInventoryOverview.reviewProcessLink));
        pageNCInventoryOverview.reviewProcessLink.click();
        Waiter.waitFor(visibilityOf(reviewPage.quantityTab));
//        reviewPage.selectGroupBy("Product");
        reviewPage.selectGroupBy("Product");
//        Thread.sleep(10000);
        Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
        List<String> columnItems = new ArrayList<String>(asList("Product #", "Product Name", "Unit Price"));
        List<String> directionItems = new ArrayList<String>(asList("Ascending", "Descending"));
//        List<String> directionItems = new ArrayList<String>(asList("Descending"));
//        List<String> directionItems = new ArrayList<String>(asList("Ascending"));
        for(int i = 0; i < columnItems.size(); i++) {
            for(int j = 0; j < directionItems.size(); j++) {
                try {
                    countBeforeSorting = dataGrid.getListItemsCount(columnItems.get(i));
                    dataGrid.sorting(columnItems.get(i), directionItems.get(j));
                    countAfterSorting = dataGrid.getListItemsCount(columnItems.get(i));
                    if (countAfterSorting != countBeforeSorting) {
                        System.out.println("ERROR! List count has been changed after sorting. List count before sorting = " + countBeforeSorting + ", list count after sorting = " + countAfterSorting);
                        testPassed = false;
                    }
                    dataGrid.checkSorting(columnItems.get(i), directionItems.get(j));

                }
                catch (Exception e) {
                    System.out.println("ERROR! Sorting for column '" + columnItems.get(i) + "' (direction '" + directionItems.get(j) + "') works incorrectly");
                    System.out.println("Exception message :" + e.getMessage());
                    System.out.println("---");
                    testPassed = false;
                }
            }
        }
        assertTrue(testPassed, "Test failed. Please see the test log for details.");
    }
}
