package com.crunchtime.ces.test.inventory.createReviewInventories;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.database.inventory.createReviewInventories.DBStmtPhysicalInventory;
import com.crunchtime.ces.helper.ActionsNC;
import com.crunchtime.ces.helper.DBMethodNC;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.List;
import java.util.Map;

public class TC_CreateReviewInvCopyScrn extends BaseTestNC {
	private static final String DB_CONNECTION_STRING = "AUTO/AUTO@CTAUTO24006.WORLD";
	private static final String TEST_SITE_NAME = "auto.net-chef.local";
	private static final String USER_ID = "INVEN";
	private static final String USER_PWD = "INVEN";
	private static final String TEST_LOCATION_NAME = "Chicago #332";

	@Parameters({"SMOKE"})
	@BeforeMethod(alwaysRun = true)
	public void logIn(@Optional(TEST_SITE_NAME) String siteName) throws Exception {
		basePageNC.openNCTestSite(siteName);
		pageNCLogin.loginNC(USER_ID, USER_PWD, TEST_LOCATION_NAME);
		basePageNC.navigateToCreateReviewInvSummScreen();
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}

	/**
	 * ces-6770, ces-7099
	 */
	@Test
	public void testCopyScheduledInventoryBtnAndCopyToPostDateList() throws Exception {
		ActionsNC.setTestMethodDescription("ces-6770", "Validate Copy Scheduled Inventory button existence.<br>Validate valid 'Copy To' Post Period list.");
		Map<String, List<Object>> map = DBMethodNC.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPhysicalInventory.getStmtSelectLastValidPostDateToCopy(TEST_LOCATION_NAME));
		String copyFromPostDate = map.get("post_date".toUpperCase()).get(0).toString().trim();
		pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBox.click();
		Waiter.waitForElementToBeVisible(driver, pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBoxList);
		List<WebElement> postPeriodsList = pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBoxList.findElements(By.cssSelector("[class*='x-boundlist-item']"));
		for (WebElement postPeriod : postPeriodsList){
			String postPeriodStr = postPeriod.getText().trim();
			if (postPeriodStr.equals(copyFromPostDate)){
				postPeriod.click();
			}
		}
		driver.navigate().refresh();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		try {
			pageCreateReviewInventories.copyScheduledInventoryBtn.click();
		} catch (NoSuchElementException e){
			e.printStackTrace();
			Assert.assertTrue(false, "Failed, as Copy Scheduled Inventory button is NOT enabled for eligible Copy From Post Period.");
		}
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		Map<String, List<Object>> map1 = DBMethodNC.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPhysicalInventory.getStmtSelectFirstCopyToPostDate(TEST_LOCATION_NAME, copyFromPostDate));
		String firstCopyToPostPeriodInDB = map1.get("inventory_period_date".toUpperCase()).get(0).toString().trim();
		String firstCopyToPostPeriodOnScrn = pageCreateReviewInventories.phyInvCopyToGrid.findElement(By.cssSelector("tr:first-child")).getText().trim();
		Assert.assertEquals(firstCopyToPostPeriodOnScrn, firstCopyToPostPeriodInDB, "Failed, expected first Post Period " +
				firstCopyToPostPeriodInDB + " but the actual on screen is " + firstCopyToPostPeriodOnScrn);
	}

	/**
	 * ces-6770, ces-7099
	 */
	@Test
	public void testCopyScheduledInventorySaveAndCloseBtn() throws Exception{
		ActionsNC.setTestMethodDescription("ces-6770", "Validate Copy Scheduled Inventory screen Save and Close button state based on panel list selection");
		Map<String, List<Object>> map = DBMethodNC.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPhysicalInventory.getStmtSelectLastValidPostDateToCopy(TEST_LOCATION_NAME));
		String copyFromPostDate = map.get("post_date".toUpperCase()).get(0).toString().trim();
		pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBox.click();
		Waiter.waitForElementToBeVisible(driver, pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBoxList);
		List<WebElement> postPeriodsList = pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBoxList.findElements(By.cssSelector("[class*='x-boundlist-item']"));
		for (WebElement postPeriod : postPeriodsList){
			String postPeriodStr = postPeriod.getText().trim();
			if (postPeriodStr.equals(copyFromPostDate)){
				postPeriod.click();
			}
		}
		driver.navigate().refresh();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		pageCreateReviewInventories.copyScheduledInventoryBtn.click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		pageCreateReviewInventories.phyInvCopyFromGrid.findElement(By.cssSelector("tr:first-child")).click();
		boolean initSaveAndCloseBtnState = pageCreateReviewInventories.phyInvCopySaveAndCloseBtn.getAttribute("class").contains("disable");
		Assert.assertTrue(initSaveAndCloseBtnState, "Failed, as initial Save and Close button state should be disabled.");
		pageCreateReviewInventories.phyInvCopyToGrid.findElement(By.cssSelector("tr:first-child")).click();
		boolean secSaveAndCloseBtnState = pageCreateReviewInventories.phyInvCopySaveAndCloseBtn.getAttribute("class").contains("disable");
		Assert.assertFalse(secSaveAndCloseBtnState, "Failed, as Save and Close button should be enabled if both Copy To and From rows are selected.");
		pageCreateReviewInventories.phyInvCopyFromGrid.findElement(By.cssSelector("tr:first-child")).click();
		boolean thirdSaveAndCloseBtnState = pageCreateReviewInventories.phyInvCopySaveAndCloseBtn.getAttribute("class").contains("disable");
		Assert.assertTrue(thirdSaveAndCloseBtnState, "Failed, as Save and Close button state should be disabled if not both Copy To and From rows are selected");
	}

	/**
	 * ces-6770, ces-7548
	 */
	@Test
	public void testCopyScheduledInventoryAction() throws Exception{
		ActionsNC.setTestMethodDescription("ces-6770", "Perform copy action to make sure inventory schedule is copied as expected.");
		Map<String, List<Object>> map = DBMethodNC.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPhysicalInventory.getStmtSelectLastValidPostDateToCopy(TEST_LOCATION_NAME));
		String copyFromPostDate = map.get("post_date".toUpperCase()).get(0).toString().trim();
		pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBox.click();
		Waiter.waitForElementToBeVisible(driver, pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBoxList);
		List<WebElement> postPeriodsList = pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBoxList.findElements(By.cssSelector("[class*='x-boundlist-item']"));
		for (WebElement postPeriod : postPeriodsList){
			String postPeriodStr = postPeriod.getText().trim();
			if (postPeriodStr.equals(copyFromPostDate)){
				postPeriod.click();
			}
		}
		driver.navigate().refresh();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		pageCreateReviewInventories.copyScheduledInventoryBtn.click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		pageCreateReviewInventories.phyInvCopyFromGrid.findElement(By.cssSelector("tr:first-child")).click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		String secondCopyToPostPeriodOnScrn = pageCreateReviewInventories.phyInvCopyToGrid.findElement(By.cssSelector("tr:nth-child(2)")).getText().trim();
		pageCreateReviewInventories.phyInvCopyToGrid.findElement(By.cssSelector("tr:nth-child(2)")).click();
		pageCreateReviewInventories.phyInvCopySaveAndCloseBtn.click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBox.click();
		Waiter.waitForElementToBeVisible(driver, pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBoxList);
		List<WebElement> newPostPeriodsList = pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBoxList.findElements(By.cssSelector("[class*='x-boundlist-item']"));
		for (WebElement postPeriod : newPostPeriodsList){
			String postPeriodStr = postPeriod.getText().trim();
			if (postPeriodStr.equals(secondCopyToPostPeriodOnScrn)){
				postPeriod.click();
			}
		}
		driver.navigate().refresh();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		Map<String, List<Object>> map1 = DBMethodNC.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPhysicalInventory.getStmtSelectLastCopiedScheduledInventory(TEST_LOCATION_NAME, secondCopyToPostPeriodOnScrn));
		String lastCopiedInv = map1.get("inv_type".toUpperCase()).get(0).toString().trim();
		boolean copiedFlag = pageCreateReviewInventories.phyInvSummScrnGrid.getText().contains(lastCopiedInv);
		if (copiedFlag){
			DBMethodNC.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPhysicalInventory.getStmtDeleteAddedScheduleInventory(TEST_LOCATION_NAME, secondCopyToPostPeriodOnScrn));
		} else {
			DBMethodNC.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPhysicalInventory.getStmtDeleteAddedScheduleInventory(TEST_LOCATION_NAME, secondCopyToPostPeriodOnScrn));
			Assert.assertTrue(copiedFlag, "Failed, Copy action did not copy the last inventory schedule");
		}
	}
}