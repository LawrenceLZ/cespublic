package com.crunchtime.ces.test.inventory.createReviewInventories;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.base.ScreenshotOnFailureListenerNC;
import com.crunchtime.ces.database.inventory.createReviewInventories.DBStmtPhysicalInventory;
import com.crunchtime.ces.helper.ActionsNC;
import com.crunchtime.ces.helper.DBMethodNC;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.*;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class TC_CreateReviewInvNewInvScrn extends BaseTestNC {
	private static final String DB_CONNECTION_STRING = "AUTO/AUTO@CTAUTO24006.WORLD";
	private static final String TEST_SITE_NAME = "auto.net-chef.local";
	private static final String USER_ID = "INVEN";
	private static final String USER_PWD = "INVEN";
	private static final String TEST_LOCATION_NAME = "Chicago #332";

	@Parameters({"SMOKE"})
    @Features("Physical Inventory Module")
	@BeforeMethod(alwaysRun = true)
	public void logIn(@Optional(TEST_SITE_NAME) String siteName) throws Exception {
		basePageNC.openNCTestSite(siteName);
		pageNCLogin.loginNC(USER_ID, USER_PWD, TEST_LOCATION_NAME);
		basePageNC.navigateToCreateReviewInvSummScreen();
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}

	/**
	 * ces-6768, ces-7052
	 */
    @Features("Physical Inventory Module")
    @Stories({"CES-6768"})
    @Severity(SeverityLevel.CRITICAL)
    @Title("Test New Schedule Inventory Date Dropdown")
    @Description("Verify the New Scheduled Inventory date dropdown list is valid.")
	@Test
	public void testNewScheduledInventoryDateDropwdown() throws Exception {
		ActionsNC.setTestMethodDescription("ces-6768", "Verify the New Scheduled Inventory date dropdown list is valid.");
		Map<String, List<Object>> map = DBMethodNC.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPhysicalInventory.getStmtSelectFirstScheduledFinalForPostDate(TEST_LOCATION_NAME));
		String postDateStr = map.get("post_date".toUpperCase()).get(0).toString().trim();
		pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBox.click();
		Waiter.waitForElementToBeVisible(driver, pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBoxList);
		List<WebElement> postPeriodsList = pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBoxList.findElements(By.cssSelector("[class*='x-boundlist-item']"));
		for (WebElement postPeriod : postPeriodsList){
			String postPeriodStr = postPeriod.getText().trim();
			if (postPeriodStr.equals(postDateStr)){
				postPeriod.click();
			}
		}
		driver.navigate().refresh();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		pageCreateReviewInventories.newScheduledInventoryBtn.click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		pageCreateReviewInventories.newScheduledInvPostPeriodComboBox.click();
		Waiter.waitForElementToBeVisible(driver, pageCreateReviewInventories.newScheduledInvPostPeriodComboBoxList);
		String defaultInvDateStr = pageCreateReviewInventories.newScheduledInvPostPeriodComboBoxList.findElement(By.cssSelector("[class*='x-boundlist-selected']")).getText().trim();
		Date postDate = ActionsNC.getDateByString(postDateStr, "MM/dd/yyyy");
		Date defaultInvDate = ActionsNC.getDateByString(defaultInvDateStr, "MM/dd/yyyy");
		Assert.assertTrue(defaultInvDate.compareTo(postDate) <= 0, "Failed, default scheduled inventory should be <= " + postDate);
	}

	/**
	 * ces-6768, ces-7053
	 */
    @Features("Physical Inventory Module")
    @Stories({"CES-7053"})
    @Severity(SeverityLevel.TRIVIAL)
    @Title("Test New Schedule Inventory Grids Order")
    @Description("Verify the New Scheduled Inventory, Inventory Type grid list is valid.")
	@Test
	public void testNewScheduledInventoryGridsOrder() throws Exception{
		ActionsNC.setTestMethodDescription("ces-7053", "Verify the New Scheduled Inventory, Inventory Type grid list is valid.");
		Map<String, List<Object>> map = DBMethodNC.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPhysicalInventory.getStmtSelectFirstScheduledFinalForPostDate(TEST_LOCATION_NAME));
		String postDateStr = map.get("post_date".toUpperCase()).get(0).toString().trim();
		pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBox.click();
		Waiter.waitForElementToBeVisible(driver, pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBoxList);
		List<WebElement> postPeriodsList = pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBoxList.findElements(By.cssSelector("div[class*='boundlist-item']"));
		for (WebElement postPeriod : postPeriodsList){
			String postPeriodStr = postPeriod.getText().trim();
			if (postPeriodStr.equals(postDateStr)){
				postPeriod.click();
			}
		}
		driver.navigate().refresh();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		pageCreateReviewInventories.newScheduledInventoryBtn.click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		int newSchInvGridsCnt = pageCreateReviewInventories.newScheduledInvGridPanelHeaders.size();
		String lastGridPanelStr = pageCreateReviewInventories.newScheduledInvGridPanelHeaders.get(newSchInvGridsCnt-1).getText();
		Assert.assertEquals(lastGridPanelStr, "All Products", "Failed, last panel should for All Products");
	}

	/**
	 * ces-6768, ces-6270
	 */
    @Features("Physical Inventory Module")
    @Stories({"CES-6270"})
    @Severity(SeverityLevel.CRITICAL)
    @Title("Test New Schedule Inventory Button Options Based on Final For Post")
    @Description("Verify the New Scheduled Inventory button is turned on/off based on valid post period.")
	@Test
	public void testNewScheduledInventoryBtnOptionsBasedOnFinalForPost() throws Exception{
		ActionsNC.setTestMethodDescription("ces-6270", "Verify the New Scheduled Inventory button is turned on/off based on valid post period.");
		Map<String, List<Object>> map = DBMethodNC.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPhysicalInventory.getStmtSelectFirstScheduledFinalForPostDate(TEST_LOCATION_NAME));
		String postDateStr = map.get("post_date".toUpperCase()).get(0).toString().trim();
		pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBox.click();
		Waiter.waitForElementToBeVisible(driver, pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBoxList);
		List<WebElement> postPeriodsList = pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBoxList.findElements(By.cssSelector("[class*='x-boundlist-item']"));
		for (WebElement postPeriod : postPeriodsList){
			String postPeriodStr = postPeriod.getText().trim();
			if (postPeriodStr.equals(postDateStr)){
				postPeriod.click();
			}
		}
		driver.navigate().refresh();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		try {
			pageCreateReviewInventories.newScheduledInventoryBtn.isDisplayed();
		} catch (NoSuchElementException e){
			Reporter.log("Initial setup Failed, as the Post Period is valid for this test = " + Reporter.getCurrentTestResult().getMethod().getMethodName());
		}
		boolean btnExistFlagInAwaitingReviewStatus = false, btnExistFlagInCompleteStatus = false;
		//test Awaiting Review status
		try {
			DBMethodNC.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPhysicalInventory.getStmtUpdateFinalForPostScheduleStatus(TEST_LOCATION_NAME, postDateStr, "Awaiting Review"));
			driver.navigate().refresh();
			pageCreateReviewInventories.waitForSpinnerToDisappear();
			pageCreateReviewInventories.newScheduledInventoryBtn.isDisplayed();
			btnExistFlagInAwaitingReviewStatus = true;
		} catch (NoSuchElementException e) {
			DBMethodNC.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPhysicalInventory.getStmtUpdateFinalForPostScheduleStatus(TEST_LOCATION_NAME, postDateStr, "Scheduled"));
		}
		//test Scheduled status
		try {
			DBMethodNC.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPhysicalInventory.getStmtUpdateFinalForPostScheduleStatus(TEST_LOCATION_NAME, postDateStr, "Complete"));
			driver.navigate().refresh();
			pageCreateReviewInventories.waitForSpinnerToDisappear();
			pageCreateReviewInventories.newScheduledInventoryBtn.isDisplayed();
			btnExistFlagInCompleteStatus = true;
		} catch (NoSuchElementException e) {
			DBMethodNC.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPhysicalInventory.getStmtUpdateFinalForPostScheduleStatus(TEST_LOCATION_NAME, postDateStr, "Scheduled"));
		}
		DBMethodNC.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPhysicalInventory.getStmtUpdateFinalForPostScheduleStatus(TEST_LOCATION_NAME, postDateStr, "Scheduled"));
		Assert.assertFalse(btnExistFlagInAwaitingReviewStatus, "Failed, as the New Scheduled Inventory button should NOT be visible if Final For Post is in Awaiting Review status.");
		Assert.assertFalse(btnExistFlagInCompleteStatus, "Failed, as the New Scheduled Inventory button should NOT be visible if Final For Post is in Complete status.");
	}

	/**
	 * ces-6768, ces-6270
	 */
    @Features("Physical Inventory Module")
    @Stories({"CES-6270"})
    @Severity(SeverityLevel.MINOR)
    @Title("Test New Scheduled Inventory Date list")
    @Description("test description")
	@Test
	public void testNewScheduledInventoryDateListBasedOnLastInvStatus() throws Exception {
		ActionsNC.setTestMethodDescription("ces-6270", "Perform create new inventory schedule action to make sure new schedule is created as expected.");
		Map<String, List<Object>> map = DBMethodNC.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPhysicalInventory.getStmtSelectFirstScheduledFinalForPostDate(TEST_LOCATION_NAME));
		String postDateStr = map.get("post_date".toUpperCase()).get(0).toString().trim();
		pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBox.click();
		Waiter.waitForElementToBeVisible(driver, pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBoxList);
		List<WebElement> postPeriodsList = pageCreateReviewInventories.phyInvSummScrnPostPeriodComboBoxList.findElements(By.cssSelector("[class*='x-boundlist-item']"));
		for (WebElement postPeriod : postPeriodsList){
			String postPeriodStr = postPeriod.getText().trim();
			if (postPeriodStr.equals(postDateStr)){
				postPeriod.click();
			}
		}
		driver.navigate().refresh();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		try {
			pageCreateReviewInventories.newScheduledInventoryBtn.isDisplayed();
		} catch (NoSuchElementException e){
			Reporter.log("Initial setup Failed, as the Post Period is NOT valid for this test = " + Reporter.getCurrentTestResult().getMethod().getMethodName());
		}
		pageCreateReviewInventories.newScheduledInventoryBtn.click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		pageCreateReviewInventories.newScheduledInvPostPeriodComboBox.click();
		Waiter.waitForElementToBeVisible(driver, pageCreateReviewInventories.newScheduledInvPostPeriodComboBoxList);
		List<WebElement> invScheduleDateList = pageCreateReviewInventories.newScheduledInvPostPeriodComboBoxList.findElements(By.cssSelector("[class*='x-boundlist-item']"));
		int invScheduleDateListCnt = invScheduleDateList.size();
		String invScheduleDateStr = invScheduleDateList.get(invScheduleDateListCnt-2).getText().trim();
		invScheduleDateList.get(invScheduleDateListCnt-2).click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		pageCreateReviewInventories.newScheduledInvTemplateGrid.findElement(By.cssSelector("tr:last-child [class*='row-checker']")).click();
		pageCreateReviewInventories.newInvScheduleSaveAndCloseBtn.click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		DBMethodNC.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPhysicalInventory.getStmtUpdateScheduledInvStatusForPostPeriod(TEST_LOCATION_NAME, postDateStr, invScheduleDateStr, "Awaiting Review"));
		driver.navigate().refresh();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		//reopen New Scheudle screen to count the date dropdown, and only expecting two date values
		pageCreateReviewInventories.newScheduledInventoryBtn.click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		pageCreateReviewInventories.newScheduledInvPostPeriodComboBox.click();
		int updatedInvScheduleDateListCnt = 0;
		try {
			Waiter.waitForElementToBeVisible(driver, pageCreateReviewInventories.newScheduledInvPostPeriodComboBoxList);
			updatedInvScheduleDateListCnt = pageCreateReviewInventories.newScheduledInvPostPeriodComboBoxList.findElements(By.cssSelector("[class*='x-boundlist-item']")).size();
		} catch (Exception e){
			//delete the new created schedule to rollback the data staging before validation
			DBMethodNC.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPhysicalInventory.getStmtDeleteAddedScheduleInventory(TEST_LOCATION_NAME, postDateStr));
		}
		//delete the new created schedule to rollback the data staging before validation
		DBMethodNC.executeSqlStmt(DB_CONNECTION_STRING, DBStmtPhysicalInventory.getStmtDeleteAddedScheduleInventory(TEST_LOCATION_NAME, postDateStr));
		Assert.assertTrue(updatedInvScheduleDateListCnt==2, "Failed, as more invalid dates are included in the dropdown list.");
	}

    @Stories({"CES-6270"})
    @Severity(SeverityLevel.CRITICAL)
    @Title("Test Fake Test")
    @Description("Fake test description")
    @Test
    public void testFakeTest() throws Exception {
        ActionsNC.setTestMethodDescription("ces-6270", "Perform create new inventory schedule action to make sure new schedule is created as expected.");
        Assert.assertEquals("true", "not true", "Failed for a purpose.");
    }



}