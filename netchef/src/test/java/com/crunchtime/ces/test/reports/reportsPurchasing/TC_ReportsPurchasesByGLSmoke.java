package com.crunchtime.ces.test.reports.reportsPurchasing;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.base.SmokeTestBase;
import com.crunchtime.ces.helper.Waiter;
import com.crunchtime.ces.pages.reports.reportsOverview.PageReportsOverview;
import com.crunchtime.ces.pages.reports.reportsPurchasing.PageReportsPurchasingPurchasesByGL;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class TC_ReportsPurchasesByGLSmoke extends SmokeTestBase {

	private static final String START_DATE = "06/15/2013";
	private PageReportsPurchasingPurchasesByGL purchasesByGLPage;
	private PageReportsOverview reportsOverviewPage;
	private PageBase pageBase;

	@BeforeMethod
	public void setUp() throws Exception {
		super.setUp();
		pageBase = PageFactory.initElements(driver, PageBase.class);
		purchasesByGLPage = PageFactory.initElements(driver, PageReportsPurchasingPurchasesByGL.class);
		reportsOverviewPage = PageFactory.initElements(driver, PageReportsOverview.class);
	}

	@Test(groups = "smoke")
	public void purchasesByGlSmokeTest() throws InterruptedException {
		Waiter.waitFor(visibilityOf(basePageNC.reportsMainMenuLink));
		basePageNC.reportsMainMenuLink.click();
		Waiter.waitFor(visibilityOf(reportsOverviewPage.reportsPanel));
		reportsOverviewPage.expandAllButtonClick();
		reportsOverviewPage.purchasesByGlLinkClick();
		purchasesByGLPage.enterStartDate(START_DATE);
		purchasesByGLPage.retrieveData();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.loadMask));
		purchasesByGLPage.filterBySupplier("abc");
		purchasesByGLPage.clearPurchasesByGlFilter();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.loadMask));
		purchasesByGLPage.collapseLocationDetails();
		purchasesByGLPage.expandLocationDetails();
		purchasesByGLPage.collapseLocationSummary();
		purchasesByGLPage.expandLocationSummary();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.loadMask));
		purchasesByGLPage.closePurchasesByGl();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.loadMask));
	}

	@AfterMethod
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}
}