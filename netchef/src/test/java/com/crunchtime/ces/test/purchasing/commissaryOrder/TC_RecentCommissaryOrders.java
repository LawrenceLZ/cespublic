package com.crunchtime.ces.test.purchasing.commissaryOrder;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.database.generic.DBStmtNCGeneric;
import com.crunchtime.ces.helper.ActionsNC;
import org.testng.Assert;
import org.testng.annotations.*;

public class TC_RecentCommissaryOrders extends BaseTestNC {

	String dbConnectionString = "autorpt/autorpt@CTAUTO24006";
	String testSiteName = "autorpt.net-chef.local";
	String userID = "autorpt";
	String userPWD = "autorpt";
	String testLocationName = "#CCI_REPORT#";

	@BeforeMethod(alwaysRun = true)
	public void logIn() throws Exception {
		basePageNC.openNCTestSite(testSiteName);
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}

	@Test(groups = {"userGroupAccess"})
	public void userGroupAccessForRecentCommissaryOrders() throws Exception {
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Other Store Tasks", "View/Edit Recent Commissary Order");
		pageNCLogout.logoutNC();
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
		pageNavigationGeneric.menuLinkMouseOver("Purchasing", "");
		//check if it is existed
		boolean existFlag = ActionsNC.isElementPresent(basePageNC.recentCommissaryOrdersMenuLink);
		if (existFlag) {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Other Store Tasks", "View/Edit Recent Commissary Order");
			Assert.assertFalse(existFlag, "Recent Commissary Orders Link Should be DISABLED if control is OFF, so test failed.");
		} else {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Other Store Tasks", "View/Edit Recent Commissary Order");
			pageNCLogout.logoutNC();
			pageNCLogin.loginNC(userID, userPWD, testLocationName);
			pageNavigationGeneric.menuLinkMouseOver("Purchasing", "");
			boolean existFlagUpdated = ActionsNC.isElementPresent(basePageNC.recentCommissaryOrdersMenuLink);
			Assert.assertTrue(existFlagUpdated, "Recent Commissary Orders Link Should be ENABLED if control is ON, so test failed.");
		}
	}
}
