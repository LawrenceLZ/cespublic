package com.crunchtime.ces.test.labor.laborSummary;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.database.generic.DBStmtNCGeneric;
import com.crunchtime.ces.helper.ActionsNC;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TC_LaborSummary extends BaseTestNC {
	String dbConnectionString = "autorpt/autorpt@CTAUTO24006";
	String testSiteName = "autorpt.net-chef.local";
	String userID = "autorpt";
	String userPWD = "autorpt";
	String testLocationName = "#CCI_REPORT#";

	@BeforeMethod(alwaysRun = true)
	public void logIn() throws Exception {
		basePageNC.openNCTestSite(testSiteName);
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}

	@Test(groups = {"userGroupAccess"})
	public void userGroupAccessForLaborSummary() throws Exception {
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Other Store Tasks", "Review/Edit Labor Information");
		pageNCLogout.logoutNC();
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
		pageNavigationGeneric.menuLinkMouseOver("Labor", "");
		//check if it is existed
		boolean existFlag = ActionsNC.isElementPresent(basePageNC.laborSummaryMenuLink);
		if (existFlag) {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Other Store Tasks", "Review/Edit Labor Information");
			Assert.assertFalse(existFlag, "Labor Summary Link Should be DISABLED if control is OFF, so test failed.");
		} else {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Other Store Tasks", "Review/Edit Labor Information");
			pageNCLogout.logoutNC();
			pageNCLogin.loginNC(userID, userPWD, testLocationName);
			pageNavigationGeneric.menuLinkMouseOver("Labor", "");
			boolean existFlagUpdated = ActionsNC.isElementPresent(basePageNC.laborSummaryMenuLink);
			Assert.assertTrue(existFlagUpdated, "Labor Summary Link Should be ENABLED if control is ON, so test failed.");
		}
	}
}
