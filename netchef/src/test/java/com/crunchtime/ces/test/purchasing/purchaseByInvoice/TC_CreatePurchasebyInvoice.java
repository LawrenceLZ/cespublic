package com.crunchtime.ces.test.purchasing.purchaseByInvoice;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.database.generic.DBStmtNCGeneric;
import com.crunchtime.ces.helper.ActionsNC;
import org.testng.Assert;
import org.testng.annotations.*;

public class TC_CreatePurchasebyInvoice extends BaseTestNC {

	String dbConnectionString = "autocoherent/autocoherent@CTAUTOCOHERENT";
	String testSiteName = "autocoherent.net-chef.local";
	String userID = "autocoherent";
	String userPWD = "autocoherent";
	String testLocationName = "RESTAURANT 0001";

	@BeforeMethod(alwaysRun = true)
	public void logIn() throws Exception {
		basePageNC.openNCTestSite(testSiteName);
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}

	@Test
	public void createPurchasebyInvoiceScreenTest() throws Exception {
		pageNavigationGeneric.menuLinkMouseOver("Purchasing", "Create Purchase by Invoice");
		basePageNC.createPurchasebyInvoiceMenuLink.click();
		ActionsNC.setSelectDropdownByValue(pageCreatePurchasebyInvoice.vendorDropdown, "2 MOMS IN THE RAW, LLC");
		pageCreatePurchasebyInvoice.invoiceNumberTextBox.sendKeys("AUTO-002");

		//take a screen shot with the edited values for a later diff validation
		ActionsNC.takeScreenshot(driver, "createPurchasebyInvoiceScreenTest", null);
		ActionsNC.pDiff("createPurchasebyInvoiceScreenTest");

		//click Cancel button to make sure the screen is re-directed to Recent Purchase by Invoice screen
		pageCreatePurchasebyInvoice.cancelBtn.click();
		int minimizeBtnCnt = pageRecentPurchasebyInvoices.minimizeBtn.size();
		Assert.assertEquals(minimizeBtnCnt, 1, "Should ONLY be one grid with minimize button.");
	}

	@Test(groups = {"userGroupAccess"})
	public void userGroupAccessForCreatePurchasebyInvoice() throws Exception {
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Daily Store Tasks", "Purchase By Invoice");
		pageNCLogout.logoutNC();
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
		pageNavigationGeneric.menuLinkMouseOver("Purchasing", "");
		//check if it is existed
		boolean existFlag = ActionsNC.isElementPresent(basePageNC.createPurchasebyInvoiceMenuLink);
		if (existFlag) {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Daily Store Tasks", "Purchase By Invoice");
			Assert.assertFalse(existFlag, "Create Purchase by Invoice Link Should be DISABLED if control is OFF, so test failed.");
		} else {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Daily Store Tasks", "Purchase By Invoice");
			pageNCLogout.logoutNC();
			pageNCLogin.loginNC(userID, userPWD, testLocationName);
			pageNavigationGeneric.menuLinkMouseOver("Purchasing", "");
			boolean existFlagUpdated = ActionsNC.isElementPresent(basePageNC.createPurchasebyInvoiceMenuLink);
			Assert.assertTrue(existFlagUpdated, "Create Purchase by Invoice Link Should be ENABLED if control is ON, so test failed.");
		}
	}
}
