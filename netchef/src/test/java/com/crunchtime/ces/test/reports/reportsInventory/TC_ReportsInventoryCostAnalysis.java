package com.crunchtime.ces.test.reports.reportsInventory;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.database.generic.DBStmtNCGeneric;
import com.crunchtime.ces.helper.ActionsNC;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TC_ReportsInventoryCostAnalysis extends BaseTestNC {
	private String dbConnectionString = "autorpt/autorpt@CTAUTO24006";
	private String testSiteName = "autorpt.net-chef.local";
	private String userID = "autorpt";
	private String userPWD = "autorpt";
	private String testLocationName = "#CCI_REPORT#";

	@BeforeMethod(alwaysRun = true)
	public void setup() throws Exception {
		basePageNC.openNCTestSite(testSiteName);
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}

	@Test(groups = {"userGroupAccess"})
	public void userGroupAccessForReportsInventoryCostAnalysis() throws Exception {
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-View Reports", "Cost Analysis");
		pageNCLogout.logoutNC();
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
		pageNavigationGeneric.menuLinkMouseOver("Inventory", "Reports");
		//check if it is existed
		boolean existFlag1 = ActionsNC.isElementPresent(basePageNC.reportsInventoryCostAnalysisMenuLink);
		pageNavigationGeneric.menuLinkMouseOver("Reports", "Inventory");
		boolean existFlag2 = ActionsNC.isElementPresent(basePageNC.reportsInventoryCostAnalysisMenuLink);
		if (existFlag1 || existFlag2) {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Cost Analysis");
			Assert.assertFalse(existFlag1, "Inventory --> Reports --> Cost Analysis Link Should be DISABLED if control is OFF, so test failed.");
			Assert.assertFalse(existFlag2, "Reports --> Inventory --> Cost Analysis Link Should be DISABLED if control is OFF, so test failed.");
		} else {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Cost Analysis");
			pageNCLogout.logoutNC();
			pageNCLogin.loginNC(userID, userPWD, testLocationName);
			pageNavigationGeneric.menuLinkMouseOver("Inventory", "Reports");
			boolean existFlagUpdated1 = ActionsNC.isElementPresent(basePageNC.reportsInventoryCostAnalysisMenuLink);
			Assert.assertTrue(existFlagUpdated1, "Inventory --> Reports --> Cost Analysis Link Should be ENABLED if control is ON, so test failed.");
			pageNavigationGeneric.menuLinkMouseOver("Reports", "Inventory");
			boolean existFlagUpdated2 = ActionsNC.isElementPresent(basePageNC.reportsInventoryCostAnalysisMenuLink);
			Assert.assertTrue(existFlagUpdated2, "Reports --> Inventory --> Cost Analysis Link Should be ENABLED if control is ON, so test failed.");
		}
	}
}
