package com.crunchtime.ces.test.inventory.createReviewInventories;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.database.inventory.createReviewInventories.DBStmtPhysicalInventory;
import com.crunchtime.ces.helper.ActionsNC;
import com.crunchtime.ces.helper.DBMethodNC;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.List;
import java.util.Map;

public class TC_CreateReviewInvDtlScrn extends BaseTestNC {
	private static final String DB_CONNECTION_STRING = "AUTO/AUTO@CTAUTO24006.WORLD";
	private static final String TEST_SITE_NAME = "auto.net-chef.local";
	private static final String USER_ID = "INVEN";
	private static final String USER_PWD = "INVEN";
	private static final String TEST_LOCATION_NAME = "Chicago #332";

	@Parameters({"SMOKE"})
	@BeforeMethod(alwaysRun = true)
	public void logIn(@Optional(TEST_SITE_NAME) String siteName) throws Exception {
		basePageNC.openNCTestSite(siteName);
		pageNCLogin.loginNC(USER_ID, USER_PWD, TEST_LOCATION_NAME);
		basePageNC.navigateToCreateReviewInvSummScreen();
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}

	/**
	 * ces-7004, ces-7431
	 */
	@Test
	public void testExcludeProductsWithoutInventoryActivityCheckbox() throws Exception{
		ActionsNC.setTestMethodDescription("ces-7004", "Verify 'Exclude Products Without Inventory Activity' checkbox on filter popup for all tabs and its filter function.");
		pageCreateReviewInventories.processLinkForFinalForPost("Review").click();
		try {
			boolean overdueTasksPopupExists = ActionsNC.isElementPresent(pageCreateReviewInventories.overdueInvTransPopupGrid);
			if (overdueTasksPopupExists){
				pageCreateReviewInventories.overdueInvTransPopupCmdBtn("Continue").click();
			}
		} catch (Exception e){
			e.printStackTrace();
		}
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		String activeDBsessionStr = ActionsNC.getDBSessionStringNC(driver);
		Map<String, List<Object>> map = DBMethodNC.getSelectSqlMap(DB_CONNECTION_STRING, DBStmtPhysicalInventory.getStmtCountInvRowsOnlyWithActivity(activeDBsessionStr));
		int dbRowCnt = Integer.parseInt(map.get("ROW_COUNT").get(0).toString());
//		System.out.println(dbRowCnt);
		pageCreateReviewInventories.phyInvReviewScrnFilterIcon.click();
		Waiter.waitForElementToBeVisible(driver, pageCreateReviewInventories.phyInvFilterPopup);
		boolean defaultCheckboxState = pageCreateReviewInventories.excludeProductsWithoutInvActivityCheckbox.getAttribute("class").contains("checked");
		Assert.assertFalse(defaultCheckboxState, "Failed, as the Exclude Products without Inventory Activity checkbox should NOT be checked by default.");
		pageCreateReviewInventories.excludeProductsWithoutInvActivityCheckbox.findElement(By.cssSelector("input")).click();
		pageCreateReviewInventories.phyInvFilterPopupCmdBtn("Apply").click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		int scrnRowCnt = ActionsNC.getGridRowCnt(pageCreateReviewInventories.phyInvReviewScrnQtyTabGrid) - 1; //minis 1 here is to deal with the on scrn default blank row
		Assert.assertTrue(scrnRowCnt<=dbRowCnt, "Failed, as screen row count not equal to db wb table row count");
		//go to Value tab
		pageCreateReviewInventories.phyInvReviewScrnValueTabLink.click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		pageCreateReviewInventories.phyInvReviewScrnFilterIcon.click();
		Waiter.waitForElementToBeVisible(driver, pageCreateReviewInventories.phyInvFilterPopup);
		boolean valTabCheckboxState = pageCreateReviewInventories.excludeProductsWithoutInvActivityCheckbox.getAttribute("class").contains("checked");
		if (!valTabCheckboxState){
			pageCreateReviewInventories.excludeProductsWithoutInvActivityCheckbox.findElement(By.cssSelector("input")).click();
			pageCreateReviewInventories.phyInvFilterPopupCmdBtn("Apply").click();
			pageCreateReviewInventories.waitForSpinnerToDisappear();
			Assert.assertTrue(valTabCheckboxState, "Failed, as filter effect is NOT persisted across tab.");
		} else {
			pageCreateReviewInventories.phyInvFilterPopupCmdBtn("Cancel").click();
			pageCreateReviewInventories.waitForSpinnerToDisappear();
		}
		int valTabScrnRowCnt = ActionsNC.getGridRowCnt(pageCreateReviewInventories.phyInvReviewScrnValueTabGrid) - 1; //minis 1 here is to deal with the on scrn default blank row
		Assert.assertTrue(valTabScrnRowCnt<=dbRowCnt, "Failed, as screen row count not equal to db wb table row count");
		//go to Analysis tab
		pageCreateReviewInventories.phyInvReviewScrnAnalysisTabLink.click();
		pageCreateReviewInventories.waitForSpinnerToDisappear();
		pageCreateReviewInventories.phyInvReviewScrnFilterIcon.click();
		Waiter.waitForElementToBeVisible(driver, pageCreateReviewInventories.phyInvFilterPopup);
		boolean analysisTabCheckboxState = pageCreateReviewInventories.excludeProductsWithoutInvActivityCheckbox.getAttribute("class").contains("checked");
		if (!analysisTabCheckboxState){
			pageCreateReviewInventories.excludeProductsWithoutInvActivityCheckbox.findElement(By.cssSelector("input")).click();
			pageCreateReviewInventories.phyInvFilterPopupCmdBtn("Apply").click();
			pageCreateReviewInventories.waitForSpinnerToDisappear();
			Assert.assertTrue(analysisTabCheckboxState, "Failed, as filter effect is NOT persisted across tab.");
		} else {
			pageCreateReviewInventories.phyInvFilterPopupCmdBtn("Cancel").click();
			pageCreateReviewInventories.waitForSpinnerToDisappear();
		}
		int analysisTabScrnRowCnt = ActionsNC.getGridRowCnt(pageCreateReviewInventories.phyInvReviewScrnAnalysisTabGrid) - 1; //minis 1 here is to deal with the on scrn default blank row
		Assert.assertTrue(analysisTabScrnRowCnt<=dbRowCnt, "Failed, as screen row count not equal to db wb table row count");
	}
}