package com.crunchtime.ces.test.purchasing.commissaryOrder;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.database.generic.DBStmtNCGeneric;
import com.crunchtime.ces.database.purchasing.commissaryOrder.DBStmtCommissaryOrder;
import com.crunchtime.ces.helper.ActionsNC;
import org.testng.Assert;
import org.testng.annotations.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TC_CreateCommissaryOrder extends BaseTestNC {

	String dbConnectionString = "AUTO/AUTO@CTAUTO24006.WORLD";
	String testSiteName = "auto.net-chef.local";
	String userID = "AUTO";
	String userPWD = "AUTO";
	String testLocationName = "Chicago #332";
	String supplyLocationName = "Commissary East";

	@BeforeMethod(alwaysRun = true)
	public void setup() throws Exception {
		basePageNC.openNCTestSite(testSiteName);
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown() throws Exception {
		pageNCLogout.logoutNC();
	}

	@Test
	public void createCommissaryOrderScreenTest() throws Exception {
		pageNavigationGeneric.menuLinkMouseOver("Purchasing", "Create Commissary Order");
		basePageNC.createCommissaryOrderMenuLink.click();
		ActionsNC.setSelectDropdownByValue(pageCreateCommissaryOrder.supplyLocationDropdown, supplyLocationName);
		pageCreateCommissaryOrder.deliveryDateTextBox.clear();

		//set Delivery Date to be current day + 5
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		String date = dateFormat.format(new Date());
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateFormat.parse(date));
		cal.add(Calendar.DATE, 5);
		date = dateFormat.format(cal.getTime());
		pageCreateCommissaryOrder.deliveryDateTextBox.sendKeys(date);

		String defaultRefNum = pageCreateCommissaryOrder.referenceNumberTextBox.getAttribute("value");
		pageCreateCommissaryOrder.referenceNumberTextBox.clear();
		pageCreateCommissaryOrder.referenceNumberTextBox.sendKeys(defaultRefNum + " - AUTO");
		pageCreateCommissaryOrder.consumptionDaysTextBox.clear();
		pageCreateCommissaryOrder.consumptionDaysTextBox.sendKeys("7");

		//take a screen shot with the edited values for a later diff validation
		ActionsNC.takeScreenshot(driver, "createCommissaryOrderScreenTest", null);
		ActionsNC.pDiff("createCommissaryOrderScreenTest");

		//click Close button to make sure the screen is re-directed to Recent Commissary Orders screen
		pageCreateCommissaryOrder.closeBtn.click();
		int minBtnCnt = pageRecentCommissaryOrders.minimizeBtn.size();
		Assert.assertEquals(minBtnCnt, 1, "Should ONLY be one grid with minimize button.");
	}

	@Test
	public void commissaryOrdersDtlScreen() throws Exception {
		/***Start setup before enter dtl screen***/
		pageNavigationGeneric.menuLinkMouseOver("Purchasing", "Create Commissary Order");
		basePageNC.createCommissaryOrderMenuLink.click();
		ActionsNC.setSelectDropdownByValue(pageCreateCommissaryOrder.supplyLocationDropdown, supplyLocationName);
		pageCreateCommissaryOrder.deliveryDateTextBox.clear();

		//set Delivery Date to be current day + 5
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		String date = dateFormat.format(new Date());
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateFormat.parse(date));
		cal.add(Calendar.DATE, 5);
		date = dateFormat.format(cal.getTime());
		pageCreateCommissaryOrder.deliveryDateTextBox.sendKeys(date);

		String defaultRefNum = pageCreateCommissaryOrder.referenceNumberTextBox.getAttribute("value");
		pageCreateCommissaryOrder.referenceNumberTextBox.clear();
		pageCreateCommissaryOrder.referenceNumberTextBox.sendKeys(defaultRefNum + " - AUTO");
		pageCreateCommissaryOrder.consumptionDaysTextBox.clear();
		pageCreateCommissaryOrder.consumptionDaysTextBox.sendKeys("7");

		pageCreateCommissaryOrder.continueBtn.click();
		//Finish setup before enter dtl screen

		//Edit in the header section (frame(0))
		//driver.switchTo().frame(pageGeneric.cgiFrame.get(0));
		driver.switchTo().frame(0);
		pageCreateCommissaryOrder.retrieveBtn.click();

		pageCreateCommissaryOrder.subcategoryRadioBtn.click();
		ActionsNC.setSelectDropdownByNum(pageCreateCommissaryOrder.selectTemplateDropdown, 1);
		pageCreateCommissaryOrder.prodSearchTextBox.sendKeys("test");
		pageCreateCommissaryOrder.searchByNumberRadioBtn.click();

		driver.switchTo().defaultContent();

		//driver.switchTo().frame(pageGeneric.cgiFrame.get(1));
		driver.switchTo().frame(1);
		pageCreateCommissaryOrder.useLastOrderBtn.click();
		pageCreateCommissaryOrder.addSelectedProdBtn.click();
		//driver.switchTo().frame(pageGeneric.cgiFrame.get(0));
		driver.switchTo().frame(0);

		//validate Category value
		DBStmtCommissaryOrder.getFilterByCategoryDropdown(dbConnectionString, "category", testLocationName, supplyLocationName);
		String[] catValue = ActionsNC.readSqlResults(DBStmtCommissaryOrder.class.getSimpleName(), DBStmtCommissaryOrder.getFilterByCategoryDropdownFileName);
		int c = ActionsNC.getSelectDropdownCount(pageCreateCommissaryOrder.filterByCategoryDropdown);
		for (int i = 0; i < c; i++) {
			ActionsNC.setSelectDropdownByNum(pageCreateCommissaryOrder.filterByCategoryDropdown, i);
			//String val1 = pageCreateCommissaryOrder.filterByCategoryDropdown.getText();
			String val = ActionsNC.getSelectDropdownValue(pageCreateCommissaryOrder.filterByCategoryDropdown);
			System.out.println(val);
			Assert.assertEquals(val, catValue[i], "Filter by Category dropdown list value failure when sets to Category.");
		}
		//validate Subcategory value
		DBStmtCommissaryOrder.getFilterByCategoryDropdown(dbConnectionString, "subcategory", testLocationName, supplyLocationName);
		String[] subcatValue = ActionsNC.readSqlResults(DBStmtCommissaryOrder.class.getSimpleName(), DBStmtCommissaryOrder.getFilterByCategoryDropdownFileName);
		pageCreateCommissaryOrder.subcategoryRadioBtn.click();
		int s = ActionsNC.getSelectDropdownCount(pageCreateCommissaryOrder.filterByCategoryDropdown);
		for (int i = 0; i < s; i++) {
			ActionsNC.setSelectDropdownByNum(pageCreateCommissaryOrder.filterByCategoryDropdown, i);
			String subActualVal = ActionsNC.getSelectDropdownValue(pageCreateCommissaryOrder.filterByCategoryDropdown);
			System.out.println(subActualVal);
			Assert.assertEquals(subActualVal, subcatValue[i], "Filter by Category dropdown list value failure when sets to Subcategory.");
		}
		//validate Microcategory value
		DBStmtCommissaryOrder.getFilterByCategoryDropdown(dbConnectionString, "microcategory", testLocationName, supplyLocationName);
		String[] micocatValue = ActionsNC.readSqlResults(DBStmtCommissaryOrder.class.getSimpleName(), DBStmtCommissaryOrder.getFilterByCategoryDropdownFileName);
		pageCreateCommissaryOrder.microcategoryRadioBtn.click();
		int m = ActionsNC.getSelectDropdownCount(pageCreateCommissaryOrder.filterByCategoryDropdown);
		for (int i = 0; i < m; i++) {
			ActionsNC.setSelectDropdownByNum(pageCreateCommissaryOrder.filterByCategoryDropdown, i);
			String microActualVal = ActionsNC.getSelectDropdownValue(pageCreateCommissaryOrder.filterByCategoryDropdown);
			System.out.println(microActualVal);
			Assert.assertEquals(microActualVal, micocatValue[i], "Filter by Category dropdown list value failure when sets to Microcategory.");
		}
		//set filter by category back to default value of All
		ActionsNC.setSelectDropdownByValue(pageCreateCommissaryOrder.filterByCategoryDropdown, "All");
	}

	@Test(groups = {"userGroupAccess"})
	public void userGroupAccessForCreateCommissaryOrder() throws Exception {
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Daily Store Tasks", "Commissary Order");
		pageNCLogout.logoutNC();
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
		pageNavigationGeneric.menuLinkMouseOver("Purchasing", "");
		//check if it is existed
		boolean existFlag = ActionsNC.isElementPresent(basePageNC.createCommissaryOrderMenuLink);
		if (existFlag) {
			Assert.assertFalse(existFlag, "Create Commissary Order Link Should be DISABLED if control is OFF, so test failed.");
		} else {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Daily Store Tasks", "Commissary Order");
			pageNCLogout.logoutNC();
			pageNCLogin.loginNC(userID, userPWD, testLocationName);
			pageNavigationGeneric.menuLinkMouseOver("Purchasing", "");
			boolean existFlagUpdated = ActionsNC.isElementPresent(basePageNC.createCommissaryOrderMenuLink);
			Assert.assertTrue(existFlagUpdated, "Create Commissary Order Link Should be ENABLED if control is ON, so test failed.");
		}
	}
}
