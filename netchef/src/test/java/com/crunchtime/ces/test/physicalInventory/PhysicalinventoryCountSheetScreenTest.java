package com.crunchtime.ces.test.physicalInventory;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.base.SmokeTestBase;
import com.crunchtime.ces.helper.Waiter;
import com.crunchtime.ces.pages.inventory.inventoryOverview.PageInventoryOverview;
import com.crunchtime.ces.pages.inventory.inventoryOverview.physicalInventory.CountSheetPage;
import com.crunchtime.ces.pages.inventory.inventoryOverview.physicalInventory.DataGrid;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;
import static org.testng.Assert.assertTrue;

public class PhysicalinventoryCountSheetScreenTest extends SmokeTestBase {

    protected PageInventoryOverview pageNCInventoryOverview;
    protected CountSheetPage countSheetPage;
    protected DataGrid dataGrid;
    protected PageBase pageBase;
    private static String comboboxValueForCountSheet = "12/29/2013";

	@BeforeMethod
	public void setUp() throws Exception {
		super.setUp();
		this.pageNCInventoryOverview = PageFactory.initElements(driver, PageInventoryOverview.class);
	}
    // Draft version
    @Test(groups = "Smoke tests")
    public void checkCountSheetScreenSmokeTest() throws Exception {
        basePageNC.inventoryMainMenuLink.click();
        Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
        pageNCInventoryOverview.selectPostPeriod(comboboxValueForCountSheet);
        Waiter.waitFor(visibilityOf(pageNCInventoryOverview.countSheetProcessLink));
        pageNCInventoryOverview.countSheetProcessLink.click();
//        Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
        Waiter.waitFor(visibilityOf(countSheetPage.gridBodyCountSheetProcessPage));
        assertTrue(countSheetPage.gridBodyCountSheetProcessPage.isDisplayed(), "Count Sheet grid body is not displayed, so test failed.");
        assertTrue(dataGrid.seqText.isDisplayed(), "Seq column is not displayed, so test failed.");
        assertTrue(dataGrid.productNumberText.isDisplayed(), "Product # column is not displayed, so test failed.");
        assertTrue(dataGrid.productNameText.isDisplayed(), "Product Name column is not displayed, so test failed.");
        assertTrue(dataGrid.inventoryUnitDividedText.isDisplayed(), "Inventory Unit column is not displayed, so test failed.");
        assertTrue(dataGrid.altUnit1Text.isDisplayed(), "Alt Unit 1 column is not displayed, so test failed.");
        assertTrue(dataGrid.altUnit2Text.isDisplayed(), "Alt Unit 2 column is not displayed, so test failed.");
        assertTrue(dataGrid.altUnit3Text.isDisplayed(), "Alt Unit 3 column is not displayed, so test failed.");
        assertTrue(dataGrid.lotText.isDisplayed(), "Lot column is not displayed, so test failed.");
        assertTrue(countSheetPage.collapsedIcon.isDisplayed(), "Collapsed/Expanded icon is not displayed, so test failed.");
        assertTrue(countSheetPage.filterIcon.isDisplayed(), "Filters icon is not displayed, so test failed.");
        assertTrue(countSheetPage.exportIcon.isDisplayed(), "Export icon is not displayed, so test failed.");
        countSheetPage.closeButton.click();
        Waiter.waitFor(visibilityOf(pageNCInventoryOverview.headerPhysicalInventory));
        assertTrue(pageNCInventoryOverview.headerPhysicalInventory.isDisplayed(), "Physical Inventory table is not displayed, so test failed.");
    }
}