package com.crunchtime.ces.test.reports.reportsInventory;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.database.generic.DBStmtNCGeneric;
import com.crunchtime.ces.helper.ActionsNC;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TC_ReportsInventoryProfitLoss extends BaseTestNC {
	private String dbConnectionString = "autorpt/autorpt@CTAUTO24006";
	private String testSiteName = "autorpt.net-chef.local";
	private String userID = "autorpt";
	private String userPWD = "autorpt";
	private String testLocationName = "#CCI_REPORT#";

	@BeforeMethod(alwaysRun = true)
	public void setup() throws Exception {
		basePageNC.openNCTestSite(testSiteName);
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}

	@Test(groups = {"userGroupAccess"})
	public void userGroupAccessForReportsInventoryProfitLoss() throws Exception {
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-View Reports", "Profit ^& Loss");
		pageNCLogout.logoutNC();
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
		pageNavigationGeneric.menuLinkMouseOver("Reports", "Inventory");
		//check if it is existed
		boolean existFlag = ActionsNC.isElementPresent(basePageNC.reportsInventoryProfitAndLossMenuLink);
		if (existFlag) {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Profit ^& Loss");
			Assert.assertFalse(existFlag, "Reports Inventory Profit & Loss Link Should be DISABLED if control is OFF, so test failed.");
		} else {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Profit ^& Loss");
			pageNCLogout.logoutNC();
			pageNCLogin.loginNC(userID, userPWD, testLocationName);
			pageNavigationGeneric.menuLinkMouseOver("Reports", "Inventory");
			boolean existFlagUpdated = ActionsNC.isElementPresent(basePageNC.reportsInventoryProfitAndLossMenuLink);
			Assert.assertTrue(existFlagUpdated, "Reports Inventory Profit & Loss Link Should be ENABLED if control is ON, so test failed.");
		}
	}
}
