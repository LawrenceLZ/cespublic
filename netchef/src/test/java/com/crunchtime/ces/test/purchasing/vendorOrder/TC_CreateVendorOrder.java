package com.crunchtime.ces.test.purchasing.vendorOrder;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.database.generic.DBStmtNCGeneric;
import com.crunchtime.ces.helper.ActionsNC;
import org.openqa.selenium.remote.ScreenshotException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import sun.misc.BASE64Decoder;

import java.io.FileOutputStream;
import java.io.OutputStream;

public class TC_CreateVendorOrder extends BaseTestNC {
	private static final String dbConnectionString = "autorpt/autorpt@CTAUTO24006.WORLD";
	private static final String testSiteName = "autorpt.net-chef.local";
	private static final String userID = "autorpt";
	private static final String userPWD = "autorpt";
	private static final String testLocationName = "#CCI_REPORT#";

	@BeforeMethod(alwaysRun = true)
	public void logIn() throws Exception {
		basePageNC.openNCTestSite(testSiteName);
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}

	@Test(groups = {"userGroupAccess"})
	public void userGroupAccessForCreateVendorOrder() throws Exception {
		try {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Daily Store Tasks", "Vendor Order");
			pageNCLogout.logoutNC();
			pageNCLogin.loginNC("userID", userPWD, testLocationName);
			pageNavigationGeneric.menuLinkMouseOver("Purchasing", "");
			//check if it is existed
			boolean existFlag = ActionsNC.isElementPresent(basePageNC.createVendorOrderMenuLink);
			if (existFlag) {
				DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Daily Store Tasks", "Vendor Order");
				Assert.assertFalse(existFlag, "Create Vendor Order Link Should be DISABLED if control is OFF, so test failed.");
			} else {
				DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Daily Store Tasks", "Vendor Order");
				pageNCLogout.logoutNC();
				pageNCLogin.loginNC(userID, userPWD, testLocationName);
				pageNavigationGeneric.menuLinkMouseOver("Purchasing", "");
				boolean existFlagUpdated = ActionsNC.isElementPresent(basePageNC.createVendorOrderMenuLink);
				Assert.assertTrue(existFlagUpdated, "Create Vendor Order Link Should be ENABLED if control is ON, so test failed.");
			}
		} catch (Exception e) {
			Throwable cause = e.getCause();
			e.printStackTrace();

			if (cause instanceof ScreenshotException) {
				String scrnShot = ((ScreenshotException) cause).getBase64EncodedScreenshot();
				BASE64Decoder decoder = new BASE64Decoder();
				byte[] decodedScrnShot = decoder.decodeBuffer(scrnShot);
				OutputStream stream = new FileOutputStream("C:\\Users\\lzhao\\Desktop\\JIRA_DF_Scrn_Shot\\usergroupAccessForCreateVendorOrderException.png");
				stream.write(decodedScrnShot);
			}
		}
	}
}
