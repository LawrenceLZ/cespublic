package com.crunchtime.ces.test.purchasing.purchasingOverview;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.database.generic.DBStmtNCGeneric;
import com.crunchtime.ces.helper.ActionsNC;
import org.testng.Assert;
import org.testng.annotations.*;

public class TC_PurchasingOverview extends BaseTestNC {
	String dbConnectionString = "autorpt/autorpt@CTAUTO24006";
	String testSiteName = "autorpt.net-chef.local";
	String userID = "autorpt";
	String userPWD = "autorpt";
	String testLocationName = "#CCI_REPORT#";

	@BeforeMethod(alwaysRun = true)
	public void logIn() throws Exception {
		basePageNC.openNCTestSite(testSiteName);
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}

	@Test(groups = {"userGroupAccess"})
	public void userGroupAccessForPurchasingOverview() throws Exception {
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Daily Store Tasks", "Commissary Order");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Other Store Tasks", "View/Edit Recent Commissary Order");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Daily Store Tasks", "Vendor Order");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Other Store Tasks", "View/Edit Recent Vendor Order");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Daily Store Tasks", "Purchase By Invoice");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Other Store Tasks", "View/Edit Recent Purchase By Invoice");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Daily Store Tasks", "Master Order");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Vendor Returns", "Look Up Invoice");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Vendor Returns", "Use Last Price");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Administrative Options", "Vendor Information");
		//user group access for each report under Purchasing page module
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-View Reports", "Contracted Vendors");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-View Reports", "Purchase Detail");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-View Reports", "Purchases By GL");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-View Reports", "Purchase Journal");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-View Reports", "TORG Reports");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Consolidated Reports", "Consolidated Commodity Pricing Trends");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Consolidated Reports", "Consolidated Purchases by GL");
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Consolidated Reports", "Consolidated Vendor Orders");

		pageNCLogout.logoutNC();
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
		//pageNavigationGeneric.menuLinkMouseOver("Purchasing", "");
		//check if it is existed
		boolean existFlag = ActionsNC.isElementPresent(pageNavigationGeneric.purchasingMenuLink);
		if (existFlag) {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Daily Store Tasks", "Commissary Order");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Other Store Tasks", "View/Edit Recent Commissary Order");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Daily Store Tasks", "Vendor Order");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Other Store Tasks", "View/Edit Recent Vendor Order");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Daily Store Tasks", "Purchase By Invoice");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Other Store Tasks", "View/Edit Recent Purchase By Invoice");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Daily Store Tasks", "Master Order");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Vendor Returns", "Look Up Invoice");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Vendor Returns", "Use Last Price");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Administrative Options", "Vendor Information");
			//user group access for each report under Purchasing page module
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Contracted Vendors");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Purchase Detail");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Purchases By GL");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Purchase Journal");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "TORG Reports");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Consolidated Reports", "Consolidated Commodity Pricing Trends");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Consolidated Reports", "Consolidated Purchases by GL");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Consolidated Reports", "Consolidated Vendor Orders");
			Assert.assertFalse(existFlag, "Purchasing module/menu Link Should be DISABLED if control is OFF, so test failed.");
		} else {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Daily Store Tasks", "Commissary Order");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Other Store Tasks", "View/Edit Recent Commissary Order");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Daily Store Tasks", "Vendor Order");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Other Store Tasks", "View/Edit Recent Vendor Order");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Daily Store Tasks", "Purchase By Invoice");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Other Store Tasks", "View/Edit Recent Purchase By Invoice");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Daily Store Tasks", "Master Order");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Vendor Returns", "Look Up Invoice");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Vendor Returns", "Use Last Price");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Administrative Options", "Vendor Information");
			//user group access for each report under Purchasing page module
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Contracted Vendors");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Purchase Detail");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Purchases By GL");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Purchase Journal");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "TORG Reports");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Consolidated Reports", "Consolidated Commodity Pricing Trends");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Consolidated Reports", "Consolidated Purchases by GL");
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Consolidated Reports", "Consolidated Vendor Orders");
			pageNCLogout.logoutNC();
			pageNCLogin.loginNC(userID, userPWD, testLocationName);
			pageNavigationGeneric.menuLinkMouseOver("Administration", "");
			boolean existFlagUpdated = ActionsNC.isElementPresent(pageNavigationGeneric.purchasingMenuLink);
			Assert.assertTrue(existFlagUpdated, "Purchasing module/menu Link Should be ENABLED if control is ON, so test failed.");
		}
	}
}

