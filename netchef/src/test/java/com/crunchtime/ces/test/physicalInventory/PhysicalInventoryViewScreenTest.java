package com.crunchtime.ces.test.physicalInventory;

import com.crunchtime.ces.base.PageBase;
import com.crunchtime.ces.base.ScreenshotOnFailureListenerNC;
import com.crunchtime.ces.base.SmokeTestBase;
import com.crunchtime.ces.dialogs.ExportDialog;
import com.crunchtime.ces.helper.Waiter;
import com.crunchtime.ces.pages.inventory.inventoryOverview.PageInventoryOverview;
import com.crunchtime.ces.pages.inventory.inventoryOverview.physicalInventory.ViewPage;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;
import static org.testng.Assert.*;

@Listeners(ScreenshotOnFailureListenerNC.class)
@Test(groups = "NCPhysicalInventory")
public class PhysicalInventoryViewScreenTest extends SmokeTestBase {

	protected ViewPage viewPage;
	protected PageInventoryOverview pageNCInventoryOverview;
	protected ExportDialog exportDialog;

	@BeforeMethod
	public void setUp() throws Exception {
		super.setUp();
		this.pageNCInventoryOverview = PageFactory.initElements(driver, PageInventoryOverview.class);
		this.viewPage = PageFactory.initElements(driver, ViewPage.class);
		this.exportDialog = PageFactory.initElements(driver, ExportDialog.class);
	}

	@Test(groups = "NCPhysicalInventory")
	public void checkViewProcessTest() throws Exception {
		basePageNC.inventoryMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.viewProcessLink));
		pageNCInventoryOverview.viewProcessLink.click();
		Waiter.waitForElementToBeVisible(driver, viewPage.headerViewProcessPage);
		assertTrue(viewPage.headerViewProcessPage.isDisplayed(), "Physical Inventory for Post Period X is not displayed, so test failed.");
	}

	@Test(groups = "NCPhysicalInventory")
	public void checkViewEnterDataTest() throws Exception {
		basePageNC.inventoryMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.viewProcessLink));
		pageNCInventoryOverview.viewProcessLink.click();
		Waiter.waitFor(visibilityOf(viewPage.closeButton));
		boolean isInputFound;
		try {
			pageNCInventoryOverview.editCell.findElement(By.tagName("input"));
			isInputFound = true;
		} catch (Exception E) {
			isInputFound = false;
		}
		assertFalse(isInputFound);
	}

	@Test(groups = "NCPhysicalInventory")
	public void checkViewAndCloseTest() throws Exception {
		basePageNC.inventoryMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.viewProcessLink));
		pageNCInventoryOverview.viewProcessLink.click();
		Waiter.waitFor(visibilityOf(viewPage.closeButton));
		viewPage.closeButton.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.headerPhysicalInventory));
		assertTrue(pageNCInventoryOverview.headerPhysicalInventory.isDisplayed(), "Physical Inventory table is not displayed, so test failed.");
	}

	@Test(groups = "NCPhysicalInventory")
	public void checkExportPopUpAppearsTest() {
		basePageNC.inventoryMainMenuLink.click();
		Waiter.waitFor(visibilityOf(pageNCInventoryOverview.viewProcessLink));
		pageNCInventoryOverview.viewProcessLink.click();
		Waiter.waitFor(invisibilityOfElementLocated(PageBase.spinner));
		viewPage.exportIcon.click();
		Waiter.waitFor(visibilityOf(exportDialog.exportDialogHeader));
		assertTrue(exportDialog.exportDialogHeader.isDisplayed());
	}
}