package com.crunchtime.ces.test.sales.viewCustomerOrderSummary;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.database.generic.DBStmtNCGeneric;
import com.crunchtime.ces.helper.ActionsNC;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TC_ViewCustomerOrderSummary extends BaseTestNC {
	String dbConnectionString = "autorpt/autorpt@CTAUTO24006";
	String testSiteName = "autorpt.net-chef.local";
	String userID = "autorpt";
	String userPWD = "autorpt";
	String testLocationName = "#CCI_REPORT#";

	@BeforeMethod(alwaysRun = true)
	public void logIn() throws Exception {
		basePageNC.openNCTestSite(testSiteName);
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}

	@Test(groups = {"userGroupAccess"})
	public void userGroupAccessForViewCustomerOrderSummary() throws Exception {
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-Customer Order", "Summary");
		pageNCLogout.logoutNC();
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
		pageNavigationGeneric.menuLinkMouseOver("Sales", "");
		//check if it is existed
		boolean existFlag = ActionsNC.isElementPresent(basePageNC.viewCustomerOrderSummaryMenuLink);
		if (existFlag) {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Customer Order", "Summary");
			Assert.assertFalse(existFlag, "View Customer Summary Link Should be DISABLED if control is OFF, so test failed.");
		} else {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-Customer Order", "Summary");
			pageNCLogout.logoutNC();
			pageNCLogin.loginNC(userID, userPWD, testLocationName);
			pageNavigationGeneric.menuLinkMouseOver("Sales", "");
			boolean existFlagUpdated = ActionsNC.isElementPresent(basePageNC.viewCustomerOrderSummaryMenuLink);
			Assert.assertTrue(existFlagUpdated, "View Customer Summary Link Should be ENABLED if control is ON, so test failed.");
		}
	}
}
