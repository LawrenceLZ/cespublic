package com.crunchtime.ces.test.reports.reportsPurchasing;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.database.generic.DBStmtNCGeneric;
import com.crunchtime.ces.helper.ActionsNC;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TC_ReportsPurchasingPurchaseJournal extends BaseTestNC {
	private String dbConnectionString;
	private String testSiteName;
	private String userID;
	private String userPWD;
	private String testLocationName;

	private TC_ReportsPurchasingPurchaseJournal() {
		dbConnectionString = "autorpt/autorpt@CTAUTO24006";
		userID = "autorpt";
		userPWD = "autorpt";
		testLocationName = "#CCI_REPORT#";
		testSiteName = "autorpt.net-chef.local";
	}

	@BeforeMethod(alwaysRun = true)
	public void setup() throws Exception {
		basePageNC.openNCTestSite(testSiteName);
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}

	@Test(groups = {"userGroupAccess"})
	public void userGroupAccessForReportsPurchasingPurchaseJournal() throws Exception {
		DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "N", userID, "NC-View Reports", "Purchase Journal");
		pageNCLogout.logoutNC();
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
		pageNavigationGeneric.menuLinkMouseOver("Purchasing", "Reports");
		//check if it is existed
		boolean existFlag1 = ActionsNC.isElementPresent(basePageNC.reportsPurchasingPurchaseJournalMenuLink);
		pageNavigationGeneric.menuLinkMouseOver("Reports", "Purchasing");
		boolean existFlag2 = ActionsNC.isElementPresent(basePageNC.reportsPurchasingPurchaseJournalMenuLink);
		if (existFlag1 || existFlag2) {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Purchase Journal");
			Assert.assertFalse(existFlag1, "Purchasing --> Reports --> Purchase Journal Link Should be DISABLED if control is OFF, so test failed.");
			Assert.assertFalse(existFlag2, "Reports --> Purchasing --> Purchase Journal Link Should be DISABLED if control is OFF, so test failed.");
		} else {
			DBStmtNCGeneric.updateUserGroupAccess(dbConnectionString, "Y", userID, "NC-View Reports", "Purchase Journal");
			pageNCLogout.logoutNC();
			pageNCLogin.loginNC(userID, userPWD, testLocationName);
			pageNavigationGeneric.menuLinkMouseOver("Purchasing", "Reports");
			boolean existFlagUpdated1 = ActionsNC.isElementPresent(basePageNC.reportsPurchasingPurchaseJournalMenuLink);
			Assert.assertTrue(existFlagUpdated1, "Purchasing --> Reports --> Purchase Journal Link Should be ENABLED if control is ON, so test failed.");
			pageNavigationGeneric.menuLinkMouseOver("Reports", "Purchasing");
			boolean existFlagUpdated2 = ActionsNC.isElementPresent(basePageNC.reportsPurchasingPurchaseJournalMenuLink);
			Assert.assertTrue(existFlagUpdated2, "Reports --> Purchasing --> Purchase Journal Link Should be ENABLED if control is ON, so test failed.");
		}
	}
}
