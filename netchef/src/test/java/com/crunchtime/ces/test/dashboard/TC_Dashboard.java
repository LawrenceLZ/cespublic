package com.crunchtime.ces.test.dashboard;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.helper.ActionsNC;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

public class TC_Dashboard extends BaseTestNC {
	private static final String testSiteName = "autorpt.net-chef.local";
	private static final String dbConnectionString = "autorpt/autorpt@CTAUTO24006.WORLD";
	private static final String userID = "dash";
	private static final String userPWD = "dash";
	private static final String testLocationName = "204 PQ SANTA MONICA (DASHBOARD)";

	@BeforeMethod(alwaysRun = true)
	public void logIn() throws Exception {
		basePageNC.openNCTestSite(testSiteName);
		pageNCLogin.loginNC(userID, userPWD, testLocationName);
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}

	@Test(dataProvider = "TestWidgetsInFirefox")
	public void testWidgetLayoutInFirefox(String widgetName, String widgetSnapshot) throws Exception {
		pageDashboard.editLayoutButtonClick();
		Thread.sleep(1000);
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.visibilityOf(pageDashboard.configureWidgetGearIcons.get(0))
		);
		//int gearCnt = pageGeneric.getGenericWebElementsCount(pageDashboard.configureWidgetGearIcons);
		//System.out.println(gearCnt);
		pageDashboard.configureWidgetGearIcons.get(0).click();
		pageDashboard.selectWidgetComboBox.click();
		ActionsNC.setComboBoxDropdownByValue(driver, pageDashboard.selectWidgetComboBox, widgetName);
		pageDashboard.configureWidgetSaveBtn.click();
		Thread.sleep(5000);
		new WebDriverWait(driver, 60, 1000).until(
				ExpectedConditions.visibilityOf(pageDashboard.finishEditingBtn)
		);
		pageDashboard.finishEditingBtn.click();

		switch (widgetName) {
			case "Purchases by GL":
				Thread.sleep(5000);
				new WebDriverWait(driver, 60, 1000).until(
						ExpectedConditions.visibilityOf(pageDashboard.widgetShowLegendBtn)
				);
				pageDashboard.widgetShowLegendBtn.click();
				ActionsNC.takeScreenshot(driver, widgetSnapshot, driver.findElements(By.cssSelector("div[id *= 'container-'][class = 'x-container singlePanel x-box-item x-container-default x-layout-fit']")).get(0));
				ActionsNC.pDiff(widgetSnapshot);
				Thread.sleep(2000);
				break;
			case "Current Inventory Value by GL":
				Thread.sleep(5000);
				new WebDriverWait(driver, 60, 1000).until(
						ExpectedConditions.visibilityOf(pageDashboard.widgetShowLegendBtn)
				);
				pageDashboard.widgetShowLegendBtn.click();
				ActionsNC.takeScreenshot(driver, widgetSnapshot, driver.findElements(By.cssSelector("div[id *= 'container-'][class = 'x-container singlePanel x-box-item x-container-default x-layout-fit']")).get(0));
				ActionsNC.pDiff(widgetSnapshot);
				Thread.sleep(2000);
				break;
			default:
				Thread.sleep(5000);
				new WebDriverWait(driver, 60, 1000).until(
						ExpectedConditions.visibilityOf(pageDashboard.widgetMaxViewIcons.get(0))
				);

				pageDashboard.widgetMaxViewIcons.get(0).click();
				Thread.sleep(2000);

				//pageDashboard.getWasteTrendWidgetMaxBtns().get(1).click();
				//take a full screen shot
				ActionsNC.takeScreenshot(driver, widgetSnapshot, null);
				//pDiff the screen layout and data
				ActionsNC.pDiff(widgetSnapshot);

				Thread.sleep(2000);
				//assert the widget is in maximized view and working as expected
				int minViewIconsCnt = pageDashboard.widgetMinViewIcons.size();
				Assert.assertEquals(minViewIconsCnt, 1, "Failed as there should ONLY be ONE minimize icon for the this widget.");
				pageDashboard.widgetMinViewIcons.get(0).click();

				//sleep for 2 seconds to setup for the next test
				Thread.sleep(2000);
				break;
		}
	}

	@DataProvider(name = "TestWidgetsInFirefox")
	public String[][] widgetsArray() {
		return new String[][]{
				{"Daily Sales Trend", "dailySalesTrendLayout_Firefox"},
				{"Daily Labor Summary", "dailyLaborSummaryLayout_Firefox"},
				{"Inventory Value Trend", "inventoryValueTrendLayout_Firefox"},
				{"Top 10 Menu Mix Sales Items", "top10MenuMixSalesItemsLayout_Firefox"},
				{"Top 10 Actual vs. Theoretical Cost Items", "top10AvsTCostItemsLayout_Firefox"},
				{"Frequently Used Vendors", "frequentlyUsedVendorsLayout_Firefox"},
				{"Daypart Sales", "daypartSalesLayout_Firefox"},
				{"Purchases by GL", "purchasesByGLLayout_Firefox"},
				{"Today's Product Shortage Alerts", "todaysProductShortageAlertsLayout_Firefox"},
				//unable to test Current Inv by GL as data cannot be frozen (always show current date)
				//{"Current Inventory Value by GL", "currentInventoryValueByGLLayout_Firefox"},
				{"Recipe Waste", "recipeWasteLayout_Firefox"},
				{"Today's Daily Prep Alerts", "todaysDailyPrepAlertsLayout_Firefox"},
				{"Today's Labor Schedule", "todaysLaborScheduleLayout_Firefox"},
				{"WTD Labor Hours", "wtdLaborHoursLayout_Firefox"},
				{"WTD Waste Trend", "wtdWasteTrendLayout_Firefox"},
				{"Posting Summary", "postingSummaryLayout_Firefox"},
				{"Posting Dependency Checklist", "postingDependencyChecklistLayout_Firefox"}
		};
	}

	@Test
	public void testDashboardDataDBValidation() {

	}
}
