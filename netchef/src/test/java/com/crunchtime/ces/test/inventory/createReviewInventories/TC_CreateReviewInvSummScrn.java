package com.crunchtime.ces.test.inventory.createReviewInventories;

import com.crunchtime.ces.base.BaseTestNC;
import com.crunchtime.ces.helper.ActionsNC;
import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.*;

public class TC_CreateReviewInvSummScrn extends BaseTestNC {
	private static final String DB_CONNECTION_STRING = "AUTO/AUTO@CTAUTO24006.WORLD";
	private static final String TEST_SITE_NAME = "auto.net-chef.local";
	private static final String USER_ID = "INVEN";
	private static final String USER_PWD = "INVEN";
	private static final String TEST_LOCATION_NAME = "Chicago #332";

	@Parameters({"SMOKE"})
	@BeforeMethod(alwaysRun = true)
	public void logIn(@Optional(TEST_SITE_NAME) String siteName) throws Exception {
		basePageNC.openNCTestSite(siteName);
		pageNCLogin.loginNC(USER_ID, USER_PWD, TEST_LOCATION_NAME);
		basePageNC.navigateToCreateReviewInvSummScreen();
	}

	@AfterMethod(alwaysRun = true)
	public void logOut() throws Exception {
		pageNCLogout.logoutNC();
	}

	@Test
	public void testPhyInvSummScrnViewOption() throws Exception {
		ActionsNC.setTestMethodDescription("ces-5728", "Verify all the view options (e.g. min/max view, collapse/expand) for Physical Inventory summary screen.");
		int i = 0, y = 0;
		try {
			i++;
			pageCreateReviewInventories.phyInvSummScrnMinIcon.click();
			Waiter.waitForOneSecond();
		} catch (Exception e) {
			System.out.println("Physical Inventory Summary screen is not maximized upon open, so minimize icon is not displayed");
			y=i;
		}
		try {
			i++;
			Waiter.waitForElementToVanish(pageCreateReviewInventories.phyInvSummScrnMinIcon);
			pageCreateReviewInventories.phyInvSummScrnCollapseIcon.click();
			Waiter.waitForOneSecond();
		} catch (Exception e) {
			System.out.println("Physical Inventory Summary screen maximize icon is not visible after minimized");
			y=i;
		}
		try {
			i++;
			Waiter.waitForElementToVanish(pageCreateReviewInventories.phyInvSummScrnCollapseIcon);
			pageCreateReviewInventories.phyInvSummScrnExpandIcon.click();
			Waiter.waitForOneSecond();
		} catch (Exception e) {
			System.out.println("Physical Inventory Summary screen expand icon is not visible after collapsed.");
			y=i;
		}
		try {
			i++;
			Waiter.waitForElementToVanish(pageCreateReviewInventories.phyInvSummScrnExpandIcon);
			pageCreateReviewInventories.phyInvSummScrnMaxIcon.click();
		} catch (Exception e) {
			System.out.println("Physical Inventory Summary screen cannot be maximized after minimized.");
			y=i;
		}
		System.out.println(i);
		Assert.assertTrue(y == 0, "Failed for view option checked at step " + y);
	}

	/**
	 * ces-7225
	 */
	@Test
	public void testPhyInvSummScrnColumnViewOption() throws Exception{
		ActionsNC.setTestMethodDescription("ces-7225", "Verify rearrange column view option.");
		try {
			Actions moveColumnAction = new Actions(driver);
			moveColumnAction.dragAndDrop(pageCreateReviewInventories.phyInvSummScrnDateColumnHdr, pageCreateReviewInventories.phyInvSummScrnProcessColumnHdr).build().perform();
		} catch (Exception e){
			e.printStackTrace();
		}
		String firstColumnHdrLabel = pageCreateReviewInventories.phyInvSummScrnGrid.findElement(By.cssSelector("div[class*='column-header-first']")).getText();
		Assert.assertTrue(firstColumnHdrLabel.contains("Date"), "Failed, since attempting to move columns did not create exception");
	}

	/**
	 * ces-7225
	 */
	@Test
	public void testPhyInvSummScrnColumnHdrTriggerOptions() throws Exception{
		ActionsNC.setTestMethodDescription("ces-7225", "Verify Physical Inventory summary screen enable/disable column options");
		Actions moveToColumnHdrTriggerAction = new Actions(driver);
		moveToColumnHdrTriggerAction.moveToElement(pageCreateReviewInventories.phyInvSummScrnDateColumnHdr);
		moveToColumnHdrTriggerAction.click(pageCreateReviewInventories.phyInvSummScrnDateColumnHdrTrigger).build().perform();
		Waiter.waitForOneSecond();
		for (WebElement triggerControl : pageCreateReviewInventories.phyInvSummScrnColHdrActiveTriggerCtrls){
			String triggerControlLabel = triggerControl.getText();
			Assert.assertFalse(triggerControlLabel.equals("Columns"), "Failed, as Change Column Visibility feature is NOT disabled");
		}
	}

	/**
	 * ces-7225
	 */
	@Test
	public void testPhyInvSummScrnColumnResize() throws Exception{
		ActionsNC.setTestMethodDescription("ces-7225", "Verify Physical Inventory summary screen resize column action");
		try {
			ActionsNC.resizeGridColumnByOffset(driver, pageCreateReviewInventories.phyInvSummScrnDateColumnHdr, 10);
		} catch (Exception e){
			e.printStackTrace();
			Actions moveToColumnHdrTriggerAction = new Actions(driver);
			moveToColumnHdrTriggerAction.moveToElement(pageCreateReviewInventories.phyInvSummScrnDateColumnHdr);
			moveToColumnHdrTriggerAction.click(pageCreateReviewInventories.phyInvSummScrnDateColumnHdrTrigger).build().perform();
			Waiter.waitForOneSecond();
			for (WebElement triggerControl : pageCreateReviewInventories.phyInvSummScrnColHdrActiveTriggerCtrls){
				String triggerControlLabel = triggerControl.getText();
				if (triggerControlLabel.equals("Reset Columns")){
					triggerControl.click();
				}
			}
			Assert.assertTrue(false, "Failed, since the try resize column action is failed");
		}
		Actions moveToColumnHdrTriggerAction = new Actions(driver);
		moveToColumnHdrTriggerAction.moveToElement(pageCreateReviewInventories.phyInvSummScrnDateColumnHdr);
		moveToColumnHdrTriggerAction.click(pageCreateReviewInventories.phyInvSummScrnDateColumnHdrTrigger).build().perform();
		Waiter.waitForOneSecond();
		for (WebElement triggerControl : pageCreateReviewInventories.phyInvSummScrnColHdrActiveTriggerCtrls){
			String triggerControlLabel = triggerControl.getText();
			if (triggerControlLabel.equals("Reset Columns")){
				triggerControl.click();
			}
		}
	}


}