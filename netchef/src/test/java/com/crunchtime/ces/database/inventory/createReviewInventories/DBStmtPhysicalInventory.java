package com.crunchtime.ces.database.inventory.createReviewInventories;

public class DBStmtPhysicalInventory {

	public static String getStmtCountInvRowsOnlyWithActivity(String dbSessionString) {
		return "SELECT COUNT(*) row_count\n" +
				"  FROM wb_dtl_inventory\n" +
				" WHERE session_string = '"+ dbSessionString +"'\n" +
				"   AND (QUANTITY_PHYSICAL != 0 OR VALUE_PHYSICAL != 0 OR QUANTITY_BOOK != 0 OR\n" +
				"       VALUE_BOOK != 0 OR QUANTITY_CUSTOMER_TRANSIT != 0 OR\n" +
				"       VALUE_IN_TRANSIT != 0 OR QUANTITY_BEGINNING != 0 OR\n" +
				"       VALUE_BEGINNING != 0 OR QUANTITY_RECEIVE != 0 OR VALUE_RECEIVE != 0 OR\n" +
				"       QUANTITY_POS != 0 OR VALUE_POS != 0 OR QUANTITY_ISSUE != 0 OR\n" +
				"       VALUE_ISSUE != 0 OR QUANTITY_BREAKAGE != 0 OR VALUE_BREAKAGE != 0 OR\n" +
				"       QUANTITY_OTHER != 0 OR VALUE_OTHER != 0 OR QUANTITY_TRANSFERS != 0 OR\n" +
				"       VALUE_TRANSFERS != 0 OR QUANTITY_PRODUCTION_CONSUMED != 0 OR\n" +
				"       VALUE_PRODUCTION_CONSUMED != 0 OR QUANTITY_CO_ISSUES != 0 OR\n" +
				"       VALUE_CO_ISSUES != 0 OR QUANTITY_BUFFET_CONSUMED != 0 OR\n" +
				"       VALUE_BUFFET_CONSUMED != 0 OR QUANTITY_ON_CUSTOMER_ORDER != 0 OR\n" +
				"       QUANTITY_ON_VENDOR_ORDER != 0)\n";
	}

	public static String getStmtSelectLastValidPostDateToCopy(String testLocationName){
		return "SELECT to_char(post_date,'mm/dd/yyyy') post_date\n" +
				"  FROM (SELECT his.post_date\n" +
				"          FROM t_hdr_inventory_schedule his, t_location loc\n" +
				"         WHERE his.location_id = loc.location_id\n" +
				"           AND his.inventory_type != 'FINAL'\n" +
				"           AND his.inventory_status = 3\n" +
				"           AND upper(loc.location_name) = upper('"+ testLocationName +"')\n" +
				"         GROUP BY his.post_date\n" +
				"         ORDER BY his.post_date DESC)\n" +
				" WHERE rownum = 1";
	}

	public static String getStmtSelectFirstCopyToPostDate(String testLocationName, String copyFromPostDate){
		return "SELECT to_char(inventory_period_date, 'mm/dd/yyyy') inventory_period_date\n" +
				"  FROM (SELECT cinv.inventory_period_date\n" +
				"          FROM t_calendar_inventory cinv, t_location loc\n" +
				"         WHERE cinv.location_id = loc.location_id\n" +
				"           AND cinv.calendar_type = 2\n" +
				"           AND cinv.inventory_period_date !=\n" +
				"               to_date('"+ copyFromPostDate +"', 'mm/dd/yyyy')\n" +
				"           AND upper(loc.location_name) = upper('"+ testLocationName +"')\n" +
				"           AND cinv.inventory_period_date NOT IN\n" +
				"               (SELECT his.post_date\n" +
				"                  FROM t_hdr_inventory_schedule his\n" +
				"                 WHERE his.location_id = loc.location_id\n" +
				"                   AND his.inventory_type = 'FINAL'\n" +
				"                   AND his.inventory_status != 1)\n" +
				"           AND cinv.inventory_period_date NOT IN\n" +
				"               (SELECT ihad.post_inv_scheduled_date\n" +
				"                  FROM t_inv_hdr_adjustment ihad\n" +
				"                 WHERE ihad.location_id = loc.location_id\n" +
				"                   AND ihad.inventory_application = 'EM')\n" +
				"         ORDER BY cinv.inventory_period_date ASC)\n" +
				" WHERE rownum = 1\n";
	}

	public static String getStmtSelectFirstScheduledFinalForPostDate(String testLocationName){
		return "SELECT to_char(post_date, 'mm/dd/yyyy') post_date\n" +
				"  FROM (SELECT his.post_date\n" +
				"          FROM t_hdr_inventory_schedule his, t_location loc\n" +
				"         WHERE his.location_id = loc.location_id\n" +
				"           AND his.inventory_status NOT IN (2, 3)\n" +
				"           AND his.inventory_type = 'FINAL'\n" +
				"           AND upper(loc.location_name) = upper('"+ testLocationName +"')\n" +
				"         ORDER BY his.post_date ASC)\n" +
				" WHERE rownum = 1\n";
	}

	public static String getStmtDeleteAddedScheduleInventory(String testLocationName, String toPostDate){
		return "DELETE FROM t_hdr_inventory_schedule his\n" +
				" WHERE his.location_id =\n" +
				"       (SELECT loc.location_id\n" +
				"          FROM t_location loc\n" +
				"         WHERE upper(loc.location_name) = upper('"+ testLocationName +"'))\n" +
				"   AND his.inventory_type != 'FINAL'\n" +
				"   AND trunc(his.last_touch_date) = trunc(SYSDATE)\n" +
				"   AND his.post_date = to_date('"+ toPostDate +"', 'mm/dd/yyyy')";
	}

	public static String getStmtSelectLastCopiedScheduledInventory(String testLocationName, String copyToPostDate){
		return "SELECT inv_type\n" +
				"  FROM (SELECT decode(his.inventory_type,\n" +
				"                      'ALL',\n" +
				"                      'All',\n" +
				"                      'CAT',\n" +
				"                      'Category',\n" +
				"                      'SUBCAT',\n" +
				"                      'Subcategory',\n" +
				"                      'MICROCAT',\n" +
				"                      'Microcategory',\n" +
				"                      'FINAL',\n" +
				"                      'Final for Post',\n" +
				"                      'STORAGE',\n" +
				"                      'Storage Location',\n" +
				"                      'TEMPLATE',\n" +
				"                      'Template',\n" +
				"                      'UDC',\n" +
				"                      'User Defined Category') inv_type\n" +
				"          FROM t_hdr_inventory_schedule his, t_location loc\n" +
				"         WHERE his.location_id = loc.location_id\n" +
				"           AND upper(loc.location_name) = upper('"+ testLocationName +"')\n" +
				"           AND his.post_date = to_date('"+ copyToPostDate +"', 'mm/dd/yyyy')\n" +
				"           AND his.inventory_status = 1\n" +
				"           AND his.inventory_type != 'FINAL'\n" +
				"           AND trunc(his.last_touch_date) = trunc(SYSDATE)\n" +
				"         ORDER BY his.last_touch_date DESC)\n" +
				" WHERE rownum = 1";
	}

	public static String getStmtUpdateFinalForPostScheduleStatus(String testLocationName, String targetPostDate, String status){
		String targetStatus;
		switch (status.toLowerCase()){
			case "scheduled":
				targetStatus = "1";
				break;
			case "awaiting review":
				targetStatus = "2";
				break;
			case "complete":
				targetStatus = "3";
				break;
			case "imported":
				targetStatus = "5";
				break;
			default:
				targetStatus = "1";
				break;
		}
		return "UPDATE t_hdr_inventory_schedule his\n" +
				"   SET his.inventory_status = '"+ targetStatus +"'\n" +
				" WHERE his.location_id =\n" +
				"       (SELECT loc.location_id\n" +
				"          FROM t_location loc\n" +
				"         WHERE upper(loc.location_name) = upper('"+ testLocationName +"'))\n" +
				"   AND his.inventory_type = 'FINAL'\n" +
				"   AND his.post_date = to_date('"+ targetPostDate +"', 'mm/dd/yyyy')";
	}

	public static String getStmtUpdateScheduledInvStatusForPostPeriod (String testLocationName, String targetPostDate, String targetScheduledDate, String status){
		String targetStatus;
		switch (status.toLowerCase()){
			case "scheduled":
				targetStatus = "1";
				break;
			case "awaiting review":
				targetStatus = "2";
				break;
			case "complete":
				targetStatus = "3";
				break;
			case "imported":
				targetStatus = "5";
				break;
			default:
				targetStatus = "1";
				break;
		}
		return "UPDATE t_hdr_inventory_schedule his\n" +
				"   SET his.inventory_status = '"+ targetStatus +"'\n" +
				" WHERE his.location_id =\n" +
				"       (SELECT loc.location_id\n" +
				"          FROM t_location loc\n" +
				"         WHERE upper(loc.location_name) = upper('"+ testLocationName +"'))\n" +
				"   AND his.inventory_type != 'FINAL'\n" +
				"   AND his.post_date = to_date('"+ targetPostDate +"', 'mm/dd/yyyy')\n" +
				"   AND his.inventory_date = to_date('"+ targetScheduledDate +"', 'mm/dd/yyyy')";
	}
}