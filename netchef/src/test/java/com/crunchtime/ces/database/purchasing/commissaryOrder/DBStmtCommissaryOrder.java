package com.crunchtime.ces.database.purchasing.commissaryOrder;

import org.testng.Assert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DBStmtCommissaryOrder {





	/**
	 * DO NOT USE THE METHOD BELOW
	 * Obsolete methods, only here for supporting existing method calls
	 */
	private static final String root = System.getProperty("user.dir");
	//Base folder name co-relates to test package name
	private static final String dbResourcesBaseClassName = "DBStmtCommissaryOrder";
	//file name co-relates to database resource sql file name
	public static final String getFilterByCategoryDropdownFileName = "Validate_FILTER_BY_CATEGORY_DDOWN";

	//get filter by category dropdown list
	public static void getFilterByCategoryDropdown(String dbConnection, String filterByCategoryRadioBtn, String orderLocationName, String supplyLocationName) throws IOException, InterruptedException {
		Process p;
		try {
			String line;
			//******ONLY need to define sqlMethodFileName******
			String sqlMethodFileName = getFilterByCategoryDropdownFileName;

			//root, dbResourcesBasedir and basedir dir stays the same
			String basedir = root + "\\src\\test\\resources\\Database\\" + dbResourcesBaseClassName + "\\" + sqlMethodFileName;

			//******ALSO need to build the sqlPlus cmd string******
			String sqlCmd = "cmd /c exit|sqlplus -L " + dbConnection + " @" + basedir + " " + basedir + ".txt \"" + filterByCategoryRadioBtn + "\" \"" + orderLocationName + "\" \"" + supplyLocationName + "\"";

			p = Runtime.getRuntime().exec(sqlCmd);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = reader.readLine()) != null) {
				//print line is used for debugging
				System.out.println(line);
				boolean errOne;
				errOne = line.contains("ERROR");
				boolean errTwo;
				errTwo = line.contains("unable");
				Assert.assertFalse(errOne || errTwo, line);
			}
			reader.close();
			System.out.println("Done.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(1000);
	}
}
