package com.crunchtime.ces.database.generic;

import org.testng.Assert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DBStmtNCGeneric {

	public static String getStmtUpdateUserGroupAccess(String accessFlag, String userId, String screenName, String controlName) {
		return "UPDATE t_user_group_access usrga\n" +
				"   SET usrga.access_allowed = upper('" + accessFlag + "')\n" +
				" WHERE (usrga.user_group_pk, usrga.user_access_pk) IN\n" +
				"       (SELECT usr.user_group_pk, usra.user_access_pk\n" +
				"          FROM t_user usr, t_user_access usra, t_screen scrn, t_control ctrl\n" +
				"         WHERE usra.screen_id = scrn.screen_id\n" +
				"           AND usra.control_id = ctrl.control_id\n" +
				"           AND upper(usr.user_id) = upper('" + userId + "')\n" +
				"           AND upper(scrn.screen_name) = upper('" + screenName + "')\n" +
				"           AND upper(ctrl.control_name) = upper('" + controlName + "'))\n";
	}











	/**
	 * DO NOT USE THE METHOD BELOW
	 * Obsolete methods, only here for supporting existing method calls
	 */
	private static final String root = System.getProperty("user.dir");
	//Base folder name co-relates to test package name
	private static final String dbResourcesBaseClassName = "DBStmtNCGeneric";
	//file name co-relates to database resource sql file name
	private static final String updateUserGroupAccessFileName = "updateUserGroupAccess";

	public static void updateUserGroupAccess(String dbConnection, String accessFlag, String userID, String scrnName, String ctrlName) throws IOException, InterruptedException {
		Process p;
		try {
			String line;
			//******ONLY need to define sqlMethodFileName******
			String sqlMethodFileName = updateUserGroupAccessFileName;

			//root, dbResourcesBasedir and basedir dir stays the same
			String basedir = root + "\\src\\test\\resources\\Database\\" + dbResourcesBaseClassName + "\\" + sqlMethodFileName;

			//******ALSO need to build the sqlPlus cmd string******
			String sqlCmd = "cmd /c exit|sqlplus -L " + dbConnection + " @" + basedir + " \"" + accessFlag + "\" \"" + userID + "\" \"" + scrnName + "\" \"" + ctrlName + "\"";

			p = Runtime.getRuntime().exec(sqlCmd);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = reader.readLine()) != null) {
				//print line is used for debugging
				System.out.println(line);
				boolean errOne;
				errOne = line.contains("ERROR");
				boolean errTwo;
				errTwo = line.contains("unable");
				Assert.assertFalse(errOne || errTwo, line);
			}
			reader.close();
			System.out.println("Done.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(1000);
	}
}