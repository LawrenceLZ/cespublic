package com.crunchtime.ces.database.purchasing.vendorOrder;

import org.testng.Assert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DBStmtVendorOrder {

	public static String getStmtSelectRowCntFromVenBidQueueTbl(){
		return "SELECT COUNT(*) row_count FROM t_vendor_bid_queue vbq";
	}





















	/**
	 * DO NOT USE THE METHOD BELOW
	 * Obsolete methods, only here for supporting existing method calls
	 */
	private static final String root = System.getProperty("user.dir");
	//Base folder name co-relates to test package name
	private static final String dbResourcesBaseClassName = "DBStmtVendorOrder";
	//file name co-relates to database resource sql file name
	public static final String getUpdateVOTransNumberSqlFileName = "updateVOTransNumber";
	public static final String getCreateRowsInVenBidQueueCES4127SqlFileName = "createRowsInVenBidQueueCES4127";

	public static void updateVOTransNumber(String dbConnection, String toTransactionNumber, String fromTransactionNumber) throws IOException, InterruptedException {
		Process p;
		try {
			String line;
			//******ONLY need to define sqlMethodFileName******
			String sqlMethodFileName = getUpdateVOTransNumberSqlFileName;

			//root, dbResourcesBasedir and basedir dir stays the same
			String basedir = root + "\\src\\test\\resources\\Database\\" + dbResourcesBaseClassName + "\\" + sqlMethodFileName;

			//******ALSO need to build the sqlPlus cmd string******
			String sqlCmd = "cmd /c exit|sqlplus -L " + dbConnection + " @" + basedir + " \"" + toTransactionNumber + "\"" + " \"" + fromTransactionNumber + "\"";

			p = Runtime.getRuntime().exec(sqlCmd);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = reader.readLine()) != null) {
				//print line is used for debugging
				System.out.println(line);
				boolean errOne;
				errOne = line.contains("ERROR");
				boolean errTwo;
				errTwo = line.contains("unable");
				Assert.assertFalse(errOne || errTwo, line);
			}
			reader.close();
			System.out.println("Done.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(1000);
	}
	/**
	 * DO NOT USE THE METHOD BELOW
	 * Obsolete methods, only here for supporting existing method calls
	 */
	public static void createRowsInVenBidQueueCES4127(String dbConnection, int rowNum, String companyProductName, String vendorName, String locationName) throws IOException, InterruptedException {
		Process p;
		try {
			String line;
			//******ONLY need to define sqlMethodFileName******
			String sqlMethodFileName = getCreateRowsInVenBidQueueCES4127SqlFileName;

			//root, dbResourcesBasedir and basedir dir stays the same
			String basedir = root + "\\src\\test\\resources\\Database\\" + dbResourcesBaseClassName + "\\" + sqlMethodFileName;

			//******ALSO need to build the sqlPlus cmd string******
			String sqlCmd = "cmd /c exit|sqlplus -L " + dbConnection + " @" + basedir + " \"" + rowNum + "\" \"" + companyProductName + "\" \"" + vendorName + "\" \"" + locationName + "\"";

			p = Runtime.getRuntime().exec(sqlCmd);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = reader.readLine()) != null) {
				//print line is used for debugging
				System.out.println(line);
				boolean errOne;
				errOne = line.contains("ERROR");
				boolean errTwo;
				errTwo = line.contains("unable");
				Assert.assertFalse(errOne || errTwo, line);
			}
			reader.close();
			System.out.println("Done.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(1000);
	}
}