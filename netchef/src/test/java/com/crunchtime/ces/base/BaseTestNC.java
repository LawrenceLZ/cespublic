package com.crunchtime.ces.base;

import com.crunchtime.ces.driver.DriverOfActiveThread;
import com.crunchtime.ces.pages.administration.administrationOverview.PageAdministrationOverview;
import com.crunchtime.ces.pages.administration.manageDailyNews.PageManageDailyNews;
import com.crunchtime.ces.pages.administration.postCurrentPeriod.PagePostCurrentPeriod;
import com.crunchtime.ces.pages.administration.undoMenuMixFileProcessing.PageUndoMenuMixFileProcessing;
import com.crunchtime.ces.pages.dashboard.PageDashboard;
import com.crunchtime.ces.pages.inventory.adjustment.PageCreateInventoryAdjustment;
import com.crunchtime.ces.pages.inventory.adjustment.PageCreateRecipeAdjustment;
import com.crunchtime.ces.pages.inventory.createReviewInventories.PageCreateReviewInventories;
import com.crunchtime.ces.pages.inventory.inventoryOverview.PageInventoryOverview;
import com.crunchtime.ces.pages.inventory.locationTransfer.PageCreateLocationTransfer;
import com.crunchtime.ces.pages.inventory.locationTransfer.PageRecentLocationTransfers;
import com.crunchtime.ces.pages.inventory.updateCountUnits.PageUpdateCountUnits;
import com.crunchtime.ces.pages.inventory.updateStorageLocations.PageUpdateStorageLocations;
import com.crunchtime.ces.pages.inventory.updateStorageSequences.PageUpdateStorageSequences;
import com.crunchtime.ces.pages.inventory.adjustment.PageViewRecentAdjustments;
import com.crunchtime.ces.pages.labor.employeeMaintenance.PageCreateEmployee;
import com.crunchtime.ces.pages.labor.laborSchedule.PageCreateLaborSchedule;
import com.crunchtime.ces.pages.labor.employeeMaintenance.PageEmployeeMaintenance;
import com.crunchtime.ces.pages.labor.idealStaffingTemplates.PageIdealStaffingTemplates;
import com.crunchtime.ces.pages.labor.laborOverview.PageLaborOverview;
import com.crunchtime.ces.pages.labor.laborSummary.PageLaborSummary;
import com.crunchtime.ces.pages.labor.positions.PagePositions;
import com.crunchtime.ces.pages.labor.laborSchedule.PageRecentLaborSchedules;
import com.crunchtime.ces.pages.labor.stations.PageStations;
import com.crunchtime.ces.pages.login.PageNCLogin;
import com.crunchtime.ces.pages.logout.PageNCLogout;
import com.crunchtime.ces.pages.purchasing.vendorOrder.PageCreateVendorOrder;
import com.crunchtime.ces.pages.purchasing.vendorOrder.PageRecentVendorOrders;
import com.crunchtime.ces.pages.purchasing.vendorReturns.PageVendorReturns;
import com.crunchtime.ces.pages.purchasing.viewVendorProfiles.PageViewVendorProfiles;
import com.crunchtime.ces.pages.production.buffetConsumption.PageBuffetConsumption;
import com.crunchtime.ces.pages.production.dailyPrep.PageCreateDailyPrep;
import com.crunchtime.ces.pages.production.menuMatrix.PageMenuMatrix;
import com.crunchtime.ces.pages.production.productionOverview.PageProductionOverview;
import com.crunchtime.ces.pages.production.productionPlanning.PageProductionPlanning;
import com.crunchtime.ces.pages.production.dailyPrep.PageRecentDailyPrep;
import com.crunchtime.ces.pages.purchasing.commissaryOrder.PageCreateCommissaryOrder;
import com.crunchtime.ces.pages.purchasing.masterOrder.PageCreateMasterOrder;
import com.crunchtime.ces.pages.purchasing.purchaseByInvoice.PageCreatePurchasebyInvoice;
import com.crunchtime.ces.pages.purchasing.purchasingOverview.PagePurchasingOverview;
import com.crunchtime.ces.pages.purchasing.commissaryOrder.PageRecentCommissaryOrders;
import com.crunchtime.ces.pages.purchasing.purchaseByInvoice.PageRecentPurchasebyInvoices;
import com.crunchtime.ces.pages.reports.reportsInventory.*;
import com.crunchtime.ces.pages.reports.reportsPurchasing.*;
import com.crunchtime.ces.pages.sales.salesTransaction.PageCreateSalesTransaction;
import com.crunchtime.ces.pages.sales.manageSalesForecasts.PageManageSalesForecasts;
import com.crunchtime.ces.pages.sales.salesTransaction.PageRecentSalesTransactions;
import com.crunchtime.ces.pages.sales.salesOverview.PageSalesOverview;
import com.crunchtime.ces.pages.sales.viewCustomerOrderSummary.PageViewCustomerOrderSummary;
import com.crunchtime.ces.pages.administration.unpostPeriod.PageUnpostPeriod;
import com.crunchtime.ces.pages.administration.updateSuggestedOrderSetup.PageUpdateSuggestedOrderSetup;
import com.crunchtime.ces.pages.administration.updateTemplates.PageUpdateTemplates;
import com.crunchtime.ces.pages.administration.uploadDownloadFiles.PageUploadDownloadFiles;
import com.crunchtime.ces.pages.administration.viewEditBudgetData.PageViewEditBudgetData;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Reporter;
import org.testng.annotations.*;

import java.net.MalformedURLException;
import java.net.URL;

@Listeners(ScreenshotOnFailureListenerNC.class)
public class BaseTestNC {
	private static final String ESCAPE_PROPERTY = "org.uncommons.reportng.escape-output";
	protected WebDriver driver;

	/**
	 * Generic actions
	 */
	public PageNavigationGeneric pageNavigationGeneric;
	public BasePageNC basePageNC;
	public PageNCLogin pageNCLogin;
	public PageNCLogout pageNCLogout;

	/**
	 * Dashboard page module
	 */
	public PageDashboard pageDashboard;

	/**
	 * Purchasing module
	 */
	//Purchasing Overview page module
	public PagePurchasingOverview pagePurchasingOverview;
	//Commissary Order module
	public PageCreateCommissaryOrder pageCreateCommissaryOrder;
	public PageRecentCommissaryOrders pageRecentCommissaryOrders;
	//Vendor Order module
	public PageCreateVendorOrder pageCreateVendorOrder;
	public PageRecentVendorOrders pageRecentVendorOrders;
	//Purchase By Invoice module
	public PageCreatePurchasebyInvoice pageCreatePurchasebyInvoice;
	public PageRecentPurchasebyInvoices pageRecentPurchasebyInvoices;
	//Master Order module
	public PageCreateMasterOrder pageCreateMasterOrder;
	//Vendor Returns page module
	public PageVendorReturns pageVendorReturns;
	//View Vendor Profiles page module
	public PageViewVendorProfiles pageViewVendorProfiles;

	/**
	 * Inventory module
	 */
	//Inventory Overview page module
	public PageInventoryOverview pageInventoryOverview;
	//Create/Review Inventories page module
	public PageCreateReviewInventories pageCreateReviewInventories;
	//Location Transfer module
	public PageCreateLocationTransfer pageCreateLocationTransfer;
	public PageRecentLocationTransfers pageRecentLocationTransfers;
	//Inventory and Recipe Adjustment module
	public PageCreateInventoryAdjustment pageCreateInventoryAdjustment;
	public PageCreateRecipeAdjustment pageCreateRecipeAdjustment;
	public PageViewRecentAdjustments pageViewRecentAdjustments;
	//Update Storage Locations page module
	public PageUpdateStorageLocations pageUpdateStorageLocations;
	//Update Storage Sequences page module
	public PageUpdateStorageSequences pageUpdateStorageSequences;
	//Update Count Units page module
	public PageUpdateCountUnits pageUpdateCountUnits;

	/**
	 * Sales module generic actions
	 */
	//Sales Overview page module
	public PageSalesOverview pageSalesOverview;
	//Sales Transaction module
	public PageCreateSalesTransaction pageCreateSalesTransaction;
	public PageRecentSalesTransactions pageRecentSalesTransactions;
	//View Customer Order Summary page module
	public PageViewCustomerOrderSummary pageViewCustomerOrderSummary;
	//Manage Sales Forecasts page module
	public PageManageSalesForecasts pageManageSalesForecasts;

	/**
	 * Production module
	 */
	//Production Overview page module
	public PageProductionOverview pageProductionOverview;
	//Production Planning page module
	public PageProductionPlanning pageProductionPlanning;
	//Daily Prep module
	public PageCreateDailyPrep pageCreateDailyPrep;
	public PageRecentDailyPrep pageRecentDailyPrep;
	//Buffet Consumption page module
	public PageBuffetConsumption pageBuffetConsumption;
	//Menu Matrix page module
	public PageMenuMatrix pageMenuMatrix;

	/**
	 * Labor module
	 */
	//Labor Overview page module
	public PageLaborOverview pageLaborOverview;
	//Labor Summary page module
	public PageLaborSummary pageLaborSummary;
	//Labor Schedule module
	public PageCreateLaborSchedule pageCreateLaborSchedule;
	public PageRecentLaborSchedules pageRecentLaborSchedules;
	//Employee Maintenance module
	public PageCreateEmployee pageCreateEmployee;
	public PageEmployeeMaintenance pageEmployeeMaintenance;
	//Ideal Staffing Templates page module
	public PageIdealStaffingTemplates pageIdealStaffingTemplates;
	//Stations page module
	public PageStations pageStations;
	//Positions page module
	public PagePositions pagePositions;

	/**
	 *
	 *
	 * Reports module generic actions
	 *
	 *
	 */
	/**
	 * Purchasing Report module
	 */
	public PageReportsPurchasingContractedVendors pageReportsPurchasingContractedVendors;
	public PageReportsPurchasingPurchaseDetail pageReportsPurchasingPurchaseDetail;
	public PageReportsPurchasingPurchaseJournal pageReportsPurchasingPurchaseJournal;
	public PageReportsPurchasingPurchasesByGL pageReportsPurchasingPurchasesByGL;
	public PageReportsPurchasingTorg29 pageReportsPurchasingTorg29;
	public PageReportsPurchasingConsolidatedCommodityPricingTrends pageReportsPurchasingConsolidatedCommodityPricingTrends;
	public PageReportsPurchasingConsolidatedPurchasesByGL pageReportsPurchasingConsolidatedPurchasesByGL;
	public PageReportsPurchasingConsolidatedVendorOrders pageReportsPurchasingConsolidatedVendorOrders;
	/**
	 * Inventory Report module
	 */
	public PageReportsInventoryActualCost pageReportsInventoryActualCost;
	public PageReportsInventoryActualTheoreticalCost pageReportsInventoryActualTheoreticalCost;
	public PageReportsInventoryAdjustmentSummary pageReportsInventoryAdjustmentSummary;
	public PageReportsInventoryBookingJournal pageReportsInventoryBookingJournal;
	public PageReportsInventoryCostAnalysis pageReportsInventoryCostAnalysis;
	public PageReportsInventoryProfitLoss pageReportsInventoryProfitLoss;

	/**
	 * Administration module
	 */
	//Administration Overview page module
	public PageAdministrationOverview pageAdministrationOverview;
	//Post Current Period page module
	public PagePostCurrentPeriod pagePostCurrentPeriod;
	//Upload/Download Files page module
	public PageUploadDownloadFiles pageUploadDownloadFiles;
	//Unpost Period page module
	public PageUnpostPeriod pageUnpostPeriod;
	//Undo Menu Mix File Processing page module
	public PageUndoMenuMixFileProcessing pageUndoMenuMixFileProcessing;
	//Manage Daily News page module
	public PageManageDailyNews pageManageDailyNews;
	//View/Edit Budget Data page module
	public PageViewEditBudgetData pageViewEditBudgetData;
	//Update Suggested Order Setup page module
	public PageUpdateSuggestedOrderSetup pageUpdateSuggestedOrderSetup;
	//Update Templates page module
	public PageUpdateTemplates pageUpdateTemplates;

	@Parameters({"IP", "BROWSER"})
	@BeforeClass(alwaysRun = true)
	public void beforeClass(@Optional("localhost") String ip, @Optional("INTERNETEXPLORER") String browser) throws MalformedURLException {
		//set the property to enable customized html log report
		System.setProperty(ESCAPE_PROPERTY, "false");
		DesiredCapabilities capabilities = null;
		switch (browser.toUpperCase()) {
			case "FIREFOX":
				capabilities = DesiredCapabilities.firefox();
//                //run through a proxy
//                String PROXY = "5.101.130.95:8080";
//                org.openqa.selenium.Proxy proxy = new org.openqa.selenium.Proxy();
//                proxy
//                        //.setHttpProxy(PROXY)
//                        //.setFtpProxy(PROXY)
//                        .setSslProxy(PROXY);
//                //set proxy capability
//                capabilities.setCapability(CapabilityType.PROXY, proxy);
				capabilities.setJavascriptEnabled(true);
				capabilities.setPlatform(Platform.ANY);
				break;
			case "INTERNETEXPLORER":
				capabilities = DesiredCapabilities.internetExplorer();
				capabilities.setJavascriptEnabled(true);
				capabilities.setPlatform(Platform.ANY);
				break;
			case "CHROME":
				capabilities = DesiredCapabilities.chrome();
				capabilities.setJavascriptEnabled(true);
				capabilities.setPlatform(Platform.ANY);
				break;
			case "SAFARI":
				capabilities = DesiredCapabilities.safari();
				capabilities.setJavascriptEnabled(true);
				capabilities.setPlatform(Platform.ANY);
				break;
			case "PHANTOMJS":
				capabilities = DesiredCapabilities.phantomjs();
				capabilities.setJavascriptEnabled(true);
				capabilities.setPlatform(Platform.ANY);
				capabilities.setCapability("takesScreenshot", true);
				break;
		}

		DriverOfActiveThread.setDriver(new Augmenter().augment(new RemoteWebDriver(new URL("http://" + ip + ":5555/wd/hub"), capabilities)));
		this.driver = DriverOfActiveThread.getDriver();

		/**
		 * Generic actions
		 */
		this.pageNavigationGeneric = PageFactory.initElements(this.driver, PageNavigationGeneric.class);
		this.basePageNC = PageFactory.initElements(this.driver, BasePageNC.class);
		this.pageNCLogin = PageFactory.initElements(this.driver, PageNCLogin.class);
		this.pageNCLogout = PageFactory.initElements(this.driver, PageNCLogout.class);

		/**
		 *
		 *
		 * Dashboard page module
		 *
		 *
		 */
		this.pageDashboard = PageFactory.initElements(this.driver, PageDashboard.class);

		/**
		 *
		 *
		 * Purchasing module
		 *
		 *
		 */
		//Purchasing Overview page module
		this.pagePurchasingOverview = PageFactory.initElements(this.driver, PagePurchasingOverview.class);
		//Commissary Order module
		this.pageCreateCommissaryOrder = PageFactory.initElements(this.driver, PageCreateCommissaryOrder.class);
		this.pageRecentCommissaryOrders = PageFactory.initElements(this.driver, PageRecentCommissaryOrders.class);
		//Vendor Order page module
		this.pageCreateVendorOrder = PageFactory.initElements(this.driver, PageCreateVendorOrder.class);
		this.pageRecentVendorOrders = PageFactory.initElements(this.driver, PageRecentVendorOrders.class);
		//Purchase By Invoice page module
		this.pageCreatePurchasebyInvoice = PageFactory.initElements(this.driver, PageCreatePurchasebyInvoice.class);
		this.pageRecentPurchasebyInvoices = PageFactory.initElements(this.driver, PageRecentPurchasebyInvoices.class);
		//Master Order module
		this.pageCreateMasterOrder = PageFactory.initElements(this.driver, PageCreateMasterOrder.class);
		//Vendor Returns page module
		this.pageVendorReturns = PageFactory.initElements(this.driver, PageVendorReturns.class);
		//View Vendor Profiles page module
		this.pageViewVendorProfiles = PageFactory.initElements(this.driver, PageViewVendorProfiles.class);

		/**
		 *
		 *
		 * Inventory module
		 *
		 *
		 */
		//Inventory Overview page module
		this.pageInventoryOverview = PageFactory.initElements(this.driver, PageInventoryOverview.class);
		//Create/Review Inventories page module
		this.pageCreateReviewInventories = PageFactory.initElements(this.driver, PageCreateReviewInventories.class);
		//Location Transfer module
		this.pageCreateLocationTransfer = PageFactory.initElements(this.driver, PageCreateLocationTransfer.class);
		this.pageRecentLocationTransfers = PageFactory.initElements(this.driver, PageRecentLocationTransfers.class);
		//Inventory and Recipe Adjustment page module
		this.pageCreateInventoryAdjustment = PageFactory.initElements(this.driver, PageCreateInventoryAdjustment.class);
		this.pageCreateRecipeAdjustment = PageFactory.initElements(this.driver, PageCreateRecipeAdjustment.class);
		this.pageViewRecentAdjustments = PageFactory.initElements(this.driver, PageViewRecentAdjustments.class);
		//Update Storage Locations page module
		this.pageUpdateStorageLocations = PageFactory.initElements(this.driver, PageUpdateStorageLocations.class);
		//Update Storage Sequences page module
		this.pageUpdateStorageSequences = PageFactory.initElements(this.driver, PageUpdateStorageSequences.class);
		//Update Count Units page module
		this.pageUpdateCountUnits = PageFactory.initElements(this.driver, PageUpdateCountUnits.class);

		/**
		 *
		 *
		 * Sales module generic actions
		 *
		 *
		 */
		//Sales Overview page module
		this.pageSalesOverview = PageFactory.initElements(this.driver, PageSalesOverview.class);
		//Sales Transaction module
		this.pageCreateSalesTransaction = PageFactory.initElements(this.driver, PageCreateSalesTransaction.class);
		this.pageRecentSalesTransactions = PageFactory.initElements(this.driver, PageRecentSalesTransactions.class);
		//View Customer Order Summary page module
		this.pageViewCustomerOrderSummary = PageFactory.initElements(this.driver, PageViewCustomerOrderSummary.class);
		//Manage Sales Forecasts page module
		this.pageManageSalesForecasts = PageFactory.initElements(this.driver, PageManageSalesForecasts.class);

		/**
		 *
		 *
		 * Production module
		 *
		 *
		 */
		//Production Overview page module
		this.pageProductionOverview = PageFactory.initElements(this.driver, PageProductionOverview.class);
		//Production Planning page module
		this.pageProductionPlanning = PageFactory.initElements(this.driver, PageProductionPlanning.class);
		//Daily Prep module
		this.pageCreateDailyPrep = PageFactory.initElements(this.driver, PageCreateDailyPrep.class);
		this.pageRecentDailyPrep = PageFactory.initElements(this.driver, PageRecentDailyPrep.class);
		//Buffet Consumption page module
		this.pageBuffetConsumption = PageFactory.initElements(this.driver, PageBuffetConsumption.class);
		//Menu Matrix page module
		this.pageMenuMatrix = PageFactory.initElements(this.driver, PageMenuMatrix.class);

		/**
		 *
		 *
		 * Labor module
		 *
		 *
		 */
		//Labor Overview page module
		this.pageLaborOverview = PageFactory.initElements(this.driver, PageLaborOverview.class);
		//Labor Summary page module
		this.pageLaborSummary = PageFactory.initElements(this.driver, PageLaborSummary.class);
		//Labor Schedule module
		this.pageCreateLaborSchedule = PageFactory.initElements(this.driver, PageCreateLaborSchedule.class);
		this.pageRecentLaborSchedules = PageFactory.initElements(this.driver, PageRecentLaborSchedules.class);
		//Employee Maintenance module
		this.pageCreateEmployee = PageFactory.initElements(this.driver, PageCreateEmployee.class);
		this.pageEmployeeMaintenance = PageFactory.initElements(this.driver, PageEmployeeMaintenance.class);
		//Ideal Staffing Templates page module
		this.pageIdealStaffingTemplates = PageFactory.initElements(this.driver, PageIdealStaffingTemplates.class);
		//Stations page module
		this.pageStations = PageFactory.initElements(this.driver, PageStations.class);
		//Positions page module
		this.pagePositions = PageFactory.initElements(this.driver, PagePositions.class);

		/**
		 *
		 *
		 * Reports module generic
		 *
		 *
		 */
		/**
		 * Purchasing Report module
		 */
		this.pageReportsPurchasingContractedVendors = PageFactory.initElements(this.driver, PageReportsPurchasingContractedVendors.class);
		this.pageReportsPurchasingPurchaseDetail = PageFactory.initElements(this.driver, PageReportsPurchasingPurchaseDetail.class);
		this.pageReportsPurchasingPurchaseJournal = PageFactory.initElements(this.driver, PageReportsPurchasingPurchaseJournal.class);
		this.pageReportsPurchasingPurchasesByGL = PageFactory.initElements(this.driver, PageReportsPurchasingPurchasesByGL.class);
		this.pageReportsPurchasingTorg29 = PageFactory.initElements(this.driver, PageReportsPurchasingTorg29.class);
		this.pageReportsPurchasingConsolidatedCommodityPricingTrends = PageFactory.initElements(this.driver, PageReportsPurchasingConsolidatedCommodityPricingTrends.class);
		this.pageReportsPurchasingConsolidatedPurchasesByGL = PageFactory.initElements(this.driver, PageReportsPurchasingConsolidatedPurchasesByGL.class);
		this.pageReportsPurchasingConsolidatedVendorOrders = PageFactory.initElements(this.driver, PageReportsPurchasingConsolidatedVendorOrders.class);
		/**
		 * Inventory Report module
		 */
		this.pageReportsInventoryActualCost = PageFactory.initElements(this.driver, PageReportsInventoryActualCost.class);
		this.pageReportsInventoryActualTheoreticalCost = PageFactory.initElements(this.driver, PageReportsInventoryActualTheoreticalCost.class);
		this.pageReportsInventoryAdjustmentSummary = PageFactory.initElements(this.driver, PageReportsInventoryAdjustmentSummary.class);
		this.pageReportsInventoryBookingJournal = PageFactory.initElements(this.driver, PageReportsInventoryBookingJournal.class);
		this.pageReportsInventoryCostAnalysis = PageFactory.initElements(this.driver, PageReportsInventoryCostAnalysis.class);
		this.pageReportsInventoryProfitLoss = PageFactory.initElements(this.driver, PageReportsInventoryProfitLoss.class);

		/**
		 *
		 *
		 * Administration page module
		 *
		 *
		 */
		//Administration Overview page module
		this.pageAdministrationOverview = PageFactory.initElements(this.driver, PageAdministrationOverview.class);
		//Post Current Period page module
		this.pagePostCurrentPeriod = PageFactory.initElements(this.driver, PagePostCurrentPeriod.class);
		//Upload/Download Files page module
		this.pageUploadDownloadFiles = PageFactory.initElements(this.driver, PageUploadDownloadFiles.class);
		//Unpost Period page module
		this.pageUnpostPeriod = PageFactory.initElements(this.driver, PageUnpostPeriod.class);
		//Undo Menu Mix File Processing page module
		this.pageUndoMenuMixFileProcessing = PageFactory.initElements(this.driver, PageUndoMenuMixFileProcessing.class);
		//Manage Daily News page module
		this.pageManageDailyNews = PageFactory.initElements(this.driver, PageManageDailyNews.class);
		//View/Edit Budget Data page module
		this.pageViewEditBudgetData = PageFactory.initElements(this.driver, PageViewEditBudgetData.class);
		//Update Suggested Order Setup page module
		this.pageUpdateSuggestedOrderSetup = PageFactory.initElements(this.driver, PageUpdateSuggestedOrderSetup.class);
		//Update Templates page module
		this.pageUpdateTemplates = PageFactory.initElements(this.driver, PageUpdateTemplates.class);

		/**
		 ***  Launch testing Browser before each test
		 */
		basePageNC.launchBrowser();
		Reporter.log("<p><style type=\"text/css\">");
		Reporter.log(".tg {border-collapse:collapse;border-spacing:0;border-color:#999;table-layout:fixed;width:830px;}");
		Reporter.log(".tg td{font-family:Verdana;font-size:9px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;word-wrap:break-word;border-color:#999;color:#444;background-color:#F7FDFA;text-align:left;}");
		Reporter.log(".tg th{font-family:Verdana;font-size:10px;font-weight:bold;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}");
		Reporter.log("</style><table class=\"tg\"><tbody><col width=\"70px\"> <col width=\"260px\"> <col width=\"200px\"><col width=\"300px\"><tr><th>JIRA ID</th><th>Test Description</th><th>Test Name</th><th>Test Class</th></tr>");
	}

//	@BeforeClass(alwaysRun = true)
//	public void beforeClass() throws Exception {
//		this.driver = DriverOfActiveThread.getDriver();
//	}

	@AfterClass(alwaysRun = true)
	public void afterClass() throws Exception {
		Reporter.log("</tbody></table></p><br>");
		Thread.sleep(1000);
		this.driver = DriverOfActiveThread.getDriver();
		driver.quit();
	}
}
