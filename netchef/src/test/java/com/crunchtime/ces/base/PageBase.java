package com.crunchtime.ces.base;

import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class PageBase {

	@FindBy(xpath = "//span[text()='Apply']")
	private WebElement applyFilterButton;

	protected static void shouldAppear(String title) {
		Waiter.waitForPageTitle(title);
	}

	public void applyFilterButtonClick() {
		Waiter.waitFor(visibilityOf(applyFilterButton));
		applyFilterButton.click();
	}

	public static By spinner = By.cssSelector(".x-form-spinner-down");
	public static By loadMask = By.cssSelector("div[class^='x-mask-msg x-layer x-mask-msg-default']:not([style*='none'])[ces-selenium-id='loadmask']");
}