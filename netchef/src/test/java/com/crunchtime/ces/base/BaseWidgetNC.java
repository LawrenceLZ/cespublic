package com.crunchtime.ces.base;

import com.crunchtime.ces.helper.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class BaseWidgetNC {
    private final WebDriver driver;

    public BaseWidgetNC(WebDriver driver) {
        this.driver = driver;
    }

	private static final int ICON_NUMBER = -1;

    @FindBy(id = "contentPanel-body")
    private WebElement contentPanel;

    @FindBy(css = "div[class='x-box-target']>div>img[class='x-tool-img x-tool-gear']")
    private List<WebElement> configureWidgetGearIcons;
    @FindBy(css = "div[class='x-box-target']>div>img[class='x-tool-img x-tool-maximize']")
    private List<WebElement> widgetMaxViewIcons;
    @FindBy(css = "div[class='x-box-target']>div>img[class='x-tool-img x-tool-restore']")
    private List<WebElement> widgetMinViewIcons;
    @FindBy(css = "div[class='x-box-target']>div>img[class='x-tool-img x-tool-filter-button']")
    private List<WebElement> widgetFiltersIcons;
    @FindBy(css = "div[class='x-box-target']>div>img[class='x-tool-img x-tool-filter-applied-button']")
    private List<WebElement> widgetFiltersAppliedIcons;
    @FindBy(css = "div[class='x-box-target']>div>img[class='x-tool-img x-tool-expand-bottom']")
    private List<WebElement> widgetExpandBottomViewIcons;
    @FindBy(css = "div[class='x-box-target']>div>img[class='x-tool-img x-tool-collapse-top']")
    private List<WebElement> widgetExpandTopViewIcons;

    @FindBy(xpath = "//img[@class='x-tool-img x-tool-expand-bottom']//..//..//span[@class='x-header-text x-panel-header-text x-panel-header-text-default']")
    private List<WebElement> headersForWidgetExpandBottomViewIcons;
    @FindBy(xpath = "//img[@class='x-tool-img x-tool-collapse-top']//..//..//span[@class='x-header-text x-panel-header-text x-panel-header-text-default']")
    private List<WebElement> headersForWidgetExpandTopViewIcons;
    @FindBy(xpath = "//img[@class='x-tool-img x-tool-maximize']//..//..//span[@class='x-header-text x-panel-header-text x-panel-header-text-default']")
    private List<WebElement> headersForWidgetMaxViewIcons;
    @FindBy(xpath = "//img[@class='x-tool-img x-tool-filter-button']//..//..//span[@class='x-header-text x-panel-header-text x-panel-header-text-default']")
    private List<WebElement> headersForWidgetFiltersIcons;
    @FindBy(xpath = "//img[@class='x-tool-img x-tool-filter-applied-button']//..//..//span[@class='x-header-text x-panel-header-text x-panel-header-text-default']")
    private List<WebElement> headersForWidgetFiltersAppliedIcons;

    @FindBy(css = "div[class='x-box-target']>div[data-qtip='Move Up']>img[class='x-tool-img x-tool-toggle']")
    private List<WebElement> widgetMoveUpIcons;
    @FindBy(css = "div[class='x-box-target']>div[data-qtip='Move Down']>img[class='x-tool-img x-tool-toggle']")
    private List<WebElement> widgetMoveDownIcons;
    @FindBy(css = "div[class='x-box-target']>div[data-qtip='Add/Remove Links']>img[class='x-tool-img x-tool-configuration']")
    private List<WebElement> widgetAddRemoveLinksIcons;
    @FindBy(xpath = "//img[@class='x-tool-img x-tool-minus']")
    private List<WebElement> widgetRemoveRowIcons;
    private static final String XPATH_FOR_WIDGET_REMOVE_ROW_PARENT_DIVS = ".//.//..//..//..//..//..//div[contains (@id, 'container')]";
    private static final String XPATH_FOR_WIDGET_REMOVE_ROW_WIDGET_HEADER_NAMES = ".//span[@class='x-header-text x-panel-header-text x-panel-header-text-default']";

    public void moveWidgetToTop() throws Exception {
        for (int i = widgetMoveUpIcons.size() - 1; i > 0; i--) {
            widgetMoveUpIcons.get(i).click();
            Thread.sleep(300);
        }
    }

    public int getConfigureWidgetGearIconsCount() throws Exception {
        return configureWidgetGearIcons.size() - 1;
    }

    public void configureWidgetGearIconsClick(int widgetNumber) throws Exception {
        Waiter.waitFor(visibilityOf(configureWidgetGearIcons.get(widgetNumber)));
        configureWidgetGearIcons.get(widgetNumber).click();
    }

    public void widgetAddRemoveLinksIconsClick(int widgetNumber) throws Exception {
        Waiter.waitFor(visibilityOf(widgetAddRemoveLinksIcons.get(widgetNumber)));
        widgetAddRemoveLinksIcons.get(widgetNumber).click();
    }

    public void widgetMoveDownIconsClick(int widgetNumber) throws Exception {
        Waiter.waitFor(visibilityOf(widgetMoveDownIcons.get(widgetNumber)));
        widgetMoveDownIcons.get(widgetNumber).click();
    }

    public void widgetRemoveRowIconsClick(int widgetNumber) throws Exception {
        Waiter.waitFor(visibilityOf(widgetRemoveRowIcons.get(widgetNumber)));
        widgetRemoveRowIcons.get(widgetNumber).click();
    }

    public void widgetMaximizeClick(int widgetNumber) throws Exception {
        Waiter.waitFor(visibilityOf(widgetMaxViewIcons.get(widgetNumber)));
        widgetMaxViewIcons.get(widgetNumber).click();
        Thread.sleep(1000);
    }

    public void widgetMinimizeClick(int widgetNumber) throws Exception {
        Waiter.waitFor(visibilityOf(widgetMinViewIcons.get(widgetNumber)));
        widgetMinViewIcons.get(widgetNumber).click();
        Thread.sleep(1000);
    }

    public int getWidgetExpandBottomViewIconsNumber(String widgetName) throws Exception {
        for (int iconCurrentItem = 0; iconCurrentItem < headersForWidgetExpandBottomViewIcons.size(); ++iconCurrentItem) {
            if (headersForWidgetExpandBottomViewIcons.get(iconCurrentItem).getText().equals(widgetName)) {
                return  iconCurrentItem;
            }
        }
        return ICON_NUMBER;
    }

    public int getWidgetExpandTopViewIconsNumber(String widgetName) throws Exception {
        for (int iconCurrentItem = 0; iconCurrentItem < headersForWidgetExpandTopViewIcons.size(); ++iconCurrentItem) {
            if (headersForWidgetExpandTopViewIcons.get(iconCurrentItem).getText().equals(widgetName)) {
                	return iconCurrentItem;
            }
        }
        return ICON_NUMBER;
    }

    public int getWidgetMaxViewIconsNumber(String widgetName) throws Exception {
        for (int iconCurrentItem = 0; iconCurrentItem < headersForWidgetMaxViewIcons.size(); ++iconCurrentItem) {
            if (headersForWidgetMaxViewIcons.get(iconCurrentItem).getText().equals(widgetName)) {
                return iconCurrentItem;
            }
        }
        return ICON_NUMBER;
    }

    public int getWidgetRemoveRowIconsNumber(String widgetName) throws Exception {
        int iconNumber = -1;
        int iconCounter = 0;
        for (int iconCurrentItem = 0; iconCurrentItem < widgetRemoveRowIcons.size(); ++iconCurrentItem) {
            if (widgetRemoveRowIcons.get(iconCurrentItem).findElements(By.xpath(XPATH_FOR_WIDGET_REMOVE_ROW_PARENT_DIVS)).get(0).findElements(By.xpath(XPATH_FOR_WIDGET_REMOVE_ROW_WIDGET_HEADER_NAMES)).get(0).getText().contains(widgetName)) {
                iconNumber = iconCurrentItem;
                ++iconCounter;
            }
        }
        if (iconCounter > 1) {
            return iconNumber;
        } else {
            return ICON_NUMBER;
        }
    }

    public void widgetExpandClick(int widgetNumber) throws Exception {
        Waiter.waitFor(visibilityOf(widgetExpandBottomViewIcons.get(widgetNumber)));
        widgetExpandBottomViewIcons.get(widgetNumber).click();
        Thread.sleep(1000);
    }

    public void widgetCollapseClick(int widgetNumber) throws Exception {
        Waiter.waitFor(visibilityOf(widgetExpandTopViewIcons.get(widgetNumber)));
        widgetExpandTopViewIcons.get(widgetNumber).click();
        Thread.sleep(1000);
    }

    public void widgetFiltersIconsClick(int widgetNumber) throws Exception {
        Waiter.waitFor(visibilityOf(widgetFiltersIcons.get(widgetNumber)));
        widgetFiltersIcons.get(widgetNumber).click();
    }

    public void widgetFiltersAppliedIconsClick(int widgetNumber) throws Exception {
        Waiter.waitFor(visibilityOf(widgetFiltersAppliedIcons.get(widgetNumber)));
        widgetFiltersAppliedIcons.get(widgetNumber).click();
    }

    public void scrollToBottomWidget() {
        while ((contentPanel.getSize().getHeight()+50) < configureWidgetGearIcons.get(configureWidgetGearIcons.size() - 1).getLocation().getY()) {
            contentPanel.sendKeys(Keys.PAGE_DOWN);
        }
    }

    public void scrollToWidget(int widgetNumber) {
        while ((contentPanel.getSize().getHeight()+50) < configureWidgetGearIcons.get(widgetNumber).getLocation().getY()) {
            contentPanel.sendKeys(Keys.PAGE_DOWN);
        }
    }

    public void scrollToWidgetByExpandBottomIcon(int widgetNumber) {
        while ((contentPanel.getSize().getHeight()+50) < widgetExpandBottomViewIcons.get(widgetNumber).getLocation().getY()) {
            contentPanel.sendKeys(Keys.PAGE_DOWN);
        }
    }

    public void scrollToWidgetByExpandTopIcon(int widgetNumber) {
        while ((contentPanel.getSize().getHeight()+50) < widgetExpandTopViewIcons.get(widgetNumber).getLocation().getY()) {
            contentPanel.sendKeys(Keys.PAGE_DOWN);
        }
    }
}