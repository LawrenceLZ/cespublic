package com.crunchtime.ces.base;

import com.crunchtime.ces.helper.Waiter;
import com.crunchtime.ces.pages.login.PageNCLogin;
import com.crunchtime.ces.pages.logout.PageNCLogout;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

public class SmokeTestBase {
	private static final String USER = "autocoherent";
	private static final String PASSWORD = "autocoherent";
	private static final String LOCATION_NAME = "RESTAURANT 0001";
	protected static Properties URL;

	protected static String BASE_URL;
	protected static String BROWSER;
	protected static String HUB_URL;
	protected static WebDriver driver;
	protected BasePageNC basePageNC;
	protected PageNCLogin pageNCLogin;
	protected PageNCLogout pageNCLogout;
	protected Waiter waiter;

	private WebDriver initDriver() throws MalformedURLException {
		BROWSER = System.getProperty("browser", "firefox");
		HUB_URL = System.getProperty("hubUrl", "");

		if (HUB_URL.isEmpty()) {
			switch (BROWSER.toLowerCase()) {
				case "firefox":
					return new FirefoxDriver();
				case "ie":
					return new InternetExplorerDriver();
				case "chrome":
					return new ChromeDriver();
				case "safari":
					return new SafariDriver();
				case "phantom":
					return new PhantomJSDriver();
				default:
					return new FirefoxDriver(DesiredCapabilities.firefox());
			}
		} else {
			switch (BROWSER.toLowerCase()) {
				case "firefox":
					return new RemoteWebDriver(new URL(HUB_URL), DesiredCapabilities.firefox());
				case "ie":
					return new RemoteWebDriver(new URL(HUB_URL), DesiredCapabilities.internetExplorer());
				case "chrome":
					return new RemoteWebDriver(new URL(HUB_URL), DesiredCapabilities.chrome());
				case "safari":
					return new RemoteWebDriver(new URL(HUB_URL), DesiredCapabilities.safari());
				case "phantom":
					return new RemoteWebDriver(new URL(HUB_URL), DesiredCapabilities.phantomjs());
				default:
					return new RemoteWebDriver(new URL(HUB_URL), DesiredCapabilities.firefox());
			}
		}
	}

	public static WebDriver getPhysicalInventoryTestBaseDriver() {
		return driver;
	}

	@BeforeMethod
	public void setUp() throws Exception {
		BASE_URL = getURL();
		driver = initDriver();
		driver.get(BASE_URL);
		this.pageNCLogin = PageFactory.initElements(driver, PageNCLogin.class);
		this.basePageNC = PageFactory.initElements(driver, BasePageNC.class);
		this.pageNCLogout = PageFactory.initElements(driver, PageNCLogout.class);
		this.waiter = PageFactory.initElements(driver, Waiter.class);
		driver.manage().window().maximize();
		pageNCLogin.shouldAppear();
		pageNCLogin.loginNC(USER, PASSWORD, LOCATION_NAME);
	}

	@AfterMethod
	public void afterMethod() {
		driver.quit();
	}

	private String getURL() throws IOException {
		URL = new Properties();
		//Property file's location: ces/netchef/src/test/resources
		InputStream is = SmokeTestBase.class.getResourceAsStream("/url.properties");
		URL.load(is);
		return URL.getProperty("urlAuto");
	}
}