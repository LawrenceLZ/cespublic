package com.crunchtime.ces.base;

import com.crunchtime.ces.helper.ActionsNC;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.TestListenerAdapter;
import ru.yandex.qatools.allure.annotations.Attachment;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ScreenshotOnFailureListenerNC extends TestListenerAdapter {
	/**
	 * Override onTestFailure method to implement
	 * Selenium WebDriver TakeScreenshot method
	 * to capture screenshot on Test Failure
	 */
	@Override
	public void onTestFailure(ITestResult result) {
		Reporter.setCurrentTestResult(result);
		Object currentClass = result.getInstance();
		WebDriver driver = ((BaseTestNC) currentClass).driver;

		//Timestamp the screen shot
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yy-HH-mm-ss");
		Calendar calendar = Calendar.getInstance();
		String timeStamp = simpleDateFormat.format(calendar.getTime());

		String root = System.getProperty("user.dir");
		String baseDir = root + "\\target\\surefire-reports\\html\\";
		File scrFile;
		scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String fileName = result.getMethod().getMethodName() + "_" + timeStamp + "_TestFAILED.png";
		File destination = new File(baseDir + fileName);
		try {
			System.out.println("Test Class = " + result.getInstanceName() + "\n" + "Test failure screenshot is at\n" + destination.toURI().toURL() + "\n");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		Reporter.log("<table class=\"tg\"><tbody>");
		Reporter.log("<td style=\"text-align:center\">Test: <span style=\"font-weight:bold\">"+ result.getMethod().getMethodName() +"</span><br>is failed (click on the image below for reference)<br><br><a target=\"_blank\" href=\"" + fileName + "\">");
		Reporter.log("<img width=\"100\" height=\"100\" src=\"" + fileName + "\" alt=\"screenshot at " + destination.toString() + "\"/></a></td></tbody></table>");
		try {
			FileUtils.copyFile(scrFile, destination);
            ActionsNC.setTestFailureScreenshot(destination);
		} catch (IOException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
	}

	@Override
	public void onConfigurationFailure(ITestResult result) {
		Reporter.setCurrentTestResult(result);
		Object currentClass = result.getInstance();
		WebDriver driver = ((BaseTestNC) currentClass).driver;

		//Timestamp the screen shot
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yy-HH-mm-ss");
		Calendar calendar = Calendar.getInstance();
		String timeStamp = simpleDateFormat.format(calendar.getTime());

		String root = System.getProperty("user.dir");
		String baseDir = root + "\\target\\surefire-reports\\html\\";
		File scrFile;
		scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String fileName = result.getMethod().getMethodName() + "_ConfigurationFailed for " + result.getInstanceName() + "_" + timeStamp + "_Test.png";
		File destination = new File(baseDir + fileName);
		try {
			System.out.println("Config failure screenshot is at\n" + destination.toURI().toURL() + "\n");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		Reporter.log("<table class=\"tg\"><tbody>");
		Reporter.log("<td style=\"text-align:center\">Configuration: <span style=\"font-weight:bold\">"+ result.getMethod().getMethodName() +"</span><br>is failed (click on the image below for reference)<br><br><a target=\"_blank\" href=\"" + fileName + "\">");
		Reporter.log("<img width=\"100\" height=\"100\" src=\"" + fileName + "\" alt=\"screenshot at " + destination.toString() + "\"/></a></td></tbody></table>");
		try {
			FileUtils.copyFile(scrFile, destination);
            ActionsNC.setTestFailureScreenshot(destination);
		} catch (IOException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
	}
}