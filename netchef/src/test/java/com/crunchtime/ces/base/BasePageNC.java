package com.crunchtime.ces.base;

import com.crunchtime.ces.helper.Waiter;
import com.crunchtime.ces.pages.bizIQ.PageBizIQ;
import com.crunchtime.ces.pages.inventory.createReviewInventories.PageCreateReviewInventories;
import com.crunchtime.ces.pages.login.PageNCLogin;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import com.crunchtime.ces.pages.purchasing.vendorOrder.PageCreateVendorOrder;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Mouse;
import org.openqa.selenium.support.FindBy;

import java.io.File;
import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class BasePageNC {
	private final WebDriver driver;

	public BasePageNC(WebDriver driver) {
		this.driver = driver;
	}

	private static final String PAGE_CREATE_REVIEW_INV_SUMM_SCRN_URL = "inventoryMenu~physicalInventoryGrid";
	private static final String PAGE_CREATE_VENDOR_ORDER_SCRN_URL = "module=vendor_order&class=create";
    private int xMenuRow;
    private int yMenuRow;
    private String jsQuery;
    private static final File JS_FILE = new File("netchef/src/test/java/com/crunchtime/ces/helper/menuUserHelper.js");

	@FindBy(css = "div[id*='loadmask'][class='x-mask-msg-text']") private List<WebElement> loadSpinnerMasks;
	@FindBy(css = "[ces-selenium-id='nc-dashboard-top-panel_topPanel'] a[ces-selenium-id='splitbutton']")
	public WebElement dashboardSnapshotWaiter;
	@FindBy(css = "div[class='x-window x-layer x-window-default x-closable x-window-closable x-window-default-closable nc-top-dock-exists x-border-box']")
	public WebElement corpDailyNewsPopupForWaiter;

//    @FindBy(xpath = "//span[@class='x-btn-wrap x-btn-split x-btn-split-right']")
    @FindBy(id = "splitbutton-1014")
    private WebElement crunchtimeMenuLink;
    @FindBy(xpath = "//span[@class='x-btn-wrap x-btn-split x-btn-split-right']//span[contains (@class, 'x-btn-icon-el')]")
    private WebElement crunchtimeArrow;
    @FindBy(xpath = "//span[text()='Enter BizIQ Password']")
    private WebElement enterBizIQPasswordMenuLink;
    @FindBy(xpath = "//input[@name='password']")
    private WebElement bizIQPasswordInput;
    @FindBy(xpath = "//span[text()='Save']")
    private WebElement saveBizIQPasswordButton;
    @FindBy(xpath = "//a[@title='Logout']")
    private WebElement logoutButton;

    /**
     * Dashboard menu navigation
     */
    @FindBy(id = "dashboardMenuLink-textEl")
    public WebElement dashboardMainMenuLink;

    /**
     * BizIQ menu navigation
     */
    @FindBy(id = "bizIqLink-textEl")
    public WebElement bizIqMainMenuLink;

	/**
	 * Purchasing menu navigation
	 */
	@FindBy(id = "purchasingMenuLink-textEl")
	public WebElement purchasingMainMenuLink;
	@FindBy(id = "purchasingMenu_purchasingOverviewMenuLink-textEl")
	public WebElement purchasingOverviewMenuLink;
	@FindBy(id = "purchasingMenu_commissaryOrderMenuLink-textEl")
	public WebElement createCommissaryOrderMenuLink;
	@FindBy(id = "purchasingMenu_commissaryOrdersWidgetMenuLink-textEl")
	public WebElement recentCommissaryOrdersMenuLink;
	@FindBy(id = "purchasingMenu_vendorOrderMenuLink-textEl")
	public WebElement createVendorOrderMenuLink;
	@FindBy(id = "purchasingMenu_recentVendorOrdersWidgetMenuLink-textEl")
	public WebElement recentVendorOrderMenuLink;
	@FindBy(id = "purchasingMenu_pbiMenuLink-textEl")
	public WebElement createPurchasebyInvoiceMenuLink;
	@FindBy(id = "purchasingMenu_recentPbiWidgetMenuLink-textEl")
	public WebElement recentPurchasebyInvoicesMenuLink;
	@FindBy(id = "purchasingMenu_masterOrderMenuLink-textEl")
	public WebElement createMasterOrderMenuLink;
	@FindBy(id = "purchasingMenu_vendorReturnsMenuLink-textEl")
	public WebElement vendorReturnsMenuLink;
	@FindBy(id = "purchasingMenu_vendorInformationMenuLink-textEl")
	public WebElement viewVendorProfilesMenuLink;
	@FindBy(id = "purchasingMenu_reportsMenuLink-textEl")
	public WebElement purchasingReportsMenuLink;

	/**
	 * Inventory menu navigation
	 */
	@FindBy(id = "inventoryMenuLink-textEl") public WebElement inventoryMainMenuLink;
	@FindBy(id = "inventoryMenu_inventoryOverviewMenuLink-textEl")
	public WebElement inventoryOverviewMenuLink;
	@FindBy(id = "inventoryMenu_physicalInventoryWidgetMenuLink-textEl")
	public WebElement createReviewInventoriesMenuLink;
	@FindBy(id = "inventoryMenu_locationTransfersMenuLink-textEl")
	public WebElement createLocationTransferMenuLink;
	@FindBy(id = "inventoryMenu_locationTransfersWidgetMenuLink-textEl")
	public WebElement recentLocationTransfersMenuLink;
	@FindBy(id = "inventoryMenu_inventoryAdjustmentMenuLink-textEl")
	public WebElement createInventoryAdjustmentMenuLink;
	@FindBy(id = "inventoryMenu_recipeAdjustmentMenuLink-textEl")
	public WebElement createRecipeAdjudtmentMenuLink;
	@FindBy(id = "inventoryMenu_adjustmentsWidgetMenuLink-textEl")
	public WebElement viewRecentAdjustmentsMenuLink;
	@FindBy(id = "inventoryMenu_setStorageLocationsMenuLink-textEl")
	public WebElement updateStorageLocationsMenuLink;
	@FindBy(id = "inventoryMenu_storageSequenceMenuLink-textEl")
	public WebElement updateStorageSequencesMenuLink;
	@FindBy(id = "inventoryMenu_setInventoryUnitsMenuLink-textEl")
	public WebElement updateCountUnitsMenuLink;

	/**
	 * Sales menu navigation
	 */
	@FindBy(id = "salesMenuLink-textEl")
	public WebElement salesMainMenuLink;
	@FindBy(id = "salesMenu_salesOverviewMenuLink-textEl")
	public WebElement salesOverviewMenuLink;
	@FindBy(id = "salesMenu_registerSalesMenuLink-textEl")
	public WebElement createSalesTransactionMenuLink;
	@FindBy(id = "salesMenu_registerSalesWidgetMenuLink-textEl")
	public WebElement recentSalesTransactionsMenuLink;
	@FindBy(id = "salesMenu_customerOrderMenuLink-textEl")
	public WebElement viewCustomerOrderSummaryMenuLink;
	@FindBy(id = "salesMenu_salesForecastWidgetMenuLink-textEl")
	public WebElement manageSalesForecastsMenuLink;

	/**
	 * Production menu navigation
	 */
	@FindBy(id = "productionMenuLink-textEl")
	public WebElement productionMainMenuLink;
	@FindBy(id = "productionMenu_productionOverviewMenuLink-textEl")
	public WebElement productionOverviewMenuLink;
	@FindBy(id = "productionMenu_productionMenuLink-textEl")
	public WebElement productionPlanningMenuLink;
	@FindBy(id = "productionMenu_dailyPrepMenuLink-textEl")
	public WebElement createDailyPrepMenuLink;
	@FindBy(id = "productionMenu_dailyPrepWidgetMenuLink-textEl")
	public WebElement recentDailyPrepMenuLink;
	@FindBy(id = "productionMenu_buffetConsumptionMenuLink-textEl")
	public WebElement buffetConsumptionMenuLink;
	@FindBy(id = "productionMenu_menuMatrixMenuLink-textEl")
	public WebElement menuMatrixMenuLink;

	/**
	 * Labor menu navigation
	 */
	@FindBy(id = "laborMenuLink-textEl")
	public WebElement laborMainMenuLink;
	@FindBy(id = "laborMenu_laborOverviewMenuLink-textEl")
	public WebElement laborOverviewMenuLink;
	@FindBy(id = "laborMenu_laborSummaryWidgetMenuLink-textEl")
	public WebElement laborSummaryMenuLink;
	@FindBy(id = "laborMenu_laborScheduleMenuLink-textEl")
	public WebElement createLaborScheduleMenuLink;
	@FindBy(id = "laborMenu_laborScheduleWidgetMenuLink-textEl")
	public WebElement recentLaborSchedulesMenuLink;
	@FindBy(id = "laborMenu_createEmployeeMenuLink-textEl")
	public WebElement createEmployeeMenuLink;
	@FindBy(id = "laborMenu_employeeInformationWidgetMenuLink-textEl")
	public WebElement employeeMaintenanceMenuLink;
	@FindBy(id = "laborMenu_staffingLevelTemplatesMenuLink-textEl")
	public WebElement idealStaffingTemplatesMenuLink;
	@FindBy(id = "laborMenu_stationsMenuLink-textEl")
	public WebElement stationsMenuLink;
	@FindBy(id = "laborMenu_positionsMenuLink-textEl")
	public WebElement positionsMenuLink;

	/**
	 * Administration menu navigation
	 */
	@FindBy(id = "administrationMenuLink-textEl")
	public WebElement administrationMainMenuLink;
	@FindBy(id = "administrationMenu_administrationOverviewMenuLink-textEl")
	public WebElement administrationOverviewMenuLink;
	@FindBy(id = "administrationMenu_postCurrentPeriodMenuLink-textEl")
	public WebElement postCurrentPeriodMenuLink;
	@FindBy(id = "administrationMenu_downloadUploadWidgetMenuLink-textEl")
	public WebElement uploadDownloadFilesMenuLink;
	@FindBy(id = "administrationMenu_undoMenumixMenuLink-textEl")
	public WebElement undoMenuMixFileProcessingMenuLink;
	@FindBy(id = "administrationMenu_unpostPeriodMenuLink-textEl")
	public WebElement unpostPeriodMenuLink;
	@FindBy(id = "administrationMenu_manageDailyNewsMenuLink-textEl")
	public WebElement manageDailyNewsMenuLink;
	@FindBy(id = "administrationMenu_budgetSetupMenuLink-textEl")
	public WebElement viewEditBudgetDataMenuLink;
	@FindBy(id = "administrationMenu_suggestedOrderSetupMenuLink-textEl")
	public WebElement updateSuggestedOrderSetupMenuLink;
	@FindBy(id = "administrationMenu_templatesGridWidgetMenuLink-textEl")
	public WebElement updateTemplatesMenuLink;

	/**
	 * Reports menu navigation
	 * NOTE that all the report link locators
	 * are RELATIVE PATH, so can be used in
	 * either Report menu or its own module menu
	 */
	@FindBy(id = "reportsMenuLink-textEl")
	public WebElement reportsMainMenuLink;
	/**
	 * Purchasing Report sub-menu navigation
	 */
	@FindBy(id = "reportsMenupurchasingMenuLink-textEl")
	public WebElement reportsPurchasingSubMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_contractedVendorsMenuLink-textEl']")
	public WebElement reportsPurchasingContractedVendorsMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_purchaseDetailMenuLink-textEl']")
	public WebElement reportsPurchasingPurchaseDetailMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_purchaseJournalMenuLink-textEl']")
	public WebElement reportsPurchasingPurchaseJournalMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_purchasesByGLMenuLink-textEl']")
	public WebElement reportsPurchasingPurchaseByGLMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_torg29MenuLink-textEl']")
	public WebElement reportsPurchasingTorg29MenuLink;
	@FindBy(css = "span[id*='_reportsMenu_consolidatedCommodityPricingTrendsMenuLink-textEl']")
	public WebElement reportsPurchasingConsolidatedCommodityPricingTrendsMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_consolidatedPurchasesByGLMenuLink-textEl']")
	public WebElement reportsPurchasingConsolidatedPurchasesByGLMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_consolidatedVendorOrdersMenuLink-textEl']")
	public WebElement reportsPurchasingConsolidatedVendorOrdersMenuLink;
	/**
	 * Inventory Report sub-menu navigation
	 */
	@FindBy(id = "reportsMenuinventoryMenuLink-textEl")
	public WebElement reportsInventorySubMenuLinks;
	@FindBy(css = "span[id*='_reportsMenu_actualCostReportMenuLink-textEl']")
	public WebElement reportsInventoryActualCostMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_actualTheoreticalCostReportMenuLink-textEl']")
	public WebElement reportsInventoryActualTheoreticalCostMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_adjustmentSummaryMenuLink-textEl']")
	public WebElement reportsInventoryAdjustmentSummaryMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_bookingJournalMenuLink-textEl']")
	public WebElement reportsInventoryBookingJournalMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_costAnalysisMenuLink-textEl']")
	public WebElement reportsInventoryCostAnalysisMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_currentInventoryMenuLink-textEl']")
	public WebElement reportsInventoryCurrentInventoryMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_inventoryAdjustmentSummaryMenuLink-textEl']")
	public WebElement reportsInventoryInventoryAdjustmentSummaryMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_inventoryEfficiencyMenuLink-textEl']")
	public WebElement reportsInventoryInventoryEfficiencyMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_inventoryExtendedValueMenuLink-textEl']")
	public WebElement reportsInventoryInventoryExtendedValueMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_physicalInventorySummaryMenuLink-textEl']")
	public WebElement reportsInventoryPhysicalInventorySummaryMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_profitAndLossMenuLink-textEl']")
	public WebElement reportsInventoryProfitAndLossMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_consolidatedActualCostReportMenuLink-textEl']")
	public WebElement reportsInventoryConsolidatedActualCostMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_consolidatedActualTheoreticalCostReportMenuLink-textEl']")
	public WebElement reportsInventoryConsolidatedActualTheoreticalCostMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_consolidatedAdjustmentSummaryMenuLink-textEl']")
	public WebElement reportsInventoryConsolidatedAdjustmentSummaryMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_consolidatedCostAnalysisMenuLink-textEl']")
	public WebElement reportsInventoryConsolidatedCostAnalysisMenuLink;
	@FindBy(css = "span[id*='_reportsMenu_consolidatedCurrentInventoryMenuLink-textEl']")
	public WebElement reportsInventoryConsolidatedCurrentInventoryMenuLink;


	public void launchBrowser() {
		driver.manage().window().maximize();
	}

	public PageNCLogin openNCTestSite(String siteName) {
		driver.get("http://" + siteName);
		if (!"CrunchTime! Information Systems | Net-Chef".equals(driver.getTitle())) {
			throw new IllegalStateException("Unable to open NC Test Site login page.");
		}
		return new PageNCLogin(driver);
	}

	public PageCreateReviewInventories navigateToCreateReviewInvSummScreen() throws Exception {
		Actions mouseMoveAction = new Actions(driver);
		mouseMoveAction.moveToElement(inventoryMainMenuLink).click(createReviewInventoriesMenuLink).build().perform();
		//wait to make sure recipe summary screen is returned
		for (WebElement loadSpinnerMask : loadSpinnerMasks){
			try {
				Waiter.waitForElementToDisappear(driver, loadSpinnerMask);
			}catch (Exception e){
				break;
			}
		}
		if (!driver.getCurrentUrl().endsWith(PAGE_CREATE_REVIEW_INV_SUMM_SCRN_URL)) {
			throw new IllegalStateException("Unable to navigate to Create/Review Inventories page.");
		}
		return new PageCreateReviewInventories(driver);
	}

    public void bizIqMainMenuLinkClick() {
        Waiter.waitFor(visibilityOf(bizIqMainMenuLink));
        bizIqMainMenuLink.click();
    }

    public void purchasingMainMenuLinkClick() {
        Waiter.waitFor(visibilityOf(purchasingMainMenuLink));
        purchasingMainMenuLink.click();
    }

    public void inventoryMainMenuLinkClick() {
        Waiter.waitFor(visibilityOf(inventoryMainMenuLink));
        inventoryMainMenuLink.click();
    }

    public void salesMainMenuLinkClick() {
        Waiter.waitFor(visibilityOf(salesMainMenuLink));
        salesMainMenuLink.click();
    }

    public void productionMainMenuLinkClick() {
        Waiter.waitFor(visibilityOf(productionMainMenuLink));
        productionMainMenuLink.click();
    }

    public void laborMainMenuLinkClick() {
        Waiter.waitFor(visibilityOf(laborMainMenuLink));
        laborMainMenuLink.click();
    }

    public void reportsMainMenuLinkClick() {
        Waiter.waitFor(visibilityOf(reportsMainMenuLink));
        reportsMainMenuLink.click();
    }

    public void administrationMainMenuLinkClick() {
        Waiter.waitFor(visibilityOf(administrationMainMenuLink));
        administrationMainMenuLink.click();
    }

    public void dashboardMainMenuLinkClick() {
        Waiter.waitFor(visibilityOf(dashboardMainMenuLink));
        dashboardMainMenuLink.click();
    }

    public void enterBizIQPasswordMenuLink() throws Exception {
        Waiter.waitFor(visibilityOf(crunchtimeMenuLink));
        xMenuRow = crunchtimeMenuLink.getLocation().getX() + crunchtimeMenuLink.getSize().getWidth() - 3;
        yMenuRow = crunchtimeMenuLink.getLocation().getY() + crunchtimeMenuLink.getSize().getHeight() - 3;
        jsQuery = FileUtils.readFileToString(JS_FILE);
        jsQuery = jsQuery.replaceAll("\\{\\{1\\}\\}", Integer.toString(xMenuRow));
        jsQuery = jsQuery.replaceAll("\\{\\{2\\}\\}", Integer.toString(yMenuRow));
        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor)driver).executeScript(jsQuery);
        }
        Waiter.waitFor(visibilityOf(enterBizIQPasswordMenuLink));
        enterBizIQPasswordMenuLink.click();
    }

    public void typeBizIQPassword(String bizIQPasswordValue) throws Exception {
        Waiter.waitFor(visibilityOf(bizIQPasswordInput));
        bizIQPasswordInput.clear();
        bizIQPasswordInput.sendKeys(bizIQPasswordValue);
        Thread.sleep(200);
    }

    public void saveBizIQPasswordClick() throws Exception {
        Waiter.waitFor(visibilityOf(saveBizIQPasswordButton));
        saveBizIQPasswordButton.click();
    }

    public void logoutClick() throws Exception {
        Waiter.waitFor(visibilityOf(logoutButton));
        logoutButton.click();
    }

	public PageCreateVendorOrder navigateToCreateVendorOrderScreen() throws Exception {
		Actions mouseMoveAction = new Actions(driver);
		mouseMoveAction.moveToElement(purchasingMainMenuLink).click(createVendorOrderMenuLink).build().perform();
		//wait to make sure recipe summary screen is returned
		for (WebElement loadSpinnerMask : loadSpinnerMasks){
			try {
				Waiter.waitForElementToDisappear(driver, loadSpinnerMask);
			}catch (Exception e){
				break;
			}
		}
		if (!driver.getCurrentUrl().endsWith(PAGE_CREATE_VENDOR_ORDER_SCRN_URL)) {
			throw new IllegalStateException("Unable to navigate to Create Vendor Order page.");
		}
		return new PageCreateVendorOrder(driver);
	}
}