package com.crunchtime.ces.base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

/**
 * DO-NOT-USE THIS CLASS IN ANY NEW/OTHER TEST.
 * <p/>
 * THIS IS ONLY CREATED FOR TESTING USER-GROUP-ACCESS TEST,
 * <p/>
 * WITH TEST SUITE FILE - userGroupAccessTestGroup.xml
 * <p/>
 * ALL FUTURE NAVIGATION ACTION SHOULD BE CREATED INSIDE OF EACH PAGE CLASS. *
 */
public class PageNavigationGeneric {
	private final WebDriver driver;

	public PageNavigationGeneric(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(id = "purchasingMenuLink")
	public WebElement purchasingMenuLink;

	@FindBy(id = "inventoryMenuLink")
	public WebElement inventoryMenuLink;

	@FindBy(id = "salesMenuLink")
	public WebElement salesMenuLink;

	@FindBy(id = "productionMenuLink")
	public WebElement productionMenuLink;

	@FindBy(id = "laborMenuLink")
	public WebElement laborMenuLink;

	@FindBy(id = "reportsMenuLink")
	public WebElement reportsMenuLink;

	@FindBy(id = "administrationMenuLink")
	public WebElement administrationMenuLink;

	public PageNavigationGeneric menuLinkMouseOver(String menuMainLinkLabel, String menuSubLinkLabel) throws InterruptedException {
		Actions menuLinkMouseOverAction = new Actions(driver);
		if (!menuMainLinkLabel.isEmpty()) {
			switch (menuMainLinkLabel) {
				case "Purchasing":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.cssSelector("div[ces-selenium-id='menuitem_purchasingMenu']"))).build().perform();
					Thread.sleep(2000);
					break;
				case "Inventory":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("inventoryMenuLink"))).build().perform();
					Thread.sleep(2000);
					break;
				case "Sales":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("salesMenuLink"))).build().perform();
					Thread.sleep(2000);
					break;
				case "Production":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("productionMenuLink"))).build().perform();
					Thread.sleep(2000);
					break;
				case "Labor":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("laborMenuLink"))).build().perform();
					Thread.sleep(2000);
					break;
				case "Reports":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("reportsMenuLink"))).build().perform();
					Thread.sleep(2000);
					break;
				case "Administration":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("administrationMenuLink"))).build().perform();
					Thread.sleep(2000);
					break;
			}
		}
		if (!menuSubLinkLabel.isEmpty()) {
			switch (menuMainLinkLabel + menuSubLinkLabel) {
				case "Purchasing" + "Purchasing Overview":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("purchasingMenu_purchasingOverviewMenuLink-textEl"))).build().perform();
					Thread.sleep(2000);
					break;
				case "Purchasing" + "Create Commissary Order":
					menuLinkMouseOverAction.moveToElement((driver.findElement(By.id("purchasingMenu_commissaryOrderMenuLink-textEl")))).build().perform();
					Thread.sleep(2000);
					break;
				case "Purchasing" + "Recent Commissary Orders":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("purchasingMenu_commissaryOrdersWidgetMenuLink-textEl"))).build().perform();
					Thread.sleep(2000);
					break;
				case "Purchasing" + "Create Vendor Order":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("purchasingMenu_vendorOrderMenuLink-textEl"))).build().perform();
					Thread.sleep(2000);
					break;
				case "Purchasing" + "Recent Vendor Orders":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("purchasingMenu_recentVendorOrdersWidgetMenuLink-textEl"))).build().perform();
					Thread.sleep(2000);
					break;
				case "Purchasing" + "Create Purchase by Invoice":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("purchasingMenu_pbiMenuLink-textEl"))).build().perform();
					Thread.sleep(2000);
					break;
				case "Purchasing" + "Recent Purchase by Invoices":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("purchasingMenu_recentPbiWidgetMenuLink-textEl"))).build().perform();
					Thread.sleep(2000);
					break;
				case "Purchasing" + "Create Master Order":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("purchasingMenu_masterOrderMenuLink-textEl"))).build().perform();
					Thread.sleep(2000);
					break;
				case "Purchasing" + "Vendor Returns":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("purchasingMenu_vendorReturnsMenuLink-textEl"))).build().perform();
					Thread.sleep(2000);
					break;
				case "Purchasing" + "Reports":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("purchasingMenu_reportsMenuLink-textEl"))).build().perform();
					Thread.sleep(2000);
					break;
				case "Inventory" + "Inventory Overview":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("inventoryMenu_inventoryOverviewMenuLink-textEl"))).build().perform();
					Thread.sleep(2000);
					break;
				case "Inventory" + "Reports":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("inventoryMenu_reportsMenuLink-textEl"))).build().perform();
					Thread.sleep(2000);
					break;

				case "Reports" + "Purchasing":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("reportsMenupurchasingMenuLink-textEl"))).build().perform();
					Thread.sleep(2000);
					break;
				case "Reports" + "Inventory":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("reportsMenuinventoryMenuLink-textEl"))).build().perform();
					Thread.sleep(2000);
					break;
				case "Reports" + "Sales":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("reportsMenusalesMenuLink-textEl"))).build().perform();
					Thread.sleep(2000);
					break;
				case "Reports" + "Production":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("reportsMenuproductionMenuLink-textEl"))).build().perform();
					Thread.sleep(2000);
					break;
				case "Reports" + "Labor":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("reportsMenulaborMenuLink-textEl"))).build().perform();
					Thread.sleep(2000);
					break;
				case "Reports" + "Administration":
					menuLinkMouseOverAction.moveToElement(driver.findElement(By.id("reportsMenuadministrationMenuLink-textEl"))).build().perform();
					Thread.sleep(2000);
					break;
			}
		}

		return this;
	}
}
