set newpage NONE
set wrap off
set linesize 10000
set pagesize 10000
set heading off
set termout off
set verify off
set feedback off
set trims on
set serveroutput on

/* Directory to export the file */
-----------------------------------------------------------------------------------
spool '&1'



/* SQL Statement */
-----------------------------------------------------------------------------------
SELECT 'All' filter_by_category_list_val FROM dual
UNION
SELECT catName.filter_by_category_val
  FROM (SELECT (decode(upper('&2'),
                       'CATEGORY',
                       cat.category_name,
                       'SUBCATEGORY',
                       cat.category_name || '--' || cat.subcategory_name,
                       'MICROCATEGORY',
                       cat.category_name || '--' || cat.subcategory_name || '--' ||
                       cat.microcategory_name)) filter_by_category_val
          FROM t_product_company  pc,
               t_product_location pl,
               t_storage          sto,
               t_location         loc,
               t_location         locsup,
               t_product_location plsup,
               t_storage          stosup,
               t_category         cat
         WHERE pc.product_id = pl.product_id
           AND pc.category_pk = cat.category_pk
           AND pl.location_id = loc.location_id
           AND pl.co_flag = 'Y'
           AND pl.storage_id = sto.storage_id
           AND sto.storage_code <> 'UNASGN'
           AND pl.product_id = plsup.product_id
           AND plsup.location_id = locsup.location_id
           AND plsup.storage_id = stosup.storage_id
           AND locsup.location_id = stosup.location_id
           AND stosup.storage_code <> 'UNASGN'
           AND pl.active_flag = 'Y'
           AND plsup.active_flag = 'Y'
           AND pc.product_active = 'Y'
           AND upper(loc.location_name) = upper('&3')
           AND upper(LOCSUP.LOCATION_NAME) = upper('&4')
         GROUP BY decode(upper('&2'),
                         'CATEGORY',
                         cat.category_name,
                         'SUBCATEGORY',
                         cat.category_name || '--' || cat.subcategory_name,
                         'MICROCATEGORY',
                         cat.category_name || '--' || cat.subcategory_name || '--' ||
                         cat.microcategory_name)
         ORDER BY upper(decode(upper('&2'),
                               'CATEGORY',
                               cat.category_name,
                               'SUBCATEGORY',
                               cat.category_name || '--' ||
                               cat.subcategory_name,
                               'MICROCATEGORY',
                               cat.category_name || '--' ||
                               cat.subcategory_name || '--' ||
                               cat.microcategory_name)) ASC) catName;

spool off

EXIT
