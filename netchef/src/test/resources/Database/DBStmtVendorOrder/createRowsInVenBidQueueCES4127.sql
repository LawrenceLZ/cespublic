DECLARE
  loop_bid_queue_rows   NUMBER := '&1'; --number of rows to be inserted
  N_VENDOR_BID_QUEUE_PK NUMBER(12);

BEGIN
  FOR bid_queue_row_num IN 1 .. loop_bid_queue_rows LOOP
  
    SELECT SEQ_T_VENDOR_BID_QUEUE.NEXTVAL
      INTO N_VENDOR_BID_QUEUE_PK
      FROM dual;
  
    INSERT INTO t_vendor_bid_queue
      (VENDOR_BID_QUEUE_PK,
       SESSION_STRING,
       MARKET_PK,
       SUPPLY_ID,
       PRODUCT_ID,
       BID_UPDATE_DATE,
       BID_PRICE,
       AUDIT_PK,
       USER_PK,
       TRANSACTION_NUMBER)
      SELECT N_VENDOR_BID_QUEUE_PK VENDOR_BID_QUEUE_PK,
             'TEST' SESSION_STRING,
             loc.market_pk MARKET_PK,
             sup.supply_id SUPPLY_ID,
             pc.product_id PRODUCT_ID,
             trunc(SYSDATE) BID_UPDATE_DATE,
             to_number(to_char(SYSDATE, 'hh24.mm')) + bid_queue_row_num BID_PRICE,
             320001 AUDIT_PK,
             (SELECT MIN(USER_PK) FROM T_USER) USER_PK,
             'TEST TRAN' TRANSACTION_NUMBER
        FROM t_product_company  pc,
             t_location         loc,
             t_product_location pl,
             t_supply           sup
       WHERE pl.product_id = pc.product_id
         AND loc.location_id = pl.location_id
         AND upper(pc.product_name) = upper('&2')
         AND upper(sup.supply_name) = upper('&3')
         AND upper(loc.location_name) = upper('&4');
  END LOOP;
END;
/ 
COMMIT;

EXIT




/***
Example of cmd sqlplus -L autocohernet/autocohernet@ctautocoherent @C:\Users\lzhao\Desktop\Selenium\Database\Generic\updateUserGroupAccess.sql para1 para2 para3 para4
***/