UPDATE t_user_group_access usrga
   SET usrga.access_allowed = upper('&1')
 WHERE (usrga.user_group_pk, usrga.user_access_pk) IN
       (SELECT usr.user_group_pk, usra.user_access_pk
          FROM t_user usr, t_user_access usra, t_screen scrn, t_control ctrl
         WHERE usra.screen_id = scrn.screen_id
           AND usra.control_id = ctrl.control_id
		   AND upper(usr.user_id) = upper('&2')
           AND upper(scrn.screen_name) = upper('&3')
           AND upper(ctrl.control_name) = upper('&4'));
COMMIT;
quit;
EXIT



/***
sqlplus -L autocohernet/autocohernet@ctautocoherent @C:\Users\lzhao\Desktop\Selenium\Database\Generic\updateUserGroupAccess.sql para1 para2 para3 para4
***/